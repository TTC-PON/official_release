--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_mgt_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU MGT Wrapper (onu_mgt_wrapper)
--
--! @brief onu MultiGigabitTransceiver wrapper
--! Kintex Ultrascale - GTH
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 14\06\2016 - EBSM - Created\n
--! 16/11/2016 - CS - Replaced Kintex 7 GTX with Kintex Ultrascale GTH
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_mgt_wrapper
--============================================================================
entity onu_mgt_wrapper is
  port (
    -- global input signals --          
    clk_sys_i      : in std_logic;
    clk_rxref240_i : in std_logic;
    clk_txref240_i : in std_logic;
    mgt_reset_i    : in std_logic;
    -------------------------

    -- global output signals -- 
    clk_trxusr240_o : out std_logic;
    -------------------------

    -- status/control --
    tx_external_locked_i : in std_logic;
    rx_slide_i           : in std_logic;

    rxfsmrstdone_o : out std_logic;
    txfsmrstdone_o : out std_logic;
    mgt_txpll_o    : out std_logic;
    mgt_rxpll_o    : out std_logic;
    phase_good_o   : out std_logic;

    drp_wr_i        : in  std_logic_vector(31 downto 0);
    drp_wr_strobe_i : in  std_logic;
    drp_monitor_o   : out std_logic_vector(31 downto 0);

    mgt_rx_equalizer_ctrl_i : in  std_logic_vector(31 downto 0);
    mgt_rx_equalizer_stat_o : out std_logic_vector(31 downto 0);
    mgt_tx_phase_ctrl_i     : in  std_logic_vector(31 downto 0);
    mgt_tx_phase_stat_o     : out std_logic_vector(31 downto 0);
    -------------------------              

    -- data in/out --   
    tx_data_i : in  std_logic_vector(9 downto 0);
    rx_data_o : out std_logic_vector(39 downto 0);
    rx_p_i    : in  std_logic;
    rx_n_i    : in  std_logic;
    tx_p_o    : out std_logic;
    tx_n_o    : out std_logic
    -------------------------              
    );
end onu_mgt_wrapper;

--============================================================================
--! Architecture declaration for onu_mgt_wrapper
--============================================================================
architecture rtl of onu_mgt_wrapper is

-- begin GTH / related components --
  component onu_gth_simplex_init is
    generic (
      FREERUN_FREQUENCY : positive := 100;
      TIMER_DURATION_US : positive := 30000);  --30000 for TX / 130000 for RX
    port (
      clk_freerun_in        : in  std_logic;
      reset_simplex_in      : in  std_logic;
      simplex_init_done_in  : in  std_logic;
      reset_simplex_out     : out std_logic;
      simplex_init_done_out : out std_logic;
      retry_ctr_out         : out std_logic_vector (3 downto 0));
  end component;

  component pon_onu_gth_trx
    port (
      gtwiz_userclk_tx_active_in         : in  std_logic_vector (0 to 0);
	  txoutclk_out                       : out std_logic_vector (0 to 0);
	  txusrclk_in                        : in  std_logic_vector (0 to 0);
	  txusrclk2_in                       : in  std_logic_vector (0 to 0);
      gtwiz_userclk_rx_active_in         : in  std_logic_vector (0 to 0);
	  rxoutclk_out                       : out std_logic_vector (0 to 0);
	  rxusrclk_in                        : in  std_logic_vector (0 to 0);
	  rxusrclk2_in                       : in  std_logic_vector (0 to 0);	  
      gtwiz_buffbypass_rx_reset_in       : in  std_logic_vector (0 to 0);
      gtwiz_buffbypass_rx_start_user_in  : in  std_logic_vector (0 to 0);
      gtwiz_buffbypass_rx_done_out       : out std_logic_vector (0 to 0);
      gtwiz_buffbypass_rx_error_out      : out std_logic_vector (0 to 0);
      gtwiz_reset_clk_freerun_in         : in  std_logic_vector (0 to 0);
      gtwiz_reset_all_in                 : in  std_logic_vector (0 to 0);
      gtwiz_reset_tx_pll_and_datapath_in : in  std_logic_vector (0 to 0);
      gtwiz_reset_tx_datapath_in         : in  std_logic_vector (0 to 0);
      gtwiz_reset_rx_pll_and_datapath_in : in  std_logic_vector (0 to 0);
      gtwiz_reset_rx_datapath_in         : in  std_logic_vector (0 to 0);
      gtwiz_reset_rx_cdr_stable_out      : out std_logic_vector (0 to 0);
      gtwiz_reset_tx_done_out            : out std_logic_vector (0 to 0);
      gtwiz_reset_rx_done_out            : out std_logic_vector (0 to 0);
      gtwiz_userdata_tx_in               : in  std_logic_vector (39 downto 0);
      gtwiz_userdata_rx_out              : out std_logic_vector (39 downto 0);
      gtrefclk01_in                      : in  std_logic_vector (0 to 0);
      qpll1lockdetclk_in                 : in  std_logic_vector (0 to 0);
      qpll1lock_out                      : out std_logic_vector (0 to 0);
      qpll1outclk_out                    : out std_logic_vector (0 to 0);
      qpll1outrefclk_out                 : out std_logic_vector (0 to 0);
      cplllockdetclk_in                  : in  std_logic_vector (0 to 0);
      cpllreset_in                       : in  std_logic_vector (0 to 0);

      drpclk_in  : in  std_logic_vector (0 to 0);
      drpaddr_in : in  std_logic_vector(8 downto 0);
      drpdi_in   : in  std_logic_vector(15 downto 0);
      drpen_in   : in  std_logic_vector(0 downto 0);
      drpwe_in   : in  std_logic_vector(0 downto 0);
      drpdo_out  : out std_logic_vector(15 downto 0);
      drprdy_out : out std_logic_vector(0 downto 0);

      gthrxn_in          : in  std_logic_vector (0 to 0);
      gthrxp_in          : in  std_logic_vector (0 to 0);
      gtrefclk0_in       : in  std_logic_vector (0 to 0);
      rx8b10ben_in       : in  std_logic_vector (0 to 0);
      rxslide_in         : in  std_logic_vector (0 to 0);
      cplllock_out       : out std_logic_vector (0 to 0);
      gthtxn_out         : out std_logic_vector (0 to 0);
      gthtxp_out         : out std_logic_vector (0 to 0);
      rxpmaresetdone_out : out std_logic_vector (0 to 0);
      txpmaresetdone_out : out std_logic_vector (0 to 0);

      rxlpmen_in         : in  std_logic_vector (0 downto 0);
      rxlpmgcovrden_in   : in  std_logic_vector (0 downto 0);
      rxlpmhfovrden_in   : in  std_logic_vector (0 downto 0);
      rxlpmlfklovrden_in : in  std_logic_vector (0 downto 0);
      rxlpmosovrden_in   : in  std_logic_vector (0 downto 0);

	  rxdfevpovrden_in   : in  std_logic_vector (0 downto 0);
      rxdfeutovrden_in   : in  std_logic_vector (0 downto 0);
      rxosovrden_in      : in  std_logic_vector (0 downto 0);
      rxdfeagcovrden_in  : in  std_logic_vector (0 downto 0);
      rxdfelfovrden_in   : in  std_logic_vector (0 downto 0);

      dmonitorclk_in     : in  std_logic_vector (0 downto 0);	  
      dmonitorout_out    : out std_logic_vector (16 downto 0);
      txpippmen_in       : in  std_logic_vector (0 downto 0);
      txpippmovrden_in   : in  std_logic_vector (0 downto 0);
      txpippmpd_in       : in  std_logic_vector (0 downto 0);
      txpippmsel_in      : in  std_logic_vector (0 downto 0);
      txpippmstepsize_in : in  std_logic_vector (4 downto 0);
      txbufstatus_out    : out std_logic_vector (1 downto 0)
      );
  end component;

  component reset_synchronizer
    port (
      clk_in  : in  std_logic;
      rst_in  : in  std_logic;
      rst_out : out std_logic
      );
  end component;

-- end GTX / related components --

  component drp_ctrl is
    port (
      -- global input signals --
      clk_i   : in std_logic;
      reset_i : in std_logic;
      --------------------------

      -- Interface to user --
      drp_wr_i        : in  std_logic_vector(31 downto 0);
      drp_wr_strobe_i : in  std_logic;
      drp_monitor_o   : out std_logic_vector(31 downto 0);
      -------------------------------

      -- DRP connection to GTH/GTX --
      mgt_drpwe_o   : out std_logic;
      mgt_drpen_o   : out std_logic;
      mgt_drpaddr_o : out std_logic_vector(8 downto 0);
      mgt_drpdi_o   : out std_logic_vector(15 downto 0);
      mgt_drprdy_i  : in  std_logic;
      mgt_drpdo_i   : in  std_logic_vector(15 downto 0)
      -------------------------------
      );
  end component drp_ctrl;

  component drp_arbiter is
    generic(
      g_NUM_MASTER : integer := 2
      );
    port (
      -- global input signals --
      clk_i   : in std_logic;
      reset_i : in std_logic;
      --------------------------

      -- Interface to user --
      master_drpwe_i   : in  std_logic_vector(g_NUM_MASTER-1 downto 0);
      master_drpen_i   : in  std_logic_vector(g_NUM_MASTER-1 downto 0);
      master_drpaddr_i : in  std_logic_vector(9*g_NUM_MASTER-1 downto 0);
      master_drpdi_i   : in  std_logic_vector(16*g_NUM_MASTER-1 downto 0);
      master_drprdy_o  : out std_logic_vector(g_NUM_MASTER-1 downto 0);
      master_drpdo_o   : out std_logic_vector(16*g_NUM_MASTER-1 downto 0);
      -------------------------------

      -- DRP connection to GTH/GTX --
      mgt_drpwe_o   : out std_logic;
      mgt_drpen_o   : out std_logic;
      mgt_drpaddr_o : out std_logic_vector(8 downto 0);
      mgt_drpdi_o   : out std_logic_vector(15 downto 0);
      mgt_drprdy_i  : in  std_logic;
      mgt_drpdo_i   : in  std_logic_vector(15 downto 0)
      -------------------------------
      );
  end component drp_arbiter;

  component tx_phase_aligner is
    generic(
      -- User choice of DRP control or port control
      -- Recommended nowadays to use in DRP control as a strange behaviour was observed using the port in PI code stepping mode
      g_DRP_NPORT_CTRL        : boolean                      := true;  --! Uses DRP control of port control for the transmitter PI
      g_DRP_ADDR_TXPI_PPM_CFG : std_logic_vector(8 downto 0) := ("010011010")  --! Check the transceiver user guide of your device for this address             
      );                                                                      
    port (
      --==============================================================================
      --! User control/monitor ports
      --==============================================================================  
      -- Clock / reset                                                     
      clk_sys_i : in std_logic;         --! system clock input
      reset_i   : in std_logic;  --! active high sync. reset (recommended to keep reset_i=1 while transceiver reset initialization is being performed)

      -- Top level interface                                                 
      tx_aligned_o : out std_logic;  --! Use it as a reset for the user transmitter logic

      -- Config (for different flavours)
      tx_pi_phase_calib_i   : in std_logic_vector(6 downto 0);  --! previous calibrated tx pi phase (tx_pi_phase_o after first reset calibration)
      tx_ui_align_calib_i   : in std_logic;  --! align with previous calibrated tx pi phase
      tx_fifo_fill_pd_max_i : in std_logic_vector(31 downto 0);  --! phase detector accumulated max output, sets precision of phase detector
                                             --! this is supposedly a static signal, this block shall be reset whenever this signal changes
                                             --! the time for each phase detection after a clear is given by tx_fifo_fill_pd_max_i * PERIOD_clk_txusr_i
      tx_fine_realign_i     : in std_logic;  --! A rising edge will cause the Tx to perform a fine realignment to the half-response

      -- It is only valid to re-shift clock once aligned (tx_aligned_o = '1') 
      ps_strobe_i     : in  std_logic;  --! pulse synchronous to clk_sys_i to activate a shift in the phase (only captured rising edge, so a signal larger than a pulse is also fine)
      ps_inc_ndec_i   : in  std_logic;  --! 1 increments phase by phase_step_i units, 0 decrements phase by phase_step_i units
      ps_phase_step_i : in  std_logic_vector(3 downto 0);  --! number of units to shift the phase of the receiver clock (see Xilinx transceiver User Guide to convert units in time)       
      ps_done_o       : out std_logic;  --! pulse synchronous to clk_sys_i to indicate a phase shift was performed

      -- Tx PI phase value
      tx_pi_phase_o : out std_logic_vector(6 downto 0);  --! phase shift accumulated

      -- Tx fifo fill level phase detector                                   
      tx_fifo_fill_pd_o : out std_logic_vector(31 downto 0);  --! phase detector output, when aligned this value should be close to (0x2_0000)

      --==============================================================================
      --! MGT ports
      --==============================================================================
      clk_txusr_i          : in std_logic;  --! txusr2clk              
      -- Tx fifo fill level - see Xilinx transceiver User Guide for more information    
      tx_fifo_fill_level_i : in std_logic;  --! connect to txbufstatus[0]

      -- Transmitter PI ports - see Xilinx transceiver User Guide for more information
      -- obs1: all txpi ports shall be connected to the transceiver even when using this block in DRP-mode              
      txpippmen_o       : out std_logic;  --! enable tx phase interpolator controller
      txpippmovrden_o   : out std_logic;  --! enable DRP control of tx phase interpolator
      txpippmsel_o      : out std_logic;  --! set to 1 when using tx pi ppm controler
      txpippmpd_o       : out std_logic;  --! power down transmitter phase interpolator 
      txpippmstepsize_o : out std_logic_vector(4 downto 0);  --! sets step size and direction of phase shift with port control PI code stepping mode

      -- DRP interface - see Xilinx transceiver User Guide for more information
      -- obs2: connect clk_sys_i to drpclk
      -- obs3: if using this block in port-mode, DRP output can be left floating and input connected to '0'             
      drpaddr_o : out std_logic_vector(8 downto 0);  --! For devices with a 10-bit DRP address interface, connect MSB to '0'
      drpen_o   : out std_logic;        --! DRP enable transaction
      drpdi_o   : out std_logic_vector(15 downto 0);  --! DRP data write
      drprdy_i  : in  std_logic;        --! DRP finished transaction
      drpdo_i   : in  std_logic_vector(15 downto 0);  --! DRP data read; not used nowadays, write only interface
      drpwe_o   : out std_logic         --! DRP write enable

      );
  end component tx_phase_aligner;

--***********************************Parameter Declarations********************

  constant DLY         : time := 1 ns;
  attribute keep       : string;
  attribute mark_debug : string;

--**************************** Wire Declarations ******************************
  signal userclk_tx_reset          : std_logic_vector (0 to 0);
  signal userclk_tx_srcclk         : std_logic_vector (0 to 0);
  signal userclk_tx_usrclk         : std_logic_vector (0 to 0);
  signal userclk_tx_usrclk2        : std_logic_vector (0 to 0);
  signal userclk_tx_active         : std_logic_vector (0 to 0);
  signal userclk_tx_active_meta    : std_logic;
  signal userclk_rx_reset          : std_logic_vector (0 to 0);
  signal userclk_rx_srcclk         : std_logic_vector (0 to 0);
  signal userclk_rx_usrclk         : std_logic_vector (0 to 0);
  signal userclk_rx_usrclk2        : std_logic_vector (0 to 0);
  signal userclk_rx_active         : std_logic_vector (0 to 0);
  signal userclk_rx_active_meta    : std_logic;
  signal buffbypass_rx_reset       : std_logic_vector (0 to 0);
  signal buffbypass_rx_start_user  : std_logic_vector (0 to 0);
  signal buffbypass_rx_done        : std_logic_vector (0 to 0);
  signal buffbypass_rx_error       : std_logic_vector (0 to 0);
  signal reset_clk_freerun         : std_logic_vector (0 to 0);
  signal reset_all                 : std_logic_vector (0 to 0);
  signal reset_tx_pll_and_datapath : std_logic_vector (0 to 0);
  signal reset_tx_datapath         : std_logic_vector (0 to 0);
  signal reset_rx_pll_and_datapath : std_logic_vector (0 to 0);
  signal reset_rx_datapath         : std_logic_vector (0 to 0);
  signal reset_rx_cdr_stable       : std_logic_vector (0 to 0);
  signal reset_tx_done             : std_logic_vector (0 to 0);
  signal reset_rx_done             : std_logic_vector (0 to 0);
  signal userdata_tx               : std_logic_vector (39 downto 0);
  signal userdata_rx               : std_logic_vector (39 downto 0);
  signal gtrefclk01                : std_logic_vector (0 to 0);
  signal qpll1lockdetclk           : std_logic_vector (0 to 0);
  signal qpll1lock                 : std_logic_vector (0 to 0);
  signal qpll1outclk               : std_logic_vector (0 to 0);
  signal qpll1outrefclk            : std_logic_vector (0 to 0);
  signal cplllockdetclk            : std_logic_vector (0 to 0);
  signal cpllreset                 : std_logic_vector (0 to 0);

  signal drpclk  : std_logic_vector (0 to 0);
  signal drpaddr : std_logic_vector(8 downto 0);
  signal drpdi   : std_logic_vector(15 downto 0);
  signal drpen   : std_logic_vector (0 to 0);
  signal drpwe   : std_logic_vector (0 to 0);
  signal drpdo   : std_logic_vector(15 downto 0);
  signal drprdy  : std_logic_vector(0 to 0);

  signal gthrxn         : std_logic_vector (0 to 0);
  signal gthrxp         : std_logic_vector (0 to 0);
  signal gtrefclk0      : std_logic_vector (0 to 0);
  signal rx8b10ben      : std_logic_vector (0 to 0);
  signal rxslide        : std_logic_vector (0 to 0);
  signal cplllock       : std_logic_vector (0 to 0);
  signal gthtxn         : std_logic_vector (0 to 0);
  signal gthtxp         : std_logic_vector (0 to 0);
  signal rxpmaresetdone : std_logic_vector (0 to 0);
  signal txpmaresetdone : std_logic_vector (0 to 0);

  signal rxlpmen            : std_logic_vector (0 downto 0);
  signal rxlpmgcovrden      : std_logic_vector (0 downto 0);
  signal rxlpmhfovrden      : std_logic_vector (0 downto 0);
  signal rxlpmlfklovrden    : std_logic_vector (0 downto 0);
  signal rxlpmosovrden      : std_logic_vector (0 downto 0);

  signal rxdfevpovrden      : std_logic_vector (0 downto 0);
  signal rxdfeutovrden      : std_logic_vector (0 downto 0);
  signal rxosovrden         : std_logic_vector (0 downto 0);
  signal rxdfeagcovrden     : std_logic_vector (0 downto 0);
  signal rxdfelfovrden      : std_logic_vector (0 downto 0);
 
  signal dmonitorout       : std_logic_vector (16 downto 0);
  signal dmonitorout_r     : std_logic_vector (16 downto 0);
  signal dmonitorout_r2    : std_logic_vector (16 downto 0);
  signal dmonitorout_latch : std_logic_vector (16 downto 0);
  signal txpippmen          : std_logic_vector (0 downto 0);
  signal txpippmovrden      : std_logic_vector (0 downto 0);
  signal txpippmpd          : std_logic_vector (0 downto 0);
  signal txpippmsel         : std_logic_vector (0 downto 0);
  signal txpippmstepsize    : std_logic_vector (4 downto 0);
  signal txbufstatus        : std_logic_vector (1 downto 0);

  ------------------------------- User Clocks ---------------------------------
  signal txusrclk  : std_logic;
  signal txusrclk2 : std_logic;
  signal rxusrclk  : std_logic;
  signal rxusrclk2 : std_logic;

  -------------------------------- Aux. ---------------------------------------
  --signal reset_all_init                           : std_logic;
  signal reset_tx_init : std_logic;
  signal reset_rx_init : std_logic;

  --signal reset_rx_datapath_init                   : std_logic;
  signal userclk_tx_not_ready  : std_logic;
  signal userclk_rx_not_ready  : std_logic;
  signal tx_init_done          : std_logic;
  signal tx_system_reset_async : std_logic;
  signal tx_system_reset       : std_logic;
  signal rx_init_done          : std_logic;
  signal rx_system_reset_async : std_logic;
  signal rx_system_reset       : std_logic;
  signal init_done             : std_logic;
  signal tx_init_fsm_done      : std_logic;
  signal rx_init_fsm_done      : std_logic;
  signal rx_init_retry_ctr     : std_logic_vector(3 downto 0);
  signal tx_init_retry_ctr     : std_logic_vector(3 downto 0);

  ------------------------------ Reset for Tx/Rx Phase Aligner --------------------------
  signal tx_phase_aligner_reset_async   : std_logic;
  signal tx_phase_aligner_reset_syssync : std_logic;

  ----------------------------- Signals for Tx Phase Aligner ---------------------------                                         
  signal tx_aligned          : std_logic;
  signal tx_pi_phase_calib   : std_logic_vector(6 downto 0);
  signal tx_ui_align_calib   : std_logic;
  signal tx_fifo_fill_pd_max : std_logic_vector(31 downto 0);
  signal tx_fine_realign     : std_logic;
  signal tx_ps_strobe        : std_logic;
  signal tx_ps_strobe_r      : std_logic;
  signal tx_ps_inc_ndec      : std_logic;
  signal tx_ps_phase_step    : std_logic_vector(3 downto 0);
  signal tx_ps_done          : std_logic;
  signal tx_ps_done_r        : std_logic;
  signal tx_ps_done_latched  : std_logic;
  signal tx_pi_phase         : std_logic_vector(6 downto 0);
  signal tx_fifo_fill_pd     : std_logic_vector(31 downto 0);

  ------------------------------ DRP arbiter ----------------------------------
  constant c_NUM_DRP_MASTER : integer := 2;
  signal   master_drpwe     : std_logic_vector(c_NUM_DRP_MASTER-1 downto 0);
  signal   master_drpen     : std_logic_vector(c_NUM_DRP_MASTER-1 downto 0);
  signal   master_drpaddr   : std_logic_vector(9*c_NUM_DRP_MASTER-1 downto 0);
  signal   master_drpdi     : std_logic_vector(16*c_NUM_DRP_MASTER-1 downto 0);
  signal   master_drprdy    : std_logic_vector(c_NUM_DRP_MASTER-1 downto 0);
  signal   master_drpdo     : std_logic_vector(16*c_NUM_DRP_MASTER-1 downto 0);

--============================================================================
--! Architecture begin
--============================================================================  
begin

  gen_bit_folding: for bit_idx in 0 to 9 generate  
    begin  
      userdata_tx(bit_idx*4+0)    <= tx_data_i(bit_idx);
      userdata_tx(bit_idx*4+1)    <= tx_data_i(bit_idx);
      userdata_tx(bit_idx*4+2)    <= tx_data_i(bit_idx);
      userdata_tx(bit_idx*4+3)    <= tx_data_i(bit_idx);	  
  end generate;  

  rx_data_o      <= userdata_rx;
  clk_trxusr240_o <= rxusrclk2;

  rxfsmrstdone_o <= not rx_system_reset;
  txfsmrstdone_o <= not tx_system_reset;
  --============================================================================
  --! User Clock Source
  --============================================================================    
  txusrclk       <= userclk_tx_usrclk(0);
  txusrclk2      <= userclk_tx_usrclk2(0);
  rxusrclk       <= userclk_rx_usrclk(0);
  rxusrclk2      <= userclk_rx_usrclk2(0);

  --============================================================================
  --! Common Blocks
  --============================================================================     
  --reset_all(0) <= mgt_reset_i or reset_all_init;
  reset_all(0) <= '0';

  tx_init_done <= reset_tx_done(0);     -- and buffbypass_tx_done(0);
  rx_init_done <= reset_rx_done(0) and buffbypass_rx_done(0);

  reset_rx_pll_and_datapath(0) <= mgt_reset_i or reset_rx_init;

  onu_gth_rx_init : onu_gth_simplex_init
    generic map (
      FREERUN_FREQUENCY => 100,
      TIMER_DURATION_US => 130000
      )
    port map(
      clk_freerun_in        => clk_sys_i,
      reset_simplex_in      => reset_rx_pll_and_datapath(0),
      simplex_init_done_in  => rx_init_done,
      reset_simplex_out     => reset_rx_init,
      simplex_init_done_out => rx_init_fsm_done,
      retry_ctr_out         => rx_init_retry_ctr
      );

  reset_tx_pll_and_datapath(0) <= mgt_reset_i or reset_tx_init or (not tx_external_locked_i);

  onu_gth_tx_init : onu_gth_simplex_init
    generic map (
      FREERUN_FREQUENCY => 100,
      TIMER_DURATION_US => 30000
      )
    port map(
      clk_freerun_in        => clk_sys_i,
      reset_simplex_in      => reset_tx_pll_and_datapath(0),
      simplex_init_done_in  => tx_init_done,
      reset_simplex_out     => reset_tx_init,
      simplex_init_done_out => tx_init_fsm_done,
      retry_ctr_out         => tx_init_retry_ctr
      );        

  init_done <= tx_init_fsm_done and rx_init_fsm_done;

  userclk_tx_reset <= not txpmaresetdone;
  userclk_rx_reset <= not rxpmaresetdone;

  userclk_tx_not_ready <= not userclk_tx_active(0);

  --reset_synchronizer_buffbypass_tx_reset_inst : reset_synchronizer
  --  port map (
  --    clk_in  => txusrclk2,
  --    rst_in  => userclk_tx_not_ready,
  --    rst_out => buffbypass_tx_reset(0)
  --    );

  userclk_rx_not_ready <= not userclk_rx_active(0);
  reset_synchronizer_buffbypass_rx_reset_inst : reset_synchronizer
    port map (
      clk_in  => rxusrclk2,
      rst_in  => userclk_rx_not_ready,
      rst_out => buffbypass_rx_reset(0)
      );

  --============================================================================
  --! GTH Wrapper
  --============================================================================        
  ----------------------------- The GT Wrapper -----------------------------
  reset_clk_freerun(0) <= clk_sys_i;

  drpclk(0)     <= clk_sys_i;
  gtrefclk01(0) <= clk_rxref240_i;
  gtrefclk0(0)  <= clk_txref240_i;
  gthrxn(0)     <= rx_n_i;
  gthrxp(0)     <= rx_p_i;
  rx8b10ben(0)  <= '0';
  rxslide(0)    <= rx_slide_i;
  

	  
  pon_onu_gth_trx_inst : pon_onu_gth_trx
    port map (
      --gtwiz_userclk_tx_reset_in          => userclk_tx_reset,
      --gtwiz_userclk_tx_srcclk_out        => open,
      --gtwiz_userclk_tx_usrclk_out        => userclk_tx_usrclk,
      --gtwiz_userclk_tx_usrclk2_out       => userclk_tx_usrclk2,
      gtwiz_userclk_tx_active_in           => userclk_tx_active,
	  txoutclk_out                         => userclk_tx_srcclk,
	  txusrclk_in                          => userclk_tx_usrclk,
	  txusrclk2_in                         => userclk_tx_usrclk2,
      --gtwiz_userclk_rx_reset_in          => userclk_rx_reset,
      --gtwiz_userclk_rx_srcclk_out        => open,
      --gtwiz_userclk_rx_usrclk_out        => userclk_rx_usrclk,
      --gtwiz_userclk_rx_usrclk2_out       => userclk_rx_usrclk2,
	  rxoutclk_out                         => userclk_rx_srcclk,
	  rxusrclk_in                          => userclk_rx_usrclk,
	  rxusrclk2_in                         => userclk_rx_usrclk2,	  
      gtwiz_userclk_rx_active_in         => userclk_rx_active,
      gtwiz_buffbypass_rx_reset_in       => buffbypass_rx_reset,
      gtwiz_buffbypass_rx_start_user_in  => "0",
      gtwiz_buffbypass_rx_done_out       => buffbypass_rx_done,
      gtwiz_buffbypass_rx_error_out      => buffbypass_rx_error,
      gtwiz_reset_clk_freerun_in         => reset_clk_freerun,
      gtwiz_reset_all_in                 => reset_all,
      gtwiz_reset_tx_pll_and_datapath_in => reset_tx_pll_and_datapath,
      gtwiz_reset_tx_datapath_in         => "0",
      gtwiz_reset_rx_pll_and_datapath_in => reset_rx_pll_and_datapath,
      gtwiz_reset_rx_datapath_in         => "0",
      gtwiz_reset_rx_cdr_stable_out      => open,
      gtwiz_reset_tx_done_out            => reset_tx_done,
      gtwiz_reset_rx_done_out            => reset_rx_done,
      gtwiz_userdata_tx_in               => userdata_tx,
      gtwiz_userdata_rx_out              => userdata_rx,
      gtrefclk01_in                      => gtrefclk01,
      qpll1lockdetclk_in                 => reset_clk_freerun,
      qpll1lock_out                      => qpll1lock,
      qpll1outclk_out                    => open,
      qpll1outrefclk_out                 => open,
      cplllockdetclk_in                  => reset_clk_freerun,
      cpllreset_in                       => "0",
      drpclk_in                          => drpclk,
      drpaddr_in                         => drpaddr,
      drpdi_in                           => drpdi,
      drpen_in                           => drpen,
      drpwe_in                           => drpwe,
      drpdo_out                          => drpdo,
      drprdy_out                         => drprdy,
      gthrxn_in                          => gthrxn,
      gthrxp_in                          => gthrxp,
      gtrefclk0_in                       => gtrefclk0,
      rx8b10ben_in                       => rx8b10ben,
      rxslide_in                         => rxslide,
      cplllock_out                       => cplllock,
      gthtxn_out                         => gthtxn,
      gthtxp_out                         => gthtxp,
      rxpmaresetdone_out                 => rxpmaresetdone,
      txpmaresetdone_out                 => txpmaresetdone,
      rxlpmen_in                         => rxlpmen,
      rxlpmgcovrden_in                   => rxlpmgcovrden,
      rxlpmhfovrden_in                   => rxlpmhfovrden,
      rxlpmlfklovrden_in                 => rxlpmlfklovrden,
      rxlpmosovrden_in                   => rxlpmosovrden,
	  rxdfevpovrden_in                   => rxdfevpovrden, 
      rxdfeutovrden_in                   => rxdfeutovrden,
      rxosovrden_in                      => rxosovrden,
      rxdfeagcovrden_in                  => rxdfeagcovrden,
      rxdfelfovrden_in                   => rxdfelfovrden,
	  dmonitorclk_in(0)                  => clk_sys_i,
      dmonitorout_out                    => dmonitorout,
      txpippmen_in                       => txpippmen,
      txpippmovrden_in                   => txpippmovrden,
      txpippmpd_in                       => txpippmpd,
      txpippmsel_in                      => txpippmsel,
      txpippmstepsize_in                 => txpippmstepsize,
      txbufstatus_out                    => txbufstatus

      );

  -- BUFG_GT: Clock Buffer Driven by Gigabit Transceiver
  -- UltraScale
  -- Xilinx HDL Libraries Guide, version 2014.1
  BUFG_GT_inst : BUFG_GT
  port map (
    O       => userclk_rx_usrclk2(0),      -- 1-bit output: Buffer
    CE      => '1',                        -- 1-bit input: Buffer enable
    CEMASK  => '0',                        -- 1-bit input: CE Mask
    CLR     => userclk_rx_reset(0),        -- 1-bit input: Asynchronous clear
    CLRMASK => '0',                        -- 1-bit input: CLR Mask
    DIV     => "000",                      -- 3-bit input: Dymanic divide Value
    I       => userclk_rx_srcclk(0)        -- 1-bit input: Buffer
  );
  
  
  p_tx_active : process (userclk_tx_reset(0), txusrclk2) is
  begin  -- process p_drp_write
    if(userclk_tx_reset(0) = '1') then
        userclk_tx_active_meta <= '0';
		userclk_tx_active(0)   <= '0';
    elsif txusrclk2'event and txusrclk2 = '1' then  -- rising clock edge
        userclk_tx_active_meta  <= '1';
		userclk_tx_active(0)    <= userclk_tx_active_meta;
    end if;
  end process p_tx_active;  

  p_rx_active : process (userclk_rx_reset(0), rxusrclk2) is
  begin  -- process p_drp_write
    if(userclk_rx_reset(0) = '1') then
        userclk_rx_active_meta <= '0';
		userclk_rx_active(0)   <= '0';
    elsif rxusrclk2'event and rxusrclk2 = '1' then  -- rising clock edge
        userclk_rx_active_meta  <= '1';
		userclk_rx_active(0)    <= userclk_rx_active_meta;
    end if;
  end process p_rx_active;  

  userclk_rx_usrclk  <= userclk_rx_usrclk2;
  userclk_tx_usrclk  <= userclk_rx_usrclk2;
  userclk_tx_usrclk2 <= userclk_rx_usrclk2;

  -- End of BUFG_GT_inst instantiation

  tx_n_o      <= gthtxn(0);
  tx_p_o      <= gthtxp(0);
  mgt_txpll_o <= cplllock(0);
  mgt_rxpll_o <= qpll1lock(0);

  --============================================================================
  --! DRP arbiter
  --============================================================================      
  cmp_drp_arbiter : drp_arbiter
    generic map(
      g_NUM_MASTER => c_NUM_DRP_MASTER
      )
    port map(
      -- global input signals --
      clk_i   => clk_sys_i,
      reset_i => mgt_reset_i,
      --------------------------

      -- Interface to user --
      master_drpwe_i   => master_drpwe,
      master_drpen_i   => master_drpen,
      master_drpaddr_i => master_drpaddr,
      master_drpdi_i   => master_drpdi,
      master_drprdy_o  => master_drprdy,
      master_drpdo_o   => master_drpdo,
      -------------------------------

      -- DRP connection to GTH/GTX --
      mgt_drpwe_o   => drpwe(0),
      mgt_drpen_o   => drpen(0),
      mgt_drpaddr_o => drpaddr,
      mgt_drpdi_o   => drpdi,
      mgt_drprdy_i  => drprdy(0),
      mgt_drpdo_i   => drpdo
      -------------------------------
      );

  --============================================================================
  --! DRP interfacing
  --============================================================================
  cmp_drp_ctrl : drp_ctrl
    port map(
      -- global input signals --
      clk_i   => clk_sys_i,
      reset_i => mgt_reset_i,
      --------------------------

      -- Interface to user --
      drp_wr_i        => drp_wr_i,
      drp_wr_strobe_i => drp_wr_strobe_i,
      drp_monitor_o   => drp_monitor_o,
      -------------------------------

      -- DRP connection to GTH/GTX --
      mgt_drpwe_o   => master_drpwe(1),
      mgt_drpen_o   => master_drpen(1),
      mgt_drpaddr_o => master_drpaddr(9*2-1 downto 9*1),
      mgt_drpdi_o   => master_drpdi(16*2-1 downto 16*1),
      mgt_drprdy_i  => master_drprdy(1),
      mgt_drpdo_i   => master_drpdo(16*2-1 downto 16*1)
      -------------------------------

      );
  -------- DRP control (end) ----------------------------------------------------- 

  --============================================================================
  --! Receiver Equalizer Control
  --============================================================================
  rxlpmen(0)                            <= mgt_rx_equalizer_ctrl_i(0);

  rxlpmgcovrden(0)                      <= mgt_rx_equalizer_ctrl_i(1);
  rxlpmhfovrden(0)                      <= mgt_rx_equalizer_ctrl_i(2);
  rxlpmlfklovrden(0)                    <= mgt_rx_equalizer_ctrl_i(3);
  rxlpmosovrden(0)                      <= mgt_rx_equalizer_ctrl_i(4);

  rxdfevpovrden(0)                      <= mgt_rx_equalizer_ctrl_i(5);
  rxdfeutovrden(0)                      <= mgt_rx_equalizer_ctrl_i(6);
  rxosovrden(0)                         <= mgt_rx_equalizer_ctrl_i(7);
  rxdfeagcovrden(0)                     <= mgt_rx_equalizer_ctrl_i(8);
  rxdfelfovrden(0)                      <= mgt_rx_equalizer_ctrl_i(9);

  mgt_rx_equalizer_stat_o(9 downto 0)   <= mgt_rx_equalizer_ctrl_i(9 downto 0);
  mgt_rx_equalizer_stat_o(16 downto 10)  <= dmonitorout_latch(6 downto 0);
  mgt_rx_equalizer_stat_o(31 downto 17) <= (others => '0');

  p_rx_monitor : process (clk_sys_i) is
  begin  -- process p_drp_write
    if clk_sys_i'event and clk_sys_i = '1' then  -- rising clock edge
      dmonitorout_r  <= dmonitorout;
      dmonitorout_r2 <= dmonitorout_r;
      if(dmonitorout_r = dmonitorout_r2) then
        dmonitorout_latch <= dmonitorout_r2;
      else
        dmonitorout_latch <= dmonitorout_latch;
      end if;
    end if;
  end process p_rx_monitor;

  --============================================================================
  --! Tx Phase Aligner
  --============================================================================    
  cmp_tx_phase_aligner : tx_phase_aligner
    generic map(
      -- User choice of DRP control or port control
      -- Recommended nowadays to use in DRP control as a strange behaviour was observed using the port in PI code stepping mode
      g_DRP_NPORT_CTRL        => true,
      g_DRP_ADDR_TXPI_PPM_CFG => "010011010"
      )                                                                 
    port map(
      --==============================================================================
      --! User control/monitor ports
      --==============================================================================  
      -- Clock / reset                                                     
      clk_sys_i => clk_sys_i,
      reset_i   => tx_phase_aligner_reset_syssync,

      -- Top level interface                                                 
      tx_aligned_o => tx_aligned,

      -- Config (for different flavours)
      tx_pi_phase_calib_i   => tx_pi_phase_calib,
      tx_ui_align_calib_i   => tx_ui_align_calib,
      tx_fifo_fill_pd_max_i => tx_fifo_fill_pd_max,


      tx_fine_realign_i => tx_fine_realign,

      -- It is only valid to re-shift clock once aligned (tx_aligned_o = '1') 
      ps_strobe_i     => tx_ps_strobe,
      ps_inc_ndec_i   => tx_ps_inc_ndec,
      ps_phase_step_i => tx_ps_phase_step,
      ps_done_o       => tx_ps_done,

      -- Tx PI phase value
      tx_pi_phase_o => tx_pi_phase,

      -- Tx fifo fill level phase detector                                   
      tx_fifo_fill_pd_o => tx_fifo_fill_pd,

      --==============================================================================
      --! MGT ports
      --==============================================================================
      clk_txusr_i          => txusrclk2,
      -- Tx fifo fill level - see Xilinx transceiver User Guide for more information    
      tx_fifo_fill_level_i => txbufstatus(0),

      -- Transmitter PI ports - see Xilinx transceiver User Guide for more information
      -- obs1: all txpi ports shall be connected to the transceiver even when using this block in DRP-mode              
      txpippmen_o       => txpippmen(0),
      txpippmovrden_o   => txpippmovrden(0),
      txpippmsel_o      => txpippmsel(0),
      txpippmpd_o       => txpippmpd(0),
      txpippmstepsize_o => txpippmstepsize,

      -- DRP interface - see Xilinx transceiver User Guide for more information
      -- obs2: connect clk_sys_i to drpclk
      -- obs3: if using this block in port-mode, DRP output can be left floating and input connected to '0'             
      drpaddr_o => master_drpaddr(9*1-1 downto 9*0),
      drpen_o   => master_drpen(0),
      drpdi_o   => master_drpdi(16*1-1 downto 16*0),
      drprdy_i  => master_drprdy(0),
      drpdo_i   => master_drpdo(16*1-1 downto 16*0),
      drpwe_o   => master_drpwe(0)

      );

  tx_ps_strobe           <= mgt_tx_phase_ctrl_i(0);
  mgt_tx_phase_stat_o(0) <= tx_ps_done_latched;
  p_tx_ps_done_latch : process (clk_sys_i) is
  begin
    if clk_sys_i'event and clk_sys_i = '1' then
      if mgt_reset_i = '1' then
        tx_ps_done_latched <= '0';
      else
        if (tx_ps_strobe_r = '0' and tx_ps_strobe = '1') then
          tx_ps_done_latched <= '0';
        elsif (tx_ps_done_r = '0' and tx_ps_done = '1') then
          tx_ps_done_latched <= '1';
        end if;
      end if;
      tx_ps_strobe_r <= tx_ps_strobe;
      tx_ps_done_r   <= tx_ps_done;
    end if;
  end process p_tx_ps_done_latch;

  tx_fine_realign                  <= '0';
  tx_ps_inc_ndec                   <= mgt_tx_phase_ctrl_i(1);
  tx_ps_phase_step                 <= mgt_tx_phase_ctrl_i(5 downto 2);
  tx_pi_phase_calib                <= mgt_tx_phase_ctrl_i(12 downto 6);
  tx_ui_align_calib                <= mgt_tx_phase_ctrl_i(13);
  mgt_tx_phase_stat_o(13 downto 1) <= mgt_tx_phase_ctrl_i(13 downto 1);

  mgt_tx_phase_stat_o(20 downto 14) <= tx_pi_phase(6 downto 0);
  mgt_tx_phase_stat_o(26 downto 21) <= tx_fifo_fill_pd(22 downto 17);
  tx_fifo_fill_pd_max               <= x"00400000";

  phase_good_o <= tx_aligned;
  
  --============================================================================
  --! Phase Aligner Reset
  --============================================================================ 
  tx_phase_aligner_reset_async <= not tx_init_done;
  reset_synchronizer_tx_phase_aligner_inst : reset_synchronizer
    port map (
      clk_in  => clk_sys_i,
      rst_in  => tx_phase_aligner_reset_async,
      rst_out => tx_phase_aligner_reset_syssync
      );

  --============================================================================
  --! FSM Reset Done Synchronizers
  --============================================================================
  ------------TX user logic reset-----------------------------------------    
  tx_system_reset_async <= (not tx_aligned) or (not tx_init_done);
  reset_synchronizer_tx_system_reset_inst : reset_synchronizer
    port map (
      clk_in  => txusrclk2,
      rst_in  => tx_system_reset_async,
      rst_out => tx_system_reset
      );
  ------------RX user logic reset-----------------------------------------    
  rx_system_reset_async <= (not rx_init_done);
  reset_synchronizer_rx_system_reset_inst : reset_synchronizer
    port map (
      clk_in  => rxusrclk2,
      rst_in  => rx_system_reset_async,
      rst_out => rx_system_reset
      );

end rtl;
