--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_gth_simplex_init.vhd
--=============================================================================

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU GTH Init (onu_gth_simplex_init)
--
--! @brief onu MultiGigabitTransceiver Init FSM
--! Kintex Ultrascale - GTH
--
--! @author Csaba SOOS
--! @date 16/11/2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Csaba SOOS
--! Author: Eduardo MENDES
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 16/11\2016 - CS - Created\n
--! 06/12/2016 - EBSM - Simplex reset to allow separate RX/TX reset\n
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_gth_simplex_init
--============================================================================
entity onu_gth_simplex_init is
  generic (
    FREERUN_FREQUENCY : positive := 100;
    TIMER_DURATION_US : positive := 30000);  --30000 for TX / 130000 for RX
  port (
    clk_freerun_in        : in  std_logic;
    reset_simplex_in      : in  std_logic;
    simplex_init_done_in  : in  std_logic;
    reset_simplex_out     : out std_logic;
    simplex_init_done_out : out std_logic;
    retry_ctr_out         : out std_logic_vector (3 downto 0));
end onu_gth_simplex_init;

--============================================================================
--! Architecture declaration for onu_gth_simplex_init
--============================================================================
architecture rtl of onu_gth_simplex_init is

  component reset_synchronizer
    port (
      clk_in  : in  std_logic;
      rst_in  : in  std_logic;
      rst_out : out std_logic
      );
  end component;
  component bit_synchronizer
    port (
      clk_in : in  std_logic;
      i_in   : in  std_logic;
      o_out  : out std_logic
      );
  end component;

  signal reset_simplex_sync     : std_logic;
  signal simplex_init_done_sync : std_logic;

  signal timer_clr : std_logic                  := '1';
  signal timer_ctr : integer range 0 to 2**25-1 := 0;
  signal timer_sat : std_logic                  := '0';

  signal retry_ctr_incr : std_logic;
  signal retry_ctr      : integer range 0 to 15 := 0;

  type   t_sm_states is (ST_START, ST_WAIT, ST_MONITOR);
  signal sm_init        : t_sm_states := ST_START;
  signal sm_init_active : std_logic;

begin

  reset_synchronizer_reset_simplex_inst : reset_synchronizer
    port map (
      clk_in  => clk_freerun_in,
      rst_in  => reset_simplex_in,
      rst_out => reset_simplex_sync);


  bit_synchronizer_simplex_init_done_inst : bit_synchronizer
    port map (
      clk_in => clk_freerun_in,
      i_in   => simplex_init_done_in,
      o_out  => simplex_init_done_sync);

  p_timers : process(clk_freerun_in)
  begin
    if rising_edge(clk_freerun_in) then
      if (timer_clr = '1') then
        timer_ctr <= 0;
        timer_sat <= '0';
      else
        
        if (timer_ctr = (TIMER_DURATION_US * FREERUN_FREQUENCY)) then
          timer_sat <= '1';
        else
          timer_ctr <= timer_ctr + 1;
        end if;
        
      end if;
    end if;
  end process;

  p_retry_ctr : process(clk_freerun_in)
  begin
    if rising_edge(clk_freerun_in) then
      if (retry_ctr_incr = '1' and retry_ctr < 15) then
        retry_ctr <= retry_ctr + 1;
      end if;
    end if;
  end process;
  retry_ctr_out <= std_logic_vector(to_unsigned(retry_ctr, 4));

  p_init_fsm : process(clk_freerun_in)
  begin
    if rising_edge(clk_freerun_in) then
      if (reset_simplex_sync = '1') then
        timer_clr             <= '1';
        reset_simplex_out     <= '0';
        retry_ctr_incr        <= '0';
        simplex_init_done_out <= '0';
        sm_init_active        <= '1';
        sm_init               <= ST_START;
      else
        case (sm_init) is
          
          when ST_START =>
            if (sm_init_active = '1') then
              timer_clr         <= '1';
              reset_simplex_out <= '0';
              retry_ctr_incr    <= '0';
              sm_init           <= ST_WAIT;
            end if;
            
          when ST_WAIT =>
            if (simplex_init_done_sync = '1') then
              timer_clr             <= '1';
              simplex_init_done_out <= '1';
              sm_init               <= ST_MONITOR;
            elsif (timer_sat = '1') then
              timer_clr         <= '1';
              reset_simplex_out <= '1';
              retry_ctr_incr    <= '1';
              sm_init           <= ST_START;
            else
              timer_clr <= '0';
            end if;
            
            
          when ST_MONITOR =>
            if (simplex_init_done_sync = '0') then
              simplex_init_done_out <= '0';
              timer_clr             <= '1';
              reset_simplex_out     <= '1';
              retry_ctr_incr        <= '1';
              sm_init               <= ST_START;
            end if;
            
          when others =>
            sm_init <= ST_START;
            
        end case;
      end if;
    end if;
  end process;

  
end rtl;
