--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file pon_onu_axi_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.pon_onu_package_static.all;
use work.pon_onu_package_modifiable.all;
--! Xilinx Packages
library UNISIM;
use UNISIM.vcomponents.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: PON ONU - Example design (pon_onu_axi_wrapper)
--
--! @brief Example design for ONU
--! <further description>
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 19/07/2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 07\07\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for pon_onu_axi_wrapper
--============================================================================
entity pon_onu_axi_wrapper is
  generic (
    -- Users to add parameters here

    -- User parameters ends
    -- Do not modify the parameters beyond this line

    -- Parameters of Axi Slave Bus Interface S00_AXI
    C_S00_AXI_DATA_WIDTH : integer := 32;
    C_S00_AXI_ADDR_WIDTH : integer := 9
    );
  port (
    -- Axi Interface
    s_onu_axi_aclk    : in  std_logic;
    s_onu_axi_aresetn : in  std_logic;
    s_onu_axi_awaddr  : in  std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s_onu_axi_awprot  : in  std_logic_vector(2 downto 0);
    s_onu_axi_awvalid : in  std_logic;
    s_onu_axi_awready : out std_logic;
    s_onu_axi_wdata   : in  std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s_onu_axi_wstrb   : in  std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
    s_onu_axi_wvalid  : in  std_logic;
    s_onu_axi_wready  : out std_logic;
    s_onu_axi_bresp   : out std_logic_vector(1 downto 0);
    s_onu_axi_bvalid  : out std_logic;
    s_onu_axi_bready  : in  std_logic;
    s_onu_axi_araddr  : in  std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s_onu_axi_arprot  : in  std_logic_vector(2 downto 0);
    s_onu_axi_arvalid : in  std_logic;
    s_onu_axi_arready : out std_logic;
    s_onu_axi_rdata   : out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s_onu_axi_rresp   : out std_logic_vector(1 downto 0);
    s_onu_axi_rvalid  : out std_logic;
    s_onu_axi_rready  : in  std_logic;

    -- Clk sys/reset              
    onu_clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
    onu_core_reset_i : in std_logic;

    -- MGT ref clocks
    onu_clk_rxref240_i       : in std_logic;  --! MGT Rx reference clock - 240MHz      
    onu_clk_txref240_i       : in std_logic;  --! MGT Tx reference clock - 240MHz (has to be derived from downstream recovered clock - synchronization required for TTC-PON)        
    onu_tx_external_locked_i : in std_logic;  --! ONU external PLL is not locked

    -- Recovered downstream 40MHz clock
    onu_clk_rx40_o    : out std_logic;
    onu_rx40_locked_o : out std_logic;

    -- Data in/out
    onu_clk_trxusr240_o   : out std_logic;  --! User_TRx 240MHz clock
    onu_rx_data_strobe_o : out std_logic;
    onu_rx_data_frame_o  : out std_logic_vector(199 downto 0);
    onu_tx_data_strobe_i : in  std_logic;
    onu_tx_data_ready_o  : out std_logic;
    onu_tx_data_i        : in  std_logic_vector(55 downto 0);
    onu_address_i        : in  std_logic_vector(7 downto 0);  --! ONU address

    -- Status
    onu_mgt_phase_good_o      : out std_logic;  --! internal MGT Tx/Rx phase alignment
    onu_rx_locked_o           : out std_logic;  --! downstream header aligned
    onu_operational_o         : out std_logic;  --! given by onu once system init (calibration) is done
    onu_rx_serror_corrected_o : out std_logic;  --! a single-error was corrected by the FEC
    onu_rx_derror_corrected_o : out std_logic;  --! a double-error was corrected by the FEC        
    onu_mgt_tx_ready_o        : out std_logic;
    onu_mgt_rx_ready_o        : out std_logic;
    onu_mgt_txpll_lock_o      : out std_logic;
    onu_mgt_rxpll_lock_o      : out std_logic;

    -- Soft-configurable interrupt output
    onu_interrupt_o     : out std_logic;
    onu_status_sticky_o : out std_logic_vector(30 downto 0);
    ------------------------

    -- serial
    onu_tx_p_o : out std_logic;
    onu_tx_n_o : out std_logic;
    onu_rx_p_i : in  std_logic;
    onu_rx_n_i : in  std_logic;

    onu_sfp_rx_los_i   : in  std_logic;
    onu_sfp_tx_fault_i : in  std_logic;
    onu_sfp_mod_abs_i  : in  std_logic;
    onu_sfp_tx_sd_i    : in  std_logic;
    onu_sfp_pdown_o    : out std_logic;
    onu_sfp_tx_dis_o   : out std_logic;

    --i2c interface
    onu_i2c_sda_io : inout std_logic;
    onu_i2c_scl_io : inout std_logic
    -------------------------

    );
end pon_onu_axi_wrapper;

--============================================================================
--! Architecture declaration for pon_onu_axi_wrapper
--============================================================================ 
architecture structural of pon_onu_axi_wrapper is

  attribute mark_debug : string;
  attribute keep       : string;

  --! Signals
  -- onu_control_axi_wrapper <-> onu_core
  signal onu_stat_reg        : t_regbank32b(127 downto 0);
  signal onu_ctrl_reg        : std_logic_vector(31 downto 0);
  signal onu_ctrl_reg_strobe : std_logic_vector(127 downto 0);

  -- global from onu_core   
  signal onu_core_rx_data_strobe : std_logic;
  signal onu_core_rx_locked      : std_logic;

  -- global from mgt
  signal onu_mgt_clk_trxusr240 : std_logic;
  signal onu_mgt_tx_ready     : std_logic;
  signal onu_mgt_rx_ready     : std_logic;
  signal onu_mgt_txpll_lock   : std_logic;
  signal onu_mgt_rxpll_lock   : std_logic;

  -- onu_core <-> onu_mgt_wrapper
  signal onu_core_to_mgt_reset   : std_logic;
  signal onu_core_to_mgt_rxslide : std_logic;
  signal onu_core_to_mgt_tx_data : std_logic_vector(9 downto 0);
  signal onu_mgt_to_core_rx_data : std_logic_vector(39 downto 0);

  signal onu_core_to_mgt_drp_wr            : std_logic_vector(31 downto 0);
  signal onu_core_to_mgt_drp_wr_strobe     : std_logic;
  signal onu_mgt_to_core_drp_monitor       : std_logic_vector(31 downto 0);
  signal onu_core_to_mgt_rx_equalizer_ctrl : std_logic_vector(31 downto 0);
  signal onu_mgt_to_core_rx_equalizer_stat : std_logic_vector(31 downto 0);
  signal onu_core_to_mgt_tx_phase_ctrl     : std_logic_vector(31 downto 0);
  signal onu_mgt_to_core_tx_phase_stat     : std_logic_vector(31 downto 0);

  -- MMCM to generate 40MHz fixed-latency in ONU design
  signal reset_rx40 : std_logic;

  -- onu_core <-> i2c_master
  signal onu_core_to_i2c_master_i2c_req      : std_logic;
  signal onu_core_to_i2c_master_i2c_rnw      : std_logic;
  signal onu_core_to_i2c_master_i2c_rb2      : std_logic;
  signal onu_core_to_i2c_master_i2c_slv_addr : std_logic_vector(6 downto 0);
  signal onu_core_to_i2c_master_i2c_reg_addr : std_logic_vector(7 downto 0);
  signal onu_core_to_i2c_master_i2c_wr_data  : std_logic_vector(7 downto 0);

  signal i2c_master_to_onu_core_i2c_done     : std_logic;
  signal i2c_master_to_onu_core_i2c_error    : std_logic;
  signal i2c_master_to_onu_core_i2c_drop_req : std_logic;
  signal i2c_master_to_onu_core_i2c_rd_data  : std_logic_vector(15 downto 0);

  -- i2c_master
  signal i2c_master_sdi      : std_logic;
  signal i2c_master_sdo      : std_logic;
  signal i2c_master_sda_ena  : std_logic;
  signal i2c_master_scl_ena  : std_logic;
  signal i2c_master_scl      : std_logic;
  signal ni2c_master_sda_ena : std_logic;
  signal ni2c_master_scl_ena : std_logic;

  --! Component declaration
  component onu_core is                 -- use work.pon_onu_package.all;
    port (
      -- global input signals --
      clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
      core_reset_i : in std_logic;
      -------------------------

      -- manager control/stat -- see manager core interfacing just below for more information about how to interface the core control
      clk_manager_i         : in  std_logic;
      manager_stat_reg_o    : out t_regbank32b(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
      manager_ctrl_reg_i    : in  std_logic_vector(31 downto 0);
      manager_ctrl_strobe_i : in  std_logic_vector(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
      --------------------------

      -- status / control --
      onu_address_i         : in  std_logic_vector(7 downto 0);  --! ONU address in TDM
      rx_locked_o           : out std_logic;  --! downstream header aligned
      onu_operational_o     : out std_logic;  --! given by onu once system init (calibration) is done; this means that ONU is allowed to transmit
      rx_serror_corrected_o : out std_logic;  --! a single-error was corrected by the FEC
      rx_derror_corrected_o : out std_logic;  --! a double-error was corrected by the FEC

      -- Soft-configurable interrupt output
      interrupt_o     : out std_logic;
      status_sticky_o : out std_logic_vector(30 downto 0);
      -------------------------             

      -- MGT connections -----------
      -- Clocks
      clk_trxusr240_i : in std_logic;    --! User_TRx 240MHz clock  

      -- Status/Ctrl
      mgt_reset_o      : out std_logic;
      mgt_tx_ready_i   : in  std_logic;
      mgt_rx_ready_i   : in  std_logic;
      mgt_txpll_lock_i : in  std_logic;
      mgt_rxpll_lock_i : in  std_logic;
      mgt_rxslide_o    : out std_logic;

      -- Xilinx specific
      mgt_drp_wr_o            : out std_logic_vector(31 downto 0);
      mgt_drp_wr_strobe_o     : out std_logic;
      mgt_drp_monitor_i       : in  std_logic_vector(31 downto 0);
      mgt_rx_equalizer_ctrl_o : out std_logic_vector(31 downto 0);
      mgt_rx_equalizer_stat_i : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_o     : out std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_i     : in  std_logic_vector(31 downto 0);

      -- Data
      mgt_tx_data_o : out std_logic_vector(c_ONU_TX_MGT_DATA_WIDTH-1 downto 0);
      mgt_rx_data_i : in  std_logic_vector(c_ONU_RX_MGT_DATA_WIDTH-1 downto 0);
      ------------------------      

      -- data in/out --  -- see TX usr core interfacing / RX usr core just below for more information about how to interface the core
      tx_data_strobe_i : in  std_logic;
      tx_data_ready_o  : out std_logic;
      tx_data_i        : in  std_logic_vector(c_ONU_TX_USR_DATA_WIDTH-1 downto 0);
      rx_data_strobe_o : out std_logic;
      rx_data_frame_o  : out std_logic_vector(c_ONU_RX_USR_DATA_WIDTH-1 downto 0);
      -------------------------

      -- sfp data / control --
      sfp_rx_los_i   : in  std_logic;
      sfp_tx_fault_i : in  std_logic;
      sfp_mod_abs_i  : in  std_logic;
      sfp_tx_sd_i    : in  std_logic;
      sfp_pdown_o    : out std_logic;
      sfp_tx_dis_o   : out std_logic;
      -------------------------

      --i2c interface
      i2c_req_o      : out std_logic;
      i2c_rnw_o      : out std_logic;
      i2c_rb2_o      : out std_logic;
      i2c_slv_addr_o : out std_logic_vector(6 downto 0);
      i2c_reg_addr_o : out std_logic_vector(7 downto 0);
      i2c_wr_data_o  : out std_logic_vector(7 downto 0);

      i2c_done_i     : in std_logic;
      i2c_error_i    : in std_logic;
      i2c_drop_req_i : in std_logic;
      i2c_rd_data_i  : in std_logic_vector(15 downto 0)
      -------------------------

      );
  end component onu_core;

  component clkdiv_phase_ctrl is
    generic(
      -- g_NUM_PHASE_SHIFT_CYCLE is the number of fine phase shifts required to shift the clkdiv_o clock by one period of clk_i
      -- !!! This is device/configuration dependent !!!
      -- -> For ALTERA ARRIA10 IOPLL:
      -- Obs: VCO_PERIOD=1440 MHz, each phase shift is 1/8 of the VCO PERIOD (see Altera IOPLL phase shift and dynamic reconfiguration)
      -- The number of phase shifts needed to shift the clock by 1/240MHz period is given by:
      -- NUM_SHIFT = (1/240)/((1/1440)/8) = 48
      -- -> For Xilinx 7-Series MMCM:
      -- Obs: VCO_PERIOD=1200 MHz, each phase shift is 1/56 of the VCO PERIOD (see 7-series clocking resources - MMCM dynamic shift interface)
      -- The number of phase shifts needed to shift the clock by 1/240MHz period is given by:
      -- NUM_SHIFT = (1/240)/((1/1200)/56) = 280
      g_NUM_PHASE_SHIFT_CYCLE : integer := 280;

      -- Number of consecutive correctly detected strobes in order to say phase is locked
      g_GOOD_STROBE_TO_LOCK : integer := 6;

      -- Number of consecutive wrongly detected strobes in order to loose lock
      g_BAD_STROBE_TO_UNLOCK : integer := 3
      );
    port (
      clk_sys_i      : in  std_logic;
      reset_i        : in  std_logic;
      clk_i          : in  std_logic;
      strobe_i       : in  std_logic;
      clkdiv_o       : out std_logic;
      phase_locked_o : out std_logic
      );
  end component clkdiv_phase_ctrl;

  component onu_mgt_wrapper is
    port (
      -- global input signals --                
      clk_sys_i      : in std_logic;
      clk_rxref240_i : in std_logic;
      clk_txref240_i : in std_logic;
      mgt_reset_i    : in std_logic;
      -------------------------

      -- global output signals --
      clk_trxusr240_o : out std_logic;
      -------------------------

      -- status/control --
      tx_external_locked_i : in  std_logic;
      rx_slide_i           : in  std_logic;
      rxfsmrstdone_o       : out std_logic;
      txfsmrstdone_o       : out std_logic;
      mgt_txpll_o          : out std_logic;
      mgt_rxpll_o          : out std_logic;
      phase_good_o         : out std_logic;

      drp_wr_i                : in  std_logic_vector(31 downto 0);
      drp_wr_strobe_i         : in  std_logic;
      drp_monitor_o           : out std_logic_vector(31 downto 0);
      mgt_rx_equalizer_ctrl_i : in  std_logic_vector(31 downto 0);
      mgt_rx_equalizer_stat_o : out std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_i     : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_o     : out std_logic_vector(31 downto 0);
      -------------------------  

      -- data in/out --         
      tx_data_i : in  std_logic_vector(9 downto 0);
      rx_data_o : out std_logic_vector(39 downto 0);
      rx_p_i    : in  std_logic;
      rx_n_i    : in  std_logic;
      tx_p_o    : out std_logic;
      tx_n_o    : out std_logic
      -------------------------            
      );
  end component onu_mgt_wrapper;

  component i2c_interface is
    generic (
      g_CLOCK_PERIOD : integer range 5 to 20 := 10;    -- clock period in ns
      g_SCL_PERIOD   : integer               := 10000  -- SCL period in ns
      );
    port (
      -- global input signals --
      clk_i   : in std_logic;
      reset_i : in std_logic;
      -------------------------

      -- I2C transaction control/status signal --
      i2c_req_i      : in std_logic;
      i2c_rnw_i      : in std_logic;
      i2c_rb2_i      : in std_logic;
      i2c_slv_addr_i : in std_logic_vector(6 downto 0);
      i2c_reg_addr_i : in std_logic_vector(7 downto 0);
      i2c_wr_data_i  : in std_logic_vector(7 downto 0);

      i2c_done_o     : out std_logic;
      i2c_error_o    : out std_logic;
      i2c_drop_req_o : out std_logic;
      i2c_rd_data_o  : out std_logic_vector(15 downto 0);
      -------------------------------------------

      -- I2C in/out signal ----
      i2c_sdi_i     : in  std_logic;
      i2c_sdo_o     : out std_logic;
      i2c_sda_ena_o : out std_logic;
      i2c_scl_o     : out std_logic;
      i2c_scl_ena_o : out std_logic
      -------------------------
      );

  end component i2c_interface;

  component onu_control_axi_wrapper is
    generic (
      -- Users to add parameters here

      -- User parameters ends
      -- Do not modify the parameters beyond this line

      -- Parameters of Axi Slave Bus Interface ONU_AXI
      C_ONU_AXI_DATA_WIDTH : integer := 32;
      C_ONU_AXI_ADDR_WIDTH : integer := 9
      );
    port (
      -- Users to add ports here
      stat_reg_i        : in  t_regbank32b(127 downto 0);
      ctrl_reg_o        : out std_logic_vector(31 downto 0);
      ctrl_reg_strobe_o : out std_logic_vector(127 downto 0);

      -- User ports ends
      -- Do not modify the ports beyond this line

      -- Ports of Axi Slave Bus Interface ONU_AXI
      onu_axi_aclk    : in  std_logic;
      onu_axi_aresetn : in  std_logic;
      onu_axi_awaddr  : in  std_logic_vector(C_ONU_AXI_ADDR_WIDTH-1 downto 0);
      onu_axi_awprot  : in  std_logic_vector(2 downto 0);
      onu_axi_awvalid : in  std_logic;
      onu_axi_awready : out std_logic;
      onu_axi_wdata   : in  std_logic_vector(C_ONU_AXI_DATA_WIDTH-1 downto 0);
      onu_axi_wstrb   : in  std_logic_vector((C_ONU_AXI_DATA_WIDTH/8)-1 downto 0);
      onu_axi_wvalid  : in  std_logic;
      onu_axi_wready  : out std_logic;
      onu_axi_bresp   : out std_logic_vector(1 downto 0);
      onu_axi_bvalid  : out std_logic;
      onu_axi_bready  : in  std_logic;
      onu_axi_araddr  : in  std_logic_vector(C_ONU_AXI_ADDR_WIDTH-1 downto 0);
      onu_axi_arprot  : in  std_logic_vector(2 downto 0);
      onu_axi_arvalid : in  std_logic;
      onu_axi_arready : out std_logic;
      onu_axi_rdata   : out std_logic_vector(C_ONU_AXI_DATA_WIDTH-1 downto 0);
      onu_axi_rresp   : out std_logic_vector(1 downto 0);
      onu_axi_rvalid  : out std_logic;
      onu_axi_rready  : in  std_logic
      );
  end component onu_control_axi_wrapper;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component onu_core
  --============================================================================
  cmp_onu_core : onu_core
    port map(
      -- global input signals --
      clk_sys_i    => onu_clk_sys_i,
      core_reset_i => onu_core_reset_i,
      -------------------------

      -- manager control/stat
      clk_manager_i         => s_onu_axi_aclk,
      manager_stat_reg_o    => onu_stat_reg,
      manager_ctrl_reg_i    => onu_ctrl_reg,
      manager_ctrl_strobe_i => onu_ctrl_reg_strobe,
      --------------------------

      -- status / control --
      onu_address_i         => onu_address_i,
      rx_locked_o           => onu_core_rx_locked,
      onu_operational_o     => onu_operational_o,
      rx_serror_corrected_o => onu_rx_serror_corrected_o,
      rx_derror_corrected_o => onu_rx_derror_corrected_o,
      interrupt_o           => onu_interrupt_o,
      status_sticky_o       => onu_status_sticky_o,
      -------------------------   

      -- MGT connections -----------
      -- Clocks
      clk_trxusr240_i => onu_mgt_clk_trxusr240,

      -- Status/Ctrl
      mgt_reset_o      => onu_core_to_mgt_reset,
      mgt_tx_ready_i   => onu_mgt_tx_ready,
      mgt_rx_ready_i   => onu_mgt_rx_ready,
      mgt_txpll_lock_i => onu_mgt_txpll_lock,
      mgt_rxpll_lock_i => onu_mgt_rxpll_lock,
      mgt_rxslide_o    => onu_core_to_mgt_rxslide,

      mgt_drp_wr_o            => onu_core_to_mgt_drp_wr,
      mgt_drp_wr_strobe_o     => onu_core_to_mgt_drp_wr_strobe,
      mgt_drp_monitor_i       => onu_mgt_to_core_drp_monitor,
      mgt_rx_equalizer_ctrl_o => onu_core_to_mgt_rx_equalizer_ctrl,
      mgt_rx_equalizer_stat_i => onu_mgt_to_core_rx_equalizer_stat,
      mgt_tx_phase_ctrl_o     => onu_core_to_mgt_tx_phase_ctrl,
      mgt_tx_phase_stat_i     => onu_mgt_to_core_tx_phase_stat,

      -- Data
      mgt_tx_data_o => onu_core_to_mgt_tx_data,
      mgt_rx_data_i => onu_mgt_to_core_rx_data,
      ------------------------    

      -- data in/out --  -- see TX usr core interfacing / RX usr core just below for more information about how to interface the core
      tx_data_strobe_i => onu_tx_data_strobe_i,
      tx_data_ready_o  => onu_tx_data_ready_o,
      tx_data_i        => onu_tx_data_i,
      rx_data_strobe_o => onu_core_rx_data_strobe,
      rx_data_frame_o  => onu_rx_data_frame_o,
      -------------------------

      -- sfp data / control --
      sfp_rx_los_i   => onu_sfp_rx_los_i,
      sfp_tx_fault_i => onu_sfp_tx_fault_i,
      sfp_mod_abs_i  => onu_sfp_mod_abs_i,
      sfp_tx_sd_i    => onu_sfp_tx_sd_i,
      sfp_pdown_o    => onu_sfp_pdown_o,
      sfp_tx_dis_o   => onu_sfp_tx_dis_o,
      -------------------------

      -- i2c interfacing --
      i2c_req_o      => onu_core_to_i2c_master_i2c_req,
      i2c_rnw_o      => onu_core_to_i2c_master_i2c_rnw,
      i2c_rb2_o      => onu_core_to_i2c_master_i2c_rb2,
      i2c_slv_addr_o => onu_core_to_i2c_master_i2c_slv_addr,
      i2c_reg_addr_o => onu_core_to_i2c_master_i2c_reg_addr,
      i2c_wr_data_o  => onu_core_to_i2c_master_i2c_wr_data,

      i2c_done_i     => i2c_master_to_onu_core_i2c_done,
      i2c_error_i    => i2c_master_to_onu_core_i2c_error,
      i2c_drop_req_i => i2c_master_to_onu_core_i2c_drop_req,
      i2c_rd_data_i  => i2c_master_to_onu_core_i2c_rd_data
      -------------------------

      );

  --============================================================================
  -- Component instantiation
  --! Component clkdiv_phase_ctrl (generates fixed latency 40MHz aligned to detected comma)
  --============================================================================
  cmp_clkdiv_phase_ctrl : clkdiv_phase_ctrl
    generic map(
      -- g_NUM_PHASE_SHIFT_CYCLE is the number of fine phase shifts required to shift the clkdiv_o clock by one period of clk_i
      -- !!! This is device/configuration dependent !!!
      -- -> For ALTERA ARRIA10 IOPLL:
      -- Obs: VCO_PERIOD=1440 MHz, each phase shift is 1/8 of the VCO PERIOD (see Altera IOPLL phase shift and dynamic reconfiguration)
      -- The number of phase shifts needed to shift the clock by 1/240MHz period is given by:
      -- NUM_SHIFT = (1/240)/((1/1440)/8) = 48
      -- -> For Xilinx 7-Series MMCM:
      -- Obs: VCO_PERIOD=1200 MHz, each phase shift is 1/56 of the VCO PERIOD (see 7-series clocking resources - MMCM dynamic shift interface)
      -- The number of phase shifts needed to shift the clock by 1/240MHz period is given by:
      -- NUM_SHIFT = (1/240)/((1/1200)/56) = 280
      g_NUM_PHASE_SHIFT_CYCLE => 280,

      -- Number of consecutive correctly detected strobes in order to say phase is locked
      g_GOOD_STROBE_TO_LOCK => 6,

      -- Number of consecutive wrongly detected strobes in order to loose lock
      g_BAD_STROBE_TO_UNLOCK => 3
      )
    port map(
      clk_sys_i      => onu_clk_sys_i,
      reset_i        => reset_rx40,
      clk_i          => onu_mgt_clk_trxusr240,
      strobe_i       => onu_core_rx_data_strobe,
      clkdiv_o       => onu_clk_rx40_o,
      phase_locked_o => onu_rx40_locked_o
      );

  reset_rx40 <= not onu_core_rx_locked;

  --============================================================================
  -- Component instantiation
  --! Component onu_mgt_wrapper
  --============================================================================
  cmp_onu_mgt_wrapper : onu_mgt_wrapper
    port map(
      -- global input signals --                
      clk_sys_i      => onu_clk_sys_i,
      clk_rxref240_i => onu_clk_rxref240_i,
      clk_txref240_i => onu_clk_txref240_i,
      mgt_reset_i    => onu_core_to_mgt_reset,
      -------------------------

      -- global output signals --       
      clk_trxusr240_o => onu_mgt_clk_trxusr240,
      -------------------------

      -- status/control --
      tx_external_locked_i => onu_tx_external_locked_i,
      rx_slide_i           => onu_core_to_mgt_rxslide,

      rxfsmrstdone_o => onu_mgt_rx_ready,
      txfsmrstdone_o => onu_mgt_tx_ready,

      mgt_txpll_o => onu_mgt_txpll_lock,
      mgt_rxpll_o => onu_mgt_rxpll_lock,

      phase_good_o => onu_mgt_phase_good_o,
	  
      drp_wr_i                => onu_core_to_mgt_drp_wr,
      drp_wr_strobe_i         => onu_core_to_mgt_drp_wr_strobe,
      drp_monitor_o           => onu_mgt_to_core_drp_monitor,
      mgt_rx_equalizer_ctrl_i => onu_core_to_mgt_rx_equalizer_ctrl,
      mgt_rx_equalizer_stat_o => onu_mgt_to_core_rx_equalizer_stat,
      mgt_tx_phase_ctrl_i     => onu_core_to_mgt_tx_phase_ctrl,
      mgt_tx_phase_stat_o     => onu_mgt_to_core_tx_phase_stat,
      -------------------------            

      -- data in/out --
      tx_data_i => onu_core_to_mgt_tx_data,
      rx_data_o => onu_mgt_to_core_rx_data,
      rx_p_i    => onu_rx_p_i,
      rx_n_i    => onu_rx_n_i,
      tx_p_o    => onu_tx_p_o,
      tx_n_o    => onu_tx_n_o
      -------------------------            
      );


  --============================================================================
  -- Component instantiation
  --! Component i2c_interface (i2c_master)
  --============================================================================   
  cmp_i2c_interface : i2c_interface
    generic map(
      g_CLOCK_PERIOD => c_ONU_PERIOD_SYS_CLK,
      g_SCL_PERIOD   => c_ONU_SCL_PERIOD
      )
    port map(
      -- global input signals --
      clk_i   => onu_clk_sys_i,
      reset_i => onu_core_reset_i,
      -------------------------

      -- I2C transaction control/status signal --
      i2c_req_i      => onu_core_to_i2c_master_i2c_req,
      i2c_rnw_i      => onu_core_to_i2c_master_i2c_rnw,
      i2c_rb2_i      => onu_core_to_i2c_master_i2c_rb2,
      i2c_slv_addr_i => onu_core_to_i2c_master_i2c_slv_addr,
      i2c_reg_addr_i => onu_core_to_i2c_master_i2c_reg_addr,
      i2c_wr_data_i  => onu_core_to_i2c_master_i2c_wr_data,

      i2c_done_o     => i2c_master_to_onu_core_i2c_done,
      i2c_error_o    => i2c_master_to_onu_core_i2c_error,
      i2c_drop_req_o => i2c_master_to_onu_core_i2c_drop_req,
      i2c_rd_data_o  => i2c_master_to_onu_core_i2c_rd_data,
      -------------------------------------------

      -- I2C in/out signal ----
      i2c_sdi_i     => i2c_master_sdi,
      i2c_sdo_o     => i2c_master_sdo,
      i2c_sda_ena_o => i2c_master_sda_ena,
      i2c_scl_o     => i2c_master_scl,
      i2c_scl_ena_o => i2c_master_scl_ena
      -------------------------
      );

  ni2c_master_scl_ena <= not i2c_master_scl_ena;
  ni2c_master_sda_ena <= not i2c_master_sda_ena;
  -- IOBUF: Simple Bi-directional Buffer
  -- UltraScale
  -- Xilinx HDL Libraries Guide, version 2014.1
  IOBUF_inst : IOBUF
    port map (
      O  => i2c_master_sdi,             -- 1-bit output: Buffer output
      I  => i2c_master_sdo,             -- 1-bit input: Buffer input
      IO => onu_i2c_sda_io,  -- 1-bit inout: Buffer inout (connect directly to top-level port)
      T  => ni2c_master_sda_ena         -- 1-bit input: 3-state enable input
      );
  -- End of IOBUF_inst instantiation

  -- OBUFT: Simple 3-state Output Buffer
  -- UltraScale
  -- Xilinx HDL Libraries Guide, version 2014.1
  OBUFT_inst : OBUFT
    port map (
      O => onu_i2c_scl_io,  -- 1-bit output: Buffer output (connect directly to top-level port)
      I => i2c_master_scl,              -- 1-bit input: Buffer input
      T => ni2c_master_scl_ena          -- 1-bit input: 3-state enable input
      );
  -- End of OBUFT_inst instantiation    

  --============================================================================
  -- Component instantiation
  --! Component onu_control_axi_wrapper
  --============================================================================          
  cmp_onu_control_axi_wrapper : onu_control_axi_wrapper
    generic map(
      -- Users to add parameters here
      -- User parameters ends
      -- Do not modify the parameters beyond this line
      -- Parameters of Axi Slave Bus Interface ONU_AXI
      C_ONU_AXI_DATA_WIDTH => 32,
      C_ONU_AXI_ADDR_WIDTH => 9
      )
    port map(
      -- Users to add ports here
      stat_reg_i        => onu_stat_reg,
      ctrl_reg_o        => onu_ctrl_reg,
      ctrl_reg_strobe_o => onu_ctrl_reg_strobe,

      -- User ports ends
      -- Do not modify the ports beyond this line

      -- Ports of Axi Slave Bus Interface onu_AXI
      onu_axi_aclk    => s_onu_axi_aclk ,
      onu_axi_aresetn => s_onu_axi_aresetn ,
      onu_axi_awaddr  => s_onu_axi_awaddr ,
      onu_axi_awprot  => s_onu_axi_awprot ,
      onu_axi_awvalid => s_onu_axi_awvalid ,
      onu_axi_awready => s_onu_axi_awready ,
      onu_axi_wdata   => s_onu_axi_wdata ,
      onu_axi_wstrb   => s_onu_axi_wstrb ,
      onu_axi_wvalid  => s_onu_axi_wvalid ,
      onu_axi_wready  => s_onu_axi_wready ,
      onu_axi_bresp   => s_onu_axi_bresp ,
      onu_axi_bvalid  => s_onu_axi_bvalid ,
      onu_axi_bready  => s_onu_axi_bready ,
      onu_axi_araddr  => s_onu_axi_araddr ,
      onu_axi_arprot  => s_onu_axi_arprot ,
      onu_axi_arvalid => s_onu_axi_arvalid ,
      onu_axi_arready => s_onu_axi_arready ,
      onu_axi_rdata   => s_onu_axi_rdata ,
      onu_axi_rresp   => s_onu_axi_rresp ,
      onu_axi_rvalid  => s_onu_axi_rvalid ,
      onu_axi_rready  => s_onu_axi_rready
      );

  --============================================================================
  --Output
  --============================================================================  
  onu_clk_trxusr240_o  <= onu_mgt_clk_trxusr240;
  onu_rx_locked_o      <= onu_core_rx_locked;
  onu_rx_data_strobe_o <= onu_core_rx_data_strobe;

  onu_mgt_tx_ready_o   <= onu_mgt_tx_ready;
  onu_mgt_rx_ready_o   <= onu_mgt_rx_ready;
  onu_mgt_txpll_lock_o <= onu_mgt_txpll_lock;
  onu_mgt_rxpll_lock_o <= onu_mgt_rxpll_lock;
  
end structural;
