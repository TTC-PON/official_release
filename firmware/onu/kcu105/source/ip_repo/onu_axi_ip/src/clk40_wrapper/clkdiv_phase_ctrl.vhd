--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file clkdiv_phase_ctrl.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: clkdiv_phase_ctrl
--
--! @brief Generates divided clock aligned to strobe_i pulse relying on dynamic phase shift of Xilinx MMCM/DCM or Altera IOPLL
--!       
--! @author Created by Eduardo Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date -
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: EBSM
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 07\06\2017 - EBSM - created\n
--!
--! - Tested with 40MHz generation from a 240MHz clock with MMCM from Kintex7 and Kintex Ultrascale and with Altera IOPLL
--! - The generation of the MMCM/PLL ip has to be carefully done in order to properly select divider ratios
-------------------------------------------------------------------------------
--! @todo - \n
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for clkdiv_phase_ctrl
--============================================================================
entity clkdiv_phase_ctrl is
  generic(
    -- g_NUM_PHASE_SHIFT_CYCLE is the number of fine phase shifts required to shift the clkdiv_o clock by one period of clk_i
    -- !!! This is device/configuration dependent !!!
    -- -> For ALTERA ARRIA10 IOPLL:
    -- Obs: VCO_PERIOD=1440 MHz, each phase shift is 1/8 of the VCO PERIOD (see Altera IOPLL phase shift and dynamic reconfiguration)
    -- The number of phase shifts needed to shift the clock by 1/240MHz period is given by:
    -- NUM_SHIFT = (1/240)/((1/1440)/8) = 48
    -- -> For Xilinx 7-Series MMCM:
    -- Obs: VCO_PERIOD=1200 MHz, each phase shift is 1/56 of the VCO PERIOD (see 7-series clocking resources - MMCM dynamic shift interface)
    -- The number of phase shifts needed to shift the clock by 1/240MHz period is given by:
    -- NUM_SHIFT = (1/240)/((1/1200)/56) = 280
    g_NUM_PHASE_SHIFT_CYCLE : integer := 280;

    -- Number of consecutive correctly detected strobes in order to say phase is locked
    g_GOOD_STROBE_TO_LOCK : integer := 6;

    -- Number of consecutive wrongly detected strobes in order to loose lock
    g_BAD_STROBE_TO_UNLOCK : integer := 3
    );
  port (
    clk_sys_i      : in  std_logic;
    reset_i        : in  std_logic;
    clk_i          : in  std_logic;
    strobe_i       : in  std_logic;
    clkdiv_o       : out std_logic;
    phase_locked_o : out std_logic
    );
end clkdiv_phase_ctrl;

--============================================================================
--! Architecture declaration for clkdiv_phase_ctrl
--============================================================================
architecture rtl of clkdiv_phase_ctrl is

  --! Functions

  --! Constants
  --! Signal declaration
  attribute ASYNC_REG  : string;
  attribute MARK_DEBUG : string;

  signal reset_sync_meta                               : std_logic;
  signal reset_sync_r                                  : std_logic;
  attribute async_reg of reset_sync_meta, reset_sync_r : signal is "true";

  -- FSM to control alignment procedure
  type t_fsm_lock_phase is (
    IDLE,
    HUNT,
    GOING_LOCK,
    LOCK,
    RESET_STROBE_FLAG,
    GOING_HUNT,
    PHASE_SHIFT,
    PHASE_STEP,
    WAIT_PHASE_SHIFT
    );  

  signal fsm_lock_phase_state : t_fsm_lock_phase;

  -- Reset needed to initialize flag counters in the clkdiv_o domain
  signal reset_cntr_async : std_logic;

  signal reset_cntr_meta                                                             : std_logic;
  signal reset_cntr_r                                                                : std_logic;
  signal reset_cntr_r2                                                               : std_logic;
  signal reset_cntr_r3                                                               : std_logic;
  attribute async_reg of reset_cntr_meta, reset_cntr_r, reset_cntr_r2, reset_cntr_r3 : signal is "true";

  signal reset_cntr_ack_meta                                                                         : std_logic;
  signal reset_cntr_ack_r                                                                            : std_logic;
  signal reset_cntr_ack_r2                                                                           : std_logic;
  signal reset_cntr_ack_r3                                                                           : std_logic;
  attribute async_reg of reset_cntr_ack_meta, reset_cntr_ack_r, reset_cntr_ack_r2, reset_cntr_ack_r3 : signal is "true";
  attribute mark_debug of reset_cntr_ack_r3                                                          : signal is "true";

  -- Flags to indicate strobe checking
  signal strobe_sample : std_logic;

  signal strobe_good_cntr                         : integer range 0 to (g_GOOD_STROBE_TO_LOCK+1);
  signal strobe_good_cntr_flag_async              : std_logic;
  signal strobe_good_cntr_flag_meta               : std_logic;
  signal strobe_good_cntr_flag_r                  : std_logic;
  attribute mark_debug of strobe_good_cntr_flag_r : signal is "true";

  signal strobe_good_seen_flag_async              : std_logic;
  signal strobe_good_seen_flag_meta               : std_logic;
  signal strobe_good_seen_flag_r                  : std_logic;
  attribute mark_debug of strobe_good_seen_flag_r : signal is "true";

  signal strobe_bad_cntr                         : integer range 0 to (g_BAD_STROBE_TO_UNLOCK+1);
  signal strobe_bad_cntr_flag_async              : std_logic;
  signal strobe_bad_cntr_flag_meta               : std_logic;
  signal strobe_bad_cntr_flag_r                  : std_logic;
  attribute mark_debug of strobe_bad_cntr_flag_r : signal is "true";

  signal strobe_bad_seen_flag_async              : std_logic;
  signal strobe_bad_seen_flag_meta               : std_logic;
  signal strobe_bad_seen_flag_r                  : std_logic;
  attribute mark_debug of strobe_bad_seen_flag_r : signal is "true";

  signal phase_locked     : std_logic;
  -- Phase shift counter
  -- This counter is used to perform a number of fine phase shifts in order to shift clkdiv_o for one clk_i period
  signal phase_shift_cntr : integer range 0 to (g_NUM_PHASE_SHIFT_CYCLE+1);
  signal phase_shift_p    : std_logic;

  attribute mark_debug of phase_shift_cntr : signal is "true";
  attribute mark_debug of phase_shift_p    : signal is "true";

  signal phase_shift_done_async               : std_logic;
  signal phase_shift_done_meta                : std_logic;
  signal phase_shift_done_r                   : std_logic;
  signal phase_shift_done_r2                  : std_logic;
  attribute mark_debug of phase_shift_done_r2 : signal is "true";

  attribute async_reg of phase_shift_done_meta, phase_shift_done_r, phase_shift_done_r2 : signal is "true";

  signal freq_locked_async              : std_logic;
  signal freq_locked_meta               : std_logic;
  signal freq_locked_r                  : std_logic;
  attribute mark_debug of freq_locked_r : signal is "true";

  signal clkdiv : std_logic;

  --! Component declaration
  component rx_dcm
    port(
      CLK_IN1  : in  std_logic;         -- Clock in ports
      CLK_OUT1 : out std_logic;         -- Clock out ports
      PSCLK    : in  std_logic;
      PSEN     : in  std_logic;
      PSINCDEC : in  std_logic;         -- Status and control signals
      PSDONE   : out std_logic;
      RESET    : in  std_logic;
      LOCKED   : out std_logic
      );
  end component;

begin

  --============================================================================
  -- Process p_phase_lock_ctrl_fsm
  --! FSM that controls phase lock procedure
  --! read: clk_sys_i, reset_sync_r, freq_locked_r, reset_cntr_ack_r, strobe_bad_seen_flag_r\n 
  --!       strobe_good_seen_flag_r, strobe_bad_cntr_flag_r, strobe_good_cntr_flag_r   \n
  --!       phase_shift_done_r   \n  
  --! write: -\n
  --! r/w: fsm_lock_phase_state \n
  --============================================================================  
  p_phase_lock_ctrl_fsm : process(clk_sys_i)
  begin
    if(clk_sys_i'event and clk_sys_i = '1') then
      if(reset_sync_r = '1') then
        fsm_lock_phase_state <= IDLE;
      else
        case fsm_lock_phase_state is
          when IDLE =>
            if(freq_locked_r = '1' and reset_cntr_ack_r3 = '1') then
              fsm_lock_phase_state <= HUNT;
            end if;

          when HUNT =>
            if(strobe_bad_seen_flag_r = '1') then
              fsm_lock_phase_state <= PHASE_SHIFT;
            elsif(strobe_good_seen_flag_r = '1') then
              fsm_lock_phase_state <= GOING_LOCK;
            end if;

          when PHASE_SHIFT =>
            fsm_lock_phase_state <= WAIT_PHASE_SHIFT;

          when WAIT_PHASE_SHIFT =>
            if(phase_shift_done_r = '1' and phase_shift_done_r2 = '0') then  --if logic gets stuck here, a timeout should be foreseen
              if(phase_shift_cntr = 0) then
                fsm_lock_phase_state <= IDLE;
              else
                fsm_lock_phase_state <= PHASE_SHIFT;
              end if;
            end if;

          when GOING_LOCK =>
            if(strobe_bad_seen_flag_r = '1') then
              fsm_lock_phase_state <= IDLE;
            elsif(strobe_good_cntr_flag_r = '1') then
              fsm_lock_phase_state <= LOCK;
            end if;

          when LOCK =>
            if(strobe_bad_seen_flag_r = '1') then
              fsm_lock_phase_state <= RESET_STROBE_FLAG;
            end if;

          when RESET_STROBE_FLAG =>
            if(reset_cntr_ack_r3 = '1') then
              fsm_lock_phase_state <= GOING_HUNT;
            end if;

          when GOING_HUNT =>
            if(strobe_good_seen_flag_r = '1') then
              fsm_lock_phase_state <= LOCK;
            elsif(strobe_bad_cntr_flag_r = '1') then
              fsm_lock_phase_state <= IDLE;
            end if;

          when others =>
            fsm_lock_phase_state <= IDLE;

        end case;
      end if;
    end if;
  end process p_phase_lock_ctrl_fsm;

  --============================================================================
  -- Process p_phase_shift_cntr
  --! read:  -\n
  --! write: -\n
  --! r/w:   -\n
  --============================================================================  
  p_phase_shift_cntr : process(clk_sys_i)
  begin
    if(clk_sys_i'event and clk_sys_i = '1') then
      if(reset_sync_r = '1') then
        phase_shift_cntr <= g_NUM_PHASE_SHIFT_CYCLE;
        phase_shift_p    <= '0';
      else
        case fsm_lock_phase_state is
          when PHASE_SHIFT =>
            phase_shift_cntr <= phase_shift_cntr-1;
            phase_shift_p    <= '1';
          when WAIT_PHASE_SHIFT =>
            phase_shift_cntr <= phase_shift_cntr;
            phase_shift_p    <= '0';
          when others =>
            phase_shift_cntr <= g_NUM_PHASE_SHIFT_CYCLE;
            phase_shift_p    <= '0';
        end case;
      end if;
    end if;
  end process p_phase_shift_cntr;

  -- Reset strobe counters
  reset_cntr_async <= '1' when (fsm_lock_phase_state = IDLE or fsm_lock_phase_state = RESET_STROBE_FLAG) else '0';

  --============================================================================
  -- Process p_clkdiv_sync  
  --! read:  -\n  
  --! write: -\n  
  --! r/w:   -\n  
  --============================================================================  
  p_clkdiv_sync : process(clkdiv)
  begin
    if(clkdiv'event and clkdiv = '1') then
      reset_cntr_meta <= reset_cntr_async;
      reset_cntr_r    <= reset_cntr_meta;
      reset_cntr_r2   <= reset_cntr_r;
      reset_cntr_r3   <= reset_cntr_r2;
    end if;
  end process p_clkdiv_sync;

  --============================================================================
  -- Process p_sys_sync
  --! read:  -\n
  --! write: -\n
  --! r/w:   -\n
  --============================================================================  
  p_sys_sync : process(clk_sys_i)
  begin
    if(clk_sys_i'event and clk_sys_i = '1') then
      reset_sync_meta <= reset_i;
      reset_sync_r    <= reset_sync_meta;

      phase_shift_done_meta <= phase_shift_done_async;
      phase_shift_done_r    <= phase_shift_done_meta;

      reset_cntr_ack_meta <= reset_cntr_r3;
      reset_cntr_ack_r    <= reset_cntr_ack_meta;
      reset_cntr_ack_r2   <= reset_cntr_ack_r;
      reset_cntr_ack_r3   <= reset_cntr_ack_r2;

      strobe_good_seen_flag_meta <= strobe_good_seen_flag_async;
      strobe_good_seen_flag_r    <= strobe_good_seen_flag_meta;
      strobe_good_cntr_flag_meta <= strobe_good_cntr_flag_async;
      strobe_good_cntr_flag_r    <= strobe_good_cntr_flag_meta;
      strobe_bad_seen_flag_meta  <= strobe_bad_seen_flag_async;
      strobe_bad_seen_flag_r     <= strobe_bad_seen_flag_meta;
      strobe_bad_cntr_flag_meta  <= strobe_bad_cntr_flag_async;
      strobe_bad_cntr_flag_r     <= strobe_bad_cntr_flag_meta;

      freq_locked_meta <= freq_locked_async;
      freq_locked_r    <= freq_locked_meta;
    end if;
  end process p_sys_sync;

  --============================================================================
  -- Process p_strobe_sample
  --! read:  -\n  
  --! write: -\n  
  --! r/w:   -\n  
  --============================================================================    
  p_strobe_sample : process(clkdiv)
  begin
    if(clkdiv'event and clkdiv = '1') then
      strobe_sample <= strobe_i;
    end if;
  end process p_strobe_sample;

  --============================================================================
  -- Process p_strobe_flag
  --! read:  -\n  
  --! write: -\n  
  --! r/w:   -\n  
  --============================================================================  
  p_strobe_flag : process(clkdiv)
  begin
    if(clkdiv'event and clkdiv = '1') then
      if(reset_cntr_r = '1')then
        strobe_bad_seen_flag_async  <= '0';
        strobe_good_seen_flag_async <= '0';
        strobe_bad_cntr             <= 0;
        strobe_good_cntr            <= 0;
      else
        if(strobe_sample = '0') then
          strobe_bad_seen_flag_async <= '1';
          strobe_good_cntr           <= 0;
          if(strobe_bad_cntr         <= (g_BAD_STROBE_TO_UNLOCK)) then
            strobe_bad_cntr <= strobe_bad_cntr+1;
          end if;
        else
          strobe_good_seen_flag_async <= '1';
          strobe_bad_cntr             <= 0;
          if(strobe_good_cntr         <= (g_GOOD_STROBE_TO_LOCK)) then
            strobe_good_cntr <= strobe_good_cntr+1;
          end if;
        end if;
      end if;
    end if;
  end process p_strobe_flag;

  strobe_bad_cntr_flag_async  <= '1' when (strobe_bad_cntr >= g_BAD_STROBE_TO_UNLOCK) else '0';
  strobe_good_cntr_flag_async <= '1' when (strobe_good_cntr >= g_GOOD_STROBE_TO_LOCK) else '0';

  -- Component instantiation    
  cmp_rx_dcm : rx_dcm
    port map(
      CLK_IN1  => clk_i,                -- Clock in ports
      clk_out1 => clkdiv,               -- Clock out ports
      PSCLK    => clk_sys_i,            -- Dynamic phase shift ports
      PSEN     => phase_shift_p,
      PSINCDEC => '0',
      PSDONE   => phase_shift_done_async,
      RESET    => reset_sync_r,         -- Status and control signals
      LOCKED   => freq_locked_async
      );  

  clkdiv_o       <= clkdiv;
  phase_locked   <= '1'          when (fsm_lock_phase_state = LOCK or fsm_lock_phase_state = GOING_HUNT or fsm_lock_phase_state = RESET_STROBE_FLAG) else '0';
  phase_locked_o <= phase_locked when rising_edge(clk_sys_i);
  
end rtl;
