### ONU_USER-IP CORE CONSTRAINTS ###
set_false_path -to [get_pins -hier -filter {NAME =~ *rxsync_reg*/D}]

set_false_path -to [get_pins -hier -filter {NAME =~ *txsync_reg*/D}]

set_false_path -to [get_pins -hier -filter {NAME =~ *axisync_reg*/D}]

set_false_path -to [get_pins -hier -filter {NAME =~ *axi_rdata_reg*/D}]

#ONU address / static
set_false_path -from [get_pins -hier -filter {NAME =~ *onu_user_axi*/slv_reg_wr_aux_reg[0]*/C}]