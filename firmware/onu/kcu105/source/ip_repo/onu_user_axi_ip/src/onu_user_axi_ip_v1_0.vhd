--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_user_axi_ip_v1_0.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
library UNISIM;
use UNISIM.vcomponents.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU user AXI wrapper for Kintex Ultrascale (onu_user_axi_ip_v1_0)
--
--! @brief ONU user logic wrapper
--! <further description>
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 19/07/2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 19\07\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_user_axi_ip_v1_0
--============================================================================
entity onu_user_axi_ip_v1_0 is
  generic (
    -- Users to add parameters here

    -- User parameters ends
    -- Do not modify the parameters beyond this line

    -- Parameters of Axi Slave Bus Interface S_ONU_USER
    C_S_ONU_USER_DATA_WIDTH : integer := 32;
    C_S_ONU_USER_ADDR_WIDTH : integer := 9
    );
  port (

    -- Ports of Axi Slave Bus Interface S_ONU_USER
    s_onu_user_aclk    : in  std_logic;
    s_onu_user_aresetn : in  std_logic;
    s_onu_user_awaddr  : in  std_logic_vector(C_S_ONU_USER_ADDR_WIDTH-1 downto 0);
    s_onu_user_awprot  : in  std_logic_vector(2 downto 0);
    s_onu_user_awvalid : in  std_logic;
    s_onu_user_awready : out std_logic;
    s_onu_user_wdata   : in  std_logic_vector(C_S_ONU_USER_DATA_WIDTH-1 downto 0);
    s_onu_user_wstrb   : in  std_logic_vector((C_S_ONU_USER_DATA_WIDTH/8)-1 downto 0);
    s_onu_user_wvalid  : in  std_logic;
    s_onu_user_wready  : out std_logic;
    s_onu_user_bresp   : out std_logic_vector(1 downto 0);
    s_onu_user_bvalid  : out std_logic;
    s_onu_user_bready  : in  std_logic;
    s_onu_user_araddr  : in  std_logic_vector(C_S_ONU_USER_ADDR_WIDTH-1 downto 0);
    s_onu_user_arprot  : in  std_logic_vector(2 downto 0);
    s_onu_user_arvalid : in  std_logic;
    s_onu_user_arready : out std_logic;
    s_onu_user_rdata   : out std_logic_vector(C_S_ONU_USER_DATA_WIDTH-1 downto 0);
    s_onu_user_rresp   : out std_logic_vector(1 downto 0);
    s_onu_user_rvalid  : out std_logic;
    s_onu_user_rready  : in  std_logic;

    -- Users to add ports here
    -- Core connection --
    onu_clk_sys_i     : in  std_logic;
    onu_core_reset_o  : out std_logic;
    onu_clk_rx40_i    : in  std_logic;  --! Downstream recovered 40MHz clock  
    onu_rx40_locked_i : in  std_logic;

    onu_clk_trxusr240_i   : in std_logic;  --! User_Rx 240MHz clock
    onu_rx_data_strobe_i : in std_logic;
    onu_rx_data_frame_i  : in std_logic_vector(199 downto 0);
    onu_tx_data_ready_i  : in  std_logic;
    onu_tx_data_strobe_o : out std_logic;
    onu_tx_data_o        : out std_logic_vector(55 downto 0);

    onu_address_o             : out std_logic_vector(7 downto 0);  --! ONU address       
    onu_mgt_phase_good_i      : in  std_logic;  --! clk_rxusr240_o to clk_txusr120_o CDC phase scanning
    onu_rx_locked_i           : in  std_logic;  --! downstream header aligned
    onu_operational_i         : in  std_logic;  --! given by OLT once system init (calibration) is done
    onu_rx_serror_corrected_i : in  std_logic;  --! a single-error was corrected by the FEC
    onu_rx_derror_corrected_i : in  std_logic;  --! a double-error was corrected by the FEC
    onu_interrupt_i           : in  std_logic;
    onu_sticky_status_i       : in  std_logic_vector(30 downto 0);

    onu_mgt_tx_ready_i   : in std_logic;
    onu_mgt_rx_ready_i   : in std_logic;
    onu_mgt_txpll_lock_i : in std_logic;
    onu_mgt_rxpll_lock_i : in std_logic;
    ---------------------

    -- Others --
    rx_match_flag_o : out std_logic;

    onu_status_o : out std_logic_vector(7 downto 0);

    clk_rx40_oddr_o : out std_logic
    --------------------------------

    );
end onu_user_axi_ip_v1_0;

--============================================================================
--! Architecture declaration for onu_user_axi_ip_v1_0
--============================================================================
architecture structural of onu_user_axi_ip_v1_0 is

  -- ! Signal declaration
  -- Control
  signal slv_reg_ctrl : t_regbank32b(127 downto 0);
  -- Status
  signal slv_reg_stat : t_regbank32b(127 downto 0);

  -- component declaration
  component onu_user_axi_ip_v1_0_S_ONU_USER is
    generic (
      C_S_AXI_DATA_WIDTH : integer := 32;
      C_S_AXI_ADDR_WIDTH : integer := 9
      );
    port (

      -- Control
      slv_reg_ctrl_o : out t_regbank32b(127 downto 0);
      -- Status
      slv_reg_stat_i : in  t_regbank32b(127 downto 0);

      S_AXI_ACLK    : in  std_logic;
      S_AXI_ARESETN : in  std_logic;
      S_AXI_AWADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_AWPROT  : in  std_logic_vector(2 downto 0);
      S_AXI_AWVALID : in  std_logic;
      S_AXI_AWREADY : out std_logic;
      S_AXI_WDATA   : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      S_AXI_WSTRB   : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
      S_AXI_WVALID  : in  std_logic;
      S_AXI_WREADY  : out std_logic;
      S_AXI_BRESP   : out std_logic_vector(1 downto 0);
      S_AXI_BVALID  : out std_logic;
      S_AXI_BREADY  : in  std_logic;
      S_AXI_ARADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_ARPROT  : in  std_logic_vector(2 downto 0);
      S_AXI_ARVALID : in  std_logic;
      S_AXI_ARREADY : out std_logic;
      S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      S_AXI_RRESP   : out std_logic_vector(1 downto 0);
      S_AXI_RVALID  : out std_logic;
      S_AXI_RREADY  : in  std_logic
      );
  end component onu_user_axi_ip_v1_0_S_ONU_USER;

  component onu_user_logic_exdsg is
    port(
      -- Core connection --
      onu_clk_sys_i     : in  std_logic;
      onu_core_reset_o  : out std_logic;
      onu_clk_rx40_i    : in  std_logic;  --! Downstream recovered 40MHz clock  
      onu_rx40_locked_i : in  std_logic;

      onu_clk_trxusr240_i   : in std_logic;  --! User_Rx 240MHz clock
      onu_rx_data_strobe_i : in std_logic;
      onu_rx_data_frame_i  : in std_logic_vector(199 downto 0);
      onu_tx_data_ready_i  : in  std_logic;
      onu_tx_data_strobe_o : out std_logic;
      onu_tx_data_o        : out std_logic_vector(55 downto 0);

      onu_address_o             : out std_logic_vector(7 downto 0);  --! ONU address
      onu_mgt_phase_good_i      : in  std_logic;  --! clk_rxusr240_o to clk_txusr120_o CDC phase scanning
      onu_rx_locked_i           : in  std_logic;  --! downstream header aligned
      onu_operational_i         : in  std_logic;  --! given by OLT once system init (calibration) is done
      onu_rx_serror_corrected_i : in  std_logic;  --! a single-error was corrected by the FEC
      onu_rx_derror_corrected_i : in  std_logic;  --! a double-error was corrected by the FEC
      onu_interrupt_i           : in  std_logic;
      onu_sticky_status_i       : in  std_logic_vector(30 downto 0);

      onu_mgt_tx_ready_i   : in std_logic;
      onu_mgt_rx_ready_i   : in std_logic;
      onu_mgt_txpll_lock_i : in std_logic;
      onu_mgt_rxpll_lock_i : in std_logic;
      ---------------------

      -- Control AXI --
      s_onu_user_aclk : in  std_logic;
      ctrl_reg_i      : in  t_regbank32b(127 downto 0);
      stat_reg_o      : out t_regbank32b(127 downto 0);
      ---------------------------------

      -- Others --
      rx_match_flag_o : out std_logic;

      onu_status_o : out std_logic_vector(7 downto 0)
      --------------------------------
      );
  end component onu_user_logic_exdsg;


--============================================================================
--! Architecture begin for onu_user_axi_ip_v1_0
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component onu_user_axi_ip_v1_0_S_ONU_USER
  --============================================================================
  onu_user_axi_ip_v1_0_S_ONU_USER_inst : onu_user_axi_ip_v1_0_S_ONU_USER
    generic map (
      C_S_AXI_DATA_WIDTH => C_S_ONU_USER_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH => C_S_ONU_USER_ADDR_WIDTH
      )
    port map (
      -- Control
      slv_reg_ctrl_o => slv_reg_ctrl,
      -- Status
      slv_reg_stat_i => slv_reg_stat,
      --------------------------------

      S_AXI_ACLK    => s_onu_user_aclk,
      S_AXI_ARESETN => s_onu_user_aresetn,
      S_AXI_AWADDR  => s_onu_user_awaddr,
      S_AXI_AWPROT  => s_onu_user_awprot,
      S_AXI_AWVALID => s_onu_user_awvalid,
      S_AXI_AWREADY => s_onu_user_awready,
      S_AXI_WDATA   => s_onu_user_wdata,
      S_AXI_WSTRB   => s_onu_user_wstrb,
      S_AXI_WVALID  => s_onu_user_wvalid,
      S_AXI_WREADY  => s_onu_user_wready,
      S_AXI_BRESP   => s_onu_user_bresp,
      S_AXI_BVALID  => s_onu_user_bvalid,
      S_AXI_BREADY  => s_onu_user_bready,
      S_AXI_ARADDR  => s_onu_user_araddr,
      S_AXI_ARPROT  => s_onu_user_arprot,
      S_AXI_ARVALID => s_onu_user_arvalid,
      S_AXI_ARREADY => s_onu_user_arready,
      S_AXI_RDATA   => s_onu_user_rdata,
      S_AXI_RRESP   => s_onu_user_rresp,
      S_AXI_RVALID  => s_onu_user_rvalid,
      S_AXI_RREADY  => s_onu_user_rready
      );
  -- Add user logic here

  --============================================================================
  -- Component instantiation
  --! Component onu_user_logic_exdsg
  --============================================================================
  cmp_onu_user_logic_exdsg : onu_user_logic_exdsg
    port map(
      -- Core connection --
      onu_clk_sys_i     => onu_clk_sys_i ,
      onu_core_reset_o  => onu_core_reset_o ,
      onu_clk_rx40_i    => onu_clk_rx40_i ,
      onu_rx40_locked_i => onu_rx40_locked_i ,

      onu_clk_trxusr240_i   => onu_clk_trxusr240_i ,
      onu_rx_data_strobe_i => onu_rx_data_strobe_i ,
      onu_rx_data_frame_i  => onu_rx_data_frame_i ,
      onu_tx_data_ready_i  => onu_tx_data_ready_i ,
      onu_tx_data_strobe_o => onu_tx_data_strobe_o ,
      onu_tx_data_o        => onu_tx_data_o ,

      onu_address_o             => onu_address_o ,
      onu_mgt_phase_good_i      => onu_mgt_phase_good_i ,
      onu_rx_locked_i           => onu_rx_locked_i ,
      onu_operational_i         => onu_operational_i ,
      onu_rx_serror_corrected_i => onu_rx_serror_corrected_i,
      onu_rx_derror_corrected_i => onu_rx_derror_corrected_i,
      onu_interrupt_i           => onu_interrupt_i ,
      onu_sticky_status_i       => onu_sticky_status_i ,

      onu_mgt_tx_ready_i   => onu_mgt_tx_ready_i ,
      onu_mgt_rx_ready_i   => onu_mgt_rx_ready_i ,
      onu_mgt_txpll_lock_i => onu_mgt_txpll_lock_i ,
      onu_mgt_rxpll_lock_i => onu_mgt_rxpll_lock_i ,


      -- Control AXI --
      s_onu_user_aclk => s_onu_user_aclk ,
      ctrl_reg_i      => slv_reg_ctrl ,
      stat_reg_o      => slv_reg_stat ,
      -------------------                                              

      -- Others --                                  
      rx_match_flag_o => rx_match_flag_o ,
      onu_status_o    => onu_status_o
      --------------------
      );


  clk_rx40_ODDRE1_inst : ODDRE1
    generic map (
      IS_C_INVERTED  => '0',            -- Optional inversion for C
      IS_D1_INVERTED => '0',            -- Optional inversion for D1
      IS_D2_INVERTED => '0',            -- Optional inversion for D2
      SRVAL          => '0'  -- Initializes the ODDRE1 Flip-Flops to the specified value ('0', '1')
      )
    port map (
      Q  => clk_rx40_oddr_o,            -- 1-bit output: Data output to IOB
      C  => onu_clk_rx40_i,             -- 1-bit input: High-speed clock input
      D1 => '0',                        -- 1-bit input: Parallel data input 1
      D2 => '1',                        -- 1-bit input: Parallel data input 2
      SR => '0'                         -- 1-bit input: Active High Async Reset
      );     
  -- User logic ends

end structural;
