--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
--Date        : Fri Mar 13 10:23:11 2020
--Host        : pcepese47 running 64-bit major release  (build 9200)
--Command     : generate_target system_wrapper.bd
--Design      : system_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_wrapper is
  port (
    CLK_300MHZ_N : in STD_LOGIC;
    CLK_300MHZ_P : in STD_LOGIC;
    FMC_HPC_DP0_C2M_N : out STD_LOGIC;
    FMC_HPC_DP0_C2M_P : out STD_LOGIC;
    FMC_HPC_DP0_M2C_N : in STD_LOGIC;
    FMC_HPC_DP0_M2C_P : in STD_LOGIC;
    FMC_HPC_GBTCLK0_M2C_C_N : in STD_LOGIC;
    FMC_HPC_GBTCLK0_M2C_C_P : in STD_LOGIC;
    FMC_HPC_GBTCLK1_M2C_C_N : in STD_LOGIC;
    FMC_HPC_GBTCLK1_M2C_C_P : in STD_LOGIC;
    FMC_HPC_LA00_CC_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    FMC_HPC_LA00_CC_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    FMC_HPC_LA02_N : out STD_LOGIC;
    FMC_HPC_LA02_P : out STD_LOGIC;
    FMC_HPC_LA03_N : in STD_LOGIC;
    FMC_HPC_LA03_P : in STD_LOGIC;
    FMC_HPC_LA04_N : in STD_LOGIC;
    FMC_HPC_LA04_P : in STD_LOGIC;
    FMC_HPC_LA05_P : in STD_LOGIC;
    GPIO_LED : out STD_LOGIC_VECTOR ( 7 downto 0 );
    USER_SMA_GPIO_N : out STD_LOGIC;
    iic_fmc_gpo : out STD_LOGIC_VECTOR ( 1 downto 0 );
    iic_fmc_scl_io : inout STD_LOGIC;
    iic_fmc_sda_io : inout STD_LOGIC
  );
end system_wrapper;

architecture STRUCTURE of system_wrapper is
  component system is
  port (
    FMC_HPC_LA03_N : in STD_LOGIC;
    FMC_HPC_LA04_N : in STD_LOGIC;
    FMC_HPC_LA03_P : in STD_LOGIC;
    FMC_HPC_LA04_P : in STD_LOGIC;
    FMC_HPC_LA05_P : in STD_LOGIC;
    iic_fmc_sda_io : inout STD_LOGIC;
    iic_fmc_scl_io : inout STD_LOGIC;
    FMC_HPC_GBTCLK1_M2C_C_N : in STD_LOGIC;
    FMC_HPC_GBTCLK1_M2C_C_P : in STD_LOGIC;
    FMC_HPC_GBTCLK0_M2C_C_N : in STD_LOGIC;
    FMC_HPC_GBTCLK0_M2C_C_P : in STD_LOGIC;
    FMC_HPC_DP0_M2C_N : in STD_LOGIC;
    FMC_HPC_DP0_M2C_P : in STD_LOGIC;
    FMC_HPC_LA02_P : out STD_LOGIC;
    FMC_HPC_LA02_N : out STD_LOGIC;
    FMC_HPC_DP0_C2M_N : out STD_LOGIC;
    FMC_HPC_DP0_C2M_P : out STD_LOGIC;
    GPIO_LED : out STD_LOGIC_VECTOR ( 7 downto 0 );
    USER_SMA_GPIO_N : out STD_LOGIC;
    FMC_HPC_LA00_CC_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    FMC_HPC_LA00_CC_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    iic_fmc_gpo : out STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK_300MHZ_N : in STD_LOGIC;
    CLK_300MHZ_P : in STD_LOGIC
  );
  end component system;
begin
system_i: component system
     port map (
      CLK_300MHZ_N => CLK_300MHZ_N,
      CLK_300MHZ_P => CLK_300MHZ_P,
      FMC_HPC_DP0_C2M_N => FMC_HPC_DP0_C2M_N,
      FMC_HPC_DP0_C2M_P => FMC_HPC_DP0_C2M_P,
      FMC_HPC_DP0_M2C_N => FMC_HPC_DP0_M2C_N,
      FMC_HPC_DP0_M2C_P => FMC_HPC_DP0_M2C_P,
      FMC_HPC_GBTCLK0_M2C_C_N => FMC_HPC_GBTCLK0_M2C_C_N,
      FMC_HPC_GBTCLK0_M2C_C_P => FMC_HPC_GBTCLK0_M2C_C_P,
      FMC_HPC_GBTCLK1_M2C_C_N => FMC_HPC_GBTCLK1_M2C_C_N,
      FMC_HPC_GBTCLK1_M2C_C_P => FMC_HPC_GBTCLK1_M2C_C_P,
      FMC_HPC_LA00_CC_N(0) => FMC_HPC_LA00_CC_N(0),
      FMC_HPC_LA00_CC_P(0) => FMC_HPC_LA00_CC_P(0),
      FMC_HPC_LA02_N => FMC_HPC_LA02_N,
      FMC_HPC_LA02_P => FMC_HPC_LA02_P,
      FMC_HPC_LA03_N => FMC_HPC_LA03_N,
      FMC_HPC_LA03_P => FMC_HPC_LA03_P,
      FMC_HPC_LA04_N => FMC_HPC_LA04_N,
      FMC_HPC_LA04_P => FMC_HPC_LA04_P,
      FMC_HPC_LA05_P => FMC_HPC_LA05_P,
      GPIO_LED(7 downto 0) => GPIO_LED(7 downto 0),
      USER_SMA_GPIO_N => USER_SMA_GPIO_N,
      iic_fmc_gpo(1 downto 0) => iic_fmc_gpo(1 downto 0),
      iic_fmc_scl_io => iic_fmc_scl_io,
      iic_fmc_sda_io => iic_fmc_sda_io
    );
end STRUCTURE;
