# User logic
set_false_path -from [get_cells -compatibility_mode *onu_user_arria10*|slv_reg_ctrl*] -to [get_cells -compatibility_mode *cmp_onu_user_logic_exdsg|*rxsync*]
set_false_path -from [get_cells -compatibility_mode *onu_user_arria10*|slv_reg_ctrl*] -to [get_cells -compatibility_mode *cmp_onu_user_logic_exdsg|*txsync*]
set_false_path -to [get_cells -compatibility_mode *cmp_onu_user_logic_exdsg|*axisync[*]]
set_false_path -to [get_cells -compatibility_mode *onu_user_arria10*|*avalon_readdata*]

# ONU address / static
set_false_path -from [get_cells -compatibility_mode *onu_user_arria10*|slv_reg_ctrl[0][*]]