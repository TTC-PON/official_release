--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_user_arria10_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU user wrapper for Arria10 design (onu_user_arria10_wrapper)
--
--! @brief ONU user wrapper
--! Example ONU user logic top design
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 27\10\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 27\10\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo More comments \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_user_arria10_wrapper
--============================================================================
entity onu_user_arria10_wrapper is
  port (
    -- Ports of Avalon Slave Bus Interface S_ONU_USER
    avalon_clk_i         : in  std_logic;
    avalon_reset         : in  std_logic;
    avalon_address       : in  std_logic_vector(6 downto 0);
    avalon_byteenable    : in  std_logic_vector(3 downto 0);
    avalon_readdata      : out std_logic_vector(31 downto 0);
    avalon_writedata     : in  std_logic_vector(31 downto 0);
    avalon_read          : in  std_logic;
    avalon_write         : in  std_logic;
    avalon_readdatavalid : out std_logic;


    -- Users to add ports here
    -- Core connection --
    onu_clk_sys_i     : in  std_logic;
    onu_core_reset_o  : out std_logic;
    onu_clk_rx40_i    : in  std_logic;  --! Downstream recovered 40MHz clock  
    onu_rx40_locked_i : in  std_logic;

    onu_clk_trxusr240_i   : in std_logic;  --! User_Rx 240MHz clock
    onu_rx_data_strobe_i : in std_logic;
    onu_rx_data_frame_i  : in std_logic_vector(199 downto 0);
    onu_tx_data_ready_i  : in  std_logic;
    onu_tx_data_strobe_o : out std_logic;
    onu_tx_data_o        : out std_logic_vector(55 downto 0);

    onu_address_o             : out std_logic_vector(7 downto 0);  --! ONU address       
    onu_mgt_phase_good_i       : in  std_logic;  --! clk_rxusr240_o to clk_txusr120_o CDC phase scanning
    onu_rx_locked_i           : in  std_logic;  --! downstream header aligned
    onu_operational_i         : in  std_logic;  --! given by OLT once system init (calibration) is done
    onu_rx_serror_corrected_i : in  std_logic;  --! a single-error was corrected by the FEC
    onu_rx_derror_corrected_i : in  std_logic;  --! a double-error was corrected by the FEC
    onu_interrupt_i           : in  std_logic;  --! software configurable interruption
    onu_sticky_status_i       : in  std_logic_vector(30 downto 0);

    onu_mgt_tx_ready_i   : in std_logic;
    onu_mgt_rx_ready_i   : in std_logic;
    onu_mgt_txpll_lock_i : in std_logic;
    onu_mgt_rxpll_lock_i : in std_logic;
    ---------------------

    -- Others --
    rx_match_flag_o : out std_logic;
    onu_status_o    : out std_logic_vector(7 downto 0)
    --------------------------------

    );
end onu_user_arria10_wrapper;

--============================================================================
--! Architecture declaration for onu_user_arria10_wrapper
--============================================================================
architecture structural of onu_user_arria10_wrapper is

  -- ! Signal declaration
  -- Control / Status
  signal slv_reg_ctrl  : t_regbank32b(127 downto 0);
  signal avalon_read_r : std_logic;
  signal slv_reg_stat  : t_regbank32b(127 downto 0);

  -- component declaration
  component onu_user_logic_exdsg is
    port(
      -- Core connection --
      onu_clk_sys_i     : in  std_logic;
      onu_core_reset_o  : out std_logic;
      onu_clk_rx40_i    : in  std_logic;  --! Downstream recovered 40MHz clock  
      onu_rx40_locked_i : in  std_logic;

      onu_clk_trxusr240_i   : in std_logic;  --! User_Rx 240MHz clock
      onu_rx_data_strobe_i : in std_logic;
      onu_rx_data_frame_i  : in std_logic_vector(199 downto 0);
      onu_tx_data_ready_i  : in  std_logic;
      onu_tx_data_strobe_o : out std_logic;
      onu_tx_data_o        : out std_logic_vector(55 downto 0);

      onu_address_o             : out std_logic_vector(7 downto 0);  --! ONU address
      onu_mgt_phase_good_i          : in  std_logic;  --! clk_rxusr240_o to clk_txusr120_o CDC phase scanning
      onu_rx_locked_i           : in  std_logic;  --! downstream header aligned
      onu_operational_i         : in  std_logic;  --! given by OLT once system init (calibration) is done
      onu_rx_serror_corrected_i : in  std_logic;  --! a single-error was corrected by the FEC
      onu_rx_derror_corrected_i : in  std_logic;  --! a double-error was corrected by the FEC
      onu_interrupt_i           : in  std_logic;
      onu_sticky_status_i       : in  std_logic_vector(30 downto 0);

      onu_mgt_tx_ready_i   : in std_logic;
      onu_mgt_rx_ready_i   : in std_logic;
      onu_mgt_txpll_lock_i : in std_logic;
      onu_mgt_rxpll_lock_i : in std_logic;
      ---------------------

      -- Control AXI --
      s_onu_user_aclk : in  std_logic;
      ctrl_reg_i      : in  t_regbank32b(127 downto 0);
      stat_reg_o      : out t_regbank32b(127 downto 0);
      ---------------------------------

      -- Others --
      rx_match_flag_o : out std_logic;

      onu_status_o : out std_logic_vector(7 downto 0)
      --------------------------------
      );
  end component onu_user_logic_exdsg;

--============================================================================
--! Architecture begin for onu_user_arria10_wrapper
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component onu_user_logic_exdsg
  --============================================================================
  cmp_onu_user_logic_exdsg : onu_user_logic_exdsg
    port map(
      -- Core connection --
      onu_clk_sys_i     => onu_clk_sys_i ,
      onu_core_reset_o  => onu_core_reset_o ,
      onu_clk_rx40_i    => onu_clk_rx40_i ,
      onu_rx40_locked_i => onu_rx40_locked_i ,

      onu_clk_trxusr240_i   => onu_clk_trxusr240_i ,
      onu_rx_data_strobe_i => onu_rx_data_strobe_i ,
      onu_rx_data_frame_i  => onu_rx_data_frame_i ,
      onu_tx_data_ready_i  => onu_tx_data_ready_i ,
      onu_tx_data_strobe_o => onu_tx_data_strobe_o ,
      onu_tx_data_o        => onu_tx_data_o ,

      onu_address_o             => onu_address_o ,
      onu_mgt_phase_good_i          => onu_mgt_phase_good_i ,
      onu_rx_locked_i           => onu_rx_locked_i ,
      onu_operational_i         => onu_operational_i ,
      onu_rx_serror_corrected_i => onu_rx_serror_corrected_i,
      onu_rx_derror_corrected_i => onu_rx_derror_corrected_i,
      onu_interrupt_i           => onu_interrupt_i ,
      onu_sticky_status_i       => onu_sticky_status_i ,

      onu_mgt_tx_ready_i   => onu_mgt_tx_ready_i ,
      onu_mgt_rx_ready_i   => onu_mgt_rx_ready_i ,
      onu_mgt_txpll_lock_i => onu_mgt_txpll_lock_i ,
      onu_mgt_rxpll_lock_i => onu_mgt_rxpll_lock_i ,


      -- Control AXI --
      s_onu_user_aclk => avalon_clk_i ,
      ctrl_reg_i      => slv_reg_ctrl ,
      stat_reg_o      => slv_reg_stat ,
      -------------------                                              

      -- Others --                                  
      rx_match_flag_o => rx_match_flag_o ,
      onu_status_o    => onu_status_o
      --------------------
      );

  --============================================================================
  -- Process p_reg_rd_wr
  --! 
  --! read:  \n
  --! write: \n
  --============================================================================            
  p_reg_rd_wr : process (avalon_clk_i) is
  begin
    if avalon_clk_i'event and avalon_clk_i = '1' then
      if(avalon_reset = '1') then
        avalon_readdatavalid <= '0';
      else
        if(avalon_write = '1') then
          for i in avalon_byteenable'range loop
            if(avalon_byteenable(i) = '1') then
              slv_reg_ctrl(to_integer(unsigned(avalon_address(6 downto 0))))((i+1)*8-1 downto i*8) <= avalon_writedata((i+1)*8-1 downto i*8);
            end if;
          end loop;
        end if;

        avalon_read_r <= avalon_read;
        if(avalon_read = '1' and avalon_read_r = '0') then  --rising edge
          avalon_readdata      <= slv_reg_stat(to_integer(unsigned(avalon_address(6 downto 0))));
          avalon_readdatavalid <= '1';
        else
          avalon_readdatavalid <= '0';
        end if;
      end if;
    end if;
  end process p_reg_rd_wr;

end architecture structural;
