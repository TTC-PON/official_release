# MGT reset sync.
set_false_path -from [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|*r_reset] -to [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|gt0_rxfsmresetdone_r]
set_false_path -from [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|*r_reset] -to [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|gt0_rxfsmresetdone_r2]
set_false_path -from [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|*r_reset] -to [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|gt0_txfsmresetdone_r]
set_false_path -from [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|*r_reset] -to [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|gt0_txfsmresetdone_r2]

# Mailbox constraining
set_false_path -from [get_cells -compatibility_mode *cmp_onu_core|cmp_onu_control|*cmp_mailbox*|*data_a2b_togflag*] -to [get_cells -compatibility_mode *cmp_onu_core|cmp_onu_control|*cmp_mailbox*|*data_b_ready*]
set_false_path -from [get_cells -compatibility_mode *cmp_onu_core|cmp_onu_control|*cmp_mailbox*|*data_a_reg[*]] -to [get_cells -compatibility_mode *cmp_onu_core|cmp_onu_control|*cmp_mailbox*|*data_b_reg[*]]
set_false_path -from [get_cells -compatibility_mode *cmp_onu_core|cmp_onu_control|*onu_regbank*] -to [get_cells -compatibility_mode *pon_onu_arria10*|*avalon_readdata*]
set_false_path -from [get_cells -compatibility_mode *cmp_onu_core|cmp_onu_control|*_latched*] -to [get_cells -compatibility_mode *pon_onu_arria10*|*avalon_readdata*]

# ONU control
set_false_path -to [get_cells -compatibility_mode *cmp_onu_control|*trxsync_meta*]
set_false_path -to [get_cells -compatibility_mode *cmp_onu_control|*syssync_meta*]
set_false_path -to [get_cells -compatibility_mode *cmp_onu_control|*mngrsync_meta*]

# Latched scheme false paths
set_false_path -from [get_cells -compatibility_mode *cmp_onu_core|cmp_onu_control|rx_crc_errdetect_cntr_latched*]
set_false_path -from [get_cells -compatibility_mode *cmp_onu_core|cmp_onu_control|rx_serror_correc_cntr_latched*]
set_false_path -from [get_cells -compatibility_mode *cmp_onu_core|cmp_onu_control|rx_derror_correc_cntr_latched*]

# MGT reset synchronizer from ONU_RX
set_false_path -to [get_cells -compatibility_mode *cmp_onu_phy_control|reset_mgt_pipe[0]]

# MGT synchronizer for Rx to Tx CDC
set_false_path -to [get_cells -compatibility_mode *cmp_fifo_sync|flag_rd]
set_false_path -to [get_cells -compatibility_mode *cmp_fifo_sync|phase_good_locked_wr_meta]
set_false_path -from [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|*r_reset] -to [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|reset_fifo_meta]
set_false_path -from [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|*r_reset] -to [get_cells -compatibility_mode *cmp_onu_mgt_wrapper|reset_fifo_r]

# Pseudo-static refgen pattern selection
set_false_path -from {inst|onu_core_avalon_ip_0|cmp_onu_core|cmp_onu_control|mgt_rx_equalizer_ctrl_o[0]}
set_false_path -from {inst|onu_core_avalon_ip_0|cmp_onu_core|cmp_onu_control|mgt_rx_equalizer_ctrl_o[1]}

# Pseudo-static disable phase good
set_false_path -from {inst|onu_core_avalon_ip_0|cmp_onu_core|cmp_onu_control|mgt_rx_equalizer_ctrl_o[2]}