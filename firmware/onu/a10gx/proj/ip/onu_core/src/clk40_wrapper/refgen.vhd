--=============================================================================
--! @file refgen.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Reference generator (refgen)
--
--! @brief Generates 40MHz fixed latency with respect to strobe by using an ODDR
--  obs: this module was developed by Jozsef Imrek (mazsi) from the ALICE experiment (ALICE-CRU)
--       the original header can be found below:
--       -----------------------------------------------------------------------------
--        Title      : reference output generator
--        Project    : 
--       -----------------------------------------------------------------------------
--        File       : refgen.vhd
--        Author     : jozsef imrek <jozsef.imrek@cern.ch>
--        Company    : 
--        Created    : 2017-02-06
--        Last update: 2018-08-20
--        Platform   : 
--        Standard   : VHDL'93/02
--       -----------------------------------------------------------------------------
--        Description: generates a 40 MHz / 120 MHz / 240 MHz output clock or forwards
--        an input data bit on an FPGA PIN from a 240 MHz input clock.  phase of the
--        output is controlled by the (optional) SYNC input.
--       -----------------------------------------------------------------------------
--        Copyright (c) 2017
--       -----------------------------------------------------------------------------
--        Revisions  :
--        Date        Author  Description
--        2017-02-06  mazsi   Created
--        2017-04-21  ebsm    removed cru_misc functions for ttc-pon refdsg impl.
--       -----------------------------------------------------------------------------
--! @author Jozsef Imrek (mazsi)
--! @date 06\02\2017
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo More comments \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for refgen
--============================================================================
entity refgen is

  port (
    ---------------------------------------------------------------------------
    CLK240 : in  std_logic;
    RST    : in  std_logic                    := '0';
    ---------------------------------------------------------------------------
    MODE   : in  std_logic_vector(1 downto 0) := "00";
    ERRCNT : out std_logic_vector(31 downto 0);
    ---------------------------------------------------------------------------
    LOCKED : in  std_logic                    := '1';
    D      : in  std_logic                    := '1';
    SYNC   : in  std_logic                    := '0';
    Q      : out std_logic
    ---------------------------------------------------------------------------
    );

end entity refgen;

--============================================================================
--! Architecture declaration for refgen
--============================================================================
architecture imp of refgen is

  signal pattern : std_logic_vector(11 downto 0);

  signal phase   : unsigned(2 downto 0);
  signal termval : std_logic;
  signal errcnti : unsigned(31 downto 0);

  signal ddrdata, ddrgated : std_logic_vector(1 downto 0);

  component ODDR_PON is
    port(
      ck      : in  std_logic;
      din     : in  std_logic_vector(1 downto 0);
      pad_out : out std_logic_vector(0 downto 0)
      );
  end component ODDR_PON;

--============================================================================
--! Architecture begin for refgen
--============================================================================
begin

  -----------------------------------------------------------------------------
  -- select output pattern based on mode
  -----------------------------------------------------------------------------

  process (CLK240) is
  begin
    if CLK240'event and CLK240 = '1' then
      case MODE is
        when "00"   => pattern <= "111111000000";  -- 40 MHz clk
        when "01"   => pattern <= "110011001100";  -- 120 MHz clk
        when "10"   => pattern <= "101010101010";  -- 240 MHz clk
        when others => pattern <= (others => D);   -- input data
      end case;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- counter that tracks phase of the "40 MHz", termval indicates phase = 5.
  -- also, record the number of times the counter was not in sync with SYNC.
  -----------------------------------------------------------------------------

  termval <= '1' when phase = "101" else '0';

  process (CLK240) is
  begin
    if CLK240'event and CLK240 = '1' then
      if RST = '1' or SYNC = '1' or termval = '1' then
        phase <= (others => '0');
      else
        phase <= phase + 1;
      end if;
    end if;
  end process;


  process (CLK240) is
  begin
    if CLK240'event and CLK240 = '1' then
      if RST = '1' then
        errcnti <= (others => '0');
      elsif SYNC /= termval then
        errcnti <= errcnti + 1;
      end if;
    end if;
  end process;

  ERRCNT <= std_logic_vector(errcnti);

  -----------------------------------------------------------------------------
  -- select ODDR input from pattern, based on phase. (output MSB first)
  -----------------------------------------------------------------------------

  process (CLK240) is
  begin
    if CLK240'event and CLK240 = '1' then
      case phase is
        when "000"  => ddrdata <= pattern(11 downto 10);
        when "001"  => ddrdata <= pattern(9 downto 8);
        when "010"  => ddrdata <= pattern(7 downto 6);
        when "011"  => ddrdata <= pattern(5 downto 4);
        when "100"  => ddrdata <= pattern(3 downto 2);
        when others => ddrdata <= pattern(1 downto 0);
      end case;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- gate output data with LOCKED
  -----------------------------------------------------------------------------

  ddrgated <= ddrdata and (LOCKED & LOCKED) when rising_edge(CLK240);

  -----------------------------------------------------------------------------
  -- instantiate ODDR
  -----------------------------------------------------------------------------

  refoddr : ODDR_PON
    port map (
      ck         => CLK240,
      din        => ddrgated,
      pad_out(0) => Q
      );


end architecture imp;

