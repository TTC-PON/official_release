--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file pon_onu_arria10_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.pon_onu_package_static.all;
use work.pon_onu_package_modifiable.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU wrapper for Arria10 design (pon_onu_arria10_wrapper)
--
--! @brief ONU top design for TTC-PON
--! Example ONU top design
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 27\10\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 27\10\2016 - EBSM - Created\n
--! 25\10\2017 - EBSM - rev0_1_0\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo More comments \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for pon_onu_arria10_wrapper
--============================================================================
entity pon_onu_arria10_wrapper is
  port (
    -- reconfig interface for XCVR and PLL --
    plls0_clk           : in  std_logic                     := '0';
    plls0_reset         : in  std_logic                     := '0';
    plls0_waitrequest   : out std_logic;
    plls0_address       : in  std_logic_vector(9 downto 0)  := (others => '0');
    plls0_write         : in  std_logic                     := '0';
    plls0_writedata     : in  std_logic_vector(31 downto 0) := (others => '0');
    plls0_read          : in  std_logic                     := '0';
    plls0_readdata      : out std_logic_vector(31 downto 0);
    plls0_readdatavalid : out std_logic;

    txs0_clk           : in  std_logic                     := '0';
    txs0_reset         : in  std_logic                     := '0';
    txs0_waitrequest   : out std_logic;
    txs0_address       : in  std_logic_vector(9 downto 0)  := (others => '0');
    txs0_write         : in  std_logic                     := '0';
    txs0_writedata     : in  std_logic_vector(31 downto 0) := (others => '0');
    txs0_read          : in  std_logic                     := '0';
    txs0_readdata      : out std_logic_vector(31 downto 0);
    txs0_readdatavalid : out std_logic;
    -------------------------

    -- Ports of Avalon Slave Bus Interface S_ONU_CORE
    avalon_clk_i         : in  std_logic;
    avalon_reset         : in  std_logic;
    avalon_address       : in  std_logic_vector(6 downto 0);
    avalon_byteenable    : in  std_logic_vector(3 downto 0);
    avalon_readdata      : out std_logic_vector(31 downto 0);
    avalon_writedata     : in  std_logic_vector(31 downto 0);
    avalon_read          : in  std_logic;
    avalon_write         : in  std_logic;
    avalon_readdatavalid : out std_logic;

    -- Clk sys/reset                
    onu_clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
    onu_core_reset_i : in std_logic;

    -- MGT ref clocks         
    onu_clk_rxref240_i       : in std_logic;  --! MGT Rx reference clock - 240MHz      
    onu_clk_txref240_i       : in std_logic;  --! MGT Tx reference clock - 240MHz (has to be derived from downstream recovered clock - synchronization required for TTC-PON)        
    onu_tx_external_locked_i : in std_logic;  --! External PLL for Tx clock is locked

    -- Recovered downstream 40MHz clock
    onu_clk_rx40_o    : out std_logic;
    onu_rx40_locked_o : out std_logic;

    -- Data in/out
    onu_clk_trxusr240_o  : out std_logic;  --! User_TRx 240MHz clock
    onu_rx_data_strobe_o : out std_logic;
    onu_rx_data_frame_o  : out std_logic_vector(199 downto 0);
    onu_tx_data_strobe_i : in  std_logic;
    onu_tx_data_ready_o  : out std_logic;
    onu_tx_data_i        : in  std_logic_vector(55 downto 0);
    onu_address_i        : in  std_logic_vector(7 downto 0);  --! ONU address

    -- Status
    onu_mgt_phase_good_o      : out std_logic;  --! Internal MGT Rx-Tx CLK CDC phase scanning       
    onu_rx_locked_o           : out std_logic;  --! downstream header aligned
    onu_operational_o         : out std_logic;  --! given by OLT once system init (calibration) is done
    onu_rx_serror_corrected_o : out std_logic;  --! a single-error was corrected by the FEC
    onu_rx_derror_corrected_o : out std_logic;  --! a double-error was corrected by the FEC
    onu_mgt_tx_ready_o        : out std_logic;
    onu_mgt_rx_ready_o        : out std_logic;
    onu_mgt_txpll_lock_o      : out std_logic;
    onu_mgt_rxpll_lock_o      : out std_logic;

    -- Soft-configurable interrupt output
    onu_interrupt_o     : out std_logic;
    onu_status_sticky_o : out std_logic_vector(30 downto 0);
    ------------------------      

    -- serial
    onu_tx_p_o : out std_logic;
    onu_rx_p_i : in  std_logic;

    onu_sfp_rx_los_i   : in  std_logic;
    onu_sfp_tx_fault_i : in  std_logic;
    onu_sfp_mod_abs_i  : in  std_logic;
    onu_sfp_tx_sd_i    : in  std_logic;
    onu_sfp_pdown_o    : out std_logic;
    onu_sfp_tx_dis_o   : out std_logic;

    --i2c interface
    onu_i2c_sda_io : inout std_logic;
    onu_i2c_scl_io : inout std_logic;
    -------------------------

    -- specific A10
    a10_rx_set_locktodata_i  : in  std_logic := '0';
    a10_rx_set_locktoref_i   : in  std_logic := '0'
    -------------------------	
    );
end entity pon_onu_arria10_wrapper;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture structural of pon_onu_arria10_wrapper is

  --! Functions

  --! Constants

  --! Signal declaration
  signal avalon_read_r       : std_logic;
  signal onu_stat_reg        : t_regbank32b(127 downto 0);
  signal onu_ctrl_reg        : std_logic_vector(31 downto 0);
  signal onu_ctrl_reg_strobe : std_logic_vector(127 downto 0);

  -- global from onu_core              
  signal onu_core_rx_data_strobe : std_logic;
  signal onu_core_rx_locked      : std_logic;

  -- global from mgt                   
  signal onu_mgt_clk_trxusr240 : std_logic;
  signal onu_mgt_tx_ready      : std_logic;
  signal onu_mgt_rx_ready      : std_logic;
  signal onu_mgt_txpll_lock    : std_logic;
  signal onu_mgt_rxpll_lock    : std_logic;

  -- onu_core <-> onu_mgt_wrapper      
  signal onu_core_to_mgt_reset   : std_logic;
  signal onu_core_to_mgt_rxslide : std_logic;
  signal onu_core_to_mgt_tx_data : std_logic_vector(9 downto 0);
  signal onu_mgt_to_core_rx_data : std_logic_vector(39 downto 0);

  signal onu_core_to_mgt_drp_wr            : std_logic_vector(31 downto 0);
  signal onu_core_to_mgt_drp_wr_strobe     : std_logic;
  signal onu_mgt_to_core_drp_monitor       : std_logic_vector(31 downto 0);
  signal onu_core_to_mgt_rx_equalizer_ctrl : std_logic_vector(31 downto 0);
  signal onu_mgt_to_core_rx_equalizer_stat : std_logic_vector(31 downto 0);
  signal onu_core_to_mgt_tx_phase_ctrl     : std_logic_vector(31 downto 0);
  signal onu_mgt_to_core_tx_phase_stat     : std_logic_vector(31 downto 0);

  -- onu_core <-> i2c_master
  signal onu_core_to_i2c_master_i2c_req      : std_logic;
  signal onu_core_to_i2c_master_i2c_rnw      : std_logic;
  signal onu_core_to_i2c_master_i2c_rb2      : std_logic;
  signal onu_core_to_i2c_master_i2c_slv_addr : std_logic_vector(6 downto 0);
  signal onu_core_to_i2c_master_i2c_reg_addr : std_logic_vector(7 downto 0);
  signal onu_core_to_i2c_master_i2c_wr_data  : std_logic_vector(7 downto 0);

  signal i2c_master_to_onu_core_i2c_done     : std_logic;
  signal i2c_master_to_onu_core_i2c_error    : std_logic;
  signal i2c_master_to_onu_core_i2c_drop_req : std_logic;
  signal i2c_master_to_onu_core_i2c_rd_data  : std_logic_vector(15 downto 0);

  -- i2c_master
  signal i2c_master_sdi     : std_logic;
  signal i2c_master_sdo     : std_logic;
  signal i2c_master_sda_ena : std_logic;
  signal i2c_master_scl_ena : std_logic;
  signal i2c_master_scl     : std_logic;

  --! Component declaration
  component onu_core is                 -- use work.pon_onu_package.all;
    port (
      -- global input signals --
      clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
      core_reset_i : in std_logic;
      -------------------------

      -- manager control/stat -- see manager core interfacing just below for more information about how to interface the core control
      clk_manager_i         : in  std_logic;
      manager_stat_reg_o    : out t_regbank32b(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
      manager_ctrl_reg_i    : in  std_logic_vector(31 downto 0);
      manager_ctrl_strobe_i : in  std_logic_vector(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
      --------------------------

      -- status / control --
      onu_address_i         : in  std_logic_vector(7 downto 0);  --! ONU address in TDM
      rx_locked_o           : out std_logic;  --! downstream header aligned
      onu_operational_o     : out std_logic;  --! given by onu once system init (calibration) is done; this means that ONU is allowed to transmit
      rx_serror_corrected_o : out std_logic;  --! a single-error was corrected by the FEC
      rx_derror_corrected_o : out std_logic;  --! a double-error was corrected by the FEC

      -- Soft-configurable interrupt output
      interrupt_o     : out std_logic;
      status_sticky_o : out std_logic_vector(30 downto 0);
      -------------------------             

      -- MGT connections -----------
      -- Clocks
      clk_trxusr240_i : in std_logic;    --! User_TRx 240MHz clock  

      -- Status/Ctrl
      mgt_reset_o      : out std_logic;
      mgt_tx_ready_i   : in  std_logic;
      mgt_rx_ready_i   : in  std_logic;
      mgt_txpll_lock_i : in  std_logic;
      mgt_rxpll_lock_i : in  std_logic;
      mgt_rxslide_o    : out std_logic;

      -- Xilinx specific
      mgt_drp_wr_o            : out std_logic_vector(31 downto 0);
      mgt_drp_wr_strobe_o     : out std_logic;
      mgt_drp_monitor_i       : in  std_logic_vector(31 downto 0);
      mgt_rx_equalizer_ctrl_o : out std_logic_vector(31 downto 0);
      mgt_rx_equalizer_stat_i : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_o     : out std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_i     : in  std_logic_vector(31 downto 0);

      -- Data
      mgt_tx_data_o : out std_logic_vector(c_ONU_TX_MGT_DATA_WIDTH-1 downto 0);
      mgt_rx_data_i : in  std_logic_vector(c_ONU_RX_MGT_DATA_WIDTH-1 downto 0);
      ------------------------      

      -- data in/out --  -- see TX usr core interfacing / RX usr core just below for more information about how to interface the core
      tx_data_strobe_i : in  std_logic;
      tx_data_ready_o  : out std_logic;
      tx_data_i        : in  std_logic_vector(c_ONU_TX_USR_DATA_WIDTH-1 downto 0);
      rx_data_strobe_o : out std_logic;
      rx_data_frame_o  : out std_logic_vector(c_ONU_RX_USR_DATA_WIDTH-1 downto 0);
      -------------------------

      -- sfp data / control --
      sfp_rx_los_i   : in  std_logic;
      sfp_tx_fault_i : in  std_logic;
      sfp_mod_abs_i  : in  std_logic;
      sfp_tx_sd_i    : in  std_logic;
      sfp_pdown_o    : out std_logic;
      sfp_tx_dis_o   : out std_logic;
      -------------------------

      --i2c interface
      i2c_req_o      : out std_logic;
      i2c_rnw_o      : out std_logic;
      i2c_rb2_o      : out std_logic;
      i2c_slv_addr_o : out std_logic_vector(6 downto 0);
      i2c_reg_addr_o : out std_logic_vector(7 downto 0);
      i2c_wr_data_o  : out std_logic_vector(7 downto 0);

      i2c_done_i     : in std_logic;
      i2c_error_i    : in std_logic;
      i2c_drop_req_i : in std_logic;
      i2c_rd_data_i  : in std_logic_vector(15 downto 0)
      -------------------------

      );
  end component onu_core;

  component refgen is
    port (
      ---------------------------------------------------------------------------
      CLK240 : in  std_logic;
      RST    : in  std_logic                    := '0';
      ---------------------------------------------------------------------------
      MODE   : in  std_logic_vector(1 downto 0) := "00";
      ERRCNT : out std_logic_vector(31 downto 0);
      ---------------------------------------------------------------------------
      LOCKED : in  std_logic                    := '1';
      D      : in  std_logic                    := '1';
      SYNC   : in  std_logic                    := '0';
      Q      : out std_logic
      ---------------------------------------------------------------------------
      );
  end component refgen;

  component onu_mgt_wrapper is
    port (
      -- reconfig interface --
      plls0_clk           : in  std_logic                     := '0';
      plls0_reset         : in  std_logic                     := '0';
      plls0_waitrequest   : out std_logic;
      plls0_address       : in  std_logic_vector(9 downto 0)  := (others => '0');
      plls0_write         : in  std_logic                     := '0';
      plls0_writedata     : in  std_logic_vector(31 downto 0) := (others => '0');
      plls0_read          : in  std_logic                     := '0';
      plls0_readdata      : out std_logic_vector(31 downto 0);
      plls0_readdatavalid : out std_logic;

      txs0_clk           : in  std_logic                     := '0';
      txs0_reset         : in  std_logic                     := '0';
      txs0_waitrequest   : out std_logic;
      txs0_address       : in  std_logic_vector(9 downto 0)  := (others => '0');
      txs0_write         : in  std_logic                     := '0';
      txs0_writedata     : in  std_logic_vector(31 downto 0) := (others => '0');
      txs0_read          : in  std_logic                     := '0';
      txs0_readdata      : out std_logic_vector(31 downto 0);
      txs0_readdatavalid : out std_logic;
      -------------------------

      -- global input signals --                
      clk_sys_i      : in std_logic;
      clk_rxref240_i : in std_logic;
      clk_txref240_i : in std_logic;
      mgt_reset_i    : in std_logic;
      -------------------------

      -- global output signals --       
      clk_trxusr240_o : out std_logic;
      -------------------------

      -- status/control --
      tx_external_locked_i : in  std_logic;
      rx_slide_i           : in  std_logic;
      rxfsmrstdone_o       : out std_logic;
      txfsmrstdone_o       : out std_logic;
      mgt_txpll_o          : out std_logic;
      mgt_rxpll_o          : out std_logic;
      phase_good_o         : out std_logic;
      disable_phase_good_i : in  std_logic;
	
      -- specific A10
      rx_set_locktodata_i  : in  std_logic;
      rx_set_locktoref_i   : in  std_logic;
      -------------------------            

      -- data in/out --         
      tx_data_i : in  std_logic_vector(9 downto 0);
      rx_data_o : out std_logic_vector(39 downto 0);
      rx_p_i    : in  std_logic;
      rx_n_i    : in  std_logic;
      tx_p_o    : out std_logic;
      tx_n_o    : out std_logic
      -------------------------            
      );
  end component onu_mgt_wrapper;

  component i2c_interface is
    generic (
      g_CLOCK_PERIOD : integer range 5 to 20 := 10;    -- clock period in ns
      g_SCL_PERIOD   : integer               := 10000  -- SCL period in ns
      );
    port (
      -- global input signals --
      clk_i   : in std_logic;
      reset_i : in std_logic;
      -------------------------

      -- I2C transaction control/status signal --
      i2c_req_i      : in std_logic;
      i2c_rnw_i      : in std_logic;
      i2c_rb2_i      : in std_logic;
      i2c_slv_addr_i : in std_logic_vector(6 downto 0);
      i2c_reg_addr_i : in std_logic_vector(7 downto 0);
      i2c_wr_data_i  : in std_logic_vector(7 downto 0);

      i2c_done_o     : out std_logic;
      i2c_error_o    : out std_logic;
      i2c_drop_req_o : out std_logic;
      i2c_rd_data_o  : out std_logic_vector(15 downto 0);
      -------------------------------------------

      -- I2C in/out signal ----
      i2c_sdi_i     : in  std_logic;
      i2c_sdo_o     : out std_logic;
      i2c_sda_ena_o : out std_logic;
      i2c_scl_o     : out std_logic;
      i2c_scl_ena_o : out std_logic
      -------------------------
      );

  end component i2c_interface;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component onu_core
  --============================================================================
  cmp_onu_core : onu_core
    port map(
      -- global input signals --
      clk_sys_i    => onu_clk_sys_i,
      core_reset_i => onu_core_reset_i,
      -------------------------

      -- manager control/stat
      clk_manager_i         => avalon_clk_i,
      manager_stat_reg_o    => onu_stat_reg,
      manager_ctrl_reg_i    => onu_ctrl_reg,
      manager_ctrl_strobe_i => onu_ctrl_reg_strobe,
      --------------------------

      -- status / control --
      onu_address_i         => onu_address_i,
      rx_locked_o           => onu_core_rx_locked,
      onu_operational_o     => onu_operational_o,
      rx_serror_corrected_o => onu_rx_serror_corrected_o,
      rx_derror_corrected_o => onu_rx_derror_corrected_o,
      interrupt_o           => onu_interrupt_o,
      status_sticky_o       => onu_status_sticky_o,
      -------------------------   

      -- MGT connections -----------
      -- Clocks
      clk_trxusr240_i => onu_mgt_clk_trxusr240,

      -- Status/Ctrl
      mgt_reset_o      => onu_core_to_mgt_reset,
      mgt_tx_ready_i   => onu_mgt_tx_ready,
      mgt_rx_ready_i   => onu_mgt_rx_ready,
      mgt_txpll_lock_i => onu_mgt_txpll_lock,
      mgt_rxpll_lock_i => onu_mgt_rxpll_lock,
      mgt_rxslide_o    => onu_core_to_mgt_rxslide,

      mgt_drp_wr_o            => onu_core_to_mgt_drp_wr,
      mgt_drp_wr_strobe_o     => onu_core_to_mgt_drp_wr_strobe,
      mgt_drp_monitor_i       => onu_mgt_to_core_drp_monitor,
      mgt_rx_equalizer_ctrl_o => onu_core_to_mgt_rx_equalizer_ctrl,
      mgt_rx_equalizer_stat_i => onu_mgt_to_core_rx_equalizer_stat,
      mgt_tx_phase_ctrl_o     => onu_core_to_mgt_tx_phase_ctrl,
      mgt_tx_phase_stat_i     => onu_mgt_to_core_tx_phase_stat,

      -- Data
      mgt_tx_data_o => onu_core_to_mgt_tx_data,
      mgt_rx_data_i => onu_mgt_to_core_rx_data,
      ------------------------    

      -- data in/out --  -- see TX usr core interfacing / RX usr core just below for more information about how to interface the core
      tx_data_strobe_i => onu_tx_data_strobe_i,
      tx_data_ready_o  => onu_tx_data_ready_o,
      tx_data_i        => onu_tx_data_i,
      rx_data_strobe_o => onu_core_rx_data_strobe,
      rx_data_frame_o  => onu_rx_data_frame_o,
      -------------------------

      -- sfp data / control --
      sfp_rx_los_i   => onu_sfp_rx_los_i,
      sfp_tx_fault_i => onu_sfp_tx_fault_i,
      sfp_mod_abs_i  => onu_sfp_mod_abs_i,
      sfp_tx_sd_i    => onu_sfp_tx_sd_i,
      sfp_pdown_o    => onu_sfp_pdown_o,
      sfp_tx_dis_o   => onu_sfp_tx_dis_o,
      -------------------------

      -- i2c interfacing --
      i2c_req_o      => onu_core_to_i2c_master_i2c_req,
      i2c_rnw_o      => onu_core_to_i2c_master_i2c_rnw,
      i2c_rb2_o      => onu_core_to_i2c_master_i2c_rb2,
      i2c_slv_addr_o => onu_core_to_i2c_master_i2c_slv_addr,
      i2c_reg_addr_o => onu_core_to_i2c_master_i2c_reg_addr,
      i2c_wr_data_o  => onu_core_to_i2c_master_i2c_wr_data,

      i2c_done_i     => i2c_master_to_onu_core_i2c_done,
      i2c_error_i    => i2c_master_to_onu_core_i2c_error,
      i2c_drop_req_i => i2c_master_to_onu_core_i2c_drop_req,
      i2c_rd_data_i  => i2c_master_to_onu_core_i2c_rd_data
      -------------------------

      );

  onu_mgt_to_core_drp_monitor       <= (others => '0');  --not implemented for A10
  onu_mgt_to_core_tx_phase_stat     <= (others => '0');  --not implemented for A10


  --============================================================================
  -- Component instantiation
  --! Component refgen (generates fixed latency 40MHz aligned to detected comma)
  --============================================================================
  cmp_refgen : refgen
    port map(
      ---------------------------------------------------------------------------
      CLK240 => onu_mgt_clk_trxusr240,
      RST    => onu_core_reset_i,
      ---------------------------------------------------------------------------
      MODE   => onu_core_to_mgt_rx_equalizer_ctrl(1 downto 0),
      ERRCNT => open,
      ---------------------------------------------------------------------------
      LOCKED => onu_core_rx_locked,
      D      => '1',
      SYNC   => onu_core_rx_data_strobe,
      Q      => onu_clk_rx40_o
      ---------------------------------------------------------------------------
      );

  onu_mgt_to_core_rx_equalizer_stat <= onu_core_to_mgt_rx_equalizer_ctrl; -- In the A10, there is a possibility of having multiple output generated frequencies (40, 120, 240 which can be selected through this register)

  -- obs: a future modification will be made here, if the 40MHz is generated without an internal PLL but
  -- with an ODDR is makes no sense to spend a flag to check a "locked" state
  onu_rx40_locked_o <= onu_core_rx_locked;

  --============================================================================
  -- Component instantiation
  --! Component onu_mgt_wrapper
  --============================================================================
  cmp_onu_mgt_wrapper : onu_mgt_wrapper
    port map(
      -- reconfig interface --
      plls0_clk           => plls0_clk ,
      plls0_reset         => plls0_reset ,
      plls0_waitrequest   => plls0_waitrequest ,
      plls0_address       => plls0_address ,
      plls0_write         => plls0_write ,
      plls0_writedata     => plls0_writedata ,
      plls0_read          => plls0_read ,
      plls0_readdata      => plls0_readdata ,
      plls0_readdatavalid => plls0_readdatavalid,

      txs0_clk           => txs0_clk ,
      txs0_reset         => txs0_reset ,
      txs0_waitrequest   => txs0_waitrequest ,
      txs0_address       => txs0_address ,
      txs0_write         => txs0_write ,
      txs0_writedata     => txs0_writedata ,
      txs0_read          => txs0_read ,
      txs0_readdata      => txs0_readdata ,
      txs0_readdatavalid => txs0_readdatavalid ,
      -------------------------

      -- global input signals --                
      clk_sys_i      => onu_clk_sys_i ,
      clk_rxref240_i => onu_clk_rxref240_i ,
      clk_txref240_i => onu_clk_txref240_i ,
      mgt_reset_i    => onu_core_to_mgt_reset,
      -------------------------

      -- global output signals --       
      clk_trxusr240_o => onu_mgt_clk_trxusr240,
      -------------------------

      -- status/control --
      tx_external_locked_i => onu_tx_external_locked_i,
      rx_slide_i           => onu_core_to_mgt_rxslide,

      rxfsmrstdone_o => onu_mgt_rx_ready,
      txfsmrstdone_o => onu_mgt_tx_ready,

      mgt_txpll_o => onu_mgt_txpll_lock,
      mgt_rxpll_o => onu_mgt_rxpll_lock,

      phase_good_o => onu_mgt_phase_good_o,
      disable_phase_good_i => onu_core_to_mgt_rx_equalizer_ctrl(2),

      -- specific A10
      rx_set_locktodata_i => a10_rx_set_locktodata_i,
      rx_set_locktoref_i  => a10_rx_set_locktoref_i,
      -------------------------            

      -- data in/out --
      tx_data_i => onu_core_to_mgt_tx_data,
      rx_data_o => onu_mgt_to_core_rx_data,
      rx_p_i    => onu_rx_p_i,
      rx_n_i    => '0',
      tx_p_o    => onu_tx_p_o,
      tx_n_o    => open
      -------------------------            
      );

  --============================================================================
  -- Component instantiation
  --! Component i2c_interface (i2c_master)
  --============================================================================   
  cmp_i2c_interface : i2c_interface
    generic map(
      g_CLOCK_PERIOD => c_ONU_PERIOD_SYS_CLK,
      g_SCL_PERIOD   => c_ONU_SCL_PERIOD
      )
    port map(
      -- global input signals --
      clk_i   => onu_clk_sys_i,
      reset_i => onu_core_reset_i,
      -------------------------

      -- I2C transaction control/status signal --
      i2c_req_i      => onu_core_to_i2c_master_i2c_req,
      i2c_rnw_i      => onu_core_to_i2c_master_i2c_rnw,
      i2c_rb2_i      => onu_core_to_i2c_master_i2c_rb2,
      i2c_slv_addr_i => onu_core_to_i2c_master_i2c_slv_addr,
      i2c_reg_addr_i => onu_core_to_i2c_master_i2c_reg_addr,
      i2c_wr_data_i  => onu_core_to_i2c_master_i2c_wr_data,

      i2c_done_o     => i2c_master_to_onu_core_i2c_done,
      i2c_error_o    => i2c_master_to_onu_core_i2c_error,
      i2c_drop_req_o => i2c_master_to_onu_core_i2c_drop_req,
      i2c_rd_data_o  => i2c_master_to_onu_core_i2c_rd_data,
      -------------------------------------------

      -- I2C in/out signal ----
      i2c_sdi_i     => i2c_master_sdi,
      i2c_sdo_o     => i2c_master_sdo,
      i2c_sda_ena_o => i2c_master_sda_ena,
      i2c_scl_o     => i2c_master_scl,
      i2c_scl_ena_o => i2c_master_scl_ena
      -------------------------
      );

  i2c_master_sdi <= onu_i2c_sda_io when i2c_master_sda_ena = '0' else '0';
  onu_i2c_sda_io <= i2c_master_sdo when i2c_master_sda_ena = '1' else 'Z';
  onu_i2c_scl_io <= i2c_master_scl when i2c_master_scl_ena = '1' else 'Z';

  -- CONTROL INTERFACING:
  -- AVALON-MM SLAVE
  --============================================================================
  -- Process p_reg_rd_wr
  --! Simple avalon-MM slave interface compatible
  --! read:  avalon_writedata, avalon_address, avalon_read, onu_stat_reg\n
  --! write: avalon_readdata, avalon_readdatavalid, onu_ctrl_reg, onu_ctrl_reg_strobe\n
  --! r/w: avalon_read_r
  --============================================================================            
  p_reg_rd_wr : process (avalon_clk_i) is
  begin
    if avalon_clk_i'event and avalon_clk_i = '1' then
      if(avalon_reset = '1') then
        avalon_readdatavalid <= '0';
        onu_ctrl_reg_strobe  <= (others => '0');
      else
        if(avalon_write = '1') then
          for i in avalon_byteenable'range loop
            if(avalon_byteenable(i) = '1') then
              onu_ctrl_reg((i+1)*8-1 downto i*8) <= avalon_writedata((i+1)*8-1 downto i*8);
            end if;
            onu_ctrl_reg_strobe(to_integer(unsigned(avalon_address(6 downto 0)))) <= '1';
          end loop;
        else
          onu_ctrl_reg_strobe <= (others => '0');
        end if;

        avalon_read_r <= avalon_read;
        if(avalon_read = '1' and avalon_read_r = '0') then  --rising edge
          avalon_readdata      <= onu_stat_reg(to_integer(unsigned(avalon_address(6 downto 0))));
          avalon_readdatavalid <= '1';
        else
          avalon_readdatavalid <= '0';
        end if;
      end if;
    end if;
  end process p_reg_rd_wr;

  --============================================================================
  --Output
  --============================================================================  
  onu_clk_trxusr240_o   <= onu_mgt_clk_trxusr240;
  onu_rx_locked_o      <= onu_core_rx_locked;
  onu_rx_data_strobe_o <= onu_core_rx_data_strobe;

  onu_mgt_tx_ready_o   <= onu_mgt_tx_ready;
  onu_mgt_rx_ready_o   <= onu_mgt_rx_ready;
  onu_mgt_txpll_lock_o <= onu_mgt_txpll_lock;
  onu_mgt_rxpll_lock_o <= onu_mgt_rxpll_lock;

end architecture structural;
--============================================================================
-- architecture end
--============================================================================
