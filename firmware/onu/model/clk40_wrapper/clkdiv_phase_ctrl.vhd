--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file clkdiv_phase_ctrl.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: clkdiv_phase_ctrl
--
--! @brief Generates divided clock aligned to strobe_i pulse (high level model)
--!
--! @author Created by Eduardo Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 07\09\2018
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: EBSM
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 07\09\2018 - EBSM - created\n
-------------------------------------------------------------------------------
--! @todo - \n
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for clkdiv_phase_ctrl
--============================================================================
entity clkdiv_phase_ctrl is
  generic(
    g_DIV_FACTOR : integer := 6
    );
  port (
    clk_sys_i      : in  std_logic;
    reset_i        : in  std_logic;
    clk_i          : in  std_logic;
    strobe_i       : in  std_logic;
    clkdiv_o       : out std_logic;
    phase_locked_o : out std_logic
    );
end clkdiv_phase_ctrl;

--============================================================================
--! Architecture declaration for clkdiv_phase_ctrl
--============================================================================
architecture high_level of clkdiv_phase_ctrl is

  --! Functions

  --! Constants

  --! Signal declaration

begin

  p_gen40 : process
  begin
    phase_locked_o <= '0';
    clkdiv_o       <= '0';
    wait until rising_edge(strobe_i);
    wait until falling_edge(clk_i);
    while(reset_i = '0') loop
      for i in 0 to g_DIV_FACTOR/2-1 loop
        clkdiv_o <= '0';
        wait until rising_edge(clk_i);
      end loop;
      for i in 0 to g_DIV_FACTOR/2-1 loop
        clkdiv_o <= '1';
        wait until rising_edge(clk_i);
      end loop;
      phase_locked_o <= '1';
    end loop;
  end process p_gen40;

end high_level;
