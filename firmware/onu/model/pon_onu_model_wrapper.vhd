--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file pon_onu_model_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.pon_onu_package_static.all;
use work.pon_onu_package_modifiable.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: PON ONU - Example design (pon_onu_model_wrapper)
--
--! @brief Example design for ONU
--! <further description>
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 07\09\2018
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 07\09\2018 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for pon_onu_model_wrapper
--============================================================================
entity pon_onu_model_wrapper is
  generic(
    g_UI_DOWN_PERIOD : time := 0.100 ns  --! UI_PERIOD: this generic should be EXACTLY equal to the period of clk_rxref240_i/40
    );
  port (
    -- Control interface
    onu_clk_manager_i     : in  std_logic;
    onu_stat_reg_o        : out t_regbank32b(127 downto 0);
    onu_ctrl_reg_i        : in  std_logic_vector(31 downto 0);
    onu_ctrl_reg_strobe_i : in  std_logic_vector(127 downto 0);

    -- Clk sys/reset              
    onu_clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
    onu_core_reset_i : in std_logic;

    -- MGT ref clocks
    onu_clk_rxref240_i       : in  std_logic;  --! MGT Rx reference clock - 240MHz      
    onu_clk_txref240_i       : in  std_logic;  --! MGT Tx reference clock - 240MHz (has to be derived from downstream recovered clock - synchronization required for TTC-PON)        
    onu_tx_external_locked_i : in  std_logic;  --! ONU Tx External PLL is not Locked
    -- Recovered downstream 40MHz clock
    onu_clk_rx40_o           : out std_logic;
    onu_rx40_locked_o        : out std_logic;

    -- Data in/out
    onu_clk_trxusr240_o   : out std_logic;  --! User_TRx 240MHz clock
    onu_rx_data_strobe_o : out std_logic;
    onu_rx_data_frame_o  : out std_logic_vector(199 downto 0);
    onu_tx_data_strobe_i : in  std_logic;
    onu_tx_data_ready_o  : out std_logic;
    onu_tx_data_i        : in  std_logic_vector(55 downto 0);
    onu_address_i        : in  std_logic_vector(7 downto 0);  --! ONU address

    -- Status
    onu_mgt_phase_good_o      : out std_logic;  --! internal Tx and Rx MGT clock domains phase is correctly chosen   
    onu_rx_locked_o           : out std_logic;  --! downstream header aligned
    onu_operational_o         : out std_logic;  --! given by onu once system init (calibration) is done
    onu_rx_serror_corrected_o : out std_logic;  --! a single-error was corrected by the FEC
    onu_rx_derror_corrected_o : out std_logic;  --! a double-error was corrected by the FEC        
    onu_mgt_tx_ready_o        : out std_logic;
    onu_mgt_rx_ready_o        : out std_logic;
    onu_mgt_txpll_lock_o      : out std_logic;
    onu_mgt_rxpll_lock_o      : out std_logic;

    -- Soft-configurable interrupt output
    onu_interrupt_o     : out std_logic;
    onu_status_sticky_o : out std_logic_vector(30 downto 0);
    ------------------------

    -- serial
    onu_tx_p_o : out std_logic;
    onu_tx_n_o : out std_logic;
    onu_rx_p_i : in  std_logic;
    onu_rx_n_i : in  std_logic;

    onu_sfp_rx_los_i   : in  std_logic;
    onu_sfp_tx_fault_i : in  std_logic;
    onu_sfp_mod_abs_i  : in  std_logic;
    onu_sfp_tx_sd_i    : in  std_logic;
    onu_sfp_pdown_o    : out std_logic;
    onu_sfp_tx_dis_o   : out std_logic;

    --i2c interface
    onu_i2c_sda_io : inout std_logic;
    onu_i2c_scl_io : inout std_logic
    -------------------------

    );
end pon_onu_model_wrapper;

--============================================================================
--! Architecture declaration for pon_onu_model_wrapper
--============================================================================ 
architecture structural of pon_onu_model_wrapper is

  --! Signals

  -- global from onu_core   
  signal onu_core_rx_data_strobe : std_logic;
  signal onu_core_rx_locked      : std_logic;

  -- global from mgt
  signal onu_mgt_clk_trxusr240 : std_logic;
  signal onu_mgt_tx_ready     : std_logic;
  signal onu_mgt_rx_ready     : std_logic;
  signal onu_mgt_txpll_lock   : std_logic;
  signal onu_mgt_rxpll_lock   : std_logic;

  -- onu_core <-> onu_mgt_wrapper
  signal onu_core_to_mgt_reset             : std_logic;
  signal onu_core_to_mgt_rxslide           : std_logic;
  signal onu_core_to_mgt_tx_data           : std_logic_vector(9 downto 0);
  signal onu_mgt_to_core_rx_data           : std_logic_vector(39 downto 0);
  signal onu_core_to_mgt_drp_wr            : std_logic_vector(31 downto 0);
  signal onu_core_to_mgt_drp_wr_strobe     : std_logic;
  signal onu_mgt_to_core_drp_monitor       : std_logic_vector(31 downto 0);
  signal onu_core_to_mgt_rx_equalizer_ctrl : std_logic_vector(31 downto 0);
  signal onu_mgt_to_core_rx_equalizer_stat : std_logic_vector(31 downto 0);
  signal onu_core_to_mgt_tx_phase_ctrl     : std_logic_vector(31 downto 0);
  signal onu_mgt_to_core_tx_phase_stat     : std_logic_vector(31 downto 0);

  -- MMCM to generate 40MHz fixed-latency in ONU design
  signal reset_rx40 : std_logic;

  -- onu_core <-> i2c_master
  signal onu_core_to_i2c_master_i2c_req      : std_logic;
  signal onu_core_to_i2c_master_i2c_rnw      : std_logic;
  signal onu_core_to_i2c_master_i2c_rb2      : std_logic;
  signal onu_core_to_i2c_master_i2c_slv_addr : std_logic_vector(6 downto 0);
  signal onu_core_to_i2c_master_i2c_reg_addr : std_logic_vector(7 downto 0);
  signal onu_core_to_i2c_master_i2c_wr_data  : std_logic_vector(7 downto 0);

  signal i2c_master_to_onu_core_i2c_done     : std_logic;
  signal i2c_master_to_onu_core_i2c_error    : std_logic;
  signal i2c_master_to_onu_core_i2c_drop_req : std_logic;
  signal i2c_master_to_onu_core_i2c_rd_data  : std_logic_vector(15 downto 0);

  -- i2c_master
  signal i2c_master_sdi     : std_logic;
  signal i2c_master_sdo     : std_logic;
  signal i2c_master_sda_ena : std_logic;
  signal i2c_master_scl_ena : std_logic;
  signal i2c_master_scl     : std_logic;

  --! Component declaration
  component onu_core is                 -- use work.pon_onu_package.all;
    port (
      -- global input signals --
      clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
      core_reset_i : in std_logic;
      -------------------------

      -- manager control/stat -- see manager core interfacing just below for more information about how to interface the core control
      clk_manager_i         : in  std_logic;
      manager_stat_reg_o    : out t_regbank32b(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
      manager_ctrl_reg_i    : in  std_logic_vector(31 downto 0);
      manager_ctrl_strobe_i : in  std_logic_vector(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
      --------------------------

      -- status / control --
      onu_address_i         : in  std_logic_vector(7 downto 0);  --! ONU address in TDM
      rx_locked_o           : out std_logic;  --! downstream header aligned
      onu_operational_o     : out std_logic;  --! given by onu once system init (calibration) is done; this means that ONU is allowed to transmit
      rx_serror_corrected_o : out std_logic;  --! a single-error was corrected by the FEC
      rx_derror_corrected_o : out std_logic;  --! a double-error was corrected by the FEC

      -- Soft-configurable interrupt output
      interrupt_o     : out std_logic;
      status_sticky_o : out std_logic_vector(30 downto 0);
      -------------------------             

      -- MGT connections -----------
      -- Clocks
      clk_trxusr240_i : in std_logic;    --! User_TRx 240MHz clock  

      -- Status/Ctrl
      mgt_reset_o      : out std_logic;
      mgt_tx_ready_i   : in  std_logic;
      mgt_rx_ready_i   : in  std_logic;
      mgt_txpll_lock_i : in  std_logic;
      mgt_rxpll_lock_i : in  std_logic;
      mgt_rxslide_o    : out std_logic;

      -- Xilinx specific
      mgt_drp_wr_o            : out std_logic_vector(31 downto 0);
      mgt_drp_wr_strobe_o     : out std_logic;
      mgt_drp_monitor_i       : in  std_logic_vector(31 downto 0);
      mgt_rx_equalizer_ctrl_o : out std_logic_vector(31 downto 0);
      mgt_rx_equalizer_stat_i : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_o     : out std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_i     : in  std_logic_vector(31 downto 0);

      -- Data
      mgt_tx_data_o : out std_logic_vector(c_ONU_TX_MGT_DATA_WIDTH-1 downto 0);
      mgt_rx_data_i : in  std_logic_vector(c_ONU_RX_MGT_DATA_WIDTH-1 downto 0);
      ------------------------      

      -- data in/out --  -- see TX usr core interfacing / RX usr core just below for more information about how to interface the core
      tx_data_strobe_i : in  std_logic;
      tx_data_ready_o  : out std_logic;
      tx_data_i        : in  std_logic_vector(c_ONU_TX_USR_DATA_WIDTH-1 downto 0);
      rx_data_strobe_o : out std_logic;
      rx_data_frame_o  : out std_logic_vector(c_ONU_RX_USR_DATA_WIDTH-1 downto 0);
      -------------------------

      -- sfp data / control --
      sfp_rx_los_i   : in  std_logic;
      sfp_tx_fault_i : in  std_logic;
      sfp_mod_abs_i  : in  std_logic;
      sfp_tx_sd_i    : in  std_logic;
      sfp_pdown_o    : out std_logic;
      sfp_tx_dis_o   : out std_logic;
      -------------------------

      --i2c interface
      i2c_req_o      : out std_logic;
      i2c_rnw_o      : out std_logic;
      i2c_rb2_o      : out std_logic;
      i2c_slv_addr_o : out std_logic_vector(6 downto 0);
      i2c_reg_addr_o : out std_logic_vector(7 downto 0);
      i2c_wr_data_o  : out std_logic_vector(7 downto 0);

      i2c_done_i     : in std_logic;
      i2c_error_i    : in std_logic;
      i2c_drop_req_i : in std_logic;
      i2c_rd_data_i  : in std_logic_vector(15 downto 0)
      -------------------------

      );
  end component onu_core;

  component clkdiv_phase_ctrl is
    generic(
      g_DIV_FACTOR : integer := 6
      );  
    port (
      clk_sys_i      : in  std_logic;
      reset_i        : in  std_logic;
      clk_i          : in  std_logic;
      strobe_i       : in  std_logic;
      clkdiv_o       : out std_logic;
      phase_locked_o : out std_logic
      );
  end component clkdiv_phase_ctrl;

  component onu_mgt_wrapper is
    generic(
      g_SPEED_FOR_SIMULATION : boolean := true;  --! If true, rx slide only moves clock every two pulses, yielding in a always even number of rx slide   
      g_UI_DOWN_PERIOD       : time    := 0.100 ns  --! UI_PERIOD: this generic should be EXACTLY equal to the period of clk_rxref240_i/40
      );  
    port (
      -- global input signals --                
      clk_sys_i      : in std_logic;
      clk_rxref240_i : in std_logic;
      clk_txref240_i : in std_logic;
      mgt_reset_i    : in std_logic;
      -------------------------

      -- global output signals --       
      clk_trxusr240_o : out std_logic;	  
      -------------------------

      -- status/control --
      tx_external_locked_i : in  std_logic;
      rx_slide_i           : in  std_logic;
      rxfsmrstdone_o       : out std_logic;
      txfsmrstdone_o       : out std_logic;
      mgt_txpll_o          : out std_logic;
      mgt_rxpll_o          : out std_logic;
      phase_good_o         : out std_logic;
	  
      drp_wr_i                : in  std_logic_vector(31 downto 0);
      drp_wr_strobe_i         : in  std_logic;
      drp_monitor_o           : out std_logic_vector(31 downto 0);
      mgt_rx_equalizer_ctrl_i : in  std_logic_vector(31 downto 0);
      mgt_rx_equalizer_stat_o : out std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_i     : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_o     : out std_logic_vector(31 downto 0);
      -------------------------  

      -- data in/out --         
      tx_data_i : in  std_logic_vector(9 downto 0);
      rx_data_o : out std_logic_vector(39 downto 0);
      rx_p_i    : in  std_logic;
      rx_n_i    : in  std_logic;
      tx_p_o    : out std_logic;
      tx_n_o    : out std_logic
      -------------------------            
      );
  end component onu_mgt_wrapper;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component onu_core
  --============================================================================
  cmp_onu_core : onu_core
    port map(
      -- global input signals --
      clk_sys_i    => onu_clk_sys_i,
      core_reset_i => onu_core_reset_i,
      -------------------------

      -- manager control/stat
      clk_manager_i         => onu_clk_manager_i,
      manager_stat_reg_o    => onu_stat_reg_o,
      manager_ctrl_reg_i    => onu_ctrl_reg_i,
      manager_ctrl_strobe_i => onu_ctrl_reg_strobe_i,
      --------------------------

      -- status / control --
      onu_address_i         => onu_address_i,
      rx_locked_o           => onu_core_rx_locked,
      onu_operational_o     => onu_operational_o,
      rx_serror_corrected_o => onu_rx_serror_corrected_o,
      rx_derror_corrected_o => onu_rx_derror_corrected_o,
      interrupt_o           => onu_interrupt_o,
      status_sticky_o       => onu_status_sticky_o,
      -------------------------   

      -- MGT connections -----------
      -- Clocks
      clk_trxusr240_i => onu_mgt_clk_trxusr240,

      -- Status/Ctrl
      mgt_reset_o => onu_core_to_mgt_reset,

      mgt_drp_wr_o            => onu_core_to_mgt_drp_wr,
      mgt_drp_wr_strobe_o     => onu_core_to_mgt_drp_wr_strobe,
      mgt_drp_monitor_i       => onu_mgt_to_core_drp_monitor,
      mgt_rx_equalizer_ctrl_o => onu_core_to_mgt_rx_equalizer_ctrl,
      mgt_rx_equalizer_stat_i => onu_mgt_to_core_rx_equalizer_stat,
      mgt_tx_phase_ctrl_o     => onu_core_to_mgt_tx_phase_ctrl,
      mgt_tx_phase_stat_i     => onu_mgt_to_core_tx_phase_stat,

      mgt_tx_ready_i   => onu_mgt_tx_ready,
      mgt_rx_ready_i   => onu_mgt_rx_ready,
      mgt_txpll_lock_i => onu_mgt_txpll_lock,
      mgt_rxpll_lock_i => onu_mgt_rxpll_lock,

      mgt_rxslide_o => onu_core_to_mgt_rxslide,

      -- Data
      mgt_tx_data_o => onu_core_to_mgt_tx_data,
      mgt_rx_data_i => onu_mgt_to_core_rx_data,
      ------------------------    

      -- data in/out --  -- see TX usr core interfacing / RX usr core just below for more information about how to interface the core
      tx_data_strobe_i => onu_tx_data_strobe_i,
      tx_data_ready_o  => onu_tx_data_ready_o,
      tx_data_i        => onu_tx_data_i,
      rx_data_strobe_o => onu_core_rx_data_strobe,
      rx_data_frame_o  => onu_rx_data_frame_o,
      -------------------------

      -- sfp data / control --
      sfp_rx_los_i   => onu_sfp_rx_los_i,
      sfp_tx_fault_i => onu_sfp_tx_fault_i,
      sfp_mod_abs_i  => onu_sfp_mod_abs_i,
      sfp_tx_sd_i    => onu_sfp_tx_sd_i,
      sfp_pdown_o    => onu_sfp_pdown_o,
      sfp_tx_dis_o   => onu_sfp_tx_dis_o,
      -------------------------

      -- i2c interfacing --
      i2c_req_o      => onu_core_to_i2c_master_i2c_req,
      i2c_rnw_o      => onu_core_to_i2c_master_i2c_rnw,
      i2c_rb2_o      => onu_core_to_i2c_master_i2c_rb2,
      i2c_slv_addr_o => onu_core_to_i2c_master_i2c_slv_addr,
      i2c_reg_addr_o => onu_core_to_i2c_master_i2c_reg_addr,
      i2c_wr_data_o  => onu_core_to_i2c_master_i2c_wr_data,

      i2c_done_i     => i2c_master_to_onu_core_i2c_done,
      i2c_error_i    => i2c_master_to_onu_core_i2c_error,
      i2c_drop_req_i => i2c_master_to_onu_core_i2c_drop_req,
      i2c_rd_data_i  => i2c_master_to_onu_core_i2c_rd_data
      -------------------------

      );

  --============================================================================
  -- Component instantiation
  --! Component clkdiv_phase_ctrl (generates fixed latency 40MHz aligned to detected comma)
  --============================================================================
  cmp_clkdiv_phase_ctrl : clkdiv_phase_ctrl
    generic map(
      g_DIV_FACTOR => 6
      )
    port map(
      clk_sys_i      => onu_clk_sys_i,
      reset_i        => reset_rx40,
      clk_i          => onu_mgt_clk_trxusr240,
      strobe_i       => onu_core_rx_data_strobe,
      clkdiv_o       => onu_clk_rx40_o,
      phase_locked_o => onu_rx40_locked_o
      );

  reset_rx40 <= not onu_core_rx_locked;

  --============================================================================
  -- Component instantiation
  --! Component onu_mgt_wrapper
  --============================================================================
  cmp_onu_mgt_wrapper : onu_mgt_wrapper
    generic map(
      g_SPEED_FOR_SIMULATION => false,
      g_UI_DOWN_PERIOD       => g_UI_DOWN_PERIOD
      )
    port map(
      -- global input signals --                
      clk_sys_i      => onu_clk_sys_i,
      clk_rxref240_i => onu_clk_rxref240_i,
      clk_txref240_i => onu_clk_txref240_i,
      mgt_reset_i    => onu_core_to_mgt_reset,
      -------------------------

      -- global output signals --       
      clk_trxusr240_o => onu_mgt_clk_trxusr240,
      -------------------------

      -- status/control --
      tx_external_locked_i => onu_tx_external_locked_i,
      rx_slide_i           => onu_core_to_mgt_rxslide,

      rxfsmrstdone_o => onu_mgt_rx_ready,
      txfsmrstdone_o => onu_mgt_tx_ready,

      mgt_txpll_o    => onu_mgt_txpll_lock,
      mgt_rxpll_o    => onu_mgt_rxpll_lock,
      phase_good_o   => onu_mgt_phase_good_o,
	  
      drp_wr_i                => onu_core_to_mgt_drp_wr,
      drp_wr_strobe_i         => onu_core_to_mgt_drp_wr_strobe,
      drp_monitor_o           => onu_mgt_to_core_drp_monitor,
      mgt_rx_equalizer_ctrl_i => onu_core_to_mgt_rx_equalizer_ctrl,
      mgt_rx_equalizer_stat_o => onu_mgt_to_core_rx_equalizer_stat,
      mgt_tx_phase_ctrl_i     => onu_core_to_mgt_tx_phase_ctrl,
      mgt_tx_phase_stat_o     => onu_mgt_to_core_tx_phase_stat,
      -------------------------            

      -- data in/out --
      tx_data_i => onu_core_to_mgt_tx_data,
      rx_data_o => onu_mgt_to_core_rx_data,
      rx_p_i    => onu_rx_p_i,
      rx_n_i    => onu_rx_n_i,
      tx_p_o    => onu_tx_p_o,
      tx_n_o    => onu_tx_n_o
      -------------------------            
      );

  --============================================================================
  --Output
  --============================================================================  
  onu_clk_trxusr240_o   <= onu_mgt_clk_trxusr240;
  onu_rx_locked_o      <= onu_core_rx_locked;
  onu_rx_data_strobe_o <= onu_core_rx_data_strobe;

  onu_mgt_tx_ready_o   <= onu_mgt_tx_ready;
  onu_mgt_rx_ready_o   <= onu_mgt_rx_ready;
  onu_mgt_txpll_lock_o <= onu_mgt_txpll_lock;
  onu_mgt_rxpll_lock_o <= onu_mgt_rxpll_lock;
  
end structural;
