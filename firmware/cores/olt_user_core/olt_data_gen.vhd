--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_data_gen.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT data generator for example design (olt_data_gen)
--
--! @brief OLT data generator for example design
--! Generates data in frame or word mode depending on g_DATA_WIDTH set to g_WORD_WIDTH or g_FRAME_WIDTH
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 19\07\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 19\07\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_data_gen
--============================================================================
entity olt_data_gen is
  generic(
    g_WORD_WIDTH  : integer := 32;
    g_FRAME_WIDTH : integer := 160;
    g_DATA_WIDTH  : integer := 160
    );
  port (
    -- global input signals --
    clk_i   : in std_logic;
    reset_i : in std_logic;
    -------------------------

    -- status / control --
    sel_pattern_i    : in std_logic_vector(1 downto 0);  -- "00" PRBS-7, "01" PRBS-23, "10" constant, "11" counter mode
    constant_frame_i : in std_logic_vector(g_FRAME_WIDTH-1 downto 0);
    -------------------------

    -- data in/out --
    data_o        : out std_logic_vector(g_DATA_WIDTH-1 downto 0);
    data_strobe_o : out std_logic
    -------------------------
    );
end entity olt_data_gen;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of olt_data_gen is

  attribute mark_debug : string;

  --! Signal declaration
  signal strobe_gen : std_logic;
  signal div6_cntr  : integer range 0 to 7;

  -- prbs mode
  signal prbs_mode  : std_logic;
  signal frame_prbs : std_logic_vector(g_FRAME_WIDTH-1 downto 0);

  -- counter mode (used for latency measurement)
  signal   byte_cntr        : unsigned(7 downto 0);
  constant modulo_byte_cntr : unsigned(7 downto 0) := x"9F";

  -- frame signal
  signal data_frame : std_logic_vector(g_FRAME_WIDTH-1 downto 0);

  --! Component declaration
  component prbs_data_gen is
    generic (
      G_DOUT_WIDTH   : integer := 20;
      G_PRBS_TAP     : integer := 0;
      G_REVERSE_BITS : integer := 0);
    port (
      clk_i : in  std_logic;
      mode_i  : in  std_logic;  -- '0' external seed, '1' internal loop-back
      ena_i   : in  std_logic;          -- '0' hold, '1' generate
      sel_i   : in  std_logic_vector(1 downto 0);  -- "00" PRBS-7, "01" PRBS-23, "1x" fix
      seed_i  : in  std_logic_vector(22 downto 0);
      dout_o  : out std_logic_vector(G_DOUT_WIDTH-1 downto 0));
  end component prbs_data_gen;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Process p_strobe_gen
  --! Generates strobe for olt-user interfacing
  --! read:  \n
  --! write: data_strobe_o\n
  --! r/w:   div6_cntr, strobe_gen\n
  --============================================================================           
  p_strobe_gen : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        strobe_gen    <= '0';
        data_strobe_o <= '0';
        div6_cntr     <= 0;
      else
        if(div6_cntr = 5) then
          div6_cntr  <= 0;
          strobe_gen <= '1';
        else
          div6_cntr  <= div6_cntr+1;
          strobe_gen <= '0';
        end if;
        data_strobe_o <= strobe_gen;
      end if;
    end if;
  end process p_strobe_gen;

  --============================================================================
  -- Component instantiation
  -- Generates data PRBS
  --! Component prbs_data_gen
  --============================================================================  
  cmp_prbs_data_gen : prbs_data_gen
    generic map(
      G_DOUT_WIDTH   => g_FRAME_WIDTH,
      G_PRBS_TAP     => 0,
      G_REVERSE_BITS => 0)
    port map(
      clk_i => clk_i,
      mode_i  => prbs_mode,
      ena_i   => strobe_gen,
      sel_i   => sel_pattern_i,         -- "00" PRBS-7, "01" PRBS-23, "1x" fix
      seed_i  => "00110100010101100111100",
      dout_o  => frame_prbs);

  prbs_mode <= not reset_i;

  --============================================================================
  -- Process p_byte_cntr
  --! Generates Byte counter
  --! read:  strobe_gen\n
  --! write: byte_cntr\n
  --! r/w:   \n
  --============================================================================        
  p_byte_cntr : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        byte_cntr <= (others => '0');
      elsif(strobe_gen = '1') then
        if(byte_cntr >= modulo_byte_cntr) then
          byte_cntr <= (others => '0');
        else
          byte_cntr <= byte_cntr + 1;
        end if;
      end if;
    end if;
  end process p_byte_cntr;

  --============================================================================
  -- Process p_word_interfacing
  --! Generates data out in word mode
  --! read:  data_frame, div6_cntr\n
  --! write: data_o\n
  --! r/w:   \n
  --============================================================================        
  gen_word_interfacing : if (data_o'length = g_WORD_WIDTH) generate
    p_word_interfacing : process(data_frame, div6_cntr) is
      variable v_fake_data_frame : std_logic_vector(g_WORD_WIDTH*6-1 downto 0);
    begin
      v_fake_data_frame(v_fake_data_frame'left downto v_fake_data_frame'left-data_frame'length+1) := data_frame;
      v_fake_data_frame(v_fake_data_frame'left-data_frame'length downto 0)                        := (others => '0');
      case div6_cntr is
        when 1 =>
          data_o <= v_fake_data_frame(g_WORD_WIDTH-1 downto 0);
        when 2 =>
          data_o <= v_fake_data_frame(2*g_WORD_WIDTH-1 downto g_WORD_WIDTH);
        when 3 =>
          data_o <= v_fake_data_frame(3*g_WORD_WIDTH-1 downto 2*g_WORD_WIDTH);
        when 4 =>
          data_o <= v_fake_data_frame(4*g_WORD_WIDTH-1 downto 3*g_WORD_WIDTH);
        when 5 =>
          data_o <= v_fake_data_frame(5*g_WORD_WIDTH-1 downto 4*g_WORD_WIDTH);
        when others =>
          data_o <= v_fake_data_frame(6*g_WORD_WIDTH-1 downto 5*g_WORD_WIDTH);
      end case;
    end process p_word_interfacing;
  end generate;

  gen_frame_interfacing : if (data_o'length = g_FRAME_WIDTH) generate
    data_o <= data_frame;
  end generate;

  --============================================================================
  -- Process p_selection_pattern
  --! Multiplexer select frame type
  --! read:  byte_cntr, frame_prbs, latency_test_frame, sel_pattern_i\n
  --! write: data_frame\n
  --! r/w:   \n
  --============================================================================           
  p_selection_pattern : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
	  if(reset_i='1') then
	    data_frame <= (others => '0');
      elsif(strobe_gen = '1') then
        case sel_pattern_i is
          when "11" =>
            for i in 0 to data_frame'length/8-1 loop
              data_frame(8*(i+1)-1 downto 8*i) <= std_logic_vector(byte_cntr);
            end loop;
          when "10" =>
            data_frame <= constant_frame_i;
          when others =>
            data_frame <= frame_prbs;
        end case;
      end if;
    end if;
  end process p_selection_pattern;

end architecture rtl;
--============================================================================
-- architecture end
--============================================================================
