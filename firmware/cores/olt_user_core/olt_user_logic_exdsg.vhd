--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_user_logic_exdsg.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT user example design logic (olt_user_logic_exdsg)
--
--! @brief OLT user example design logic
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 19\07\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 17\08\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------
entity olt_user_logic_exdsg is
  port(
    -- core connections
    olt_clk_sys_i        : in  std_logic;
    olt_core_reset_o     : out std_logic;
    olt_interrupt_i      : in  std_logic;
    olt_status_sticky_i  : in  std_logic_vector(30 downto 0);
    olt_clk_trxusr240_i   : in  std_logic;
    olt_tx_data_o        : out std_logic_vector(199 downto 0);
    olt_tx_data_strobe_o : out std_logic;
    olt_rx_locked_i      : in  std_logic;
    olt_rx_data_i        : in  std_logic_vector(55 downto 0);
    olt_rx_onu_addr_i    : in  std_logic_vector(7 downto 0);
    olt_rx_data_strobe_i : in  std_logic;
    olt_rx_data_error_i  : in  std_logic;
    olt_mgt_pll_lock_i   : in  std_logic;
    olt_mgt_tx_ready_i   : in  std_logic;
    olt_mgt_rx_ready_i   : in  std_logic;

    -- Control AXI --
    s_olt_user_axi_aclk : in  std_logic;
    ctrl_reg_i          : in  t_regbank32b(127 downto 0);
    stat_reg_o          : out t_regbank32b(127 downto 0);
    ---------------------------------

    -- Others ----------
    tx_match_flag_o : out std_logic
    ----------------------------------

    );
end olt_user_logic_exdsg;

architecture rtl of olt_user_logic_exdsg is
  
  attribute mark_debug : string;
  attribute keep       : string;
  attribute async_reg  : string;

  --! Functions

  --! Constants

  --! Signal declaration
  signal olt_status_reg_async     : std_logic_vector(3 downto 0);
  signal olt_status_reg_axisync   : std_logic_vector(3 downto 0);
  signal olt_status_reg_axisync_r : std_logic_vector(3 downto 0);


  -- TX -------------------------------------------------------------------
  signal reset_tx_logic : std_logic;

  signal olt_tx_data        : std_logic_vector(olt_tx_data_o'range);
  signal olt_tx_data_strobe : std_logic;

  attribute mark_debug of olt_tx_data        : signal is "true";
  attribute mark_debug of olt_tx_data_strobe : signal is "true";
  attribute keep of olt_tx_data              : signal is "true";
  attribute keep of olt_tx_data_strobe       : signal is "true";

  signal sel_tx_pattern_async    : std_logic_vector(1 downto 0);
  signal sel_tx_pattern_txsync   : std_logic_vector(1 downto 0);
  signal sel_tx_pattern_txsync_r : std_logic_vector(1 downto 0);

  -- pattern generator signals
  signal constant_frame_pattern : std_logic_vector(olt_tx_data_o'range);
  -----------------------------------------------------------------------

  -- RX -----------------------------------------------------------------
  signal reset_rx_logic     : std_logic;
  signal olt_rx_data        : std_logic_vector(olt_rx_data_i'range);
  signal olt_rx_onu_addr    : std_logic_vector(7 downto 0);
  signal olt_rx_data_strobe : std_logic;

  attribute mark_debug of olt_rx_data        : signal is "true";
  attribute mark_debug of olt_rx_onu_addr    : signal is "true";
  attribute mark_debug of olt_rx_data_strobe : signal is "true";

  attribute keep of olt_rx_data        : signal is "true";
  attribute keep of olt_rx_onu_addr    : signal is "true";
  attribute keep of olt_rx_data_strobe : signal is "true";

  -- BBERT
  signal rx_bbert_clear_async            : std_logic;
  signal rx_bbert_latch_async            : std_logic;
  signal rx_bbert_heartbeat_period_async : std_logic_vector(15 downto 0);

  signal rx_bbert_clear_rxsync            : std_logic;
  signal rx_bbert_clear_rxsync_r          : std_logic;
  signal rx_bbert_latch_rxsync            : std_logic;
  signal rx_bbert_latch_rxsync_r          : std_logic;
  signal rx_bbert_heartbeat_period_rxsync : std_logic_vector(15 downto 0);

  attribute async_reg of rx_bbert_clear_rxsync            : signal is "true";
  attribute async_reg of rx_bbert_clear_rxsync_r          : signal is "true";
  attribute async_reg of rx_bbert_latch_rxsync            : signal is "true";
  attribute async_reg of rx_bbert_latch_rxsync_r          : signal is "true";
  attribute async_reg of rx_bbert_heartbeat_period_rxsync : signal is "true";


  -- MULTI-BERT Test
  type t_vector48b is array (natural range <>) of std_logic_vector(47 downto 0);

  signal rx_error_sum_latched_onu      : t_vector48b(15 downto 0);
  signal rx_bit_sum_latched_onu        : t_vector48b(15 downto 0);
  signal rx_packetloss_sum_latched_onu : t_vector48b(15 downto 0);
  signal rx_prbs_locked_onu            : std_logic_vector(15 downto 0);
  signal rx_error_detected_onu         : std_logic_vector(15 downto 0);

  signal rx_error_sum_latched_axisync      : std_logic_vector(47 downto 0);
  signal rx_bit_sum_latched_axisync        : std_logic_vector(47 downto 0);
  signal rx_packetloss_sum_latched_axisync : std_logic_vector(47 downto 0);

  signal select_onu_bbert : std_logic_vector(7 downto 0);

  attribute mark_debug of rx_prbs_locked_onu    : signal is "true";
  attribute mark_debug of rx_error_detected_onu : signal is "true";

  attribute keep of rx_prbs_locked_onu    : signal is "true";
  attribute keep of rx_error_detected_onu : signal is "true";
  -------------------------------------------------------------------- 


  --! Components
  component olt_data_gen is
    generic(
      g_WORD_WIDTH  : integer := 32;
      g_FRAME_WIDTH : integer := 160;
      g_DATA_WIDTH  : integer := 160

      );
    port (
      -- global input signals --
      clk_i            : in  std_logic;
      reset_i          : in  std_logic;
      -------------------------
      -- status / control --
      sel_pattern_i    : in  std_logic_vector(1 downto 0);  -- "00" PRBS-7, "01" PRBS-23, "10" constant, "11" counter mode
      constant_frame_i : in  std_logic_vector(g_FRAME_WIDTH-1 downto 0);
      -------------------------
      -- data in/out --
      data_o           : out std_logic_vector(g_DATA_WIDTH-1 downto 0);
      data_strobe_o    : out std_logic
      -------------------------
      );
  end component olt_data_gen;

  component bbert_top is
    generic(
      g_CLK_CYCLES_FRAME  : integer   := 1;
      g_BLOCK_ACCUMULATOR : std_logic := '1'
      );
    port (
      clk_i   : in std_logic;
      reset_i : in std_logic;

      bbert_clear_acc_i   : in std_logic;
      bbert_latch_acc_i   : in std_logic;
      superframe_length_i : in std_logic_vector;
      onu_addr_chkr_i     : in std_logic_vector(7 downto 0);

      data_i        : in std_logic_vector;
      onu_addr_i    : in std_logic_vector(7 downto 0);
      data_strobe_i : in std_logic;

      error_sum_o       : out std_logic_vector;
      bit_sum_o         : out std_logic_vector;
      packet_loss_sum_o : out std_logic_vector;
      acc_full_o        : out std_logic;
      onu_locked_o      : out std_logic;
      onu_error_o       : out std_logic
      );
  end component bbert_top;
  
begin

  --============================================================================
  -- TX Path
  --============================================================================
  reset_tx_logic <= not olt_mgt_tx_ready_i when rising_edge(olt_clk_trxusr240_i);
  reset_rx_logic <= not olt_mgt_rx_ready_i when rising_edge(olt_clk_trxusr240_i);

  --============================================================================
  -- Component instantiation
  --! Component olt_data_gen
  --============================================================================
  cmp_olt_data_gen : olt_data_gen
    generic map(
      g_WORD_WIDTH  => 40,
      g_FRAME_WIDTH => olt_tx_data_o'length,
      g_DATA_WIDTH  => olt_tx_data_o'length
      )
    port map(
      -- global input signals --
      clk_i   => olt_clk_trxusr240_i,
      reset_i => reset_tx_logic,
      -------------------------

      -- status / control --
      sel_pattern_i    => sel_tx_pattern_txsync_r,
      constant_frame_i => constant_frame_pattern,
      -------------------------

      -- data in/out --
      data_o        => olt_tx_data,
      data_strobe_o => olt_tx_data_strobe
      -------------------------
      );

  olt_tx_data_o        <= olt_tx_data;
  olt_tx_data_strobe_o <= olt_tx_data_strobe;

  constant_frame_pattern <= x"0123456789abcdefbeefcafefedcba98765432105555555555";

  --============================================================================
  -- Process p_tx_match_flag
  -- In order to test latency, put olt_data_gen in counter mode
  -- this will generate a tx_match_flag every (modulo_byte_cntr-1)*25ns=4us
  --! read:  olt_clk_trxusr240_i, olt_tx_data_strobe, olt_tx_data
  --! write: tx_match_flag_o\n
  --! r/w:   \n
  --============================================================================          
  p_tx_match_flag : process(olt_clk_trxusr240_i) is
  begin
    if(olt_clk_trxusr240_i'event and olt_clk_trxusr240_i = '1') then
      if(olt_tx_data_strobe = '1') then
        if(olt_tx_data(7 downto 0) = x"00") then
          tx_match_flag_o <= '1';
        else
          tx_match_flag_o <= '0';
        end if;
      end if;
    end if;
  end process p_tx_match_flag;

  --============================================================================
  -- Process p_txsync
  --! read:  olt_clk_trxusr240_i, sel_tx_pattern_async
  --! write: sel_tx_pattern_txsync_r\n
  --! r/w:   sel_tx_pattern_txsync\n
  --============================================================================        
  p_txsync : process(olt_clk_trxusr240_i) is
  begin
    if(olt_clk_trxusr240_i'event and olt_clk_trxusr240_i = '1') then
      sel_tx_pattern_txsync   <= sel_tx_pattern_async;
      sel_tx_pattern_txsync_r <= sel_tx_pattern_txsync;
    end if;
  end process p_txsync;

  ------------------------------------------------------------------------------

  --============================================================================
  -- RX Path
  --============================================================================   
  --============================================================================
  -- Component instantiation
  --! Component bbert_top / prbs-7 checker
  --============================================================================ 
  gen_bbert_top : for i in 1 to rx_error_detected_onu'length generate  --16x BBERT checker
    
    cmp_bbert_top_onu : bbert_top
      generic map(
        g_CLK_CYCLES_FRAME  => 1,
        g_BLOCK_ACCUMULATOR => '0'
        )
      port map(
        clk_i   => olt_clk_trxusr240_i,
        reset_i => reset_rx_logic,

        bbert_clear_acc_i   => rx_bbert_clear_rxsync_r,
        bbert_latch_acc_i   => rx_bbert_latch_rxsync_r,
        superframe_length_i => rx_bbert_heartbeat_period_rxsync,
        onu_addr_chkr_i     => std_logic_vector(to_unsigned(i, 8)),

        data_i        => olt_rx_data,
        onu_addr_i    => olt_rx_onu_addr,
        data_strobe_i => olt_rx_data_strobe,

        error_sum_o       => rx_error_sum_latched_onu(i-1),
        bit_sum_o         => rx_bit_sum_latched_onu(i-1),
        packet_loss_sum_o => rx_packetloss_sum_latched_onu(i-1),
        acc_full_o        => open,
        onu_locked_o      => rx_prbs_locked_onu(i-1),
        onu_error_o       => rx_error_detected_onu(i-1)
        );  
  end generate gen_bbert_top;


  p_select_bbert : process(s_olt_user_axi_aclk) is
  begin
    if(s_olt_user_axi_aclk'event and s_olt_user_axi_aclk = '1') then
      rx_error_sum_latched_axisync      <= rx_error_sum_latched_onu(to_integer(unsigned(select_onu_bbert(3 downto 0))));
      rx_bit_sum_latched_axisync        <= rx_bit_sum_latched_onu(to_integer(unsigned(select_onu_bbert(3 downto 0))));
      rx_packetloss_sum_latched_axisync <= rx_packetloss_sum_latched_onu(to_integer(unsigned(select_onu_bbert(3 downto 0))));
    end if;
  end process p_select_bbert;

  olt_rx_data        <= olt_rx_data_i;
  olt_rx_onu_addr    <= olt_rx_onu_addr_i;
  olt_rx_data_strobe <= olt_rx_data_strobe_i;

  --============================================================================
  -- Process p_rxsync
  --! read:  olt_clk_trxusr240_i, rx_bbert_clear_async, rx_bbert_latch_async, rx_bbert_heartbeat_period_async
  --! write: rx_bbert_clear_rxsync_r, rx_bbert_latch_rxsync_r, rx_bbert_heartbeat_period_rxsync\n
  --! r/w:   rx_bbert_clear_rxsync, rx_bbert_latch_rxsync\n
  --============================================================================        
  p_rxsync : process(olt_clk_trxusr240_i) is
  begin
    if(olt_clk_trxusr240_i'event and olt_clk_trxusr240_i = '1') then
      rx_bbert_clear_rxsync            <= rx_bbert_clear_async;
      rx_bbert_clear_rxsync_r          <= rx_bbert_clear_rxsync;
      rx_bbert_latch_rxsync            <= rx_bbert_latch_async;
      rx_bbert_latch_rxsync_r          <= rx_bbert_latch_rxsync;
      rx_bbert_heartbeat_period_rxsync <= rx_bbert_heartbeat_period_async;
    end if;
  end process p_rxsync;
  ------------------------------------------------------------------------------


  --============================================================================
  -- AXI clk
  --============================================================================    
  --============================================================================
  -- Process p_axisync
  --! read:  s_olt_user_axi_aclk
  --! write: olt_status_reg_axisync_r\n
  --! r/w:   olt_status_reg_axisync\n
  --============================================================================    
  p_axisync : process(s_olt_user_axi_aclk) is
  begin
    if(s_olt_user_axi_aclk'event and s_olt_user_axi_aclk = '1') then
      olt_status_reg_axisync   <= olt_status_reg_async;
      olt_status_reg_axisync_r <= olt_status_reg_axisync;
    end if;
  end process p_axisync;

  --============================================================================
  -- CONTROL / STAT
  --============================================================================       
  --REGISTER0 / R/W - SYS CONTROL
  olt_core_reset_o <= ctrl_reg_i(0)(0);

  stat_reg_o(0) <= ctrl_reg_i(0);

  --REGISTER1 / R/W - RX CONTROL   
  rx_bbert_clear_async            <= ctrl_reg_i(1)(0);
  rx_bbert_latch_async            <= ctrl_reg_i(1)(1);
  rx_bbert_heartbeat_period_async <= ctrl_reg_i(1)(17 downto 2);

  stat_reg_o(1) <= ctrl_reg_i(1);


  --REGISTER2 / R/W - TX CONTROL   
  sel_tx_pattern_async <= ctrl_reg_i(2)(1 downto 0);
  stat_reg_o(2)        <= ctrl_reg_i(2);

  --REGISTER3 / R - SYS STATUS
  olt_status_reg_async(0) <= olt_mgt_pll_lock_i;
  olt_status_reg_async(1) <= olt_mgt_tx_ready_i;
  olt_status_reg_async(2) <= olt_mgt_rx_ready_i;
  olt_status_reg_async(3) <= olt_rx_locked_i;
  stat_reg_o(3)(3 downto 0)   <= olt_status_reg_axisync_r;
  stat_reg_o(3)(31 downto 4)  <= (others => '0');
  --------------------------- BBERT -----------------------------------                          
  --REGISTER4/5 / R - BIT SUM - ONU1              
  stat_reg_o(4)(31 downto 0)  <= rx_bit_sum_latched_axisync(31 downto 0);
  stat_reg_o(5)(15 downto 0)  <= rx_bit_sum_latched_axisync(47 downto 32);
  stat_reg_o(5)(31 downto 16) <= (others => '0');

  --REGISTER6/7 / R - ERROR SUM - ONU1              
  stat_reg_o(6)(31 downto 0)  <= rx_error_sum_latched_axisync(31 downto 0);
  stat_reg_o(7)(15 downto 0)  <= rx_error_sum_latched_axisync(47 downto 32);
  stat_reg_o(7)(31 downto 16) <= (others => '0');

  --REGISTER8/9 / R - PACKET LOSS SUM - ONU1              
  stat_reg_o(8)(31 downto 0)  <= rx_packetloss_sum_latched_axisync(31 downto 0);
  stat_reg_o(9)(15 downto 0)  <= rx_packetloss_sum_latched_axisync(47 downto 32);
  stat_reg_o(9)(31 downto 16) <= (others => '0');


  select_onu_bbert <= ctrl_reg_i(10)(7 downto 0);

  stat_reg_o(10) <= ctrl_reg_i(10);
  --------------------------- BBERT ----------------------------------- 
  
  
end rtl;
