--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_rx.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU RX top design (onu_rx)
--
--! @brief ONU RX top design for TTC-PON
--! <further description>
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 14\06\2016 - EBSM - first .vhd definition\n
--! 01\09\2016 - EBSM - comma -> header (FEC implementation)
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_rx
--============================================================================
entity onu_rx is
  generic(
    g_DATA_MGT_WIDTH           : integer          := 40;
    g_DATA_FRAME_WIDTH         : integer          := 180;
    g_DATA_CTRL_WIDTH          : integer          := 24;
    g_HEADERMASK               : std_logic_vector := "11111111";
    g_REGULAR_HEADER_PATTERN   : std_logic_vector := "01101010";
    g_HEARTBEAT_HEADER_PATTERN : std_logic_vector := "10011010";
    g_NBR_HEADER_ACC_MAX       : integer          := 15;
    g_NBR_HEADER_ACQUIRE_LOCK  : integer          := 13;
    g_NBR_HEADER_LOSS_LOCK     : integer          := 10
    );
  port (
    -- global input signals
    clk_trxusr240_i : in std_logic;      --! Downstream recovered 240MHz clock
    reset_i        : in std_logic;      --! sync reset active high
    -------------------------

    -- status / control --
    regular_header_o   : out std_logic;
    heartbeat_header_o : out std_logic;
    header_detect_o    : out std_logic;
    header_locked_o    : out std_logic;
    mgt_reset_o        : out std_logic;  --! control MGT - ! Latency fixed strategy might change for different devices
    rx_slide_o         : out std_logic;  --! control MGT - ! Latency fixed strategy might change for different devices 
    barrel_dout_o      : out std_logic_vector(g_DATA_MGT_WIDTH-1 downto 0);
    derror_corrected_o : out std_logic;  --! double error corrected
    serror_corrected_o : out std_logic;  --! single error corrected 
    -------------------------

    -- data in/out --
    data_mgt_i    : in  std_logic_vector(g_DATA_MGT_WIDTH-1 downto 0);
    data_strobe_o : out std_logic;
    data_frame_o  : out std_logic_vector(g_DATA_FRAME_WIDTH-1 downto 0);
    data_ctrl_o   : out std_logic_vector(g_DATA_CTRL_WIDTH-1 downto 0)
    -------------------------
    );
end entity onu_rx;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture structural of onu_rx is

  attribute MARK_DEBUG : string;

  --! Functions

  --! Constants

  --! Signal declaration
  -- word_aligner / barrel_shifter   
  signal nshift_from_wordalign           : std_logic_vector(5 downto 0);  --! barrel shifter position
  signal aligned_from_wordalign          : std_logic;  --! word aligner - aligned flag
  signal header_detect_from_wordalign    : std_logic;  --! header detect from word aligner
  signal regular_header_from_wordalign   : std_logic;  --! regular header detect from word aligner
  signal heartbeat_header_from_wordalign : std_logic;  --! heartbeat header detect from word aligner (used for TDM arbitration)
  signal dout_from_barrel                : std_logic_vector(39 downto 0);  --! shifted data from barrel shifter

  -- FEC decoder / descrambler
  signal start_decoding   : std_logic;
  signal data_fec_decoded : std_logic_vector(105 downto 0);

  signal en_descrambling  : std_logic;
  signal en_descrambling1 : std_logic;
  signal en_descrambling2 : std_logic;
  signal data_descrambled : std_logic_vector(105 downto 0);

  -- Payload Extract / Decoding scheduling
  signal data_descrambled_buf : std_logic_vector(data_descrambled'range);
  signal payload_cntr         : integer range 0 to 7;

  --! Component declaration
  component new_barrel is
    port(
      clk_i  : in  std_logic;
      din_i    : in  std_logic_vector(39 downto 0);
      nshift_i : in  std_logic_vector(5 downto 0);
      dout_o   : out std_logic_vector(39 downto 0)
      );
  end component new_barrel;

  component onu_word_align is
    generic (
      g_HEADERMASK               : std_logic_vector := "11111111";
      g_REGULAR_HEADER_PATTERN   : std_logic_vector := "01101010";
      g_HEARTBEAT_HEADER_PATTERN : std_logic_vector := "10011010";
      g_NBR_HEADER_ACC_MAX       : integer          := 15;
      g_NBR_HEADER_ACQUIRE_LOCK  : integer          := 13;
      g_NBR_HEADER_LOSS_LOCK     : integer          := 10
      );
    port (
      clk_i               : in  std_logic;
      reset_i               : in  std_logic;
      enable_i              : in  std_logic;
      nshift_o              : out std_logic_vector(5 downto 0);  -- control barrel_shifter
      aligned_o             : out std_logic;  -- control barrel_shifter
      reset_gtx_o           : out std_logic;  -- control gtx
      rx_slide_o            : out std_logic := '0';              -- control gtx
      header_detect_o       : out std_logic;
      rx_regular_header_o   : out std_logic;
      rx_heartbeat_header_o : out std_logic;
      rxdata_i              : in  std_logic_vector(39 downto 0)
      );
  end component onu_word_align;

  component dec_bch120_106 is
    port (
      clk_i              : in  std_logic;  --! clock input
      reset_i            : in  std_logic;  --! active high sync. reset
      start_dec_i        : in  std_logic;  --! control scheduling
      data_i             : in  std_logic_vector(39 downto 0);   --! input data
      data_undecoded_o   : out std_logic_vector(105 downto 0);  --! output data undecoded
      data_o             : out std_logic_vector(105 downto 0);  --! output data decoded
      derror_corrected_o : out std_logic;  --! double error corrected
      serror_corrected_o : out std_logic   --! single error corrected
      );
  end component dec_bch120_106;

  component descrambling is
    generic (
      g_PARAL_FACTOR  : integer := 40;
      g_HEADER_LENGTH : integer := 8
      );
    port (
      clk_i            : in  std_logic;  --! clock input
      en_i             : in  std_logic;  --! enable input
      data_is_header_i : in  std_logic;  --! data is header -> bypass descrambler
      data_i           : in  std_logic_vector(g_PARAL_FACTOR-1 downto 0);  --! input data
      data_o           : out std_logic_vector(g_PARAL_FACTOR-1 downto 0)  --! descrambled output data
      );
  end component descrambling;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component Barrel Shifter
  --============================================================================   
  cmp_barrel : new_barrel
    port map(
      clk_i  => clk_trxusr240_i,
      din_i    => data_mgt_i,
      nshift_i => nshift_from_wordalign,
      dout_o   => dout_from_barrel
      );
  barrel_dout_o <= dout_from_barrel;

  --============================================================================
  -- Component instantiation
  --! Component Word Aligner
  --============================================================================   
  cmp_word_aligner : onu_word_align
    generic map(
      g_HEADERMASK               => g_HEADERMASK,
      g_REGULAR_HEADER_PATTERN   => g_REGULAR_HEADER_PATTERN,
      g_HEARTBEAT_HEADER_PATTERN => g_HEARTBEAT_HEADER_PATTERN,
      g_NBR_HEADER_ACC_MAX       => g_NBR_HEADER_ACC_MAX,
      g_NBR_HEADER_ACQUIRE_LOCK  => g_NBR_HEADER_ACQUIRE_LOCK,
      g_NBR_HEADER_LOSS_LOCK     => g_NBR_HEADER_LOSS_LOCK
      )
    port map(
      clk_i               => clk_trxusr240_i,
      reset_i               => reset_i,
      enable_i              => '1',
      nshift_o              => nshift_from_wordalign,
      aligned_o             => aligned_from_wordalign,
      reset_gtx_o           => mgt_reset_o,
      rx_slide_o            => rx_slide_o,
      header_detect_o       => header_detect_from_wordalign,
      rx_regular_header_o   => regular_header_from_wordalign,
      rx_heartbeat_header_o => heartbeat_header_from_wordalign,
      rxdata_i              => dout_from_barrel
      );

  --============================================================================
  -- Component instantiation
  --! Component FEC BCH(120,106) decoder
  --============================================================================   
  cmp_dec_bch120_106 : dec_bch120_106
    port map(
      clk_i              => clk_trxusr240_i,
      reset_i            => reset_i,
      start_dec_i        => start_decoding,
      data_i             => data_mgt_i,
      data_undecoded_o   => open,
      data_o             => data_fec_decoded,
      derror_corrected_o => derror_corrected_o,
      serror_corrected_o => serror_corrected_o
      );

  --============================================================================
  -- Component instantiation
  --! Component Self-synchronous descrambler
  --============================================================================   
  cmp_descrambling : descrambling
    generic map(
      g_PARAL_FACTOR  => 106,
      g_HEADER_LENGTH => g_REGULAR_HEADER_PATTERN'length
      )
    port map(
      clk_i            => clk_trxusr240_i,
      en_i             => en_descrambling,
      data_is_header_i => en_descrambling1,
      data_i           => data_fec_decoded,
      data_o           => data_descrambled
      );

  p_payload_cntr : process (clk_trxusr240_i) is
  begin
    if(clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(aligned_from_wordalign = '0') then
        payload_cntr <= 5;
      else
        if (header_detect_from_wordalign = '1') then
          payload_cntr <= 5;
        elsif(payload_cntr = 0) then
          payload_cntr <= 5;
        else
          payload_cntr <= payload_cntr - 1;
        end if;
      end if;
    end if;
  end process p_payload_cntr;

  p_scheduling_decoder : process (clk_trxusr240_i) is
  begin
    if(clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(aligned_from_wordalign = '0') then
        start_decoding   <= '0';
        en_descrambling1 <= '0';
        en_descrambling2 <= '0';
      else
        if(payload_cntr = 2 or payload_cntr = 5) then
          start_decoding <= '1';
        else
          start_decoding <= '0';
        end if;

        if(payload_cntr = 1) then
          en_descrambling1 <= '1';
        else
          en_descrambling1 <= '0';
        end if;

        if(payload_cntr = 4) then
          en_descrambling2 <= '1';
        else
          en_descrambling2 <= '0';
        end if;
      end if;
    end if;
  end process p_scheduling_decoder;
  en_descrambling <= en_descrambling1 or en_descrambling2;

  p_output_data : process (clk_trxusr240_i) is
  begin
    if(clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(en_descrambling1 = '1') then
        data_descrambled_buf <= data_descrambled;
      end if;

      if(en_descrambling2 = '1') then
        data_frame_o  <= data_descrambled&data_descrambled_buf(data_descrambled_buf'left downto g_DATA_CTRL_WIDTH+g_REGULAR_HEADER_PATTERN'length);
        data_ctrl_o   <= data_descrambled_buf(g_DATA_CTRL_WIDTH+g_REGULAR_HEADER_PATTERN'length-1 downto g_REGULAR_HEADER_PATTERN'length);
        data_strobe_o <= '1';
      else
        data_strobe_o <= '0';
      end if;
    end if;
  end process p_output_data;

  --============================================================================
  --Comma output
  --============================================================================
  heartbeat_header_o <= heartbeat_header_from_wordalign;
  regular_header_o   <= regular_header_from_wordalign;
  header_detect_o    <= header_detect_from_wordalign;
  header_locked_o    <= aligned_from_wordalign;


end architecture structural;
--============================================================================
-- architecture end
--============================================================================
