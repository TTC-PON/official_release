--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file new_barrel.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Barrel shifter 40b (new_barrel)
--
--! @brief 40b barrel shifter - left shifter
--! <further description>
--!
--! @author Fr�d�ric Marin (code provided by Paschalis Vichoudis)
--! @date 25\09\2008
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Dimitrios-Marios Kolotouros
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 25\09\2008 - FM - Created\n
--! 29\01\2013 - DMK - \n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for new_barrel
--==============================================================================                                        
entity new_barrel is
  port(
    clk_i  : in  std_logic;
    din_i    : in  std_logic_vector(39 downto 0);
    nshift_i : in  std_logic_vector(5 downto 0);
    dout_o   : out std_logic_vector(39 downto 0));
end new_barrel;

--==============================================================================
-- architecture declaration
--==============================================================================
architecture rtl of new_barrel is

  signal previous_word : std_logic_vector(39 downto 0);

begin

  ----------------------------------------------------------------------------
  --! Process Barrel Shifter
  --! read: nshift_i\n
  --! write: dout_o\n
  --! r/w:   previous_word\n
  ----------------------------------------------------------------------------
  process (clk_i)
  begin
    if rising_edge(clk_i) then
      previous_word <= din_i;
      case nshift_i is
        when "000000" =>                -- 0
          dout_o <= din_i;
        when "000001" =>                -- 1
          dout_o <= din_i(38 downto 0) & previous_word(39 downto 39);
        when "000010" =>                -- 2
          dout_o <= din_i(37 downto 0) & previous_word(39 downto 38);
        when "000011" =>                -- 3
          dout_o <= din_i(36 downto 0) & previous_word(39 downto 37);
        when "000100" =>                -- 4
          dout_o <= din_i(35 downto 0) & previous_word(39 downto 36);
        when "000101" =>                -- 5
          dout_o <= din_i(34 downto 0) & previous_word(39 downto 35);
        when "000110" =>                -- 6
          dout_o <= din_i(33 downto 0) & previous_word(39 downto 34);
        when "000111" =>                -- 7
          dout_o <= din_i(32 downto 0) & previous_word(39 downto 33);
        when "001000" =>                -- 8
          dout_o <= din_i(31 downto 0) & previous_word(39 downto 32);
        when "001001" =>                -- 9
          dout_o <= din_i(30 downto 0) & previous_word(39 downto 31);
        when "001010" =>                -- 10
          dout_o <= din_i(29 downto 0) & previous_word(39 downto 30);
        when "001011" =>                -- 11
          dout_o <= din_i(28 downto 0) & previous_word(39 downto 29);
        when "001100" =>                -- 12
          dout_o <= din_i(27 downto 0) & previous_word(39 downto 28);
        when "001101" =>                -- 13
          dout_o <= din_i(26 downto 0) & previous_word(39 downto 27);
        when "001110" =>                -- 14
          dout_o <= din_i(25 downto 0) & previous_word(39 downto 26);
        when "001111" =>                -- 15
          dout_o <= din_i(24 downto 0) & previous_word(39 downto 25);
        when "010000" =>                -- 16
          dout_o <= din_i(23 downto 0) & previous_word(39 downto 24);
        when "010001" =>                -- 17
          dout_o <= din_i(22 downto 0) & previous_word(39 downto 23);
        when "010010" =>                -- 18
          dout_o <= din_i(21 downto 0) & previous_word(39 downto 22);
        when "010011" =>                -- 19
          dout_o <= din_i(20 downto 0) & previous_word(39 downto 21);
        when "010100" =>                -- 20
          dout_o <= din_i(19 downto 0) & previous_word(39 downto 20);
        when "010101" =>                -- 21
          dout_o <= din_i(18 downto 0) & previous_word(39 downto 19);
        when "010110" =>                -- 22
          dout_o <= din_i(17 downto 0) & previous_word(39 downto 18);
        when "010111" =>                -- 23
          dout_o <= din_i(16 downto 0) & previous_word(39 downto 17);
        when "011000" =>                -- 24
          dout_o <= din_i(15 downto 0) & previous_word(39 downto 16);
        when "011001" =>                -- 25
          dout_o <= din_i(14 downto 0) & previous_word(39 downto 15);
        when "011010" =>                -- 26
          dout_o <= din_i(13 downto 0) & previous_word(39 downto 14);
        when "011011" =>                -- 27
          dout_o <= din_i(12 downto 0) & previous_word(39 downto 13);
        when "011100" =>                -- 28
          dout_o <= din_i(11 downto 0) & previous_word(39 downto 12);
        when "011101" =>                -- 29
          dout_o <= din_i(10 downto 0) & previous_word(39 downto 11);
        when "011110" =>                -- 30
          dout_o <= din_i(9 downto 0) & previous_word(39 downto 10);
        when "011111" =>                -- 31
          dout_o <= din_i(8 downto 0) & previous_word(39 downto 9);
        when "100000" =>                -- 32
          dout_o <= din_i(7 downto 0) & previous_word(39 downto 8);
        when "100001" =>                -- 33
          dout_o <= din_i(6 downto 0) & previous_word(39 downto 7);
        when "100010" =>                -- 34
          dout_o <= din_i(5 downto 0) & previous_word(39 downto 6);
        when "100011" =>                -- 35
          dout_o <= din_i(4 downto 0) & previous_word(39 downto 5);
        when "100100" =>                -- 36
          dout_o <= din_i(3 downto 0) & previous_word(39 downto 4);
        when "100101" =>                -- 37
          dout_o <= din_i(2 downto 0) & previous_word(39 downto 3);
        when "100110" =>                -- 38
          dout_o <= din_i(1 downto 0) & previous_word(39 downto 2);
        when "100111" =>                -- 39
          dout_o <= din_i(0 downto 0) & previous_word(39 downto 1);
        when others =>                  -- 40
          dout_o <= previous_word;
      end case;
    end if;
  end process;
end rtl;
