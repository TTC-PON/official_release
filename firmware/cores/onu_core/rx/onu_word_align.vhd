--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file onu_word_align.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.pon_onu_package_modifiable.c_IS_SIMULATION;
--------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
--------------------------------------------------------------------------------
--
-- unit name: ONU word aligner (onu_word_align)
--
--! @brief This module performs word alignment in the ONU receiver path.
--! The word alignment module controls the barrel shifter, in order to align
--! the incoming data. The word alignment is successful, if the header character
--! is found.
--
--! @author Csaba Soos (csaba.soos AT cern.ch)
--
--! @date 13-07-2009
--
--! @version 1.0
--
--! @details
--!
--! <b>Dependencies:</b>\n
--! 
--!
--! <b>References:</b>\n
--!
--! <b>Modified by:</b>\n
--! Author: Csaba Soos
--------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 13-07-2009 CS  Created\n
--! 01-09-2016 EBSM -Modifications for FEC implementation:
--!                    -word_counter is a signal instead of variable to be used outside the process
--!                    -when locked, only look for headers when word_counter(8)='1'
--!                    -comma -> header
--!                    -increase lock window (make it parametrisable as generic)
--!                    -modify @brief
--! 23-05-2017 EBSM - header_detect_o modified (to review the code)
--------------------------------------------------------------------------------
--! @todo -\n
--! -
--------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for ONU word aligner
--==============================================================================
entity onu_word_align is
  generic (
    g_HEADERMASK               : std_logic_vector := "11111111";
    g_REGULAR_HEADER_PATTERN   : std_logic_vector := "01101010";
    g_HEARTBEAT_HEADER_PATTERN : std_logic_vector := "10011010";
    g_NBR_HEADER_ACC_MAX       : integer          := 15;
    g_NBR_HEADER_ACQUIRE_LOCK  : integer          := 13;
    g_NBR_HEADER_LOSS_LOCK     : integer          := 10
    );
  port (
    clk_i               : in  std_logic;
    reset_i               : in  std_logic;
    enable_i              : in  std_logic;
    nshift_o              : out std_logic_vector(5 downto 0);  -- control barrel_shifter
    aligned_o             : out std_logic;  -- control barrel_shifter
    reset_gtx_o           : out std_logic;  -- control gtx
    rx_slide_o            : out std_logic := '0';              -- control gtx
    header_detect_o       : out std_logic;
    rx_regular_header_o   : out std_logic;
    rx_heartbeat_header_o : out std_logic;
    rxdata_i              : in  std_logic_vector(39 downto 0)
    );
end entity onu_word_align;

--==============================================================================
-- architecture declaration
--==============================================================================
architecture rtl of onu_word_align is

  attribute MARK_DEBUG : string;
  
  type t_alignment_state is (
    RESET,
    SEARCH,
    ROTATE,
    WAITING,
    RESET_GTX,
    GTX_BITSLIDE,
    GTX_BITSLIDE_WAIT,
    GTX_BITSLIDE_TEST,
    ALIGNED);
  signal align_FSM_state : t_alignment_state;

  signal header_detect : std_logic;

  signal rx_heartbeat_header_dec_r : std_logic;
  signal rx_regular_header_dec_r   : std_logic;

  signal word_counter : unsigned(8 downto 0);

  signal frame_aligned  : std_logic;
  signal sliding_done   : std_logic;
  signal search_timeout : std_logic;
  signal rotate_count     : unsigned(5 downto 0);
  signal reset_gtx_aux_delay  : integer range 0 to 15;
  signal gtx_slide_count  : unsigned(5 downto 0);
  signal gtx_slide_delay  : integer range 0 to 63;
  signal reset_gtx_aux : std_logic := '0';


--==============================================================================
-- architecture begin
--==============================================================================
begin  -- architecture rtl
--    header_detect_o <= header_detect;
-- Debugging purposes: (add -> word_align_FSM_o : out std_logic_vector(2 downto 0) to port declaration)
--       word_align_FSM_o <= "000" when align_FSM_state = RESET     else --##############
--                           "001" when align_FSM_state = SEARCH    else --##############
--                           "010" when align_FSM_state = ROTATE    else --##############
--                           "011" when align_FSM_state = WAITING   else --##############
--                           "100" when align_FSM_state = ALIGNED   else --##############
--                           "111";  

  ----------------------------------------------------------------------------
  --! Process Alignment FSM (state transitions)
  --! read:  clk_i, reset_i, frame_aligned, search_timeout\n
  --!        sliding_done\n
  --! write: \n
  --! r/w:   align_FSM_state\n
  ----------------------------------------------------------------------------
  p_align_FSM_transitions : process (clk_i) is
  begin  -- process p_align_FSM_transitions
    if clk_i'event and clk_i = '1' then  -- rising clock edge
      if reset_i = '1' then
        align_FSM_state <= RESET;
        reset_gtx_aux_delay   <= 0;
        gtx_slide_delay   <= 0;
        gtx_slide_count   <= (others => '0');
        reset_gtx_aux       <= '0';
        rx_slide_o        <= '0';
      else
        reset_gtx_aux       <= '0';
        rx_slide_o        <= '0';
        align_FSM_state <= align_FSM_state;
        case align_FSM_state is

          when RESET =>
            if enable_i = '1' then
              align_FSM_state <= SEARCH;
            end if;

          when SEARCH =>
            reset_gtx_aux_delay <= 0;
            gtx_slide_count <= 40-rotate_count;
            if frame_aligned = '1' then
              if rotate_count = 0 then
                align_FSM_state <= ALIGNED;
              elsif rotate_count(0) = '1' then
                align_FSM_state <= RESET_GTX;
              else
                align_FSM_state <= GTX_BITSLIDE;
              end if;
            elsif search_timeout = '1' then
              align_FSM_state <= ROTATE;
            end if;
            
          when ROTATE =>
            align_FSM_state <= WAITING;

          when WAITING =>
            if sliding_done = '1' then
              align_FSM_state <= SEARCH;
            end if;

          when RESET_GTX =>
            reset_gtx_aux     <= '1';
            reset_gtx_aux_delay <= reset_gtx_aux_delay + 1;
            if reset_gtx_aux_delay = 6 then
              align_FSM_state <= RESET;
            else
              align_FSM_state <= RESET_GTX;
            end if;

          when GTX_BITSLIDE =>
            rx_slide_o        <= '1';
            gtx_slide_delay   <= 0;
            gtx_slide_count   <= gtx_slide_count - 1;
            align_FSM_state <= GTX_BITSLIDE_WAIT;

          when GTX_BITSLIDE_WAIT =>
            gtx_slide_delay <= gtx_slide_delay + 1;
            if gtx_slide_delay = 32 then
              align_FSM_state <= GTX_BITSLIDE_TEST;
            else
              align_FSM_state <= GTX_BITSLIDE_WAIT;
            end if;

          when GTX_BITSLIDE_TEST =>
            if gtx_slide_count > 0 then
              align_FSM_state <= GTX_BITSLIDE;
            else
              align_FSM_state <= RESET;
            end if;

          when ALIGNED =>
            if enable_i = '1' and frame_aligned = '0' then
              align_FSM_state <= RESET;
            end if;

          when others =>
            align_FSM_state <= RESET;

        end case;
      end if;
    end if;
  end process p_align_FSM_transitions;

  aligned_o <= '1' when align_FSM_state = ALIGNED else '0';

  reset_gtx_o <= reset_gtx_aux;
  ----------------------------------------------------------------------------
  --! Process Rotate control
  --! read:  clk_i, reset_i, align_FSM_state\n
  --! write: nshift_o\n
  --! r/w:   \n
  ----------------------------------------------------------------------------
  p_rotate_control : process (clk_i, reset_i) is
  begin  -- process p_rotate_control
    if reset_i = '1' then               -- asynchronous reset (active low)
      nshift_o     <= (others => '0');
      rotate_count <= (others => '0');
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if align_FSM_state = ROTATE then
        if rotate_count < 39 then
          rotate_count <= rotate_count + 1;
        else
          rotate_count <= (others => '0');
        end if;
	  elsif(align_FSM_state = RESET) then
          rotate_count <= (others => '0');
      end if;
      nshift_o <= std_logic_vector(rotate_count);
    end if;
  end process p_rotate_control;

  ----------------------------------------------------------------------------
  --! Process Header detect
  --! read:  clk_i, reset_i, rxdata_i\n
  --! write: header_detect\n
  --! r/w:   \n
  ----------------------------------------------------------------------------
  p_header_detect : process (clk_i, reset_i) is
  begin  -- process p_comma_detect
    if reset_i = '1' then               -- asynchronous reset (active high)
      header_detect           <= '0';
      rx_regular_header_dec_r   <= '0';
      rx_heartbeat_header_dec_r <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      -- header_detect_o          <= header_detect and word_counter(8);
      rx_heartbeat_header_o <= rx_heartbeat_header_dec_r and word_counter(8);
      rx_regular_header_o   <= rx_regular_header_dec_r and word_counter(8);

      if align_FSM_state = ALIGNED then
        
        header_detect_o <= word_counter(8);
        
        if ((rxdata_i(g_REGULAR_HEADER_PATTERN'length-1 downto 0) and g_HEADERMASK) = (g_REGULAR_HEADER_PATTERN and g_HEADERMASK) or
            (rxdata_i(g_REGULAR_HEADER_PATTERN'length-1 downto 0) and g_HEADERMASK) = (g_HEARTBEAT_HEADER_PATTERN and g_HEADERMASK)) and (word_counter="000000000") then
          header_detect <= '1';
        else
          header_detect <= '0';
        end if;

        if (rxdata_i(g_REGULAR_HEADER_PATTERN'length-1 downto 0) = g_REGULAR_HEADER_PATTERN) and (word_counter = "000000000") then
          rx_regular_header_dec_r <= '1';
        else
          rx_regular_header_dec_r <= '0';
        end if;

        if (rxdata_i(g_REGULAR_HEADER_PATTERN'length-1 downto 0) = g_HEARTBEAT_HEADER_PATTERN) and (word_counter = "000000000") then
          rx_heartbeat_header_dec_r <= '1';
        else
          rx_heartbeat_header_dec_r <= '0';
        end if;
        
      else
        
        header_detect_o <= header_detect and word_counter(8);

        rx_heartbeat_header_dec_r <= '0';
        rx_regular_header_dec_r   <= '0';

        if (rxdata_i(g_REGULAR_HEADER_PATTERN'length-1 downto 0) and g_HEADERMASK) = (g_REGULAR_HEADER_PATTERN and g_HEADERMASK) or
          (rxdata_i(g_REGULAR_HEADER_PATTERN'length-1 downto 0) and g_HEADERMASK) = (g_HEARTBEAT_HEADER_PATTERN and g_HEADERMASK) then
          header_detect <= '1';
        else
          header_detect <= '0';
        end if;
        
      end if;
    end if;
  end process p_header_detect;

  ----------------------------------------------------------------------------
  --! Process Frame check
  --! read:  clk_i, reset_i, header_detect\n
  --! write: frame_aligned\n
  --! r/w:   word_counter\n
  ----------------------------------------------------------------------------
  p_frame_check : process (clk_i, reset_i) is
    variable v_good_frame_counter : integer range 0 to g_NBR_HEADER_ACC_MAX;
  begin  -- process p_frame_check
    if reset_i = '1' then               -- asynchronous reset (active high)
      frame_aligned      <= '0';
      word_counter       <= "000000100";      --010000000
      v_good_frame_counter := 0;
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      
      if v_good_frame_counter < g_NBR_HEADER_LOSS_LOCK or align_FSM_state = RESET then
        frame_aligned <= '0';
      elsif v_good_frame_counter > g_NBR_HEADER_ACQUIRE_LOCK then
        frame_aligned <= '1';
      else
        frame_aligned <= frame_aligned;
      end if;

      if align_FSM_state = RESET then
        v_good_frame_counter := 0;
      else
        if word_counter(8) = '1' then
          if header_detect = '1' and v_good_frame_counter < g_NBR_HEADER_ACC_MAX then
            v_good_frame_counter := v_good_frame_counter + 1;
          elsif header_detect = '0' and v_good_frame_counter > 0 then
            v_good_frame_counter := v_good_frame_counter - 1;
          end if;
        elsif header_detect = '1' then
          if v_good_frame_counter > 0 then
            v_good_frame_counter := v_good_frame_counter - 1;
          end if;
        end if;
      end if;

      if align_FSM_state = ALIGNED then
        if word_counter(8) = '1' then
          word_counter <= "000000100";  --010000000
        else
          word_counter <= word_counter - 1;
        end if;
      else
        if header_detect = '1' or word_counter(8) = '1' then
          word_counter <= "000000100";  --010000000
        else
          word_counter <= word_counter - 1;
        end if;
      end if;
      
    end if;
  end process p_frame_check;

  ----------------------------------------------------------------------------
  --! Process Search timer
  --! read:  clk_i, reset_i, align_FSM_state\n
  --! write: search_timeout\n
  --! r/w:   \n
  ----------------------------------------------------------------------------
  p_search_timer : process (clk_i, reset_i) is
    variable v_search_time_counter : unsigned(10 downto 0);
  begin  -- process p_search_timer
    if reset_i = '1' then               -- asynchronous reset (active high)
      search_timeout      <= '0';
	  if(c_IS_SIMULATION) then
        v_search_time_counter := "00011111111"; 
      else
        v_search_time_counter := "01111111111";
      end if;		
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      search_timeout <= v_search_time_counter(10);
      if align_FSM_state = SEARCH then
        v_search_time_counter := v_search_time_counter - 1;
      else
	    if(c_IS_SIMULATION) then
          v_search_time_counter := "00011111111"; 
        else
          v_search_time_counter := "01111111111";
        end if;	  
      end if;
    end if;
  end process p_search_timer;

  ----------------------------------------------------------------------------
  --! Process Wait timer
  --! read:  clk_i, reset_i, align_FSM_state\n
  --! write: sliding_done\n
  --! r/w:   \n
  ----------------------------------------------------------------------------
--    p_wait_timer: process (clk_i, reset_i) is
--        variable v_wait_time_counter : unsigned(2 downto 0);
--    begin  -- process p_wait_timer
--        if reset_i = '1' then         -- asynchronous reset (active high)
--            sliding_done <= '0';
--            v_wait_time_counter := "011";
--        elsif clk_i'event and clk_i = '1' then  -- rising clock edge
--            sliding_done <= v_wait_time_counter(2);
--            if align_FSM_state = WAITING then
--                v_wait_time_counter := v_wait_time_counter - 1;
--            else
--                v_wait_time_counter := "011";
--            end if;
--        end if;
--    end process p_wait_timer;



  ----------------------------------------------------------------------------
  --! Process Wait timer
  --! read:  clk_i, reset_i, align_FSM_state\n
  --! write: sliding_done\n
  --! r/w:   \n
  ----------------------------------------------------------------------------
  p_wait_timer : process (clk_i, reset_i) is
    variable v_wait_time_counter : unsigned(3 downto 0);
    constant delay               : unsigned(3 downto 0) := x"4";
  begin  -- process p_wait_timer
    if reset_i = '1' then               -- asynchronous reset (active high)
      sliding_done      <= '0';
      v_wait_time_counter := x"0";
    elsif clk_i'event and clk_i = '1' then           -- rising clock edge
      if align_FSM_state = SEARCH then
        sliding_done      <= '0';
        v_wait_time_counter := delay;   -- load down counter
      elsif align_FSM_state = WAITING then
        if v_wait_time_counter /= x"0" then
          v_wait_time_counter := v_wait_time_counter-1;  -- count down when not zero
        else
          sliding_done <= '1';
      end if;
    end if;
  end if;
end process;

end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================

