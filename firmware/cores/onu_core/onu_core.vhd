--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_core.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.protocol_package.all;
use work.pon_onu_package_static.all;
use work.pon_onu_package_modifiable.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU core design (onu_core)
--
--! @brief ONU core design for TTC-PON
--! <further description>
--
--! @author 
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: 
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_core
--============================================================================
entity onu_core is                      -- use work.pon_onu_package.all;
  port (
    -- global input signals --
    clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
    core_reset_i : in std_logic;
    -------------------------

    -- manager control/stat -- see manager core interfacing just below for more information about how to interface the core control
    clk_manager_i         : in  std_logic;
    manager_stat_reg_o    : out t_regbank32b(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
    manager_ctrl_reg_i    : in  std_logic_vector(31 downto 0);
    manager_ctrl_strobe_i : in  std_logic_vector(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
    --------------------------

    -- status / control --
    onu_address_i         : in  std_logic_vector(7 downto 0);  --! ONU address in TDM
    phase_good_o          : out std_logic;  --! clk_trxusr240_i to clk_trxusr240_i CDC phase scanning
    rx_locked_o           : out std_logic;  --! downstream header aligned
    onu_operational_o     : out std_logic;  --! given by OLT once system init (calibration) is done; this means that ONU is allowed to transmit
    rx_serror_corrected_o : out std_logic;  --! a single-error was corrected by the FEC
    rx_derror_corrected_o : out std_logic;  --! a double-error was corrected by the FEC

    -- Soft-configurable interrupt output
    interrupt_o     : out std_logic;
    status_sticky_o : out std_logic_vector(30 downto 0);
    -------------------------     

    -- MGT connections -----------
    -- Clocks
    clk_trxusr240_i : in std_logic;      --! User_TRx 240MHz clock       

    -- Status/Ctrl
    mgt_reset_o      : out std_logic;
    mgt_tx_ready_i   : in std_logic;
    mgt_rx_ready_i   : in std_logic;
    mgt_txpll_lock_i : in std_logic;
    mgt_rxpll_lock_i : in std_logic;
    mgt_rxslide_o    : out std_logic;

    -- Xilinx specific
    mgt_drp_wr_o            : out std_logic_vector(31 downto 0);
    mgt_drp_wr_strobe_o     : out std_logic;
    mgt_drp_monitor_i       : in  std_logic_vector(31 downto 0);
    mgt_rx_equalizer_ctrl_o : out std_logic_vector(31 downto 0);
    mgt_rx_equalizer_stat_i : in  std_logic_vector(31 downto 0);
    mgt_tx_phase_ctrl_o     : out std_logic_vector(31 downto 0);
    mgt_tx_phase_stat_i     : in  std_logic_vector(31 downto 0);

    -- Data
    mgt_tx_data_o : out std_logic_vector(c_ONU_TX_MGT_DATA_WIDTH-1 downto 0);
    mgt_rx_data_i : in  std_logic_vector(c_ONU_RX_MGT_DATA_WIDTH-1 downto 0);
    ------------------------      

    -- data in/out --  -- see TX usr core interfacing / RX usr core just below for more information about how to interface the core
    tx_data_strobe_i : in  std_logic;
    tx_data_ready_o  : out std_logic;
    tx_data_i        : in  std_logic_vector(c_ONU_TX_USR_DATA_WIDTH-1 downto 0);
    rx_data_strobe_o : out std_logic;
    rx_data_frame_o  : out std_logic_vector(c_ONU_RX_USR_DATA_WIDTH-1 downto 0);
    -------------------------

    -- sfp data / control --
    sfp_rx_los_i   : in  std_logic;
    sfp_tx_fault_i : in  std_logic;
    sfp_mod_abs_i  : in  std_logic;
    sfp_tx_sd_i    : in  std_logic;
    sfp_pdown_o    : out std_logic;
    sfp_tx_dis_o   : out std_logic;
    -------------------------

    --i2c interface
    i2c_req_o      : out std_logic;
    i2c_rnw_o      : out std_logic;
    i2c_rb2_o      : out std_logic;
    i2c_slv_addr_o : out std_logic_vector(6 downto 0);
    i2c_reg_addr_o : out std_logic_vector(7 downto 0);
    i2c_wr_data_o  : out std_logic_vector(7 downto 0);

    i2c_done_i     : in std_logic;
    i2c_error_i    : in std_logic;
    i2c_drop_req_i : in std_logic;
    i2c_rd_data_i  : in std_logic_vector(15 downto 0)
    -------------------------
    );
end entity onu_core;

------------------------------------ TX USR CORE INTERFACING --------------------------------------  (sync to clk_trxusr240_i)
-- ------------------------------------------------------------------------------------------------
-- * To select the interfacing mode for data_usr_i (word interfacing or frame_interfacing)
--   See below timing for tx_data_i interfacing: each .____. represents a clk_trxusr240_i clock period
--
-- ------------------------------- Timing input/output (clocked out)-------------------------------
--
--                     ______________________________________________________________
-- onu_operational_o _/
--
-- *** (1) STROBE BEFORE READY (strobe (REQ) -> ready (ACK)) ***  
--                                       _______________________                                             
-- tx_data_strobe_i  _________.____.____/                       \____.____.____.____.
--
-- tx_data_i       X   PREVIOUS DATA    X        NEW DATA                           X                         
--                                                           ___                     
-- tx_data_ready_o  __________.____.____.____.____.____.____/   \____.____.____.____.
--
--    
-- *** (2) READY BEFORE STROBE (ready -> strobe) ***  
--                                                           ___                                              
-- tx_data_strobe_i  __________.____.____.____.____.____.___/   \____.____.____.____. 
--
-- tx_data_i       X   PREVIOUS DATA                       X     NEW DATA          X                              
--                                       _______________________                      
-- tx_data_ready_o   _________.____.____/                       \____.____.____.____. 
--    
--
-- *** (3) READY-STROBE AT THE SAME TIME (strobe - ready) ***  
--                                                          ___                      
-- tx_data_strobe_i __________.____.____.____.____.____.___/   \____.____.____.____. 
--
-- tx_data_i       X   PREVIOUS DATA                       X     NEW DATA          X 
--                                                          ___                      
-- tx_data_ready_o  __________.____.____.____.____.____.___/   \____.____.____.____. 
--
--
-- *** (4) READY BUT NO STROBE (capture data falling edge) ***  
--                                                                  
-- tx_data_strobe_i __________.____.____.____.____.____.____.____.____.____.____.___
--
-- tx_data_i      X   PREVIOUS DATA                                               X
--                                       ______________________________                     
-- tx_data_ready_o  __________.____.____/                              \___.____.___
--                                                                   |
-- ----------------------------------------------------------------------------------

------------------------------------ RX USR CORE INTERFACING -------------------------------------- (sync to clk_trxusr240_i)
-- Rx usr interfacing - each .____. represents a clk_trxusr240_i clock period
-- ------------------------------- Timing input/output ------------------------------- 
--
-- rx_data_frame_o                              X             DATA0           X-        DATA1
--                                               ___                           ___ 
-- rx_data_strobe_o    \____.____.____.____.____/   \____.____.____.____._____/   \
--
--                          ___________________________________________________________
-- rx_locked_o  ___________/
-- -----------------------------------------------------------------------------------     

-- ### CONTROL CORE INTERFACING:
------------------------------------ MANAGER CORE INTERFACING -------------------------------------- (sync to clk_rxusr120_o)
-- Manager core interfacing - each .____. represents a clk_manager_i clock period
-- ------------------------------- Timing input/output --------------------------------------------
--
-- CONTROL WRITE VALUES: 
-- onu_control.vhd has a register bank composed by 64 x 32b registers which are used for core control by the manager
-- in order to write a value to a specific register, user should keep manager_ctrl_strobe(REG_ADDR) to '1' for a single clk_manager_i clock cycle
-- and put the value to write in manager_ctrl_reg_i
--
-- Note: manager_ctrl_strobe is a hot_one encoding of the address user wants to write
--
-- Example - write value CW2 to control register #2:
--
-- CONTROL WRITE REGISTERS:
--                                                                                             
-- manager_ctrl_strobe_i(0)         _.____.____.____.____.____ ...____.____.____.____.____.____.____
--                                                                                                
-- manager_ctrl_strobe_i(1)         _.____.____.____.____.____ ...____.____.____.____.____.____.____
--                                              ___                                                  
-- manager_ctrl_strobe_i(2)         _.____.____/   \____.____  ...____.____.____.____.____.____.____
--                                                                                                
-- manager_ctrl_strobe_i(3)         _.____.____.____.____.____ ...____.____.____.____.____.____.____
--
--         ...
--                                                                                              
-- manager_ctrl_strobe_i(63)        _.____.____.____.____.____ ...____.____.____.____.____.____.____
--
-- manager_ctrl_reg_i(31 downto 0) X          x CW2 x                                               X
--
--
-- STATUS READ VALUES:
-- onu_control.vhd has a register bank composed by 16 x 32b registers
--
-- --------------------------------------------------------------------------------------------------   

--============================================================================
-- ! architecture declaration
--============================================================================
architecture structural of onu_core is

  attribute mark_debug : string;

  --! Functions

  --! Constants

  --! Signal declaration

  -- global from onu_rx
  signal rx_header_locked : std_logic;
  signal rx_data_strobe   : std_logic;

  --global from phy
  signal phy_reset_rxclk : std_logic;
  signal phy_reset_txclk : std_logic;

  -- global from onu_control
  signal ctrl_onu_operational : std_logic;

  --onu_control <-> onu_tx
  signal ctrl_to_tx_frame_ena      : std_logic;
  signal ctrl_to_tx_calibration_on : std_logic;
  signal ctrl_to_tx_slow_beat      : std_logic;
  signal ctrl_to_tx_fast_beat      : std_logic;
  signal ctrl_to_tx_barrel_pos     : std_logic_vector(3 downto 0);
  signal tx_to_ctrl_ready          : std_logic;
  signal ctrl_to_tx_data_usr_ovr   : std_logic_vector(c_ONU_TX_USR_DATA_WIDTH-1 downto 0);
  signal ctrl_to_tx_data           : std_logic_vector(c_ONU_TX_CTRL_DATA_WIDTH-1 downto 0);

  attribute mark_debug of tx_to_ctrl_ready : signal is "true";

  --onu_rx <-> onu_control
  signal rx_to_ctrl_regular_header   : std_logic;
  signal rx_to_ctrl_heartbeat_header : std_logic;
  signal rx_to_ctrl_header_detect    : std_logic;
  signal rx_to_ctrl_derror_corrected : std_logic;
  signal rx_to_ctrl_serror_corrected : std_logic;
  signal rx_to_ctrl_data             : std_logic_vector(c_ONU_RX_CTRL_DATA_WIDTH-1 downto 0);

  --onu_rx <-> onu_phy_control
  signal rx_to_phy_mgt_reset : std_logic;  --Note: this signal passes through phy control for synchronization purposes   

  --onu_control <-> onu_phy_control
  signal ctrl_to_phy_tx_frame_ena     : std_logic;
  signal ctrl_to_phy_tx_continuous_en : std_logic;
  signal ctrl_to_phy_tx_off           : std_logic;
  signal ctrl_to_phy_tx_en_pos        : std_logic_vector(3 downto 0);

  signal ctrl_to_phy_i2c_ctrl    : std_logic_vector(31 downto 0);
  signal ctrl_to_phy_i2c_ctrl_en : std_logic;
  signal phy_to_ctrl_i2c_stat    : std_logic_vector(31 downto 0);

  --! Component instantiation
  component onu_tx is
    generic(
      g_PREAMBLE_LEN_CLK_CYCLES : integer := 13;
      g_DATA_MGT_WIDTH          : integer := 10;
      g_DATA_USR_WIDTH          : integer := 48;
      g_DATA_CTRL_WIDTH         : integer := 16
      );
    port (
      -- global input signals --
      clk_trxusr240_i : in std_logic;
      reset_i        : in std_logic;
      -------------------------   

      -- status / control --
      frame_ena_i         : in std_logic;  --! tx burst enable
      calibration_ena_i   : in std_logic;  --! calibration on
      slow_beat_i         : in std_logic;  --! slow beat during calibration (K28.1)
      fast_beat_i         : in std_logic;  --! fast beat during calibration (K28.5)
      tx_barrel_pos_i     : in std_logic_vector(3 downto 0);  --! 20b barrel shifter position
      onu_addr_i          : in std_logic_vector(7 downto 0);  --! onu address
      interface_usr_ena_i : in std_logic;  --! enable user interface data
      -------------------------   

      -- data in/out --
      data_usr_strobe_i : in  std_logic;
      data_usr_ready_o  : out std_logic;
      data_ctrl_ready_o : out std_logic;
      data_usr_ovr_i    : in  std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);
      data_usr_i        : in  std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);
      data_ctrl_i       : in  std_logic_vector(g_DATA_CTRL_WIDTH-1 downto 0);
      data_mgt_o        : out std_logic_vector(g_DATA_MGT_WIDTH-1 downto 0)
      -------------------------

      );
  end component onu_tx;

  component onu_rx is
    generic(
      g_DATA_MGT_WIDTH           : integer          := 40;
      g_DATA_FRAME_WIDTH         : integer          := 180;
      g_DATA_CTRL_WIDTH          : integer          := 24;
      g_HEADERMASK               : std_logic_vector := "11111111";
      g_REGULAR_HEADER_PATTERN   : std_logic_vector := "01101010";
      g_HEARTBEAT_HEADER_PATTERN : std_logic_vector := "10011010";
      g_NBR_HEADER_ACC_MAX       : integer          := 15;
      g_NBR_HEADER_ACQUIRE_LOCK  : integer          := 13;
      g_NBR_HEADER_LOSS_LOCK     : integer          := 10
      );
    port (
      -- global input signals
      clk_trxusr240_i : in std_logic;    --! Downstream recovered 240MHz clock
      reset_i        : in std_logic;    --! sync reset active high
      -------------------------

      -- status / control --
      regular_header_o   : out std_logic;
      heartbeat_header_o : out std_logic;
      header_detect_o    : out std_logic;
      header_locked_o    : out std_logic;
      mgt_reset_o        : out std_logic;  --! control MGT - ! Latency fixed strategy might change for different devices
      rx_slide_o         : out std_logic;  --! control MGT - ! Latency fixed strategy might change for different devices 
      barrel_dout_o      : out std_logic_vector(g_DATA_MGT_WIDTH-1 downto 0);
      derror_corrected_o : out std_logic;  --! double error corrected
      serror_corrected_o : out std_logic;  --! single error corrected 
      -------------------------

      -- data in/out --
      data_mgt_i    : in  std_logic_vector(g_DATA_MGT_WIDTH-1 downto 0);
      data_strobe_o : out std_logic;
      data_frame_o  : out std_logic_vector(g_DATA_FRAME_WIDTH-1 downto 0);
      data_ctrl_o   : out std_logic_vector(g_DATA_CTRL_WIDTH-1 downto 0)
      -------------------------
      );
  end component onu_rx;

  component onu_control is
    generic(
      g_ENABLE_MANAGER     : std_logic := '1';  -- 1 = ON / 0 = OFF
      g_NBR_ONU_CTRL_BYTE  : integer   := 32;
      g_RX_DATA_CTRL_WIDTH : integer   := 24;
      g_TX_DATA_CTRL_WIDTH : integer   := 16;
      g_TX_DATA_USR_WIDTH  : integer   := 48
      );
    port (
      -- global input signals --
      clk_manager_i  : in std_logic;
      clk_trxusr240_i : in std_logic;
      clk_sys_i      : in std_logic;

      reset_txclk_i : in std_logic;
      reset_rxclk_i : in std_logic;
      -------------------------

      -- control from manager --
      manager_wr_strobe_i : in  std_logic_vector(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
      manager_wr_reg_i    : in  std_logic_vector(31 downto 0);
      manager_stat_reg_o  : out t_regbank32b(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
      --------------------------

      -- status / control --
      -- core_top
      onu_address_i     : in  std_logic_vector(7 downto 0);
      onu_operational_o : out std_logic;

      -- onu_rx
      rx_locked_i           : in std_logic;
      rx_comma_detect_i     : in std_logic;
      rx_regular_header_i   : in std_logic;
      rx_heartbeat_header_i : in std_logic;
      rx_serror_corrected_i : in std_logic;
      rx_derror_corrected_i : in std_logic;

      -- onu_tx  
      tx_ena_o            : out std_logic;
      tx_calibration_on_o : out std_logic;
      tx_slow_beat_o      : out std_logic;
      tx_fast_beat_o      : out std_logic;
      tx_nb_barrel_o      : out std_logic_vector(3 downto 0);

      -- onu_phy          
      phy_tx_ena_o     : out std_logic;
      phy_tx_cont_en_o : out std_logic;
      phy_tx_off_o     : out std_logic;
      phy_tx_en_pos_o  : out std_logic_vector(3 downto 0);

      phy_i2c_ctrl_o    : out std_logic_vector(31 downto 0);
      phy_i2c_ctrl_en_o : out std_logic;
      phy_i2c_stat_i    : in  std_logic_vector(31 downto 0);

      -- mgt
      mgt_drp_wr_o            : out std_logic_vector(31 downto 0);
      mgt_drp_wr_strobe_o     : out std_logic;
      mgt_drp_monitor_i       : in  std_logic_vector(31 downto 0);
      mgt_rx_equalizer_ctrl_o : out std_logic_vector(31 downto 0);
      mgt_rx_equalizer_stat_i : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_o     : out std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_i     : in  std_logic_vector(31 downto 0);

      mgt_tx_ready_i   : in std_logic;
      mgt_rx_ready_i   : in std_logic;
      mgt_txpll_lock_i : in std_logic;
      mgt_rxpll_lock_i : in std_logic;

      -- Soft-configurable interrupt output
      interrupt_o     : out std_logic;
      status_sticky_o : out std_logic_vector(30 downto 0);
      -------------------------

      -- data in/out --
      rx_data_strobe_i : in std_logic;
      rx_data_ctrl_i   : in std_logic_vector(g_RX_DATA_CTRL_WIDTH-1 downto 0);

      tx_data_ctrl_ready_i : in  std_logic;
      tx_data_usr_ovr_o    : out std_logic_vector(g_TX_DATA_USR_WIDTH-1 downto 0);
      tx_data_ctrl_o       : out std_logic_vector(g_TX_DATA_CTRL_WIDTH-1 downto 0)
      -------------------------
      );
  end component onu_control;

  component onu_phy_control is
    generic(
      g_CLK_SYS_PERIOD : integer range 5 to 20 := 10  -- clock period in ns
      );
    port (
      -- global input signals --
      clk_sys_i      : in std_logic;
      clk_trxusr240_i : in std_logic;

      core_reset_i : in std_logic;
      -------------------------

      -- global output signals --        
      reset_rxclk_o : out std_logic;
      reset_txclk_o : out std_logic;
      reset_mgt_o   : out std_logic;
      -------------------------

      -- status / control --
      -- from mgt
      mgt_rx_ready_i : in std_logic;
      mgt_tx_ready_i : in std_logic;

      -- from onu_rx
      header_locked_i : in std_logic;
      rx_mgt_reset_i  : in std_logic;

      -- onu_ctrl
      tx_ena_i           : in std_logic;
      sfp_enable_delay_i : in std_logic_vector(3 downto 0);
      sfp_tx_cont_en_i   : in std_logic;
      sfp_tx_off_i       : in std_logic;

      -- sfp
      sfp_rx_los_i   : in  std_logic;
      sfp_tx_fault_i : in  std_logic;
      sfp_mod_abs_i  : in  std_logic;
      sfp_tx_sd_i    : in  std_logic;
      sfp_pdown_o    : out std_logic;
      sfp_tx_dis_o   : out std_logic;

      --i2c interface
      i2c_ctrl_i    : in  std_logic_vector(31 downto 0);
      i2c_ctrl_en_i : in  std_logic;
      i2c_stat_o    : out std_logic_vector(31 downto 0);

      -- user interface
      i2c_req_o      : out std_logic;
      i2c_rnw_o      : out std_logic;
      i2c_rb2_o      : out std_logic;
      i2c_slv_addr_o : out std_logic_vector(6 downto 0);
      i2c_reg_addr_o : out std_logic_vector(7 downto 0);
      i2c_wr_data_o  : out std_logic_vector(7 downto 0);

      i2c_done_i     : in std_logic;
      i2c_error_i    : in std_logic;
      i2c_drop_req_i : in std_logic;
      i2c_rd_data_i  : in std_logic_vector(15 downto 0)
      -------------------------
      );
  end component onu_phy_control;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component onu_tx
  --============================================================================ 
  cmp_onu_tx : onu_tx
    generic map(
      g_PREAMBLE_LEN_CLK_CYCLES => c_PREAMBLE_LEN_TX_CLK_CYCLES,
      g_DATA_MGT_WIDTH          => c_ONU_TX_MGT_DATA_WIDTH,
      g_DATA_USR_WIDTH          => c_ONU_TX_USR_DATA_WIDTH,
      g_DATA_CTRL_WIDTH         => c_ONU_TX_CTRL_DATA_WIDTH
      )
    port map(
      -- global input signals --
      clk_trxusr240_i => clk_trxusr240_i,
      reset_i        => phy_reset_txclk,
      -------------------------   

      -- status / control --
      frame_ena_i         => ctrl_to_tx_frame_ena,
      calibration_ena_i   => ctrl_to_tx_calibration_on,
      slow_beat_i         => ctrl_to_tx_slow_beat,
      fast_beat_i         => ctrl_to_tx_fast_beat,
      tx_barrel_pos_i     => ctrl_to_tx_barrel_pos,
      onu_addr_i          => onu_address_i,
      interface_usr_ena_i => ctrl_onu_operational,
      -------------------------   

      -- data in/out --
      data_usr_strobe_i => tx_data_strobe_i,
      data_usr_ready_o  => tx_data_ready_o,
      data_ctrl_ready_o => tx_to_ctrl_ready,
      data_usr_ovr_i    => ctrl_to_tx_data_usr_ovr,
      data_usr_i        => tx_data_i,
      data_ctrl_i       => ctrl_to_tx_data,
      data_mgt_o        => mgt_tx_data_o
      -------------------------

      );

  --============================================================================
  -- Component instantiation
  --! Component onu_rx
  --============================================================================
  cmp_onu_rx : onu_rx
    generic map(
      g_DATA_MGT_WIDTH           => c_ONU_RX_MGT_DATA_WIDTH,
      g_DATA_FRAME_WIDTH         => c_ONU_RX_USR_DATA_WIDTH,
      g_DATA_CTRL_WIDTH          => c_ONU_RX_CTRL_DATA_WIDTH,
      g_HEADERMASK               => c_DOWN_HEADERMASK,
      g_REGULAR_HEADER_PATTERN   => c_DOWN_REGULAR_HEADER,
      g_HEARTBEAT_HEADER_PATTERN => c_DOWN_HEARTBEAT_HEADER,
      g_NBR_HEADER_ACC_MAX       => c_DOWN_NBR_HEADER_ACC_MAX,
      g_NBR_HEADER_ACQUIRE_LOCK  => c_DOWN_NBR_HEADER_ACQUIRE_LOCK,
      g_NBR_HEADER_LOSS_LOCK     => c_DOWN_NBR_HEADER_LOSS_LOCK
      )
    port map(
      -- global input signals
      clk_trxusr240_i => clk_trxusr240_i,
      reset_i        => phy_reset_rxclk,
      -------------------------

      -- status / control --
      regular_header_o   => rx_to_ctrl_regular_header,
      heartbeat_header_o => rx_to_ctrl_heartbeat_header,
      header_detect_o    => rx_to_ctrl_header_detect,
      header_locked_o    => rx_header_locked,
      mgt_reset_o        => rx_to_phy_mgt_reset,
      rx_slide_o         => mgt_rxslide_o,
      barrel_dout_o      => open,
      derror_corrected_o => rx_to_ctrl_derror_corrected,
      serror_corrected_o => rx_to_ctrl_serror_corrected,
      -------------------------

      -- data in/out --
      data_mgt_i    => mgt_rx_data_i,
      data_strobe_o => rx_data_strobe,
      data_frame_o  => rx_data_frame_o,
      data_ctrl_o   => rx_to_ctrl_data
      -------------------------
      );


  --============================================================================
  -- Component instantiation
  --! Component onu_control
  --============================================================================          
  cmp_onu_control : onu_control
    generic map(
      g_ENABLE_MANAGER     => c_ONU_ENABLE_MANAGER,
      g_NBR_ONU_CTRL_BYTE  => c_ONU_NBR_CTRL_BYTES,
      g_RX_DATA_CTRL_WIDTH => c_ONU_RX_CTRL_DATA_WIDTH,
      g_TX_DATA_CTRL_WIDTH => c_ONU_TX_CTRL_DATA_WIDTH,
      g_TX_DATA_USR_WIDTH  => c_ONU_TX_USR_DATA_WIDTH
      )
    port map(
      -- global input signals --
      clk_manager_i  => clk_manager_i,
      clk_trxusr240_i => clk_trxusr240_i,
      clk_sys_i      => clk_sys_i,

      reset_txclk_i => phy_reset_txclk,
      reset_rxclk_i => phy_reset_rxclk,
      -------------------------

      -- control from manager --
      manager_wr_strobe_i => manager_ctrl_strobe_i,
      manager_wr_reg_i    => manager_ctrl_reg_i,
      manager_stat_reg_o  => manager_stat_reg_o,
      --------------------------

      -- status / control --
      -- core_top
      onu_address_i     => onu_address_i,
      onu_operational_o => ctrl_onu_operational,

      -- onu_rx
      rx_locked_i           => rx_header_locked,
      rx_comma_detect_i     => rx_to_ctrl_header_detect,
      rx_regular_header_i   => rx_to_ctrl_regular_header,
      rx_heartbeat_header_i => rx_to_ctrl_heartbeat_header,
      rx_serror_corrected_i => rx_to_ctrl_serror_corrected,
      rx_derror_corrected_i => rx_to_ctrl_derror_corrected,

      -- onu_tx  
      tx_ena_o            => ctrl_to_tx_frame_ena,
      tx_calibration_on_o => ctrl_to_tx_calibration_on,
      tx_slow_beat_o      => ctrl_to_tx_slow_beat,
      tx_fast_beat_o      => ctrl_to_tx_fast_beat,
      tx_nb_barrel_o      => ctrl_to_tx_barrel_pos,

      -- onu_phy          
      phy_tx_ena_o     => ctrl_to_phy_tx_frame_ena,
      phy_tx_cont_en_o => ctrl_to_phy_tx_continuous_en,
      phy_tx_off_o     => ctrl_to_phy_tx_off,
      phy_tx_en_pos_o  => ctrl_to_phy_tx_en_pos,

      phy_i2c_ctrl_o    => ctrl_to_phy_i2c_ctrl,
      phy_i2c_ctrl_en_o => ctrl_to_phy_i2c_ctrl_en,
      phy_i2c_stat_i    => phy_to_ctrl_i2c_stat,

      -- mgt
      mgt_drp_wr_o            => mgt_drp_wr_o           ,   
      mgt_drp_wr_strobe_o     => mgt_drp_wr_strobe_o    ,
      mgt_drp_monitor_i       => mgt_drp_monitor_i      ,
      mgt_rx_equalizer_ctrl_o => mgt_rx_equalizer_ctrl_o,
      mgt_rx_equalizer_stat_i => mgt_rx_equalizer_stat_i,
      mgt_tx_phase_ctrl_o     => mgt_tx_phase_ctrl_o    ,
      mgt_tx_phase_stat_i     => mgt_tx_phase_stat_i    ,

      mgt_tx_ready_i   => mgt_tx_ready_i,
      mgt_rx_ready_i   => mgt_rx_ready_i,
      mgt_txpll_lock_i => mgt_txpll_lock_i,
      mgt_rxpll_lock_i => mgt_rxpll_lock_i,

      -- Soft-configurable interrupt output
      interrupt_o     => interrupt_o,
      status_sticky_o => status_sticky_o,
      -------------------------

      -- data in/out --
      rx_data_strobe_i => rx_data_strobe,
      rx_data_ctrl_i   => rx_to_ctrl_data,

      tx_data_ctrl_ready_i => tx_to_ctrl_ready,
      tx_data_usr_ovr_o    => ctrl_to_tx_data_usr_ovr,
      tx_data_ctrl_o       => ctrl_to_tx_data
      -------------------------
      );

  --============================================================================
  -- Component instantiation
  --! Component onu_phy_ctrl
  --============================================================================          
  cmp_onu_phy_control : onu_phy_control
    generic map(
      g_CLK_SYS_PERIOD => c_ONU_PERIOD_SYS_CLK
      )
    port map(
      -- global input signals --
      clk_sys_i      => clk_sys_i,
      clk_trxusr240_i => clk_trxusr240_i,
      core_reset_i   => core_reset_i,
      -------------------------

      -- global output signals --        
      reset_rxclk_o => phy_reset_rxclk,
      reset_txclk_o => phy_reset_txclk,
      reset_mgt_o   => mgt_reset_o,
      -------------------------

      -- status / control --
      -- from mgt
      mgt_rx_ready_i => mgt_rx_ready_i,
      mgt_tx_ready_i => mgt_tx_ready_i,

      -- from onu_rx
      header_locked_i => rx_header_locked,
      rx_mgt_reset_i  => rx_to_phy_mgt_reset,

      -- onu_ctrl
      tx_ena_i           => ctrl_to_phy_tx_frame_ena,
      sfp_enable_delay_i => ctrl_to_phy_tx_en_pos,
      sfp_tx_cont_en_i   => ctrl_to_phy_tx_continuous_en,
      sfp_tx_off_i       => ctrl_to_phy_tx_off,

      -- sfp
      sfp_rx_los_i   => sfp_rx_los_i,
      sfp_tx_fault_i => sfp_tx_fault_i,
      sfp_mod_abs_i  => sfp_mod_abs_i,
      sfp_tx_sd_i    => sfp_tx_sd_i,
      sfp_pdown_o    => sfp_pdown_o,
      sfp_tx_dis_o   => sfp_tx_dis_o,

      -- i2c interface
      i2c_ctrl_i    => ctrl_to_phy_i2c_ctrl,
      i2c_ctrl_en_i => ctrl_to_phy_i2c_ctrl_en,
      i2c_stat_o    => phy_to_ctrl_i2c_stat,

      i2c_req_o      => i2c_req_o,
      i2c_rnw_o      => i2c_rnw_o,
      i2c_rb2_o      => i2c_rb2_o,
      i2c_slv_addr_o => i2c_slv_addr_o,
      i2c_reg_addr_o => i2c_reg_addr_o,
      i2c_wr_data_o  => i2c_wr_data_o,

      i2c_done_i     => i2c_done_i,
      i2c_error_i    => i2c_error_i,
      i2c_drop_req_i => i2c_drop_req_i,
      i2c_rd_data_i  => i2c_rd_data_i
      -------------------------
      );

  --============================================================================
  -- Output
  --============================================================================ 
  rx_data_strobe_o  <= rx_data_strobe;
  rx_locked_o       <= rx_header_locked;
  onu_operational_o <= ctrl_onu_operational;

  rx_serror_corrected_o <= rx_to_ctrl_serror_corrected;
  rx_derror_corrected_o <= rx_to_ctrl_derror_corrected;

end architecture structural;
--============================================================================
-- architecture end
--============================================================================
