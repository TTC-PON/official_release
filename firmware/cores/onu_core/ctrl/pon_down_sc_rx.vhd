--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file pon_down_sc_rx.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages
use work.fec_package.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Slow control decoder design for TTC-PON downstream (pon_down_sc_rx)
--
--! @brief Slow control decoder design for TTC-PON downstream including CRC-7 protection
--! - It assembles the control frame from nibbles
--! - It finds automatically the boundaries of the control frame by using a CRC-framing technique
--! - This design can be re-used for any data_o width, respecting the following rule: mod(data_o'length+7,4)=0
--! - CRC-7 is an error detection code with the following properties:
--! - any triple-error detection
--! - burst error detection up to 7 bits
--! - the codeword space vector are the polynomials divisible by g(x) = 1 + x + x^2 + x^4 + x^5 + x^7
--! - the properties above are true for data_o'length up to 56 bits
--! - more information on the CRC polynomial design can be found under the paper "Cyclic Redundancy Code (CRC) Polynomial Selection for Embedded Networks"
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 03\05\2017
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 03\05\2017 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for pon_down_sc_rx
--==============================================================================
entity pon_down_sc_rx is
  generic(
    g_CRC_GOOD_TO_LOCK  : integer := 15;
    g_CRC_BAD_TO_UNLOCK : integer := 5
    );
  port (
    clk_i             : in  std_logic;  --! clock input
    reset_i           : in  std_logic;  --! active high sync. reset
    nibble_rcvd_i     : in  std_logic;  --! control scheduling
    data_i            : in  std_logic_vector(3 downto 0);  --! input data         
    rcvd_ctrl_frame_o : out std_logic;  --! user control
    rcvd_ctrl_error_o : out std_logic;  --! error detected CRC
    framing_sync_o    : out std_logic;  --! CRC-framing is synchronized
    data_o            : out std_logic_vector(28 downto 0)  --! user output data
    );
end pon_down_sc_rx;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of pon_down_sc_rx is

  --! Constant declaration
  constant c_MOD_NIBBLE_CNTR : integer := (data_o'length+7)/(data_i'length);

  --! Signal declaration
  -- CRC related signals
  signal en_crc_calcul    : std_logic;
  signal reset_crc_calcul : std_logic;
  signal crc_error_detect : std_logic;

  signal parity_bits : std_logic_vector(6 downto 0);

  -- frame reconstitution
  signal nibble_cntr : integer range 0 to c_MOD_NIBBLE_CNTR;

  -- reconstituted frame
  signal data_r : std_logic_vector(data_o'left+7 downto 0);

  -- FSM CRC-framing
  -- principle:
  -- HUNT            : received a correct CRC                       -> GOING_SYNC
  --                   received a wrong CRC                         -> SKIP CYCLE
  -- SKIP_CYCLE_ST   : retards the nibble_cntr by 1 and then        -> SKIP_CYCLE_ST_WAIT
  -- SKIP_CYCLE_ST_WAIT : waits for CRC to be reset and then        -> HUNT
  -- GOING_SYNC      : received a consecutive number of correct CRC -> SYNC
  --                   received a wrong CRC                         -> HUNT   
  -- SYNC            : received a wrong CRC                         -> GOING_HUNT
  -- GOING_HUNT      : received a consecutive number of wrong CRC   -> HUNT
  --                   received a correct CRC                       -> SYNC

  type   t_CRC_FRAMING_FSM_STATE is (HUNT, SKIP_CYCLE_ST, SKIP_CYCLE_ST_WAIT, GOING_SYNC, SYNC, GOING_HUNT);
  signal crc_framing_state : t_CRC_FRAMING_FSM_STATE;

  signal framing_sync : std_logic;

  signal skip_cycle : std_logic;

  signal correct_crc_count : integer range 0 to (g_CRC_GOOD_TO_LOCK + 1);
  signal wrong_crc_count   : integer range 0 to (g_CRC_BAD_TO_UNLOCK + 1);

begin

  --============================================================================
  -- Process p_crc_framing_fsm
  --! FSM for CRC-framing locking procedure
  --! read: nibble_rcvd_i, reset_crc_calcul, correct_crc_count, wrong_crc_count, crc_error_detect\n
  --! write: -\n
  --! r/w: crc_framing_state \n
  --============================================================================  
  p_crc_framing_fsm : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        crc_framing_state <= HUNT;
      else
        case crc_framing_state is
          when HUNT =>
            if(reset_crc_calcul = '1') then
              if(crc_error_detect = '1') then
                crc_framing_state <= SKIP_CYCLE_ST;
              else
                crc_framing_state <= GOING_SYNC;
              end if;
            end if;
            
          when SKIP_CYCLE_ST =>
            if(nibble_rcvd_i = '1') then
              crc_framing_state <= SKIP_CYCLE_ST_WAIT;
            end if;
          when SKIP_CYCLE_ST_WAIT =>
            if(reset_crc_calcul = '1') then
              crc_framing_state <= HUNT;
            end if;
          when GOING_SYNC =>
            if(reset_crc_calcul = '1') then
              if(correct_crc_count >= g_CRC_GOOD_TO_LOCK) then
                crc_framing_state <= SYNC;
              elsif(crc_error_detect = '1') then
                crc_framing_state <= HUNT;
              end if;
            end if;
          when SYNC =>
            if(reset_crc_calcul = '1') then
              if(crc_error_detect = '1') then
                crc_framing_state <= GOING_HUNT;
              end if;
            end if;
          when GOING_HUNT =>
            if(reset_crc_calcul = '1') then
              if(wrong_crc_count >= g_CRC_BAD_TO_UNLOCK) then
                crc_framing_state <= HUNT;
              elsif(crc_error_detect = '0') then
                crc_framing_state <= SYNC;
              end if;
            end if;
          when others => crc_framing_state <= HUNT;
        end case;
      end if;
    end if;
  end process p_crc_framing_fsm;

  --============================================================================
  -- Process p_crc_framing_fsm_aux
  --! counters for consecutive correct/wrong CRC frame
  --! read: nibble_sent_i, crc_framing_state\n
  --! write: - \n
  --! r/w: correct_crc_count, wrong_crc_count \n
  --============================================================================    
  p_crc_framing_fsm_aux : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        correct_crc_count <= 0;
        wrong_crc_count   <= 0;
      elsif(reset_crc_calcul = '1') then
        if(crc_framing_state = GOING_HUNT) then
          if(crc_error_detect = '1') then
            wrong_crc_count <= wrong_crc_count + 1;
          end if;
        else
          wrong_crc_count <= 0;
        end if;

        if(crc_framing_state = GOING_SYNC) then
          if(crc_error_detect = '0') then
            correct_crc_count <= correct_crc_count + 1;
          end if;
        else
          correct_crc_count <= 0;
        end if;
      end if;
    end if;
  end process p_crc_framing_fsm_aux;

  skip_cycle   <= '1' when (crc_framing_state = SKIP_CYCLE_ST)                             else '0';
  framing_sync   <= '1' when (crc_framing_state = SYNC or crc_framing_state = GOING_HUNT) else '0';
  framing_sync_o <= framing_sync;

  --============================================================================
  -- Process p_nibble_cntr
  --! counter for scheduling
  --! read: nibble_rcvd_i, skip_cycle\n
  --! write: -\n
  --! r/w: nibble_cntr \n
  --============================================================================   
  p_nibble_cntr : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        nibble_cntr <= 0;
      elsif(nibble_rcvd_i = '1' and skip_cycle = '0') then
        if(nibble_cntr = (c_MOD_NIBBLE_CNTR-1)) then
          nibble_cntr <= 0;
        else
          nibble_cntr <= nibble_cntr + 1;
        end if;
      end if;
    end if;
  end process p_nibble_cntr;

  --============================================================================
  -- Process p_data_r
  --! register data input + sr
  --! read: nibble_rcvd_i, data_i\n
  --! write: en_crc_calcul\n
  --! r/w: data_r \n
  --============================================================================   
  p_data_r : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      en_crc_calcul <= nibble_rcvd_i;
      if(nibble_rcvd_i = '1') then
        for i in data_r'left-4 downto 0 loop
          data_r(i) <= data_r(i+4);
        end loop;
        data_r(data_r'left downto data_r'left-3) <= data_i;
      end if;
    end if;
  end process p_data_r;

  reset_crc_calcul <= '1' when (en_crc_calcul = '1' and nibble_cntr = c_MOD_NIBBLE_CNTR-1) else '0';

  --============================================================================
  -- Component instantiation - crc7 generator
  --! Component cmp_poly_div
  --! Calculates r(x) = rem(data_i/(g(x)))
  --============================================================================
  cmp_poly_div : poly_div
    generic map(
      g_DIVISOR_POLY   => c_CRC7_POLY,
      g_DIVIDEND_WIDTH => 4
      )
    port map(
      clk_i       => clk_i,
      reset_i     => reset_crc_calcul,
      enable_i    => en_crc_calcul,
      dividend_i  => data_r(data_r'left downto data_r'left-3),
      quotient_o  => open,
      remainder_o => parity_bits
      );

  crc_error_detect <= '1' when (parity_bits /= c_CRC7_HEADER and reset_crc_calcul = '1') else '0';

  --============================================================================
  -- Process p_interface_user_out
  --! user output interface
  --! read: framing_sync, reset_crc_calcul, crc_error_detect, data_r\n
  --! write: rcvd_ctrl_frame_o, data_o, rcvd_ctrl_error_o\n
  --! r/w: - \n
  --============================================================================     
  p_interface_user_out : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(framing_sync = '1') then
        rcvd_ctrl_frame_o <= reset_crc_calcul;
        if(reset_crc_calcul = '1')then
          data_o            <= data_r(data_o'range);
          rcvd_ctrl_error_o <= crc_error_detect;
        else
          rcvd_ctrl_error_o <= '0';
        end if;
      else
        data_o            <= (others => '0');
        rcvd_ctrl_frame_o <= '0';
        rcvd_ctrl_error_o <= '0';
      end if;
    end if;
  end process p_interface_user_out;
  
end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================

