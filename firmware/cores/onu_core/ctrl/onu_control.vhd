--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_control.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.protocol_package.all;
use work.pon_onu_package_static.all;
use work.pon_onu_package_modifiable.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU Control top design (onu_control)
--
--! @brief ONU Control top design for TTC-PON
--! <further description>
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--
--! @date 29\06\2016
--
--! @version 1.0
--
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 29\06\2016 - EBSM - \n
--! 15\01\2018 - EBSM - Unified tx and rx clock domain
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_control
--============================================================================
entity onu_control is                         -- use work.ttc_pon_package.all;
  generic(
    g_ENABLE_MANAGER     : std_logic := '1';  -- 1 = ON / 0 = OFF
    g_NBR_ONU_CTRL_BYTE  : integer   := 256;
    g_RX_DATA_CTRL_WIDTH : integer   := 24;
    g_TX_DATA_CTRL_WIDTH : integer   := 16;
    g_TX_DATA_USR_WIDTH  : integer   := 48
    );
  port (
    -- global input signals --
    clk_manager_i  : in std_logic;
    clk_trxusr240_i : in std_logic;
    clk_sys_i      : in std_logic;

    reset_txclk_i : in std_logic;
    reset_rxclk_i : in std_logic;
    -------------------------

    -- global output signals --
    -------------------------

    -- control from manager --
    manager_wr_strobe_i : in  std_logic_vector(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
    manager_wr_reg_i    : in  std_logic_vector(31 downto 0);
    manager_stat_reg_o  : out t_regbank32b(c_ONU_NBR_CTRL_BYTES/4-1 downto 0);
    --------------------------

    -- status / control --
    -- core_top
    onu_address_i     : in  std_logic_vector(7 downto 0);
    onu_operational_o : out std_logic;

    -- onu_rx
    rx_locked_i           : in std_logic;
    rx_comma_detect_i     : in std_logic;
    rx_regular_header_i   : in std_logic;
    rx_heartbeat_header_i : in std_logic;
    rx_serror_corrected_i : in std_logic;
    rx_derror_corrected_i : in std_logic;

    -- onu_tx  
    tx_ena_o            : out std_logic;
    tx_calibration_on_o : out std_logic;
    tx_slow_beat_o      : out std_logic;
    tx_fast_beat_o      : out std_logic;
    tx_nb_barrel_o      : out std_logic_vector(3 downto 0);

    -- onu_phy    
    phy_tx_ena_o     : out std_logic;
    phy_tx_cont_en_o : out std_logic;
    phy_tx_off_o     : out std_logic;
    phy_tx_en_pos_o  : out std_logic_vector(3 downto 0);

    phy_i2c_ctrl_o    : out std_logic_vector(31 downto 0);
    phy_i2c_ctrl_en_o : out std_logic;
    phy_i2c_stat_i    : in  std_logic_vector(31 downto 0);

    -- mgt
    mgt_drp_wr_o            : out std_logic_vector(31 downto 0);
    mgt_drp_wr_strobe_o     : out std_logic;
    mgt_drp_monitor_i       : in  std_logic_vector(31 downto 0);
    mgt_rx_equalizer_ctrl_o : out std_logic_vector(31 downto 0);
    mgt_rx_equalizer_stat_i : in  std_logic_vector(31 downto 0);
    mgt_tx_phase_ctrl_o     : out std_logic_vector(31 downto 0);
    mgt_tx_phase_stat_i     : in  std_logic_vector(31 downto 0);

    mgt_tx_ready_i   : in std_logic;
    mgt_rx_ready_i   : in std_logic;
    mgt_txpll_lock_i : in std_logic;
    mgt_rxpll_lock_i : in std_logic;

    -- Soft-configurable interrupt output
    interrupt_o     : out std_logic;
    status_sticky_o : out std_logic_vector(30 downto 0);
    -------------------------

    -- data in/out --
    rx_data_strobe_i : in std_logic;
    rx_data_ctrl_i   : in std_logic_vector(g_RX_DATA_CTRL_WIDTH-1 downto 0);

    tx_data_ctrl_ready_i : in  std_logic;
    tx_data_usr_ovr_o    : out std_logic_vector(g_TX_DATA_USR_WIDTH-1 downto 0);
    tx_data_ctrl_o       : out std_logic_vector(g_TX_DATA_CTRL_WIDTH-1 downto 0)
    -------------------------
    );
end entity onu_control;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of onu_control is

  attribute ASYNC_REG  : string;
  attribute MARK_DEBUG : string;
  attribute KEEP       : string;

  --! Constants
  --! Functions   
  function fcn_or_reduce(arg : std_logic_vector) return std_logic is
    variable v_result : std_logic;
  begin
    v_result := '0';
    for i in arg'range loop
      v_result := v_result or arg(i);
    end loop;
    return v_result;
  end;

  --! Signal declaration
  --============================================================================
  -- Manager Control (logic is only generated if g_ENABLE_MANAGER='1')
  --============================================================================ 
  signal wr_from_manager_strobe : std_logic_vector(g_NBR_ONU_CTRL_BYTE/4-1 downto 0);
  signal wr_from_manager_data   : std_logic_vector(manager_wr_reg_i'range);
  signal wr_from_manager_p      : std_logic;

  --============================================================================
  -- Slow Control
  --============================================================================
  --  clocked by clk_trxusr240_i
  signal decoded_ctrl_data_async    : std_logic_vector(c_CTRL_DATA_WIDTH-1 downto 0);
  signal decoded_ctrl_p_async       : std_logic;
  signal decoded_ctrl_error_p_async : std_logic;
  signal sc_framing_locked_async    : std_logic;
  signal sc_from_olt_async          : std_logic_vector(c_CTRL_DATA_WIDTH downto 0);

  -- clocked by clk_manager_i
  signal sc_framing_locked_mngrsync_meta                                               : std_logic;
  signal sc_framing_locked_mngrsync_r                                                  : std_logic;
  attribute async_reg of sc_framing_locked_mngrsync_meta, sc_framing_locked_mngrsync_r : signal is "true";

  signal sc_from_olt_mngrsync : std_logic_vector(c_CTRL_DATA_WIDTH downto 0);
  signal decoded_ctrl_data    : std_logic_vector(c_CTRL_DATA_WIDTH-1 downto 0);
  signal decoded_ctrl_p       : std_logic;
  signal decoded_ctrl_error_p : std_logic;

  -- Mailbox clk_manager_i -> clk_trxusr240_i (send requested reg_bank value - read or write w/ acknowledgment)
  signal data_send_async      : std_logic_vector(c_CTRL_DATA_WIDTH-1 downto 0);
  signal ready_send_p_async   : std_logic;
  signal data_send_trxsync     : std_logic_vector(c_CTRL_DATA_WIDTH-1 downto 0);
  signal accept_send_p_trxsync : std_logic;

  --============================================================================
  -- ONU REGBANK (clocked by clk_manager_i)
  --============================================================================   
  -- Register bank accessible through OLT:
  type   t_onu_regbank is array ((g_NBR_ONU_CTRL_BYTE-1) downto 0) of std_logic_vector(7 downto 0);
  signal onu_regbank_ctrl : t_onu_regbank := (others => (others => '0'));
  signal onu_regbank_stat : t_onu_regbank;

  --============================================================================
  -- ONU MODES
  --============================================================================
  signal onu_mode_async       : std_logic_vector(7 downto 0);
  signal onu_mode_trxsync_meta : std_logic_vector(7 downto 0);
  signal onu_mode_trxsync_r    : std_logic_vector(7 downto 0);

  attribute async_reg of onu_mode_trxsync_meta, onu_mode_trxsync_r : signal is "true";

  signal calibration_on    : std_logic;
  signal prbs_on           : std_logic;
  signal onu_operational   : std_logic;
  signal calibration_on_r  : std_logic;
  signal prbs_on_r         : std_logic;
  signal onu_operational_r : std_logic;

  signal tx_nb_barrel_async                                    : std_logic_vector(3 downto 0);
  signal tx_nb_barrel_trxsync_meta                              : std_logic_vector(3 downto 0);
  attribute ASYNC_REG of tx_nb_barrel_trxsync_meta                              : signal is "TRUE";

  --============================================================================
  -- TDM local timer (Note: clocked by clk_trxusr240_i)
  --============================================================================
  signal tdm_tx_en_limit      : std_logic_vector(7 downto 0);  -- Burst Duration    (Given in RX_CLK_PERIOD)
  signal tdm_heartbeat_locked : std_logic;
  signal tdm_tx_frame_ena     : std_logic;

  signal tdm_local_time_limit_async                                                        : std_logic_vector(15 downto 0);
  signal tdm_local_time_offset_async                                                       : std_logic_vector(15 downto 0);
  signal tdm_local_time_limit_trxsync_meta                                                  : std_logic_vector(15 downto 0);
  signal tdm_local_time_offset_trxsync_meta                                                 : std_logic_vector(15 downto 0);
  signal tdm_local_time_limit_trxsync_r                                                     : std_logic_vector(15 downto 0);  -- Heartbeat period  (Given in RX_CLK_PERIOD)
  signal tdm_local_time_offset_trxsync_r                                                    : std_logic_vector(15 downto 0);  -- ONU offset in TDM
  attribute async_reg of tdm_local_time_limit_trxsync_meta, tdm_local_time_limit_trxsync_r   : signal is "true";
  attribute async_reg of tdm_local_time_offset_trxsync_meta, tdm_local_time_offset_trxsync_r : signal is "true";

  signal phy_tx_en_pos_async                                               : std_logic_vector(3 downto 0);
  signal phy_tx_en_pos_trxsync_meta                                         : std_logic_vector(3 downto 0);
  signal phy_tx_en_pos_trxsync_r                                            : std_logic_vector(3 downto 0);
  attribute async_reg of phy_tx_en_pos_trxsync_meta, phy_tx_en_pos_trxsync_r : signal is "true";

  --============================================================================
  -- FEC Single-Error / Double-Error counter
  --============================================================================
  signal rx_serror_correc_cntr                                                                                        : unsigned(7 downto 0);
  signal rx_derror_correc_cntr                                                                                        : unsigned(7 downto 0);
  signal rx_serror_correc_cntr_latched                                                                                : std_logic_vector(7 downto 0);
  signal rx_derror_correc_cntr_latched                                                                                : std_logic_vector(7 downto 0);
  signal rx_clear_error_cntr_async                                                                                    : std_logic;
  signal rx_latch_error_cntr_async                                                                                    : std_logic;
  signal rx_clear_error_cntr_trxsync_meta                                                                              : std_logic;
  signal rx_latch_error_cntr_trxsync_meta                                                                              : std_logic;
  signal rx_clear_error_cntr_trxsync_r                                                                                 : std_logic;
  signal rx_latch_error_cntr_trxsync_r                                                                                 : std_logic;
  signal rx_clear_error_cntr_trxsync_r2                                                                                : std_logic;
  signal rx_latch_error_cntr_trxsync_r2                                                                                : std_logic;
  attribute async_reg of rx_clear_error_cntr_trxsync_meta, rx_clear_error_cntr_trxsync_r, rx_clear_error_cntr_trxsync_r2 : signal is "true";
  attribute async_reg of rx_latch_error_cntr_trxsync_meta, rx_latch_error_cntr_trxsync_r, rx_latch_error_cntr_trxsync_r2 : signal is "true";

  signal rx_seen_serror_correc                                                                 : std_logic;
  signal rx_seen_derror_correc                                                                 : std_logic;
  signal rx_seen_serror_correc_mngrsync_meta                                                   : std_logic;
  signal rx_seen_derror_correc_mngrsync_meta                                                   : std_logic;
  signal rx_seen_serror_correc_mngrsync_r                                                      : std_logic;
  signal rx_seen_derror_correc_mngrsync_r                                                      : std_logic;
  attribute async_reg of rx_seen_serror_correc_mngrsync_meta, rx_seen_serror_correc_mngrsync_r : signal is "true";
  attribute async_reg of rx_seen_derror_correc_mngrsync_meta, rx_seen_derror_correc_mngrsync_r : signal is "true";

  --============================================================================
  -- Slow Control CRC-7 Error detected cntr (cleared/latched by rx_clear_error_cntr / rx_latch_error_cntr)
  --============================================================================
  signal rx_crc_errdetect_cntr                                                                 : unsigned(7 downto 0);
  signal rx_crc_errdetect_cntr_latched                                                         : std_logic_vector(7 downto 0);
  signal rx_seen_crc_errdetect                                                                 : std_logic;
  signal rx_seen_crc_errdetect_mngrsync_meta                                                   : std_logic;
  signal rx_seen_crc_errdetect_mngrsync_r                                                      : std_logic;
  attribute async_reg of rx_seen_crc_errdetect_mngrsync_meta, rx_seen_crc_errdetect_mngrsync_r : signal is "true";

  --============================================================================
  -- I2C sync.
  --============================================================================
  signal i2c_en_async                                                                                : std_logic;
  signal i2c_en_syssync_meta                                                                         : std_logic;
  signal i2c_en_syssync_r                                                                            : std_logic;
  signal i2c_en_syssync_r2                                                                           : std_logic;
  signal i2c_en_syssync_r3                                                                           : std_logic;
  attribute async_reg of i2c_en_syssync_meta, i2c_en_syssync_r, i2c_en_syssync_r2, i2c_en_syssync_r3 : signal is "true";

  --============================================================================
  -- DRP sync.
  --============================================================================
  signal drp_en_async                                                                                : std_logic;
  signal drp_en_syssync_meta                                                                         : std_logic;
  signal drp_en_syssync_r                                                                            : std_logic;
  signal drp_en_syssync_r2                                                                           : std_logic;
  signal drp_en_syssync_r3                                                                           : std_logic;
  attribute async_reg of drp_en_syssync_meta, drp_en_syssync_r, drp_en_syssync_r2, drp_en_syssync_r3 : signal is "true";

  --============================================================================
  -- Rx Equalizer CTRL sync.
  --============================================================================
  signal rx_eq_ctrl_async                                                                            : std_logic;
  signal rx_eq_ctrl_syssync_meta                                                                     : std_logic;
  signal rx_eq_ctrl_syssync_r                                                                        : std_logic;
  signal rx_eq_ctrl_syssync_r2                                                                       : std_logic;
  signal rx_eq_ctrl_syssync_r3                                                                       : std_logic;
  attribute async_reg of rx_eq_ctrl_syssync_meta, rx_eq_ctrl_syssync_r, rx_eq_ctrl_syssync_r2, rx_eq_ctrl_syssync_r3 : signal is "true";

  --============================================================================
  -- Tx Phase CTRL sync.
  --============================================================================
  signal tx_phase_ctrl_async                                                                            : std_logic;
  signal tx_phase_ctrl_syssync_meta                                                                     : std_logic;
  signal tx_phase_ctrl_syssync_r                                                                        : std_logic;
  signal tx_phase_ctrl_syssync_r2                                                                       : std_logic;
  signal tx_phase_ctrl_syssync_r3                                                                       : std_logic;
  attribute async_reg of tx_phase_ctrl_syssync_meta, tx_phase_ctrl_syssync_r, tx_phase_ctrl_syssync_r2, tx_phase_ctrl_syssync_r3 : signal is "true";


  --============================================================================
  -- Sticky Monitoring
  --============================================================================  
  signal phy_mon_clear                      : std_logic;
  signal mgt_seen_tx_nready_mngrsync_meta   : std_logic;
  signal mgt_seen_rx_nready_mngrsync_meta   : std_logic;
  signal mgt_seen_txpll_nlock_mngrsync_meta : std_logic;
  signal mgt_seen_rxpll_nlock_mngrsync_meta : std_logic;
  signal seen_rx_nlocked_mngrsync_meta      : std_logic;
  signal mgt_seen_tx_nready_mngrsync_r      : std_logic;
  signal mgt_seen_rx_nready_mngrsync_r      : std_logic;
  signal mgt_seen_txpll_nlock_mngrsync_r    : std_logic;
  signal mgt_seen_rxpll_nlock_mngrsync_r    : std_logic;
  signal seen_rx_nlocked_mngrsync_r         : std_logic;

  attribute async_reg of mgt_seen_tx_nready_mngrsync_meta, mgt_seen_tx_nready_mngrsync_r     : signal is "true";
  attribute async_reg of mgt_seen_rx_nready_mngrsync_meta, mgt_seen_rx_nready_mngrsync_r     : signal is "true";
  attribute async_reg of mgt_seen_txpll_nlock_mngrsync_meta, mgt_seen_txpll_nlock_mngrsync_r : signal is "true";
  attribute async_reg of mgt_seen_rxpll_nlock_mngrsync_meta, mgt_seen_rxpll_nlock_mngrsync_r : signal is "true";
  attribute async_reg of seen_rx_nlocked_mngrsync_meta, seen_rx_nlocked_mngrsync_r           : signal is "true";

  --============================================================================
  -- Monitoring interrupt output
  --============================================================================  
  signal interrupt_mask                                                : std_logic_vector(30 downto 0);
  signal interrupt_async                                               : std_logic;
  signal interrupt_mngrsync_meta                                       : std_logic;
  signal interrupt_mngrsync_r                                          : std_logic;
  attribute async_reg of interrupt_mngrsync_meta, interrupt_mngrsync_r : signal is "true";

  signal status_sticky                  : std_logic_vector(30 downto 0);
  attribute mark_debug of status_sticky : signal is "true";
  attribute keep of status_sticky       : signal is "true";
  ------------------------------------------------------------------------------

  --============================================================================
  -- Disable OLT communication (used only for debug purposes - e.g. tests at very high attenuation)
  --============================================================================  
  signal dis_olt_comm : std_logic := '0';
  ------------------------------------------------------------------------------

  --! Component declaration
  component pon_down_sc_rx is
    generic(
      g_CRC_GOOD_TO_LOCK  : integer := 15;
      g_CRC_BAD_TO_UNLOCK : integer := 5
      );
    port (
      clk_i             : in  std_logic;  --! clock input
      reset_i           : in  std_logic;  --! active high sync. reset
      nibble_rcvd_i     : in  std_logic;  --! control scheduling
      data_i            : in  std_logic_vector(3 downto 0);  --! input data       
      rcvd_ctrl_frame_o : out std_logic;  --! user control
      rcvd_ctrl_error_o : out std_logic;  --! error detected CRC
      framing_sync_o    : out std_logic;  --! CRC-framing is synchronized
      data_o            : out std_logic_vector(28 downto 0)  --! user output data
      );
  end component pon_down_sc_rx;

  component pon_up_sc_tx is
    generic(
      g_HEADER    : std_logic_vector(2 downto 0) := "110";
      g_IDLE_BYTE : std_logic_vector(7 downto 0) := "00000000"
      );
    port (
      clk_i            : in  std_logic;  --! clock input
      reset_i          : in  std_logic;  --! active high sync. reset
      byte_sent_i      : in  std_logic;  --! control scheduling
      data_o           : out std_logic_vector(7 downto 0);  --! encoded output data   
      data_i           : in  std_logic_vector(20 downto 0);  --! user input data
      send_data_req_i  : in  std_logic;  --! user interface control
      send_data_busy_o : out std_logic  --! user interface control    
      );
  end component pon_up_sc_tx;

  component cdc_mailbox is
    generic(
      g_mailbox_type : integer := 0     -- 0 = regout / >0 = mailbox
      );
    port(
      clk_a         : in  std_logic;
      data_a_in     : in  std_logic_vector;
      data_a_ready  : in  std_logic;
      clk_b         : in  std_logic;
      data_b_out    : out std_logic_vector;
      data_b_accept : out std_logic
      );
  end component cdc_mailbox;

  component prbs_data_gen is
    generic (
      G_DOUT_WIDTH   : integer := 20;
      G_PRBS_TAP     : integer := 0;
      G_REVERSE_BITS : integer := 0);
    port (
      clk_i : in  std_logic;
      mode_i  : in  std_logic;  -- '0' external seed, '1' internal loop-back
      ena_i   : in  std_logic;          -- '0' hold, '1' generate
      sel_i   : in  std_logic_vector(1 downto 0);  -- "00" PRBS-7, "01" PRBS-23, "1x" fix
      seed_i  : in  std_logic_vector(22 downto 0);
      dout_o  : out std_logic_vector(G_DOUT_WIDTH-1 downto 0));
  end component prbs_data_gen;

  component onu_tdm_timer is
    port (
      clk_i               : in  std_logic;
      reset_i             : in  std_logic;
      rx_heartbeat_i      : in  std_logic;
      tx_enable_limit_i   : in  std_logic_vector(7 downto 0);  --! Burst duration  (LSB = PERIOD clk_i)
      local_time_limit_i  : in  std_logic_vector(15 downto 0);  --! Heartbeat period (LSB = PERIOD clk_i)    
      local_time_offset_i : in  std_logic_vector(15 downto 0);  --! Local offset when ONU is allowed to transmit
      heartbeat_locked_o  : out std_logic;
      tx_frame_ena_o      : out std_logic
      );
  end component onu_tdm_timer;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- REG BANK ASSIGNMENT
  --============================================================================
  -- REG0 - R/W - ONU MODE
  onu_mode_async <= onu_regbank_ctrl(0);

  onu_regbank_stat(0) <= onu_regbank_ctrl(0);
  onu_regbank_stat(1) <= onu_regbank_ctrl(1);
  onu_regbank_stat(2) <= onu_regbank_ctrl(2);
  onu_regbank_stat(3) <= onu_regbank_ctrl(3);

  -- REG1 - R/W - UPSTREAM RELATED
  tdm_local_time_limit_async  <= onu_regbank_ctrl(5)&onu_regbank_ctrl(4);
  tdm_local_time_offset_async <= onu_regbank_ctrl(7)&onu_regbank_ctrl(6);

  onu_regbank_stat(4) <= onu_regbank_ctrl(4);
  onu_regbank_stat(5) <= onu_regbank_ctrl(5);
  onu_regbank_stat(6) <= onu_regbank_ctrl(6);
  onu_regbank_stat(7) <= onu_regbank_ctrl(7);

  -- REG2 - R/W - UPSTREAM RELATED
  tdm_tx_en_limit     <= std_logic_vector(to_unsigned(c_BURST_EN_LIMIT, tdm_tx_en_limit'length));  --c_BURST_EN_LIMIT defined on pon_onu_package.vhd
  phy_tx_en_pos_async <= onu_regbank_ctrl(8)(3 downto 0);  --tx enable delay
  tx_nb_barrel_async  <= onu_regbank_ctrl(9)(3 downto 0);  --tx barrel shifter used for fine calibration

  onu_regbank_stat(8)  <= onu_regbank_ctrl(8);
  onu_regbank_stat(9)  <= onu_regbank_ctrl(9);
  onu_regbank_stat(10) <= onu_regbank_ctrl(10);
  onu_regbank_stat(11) <= onu_regbank_ctrl(11);

  -- REG3 - BYTE3-1 R; BYTE0 R/W  - FEC Status / CRC status
  rx_clear_error_cntr_async <= onu_regbank_ctrl(12)(0);
  rx_latch_error_cntr_async <= onu_regbank_ctrl(12)(1);

  onu_regbank_stat(12)(6 downto 0) <= onu_regbank_ctrl(12)(6 downto 0);
  onu_regbank_stat(12)(7)          <= sc_framing_locked_mngrsync_r;
  onu_regbank_stat(13)             <= rx_serror_correc_cntr_latched;
  onu_regbank_stat(14)             <= rx_derror_correc_cntr_latched;
  onu_regbank_stat(15)             <= rx_crc_errdetect_cntr_latched;

  -- REG4 - BYTE0 R - Error Seen
  onu_regbank_stat(16)(0)          <= rx_seen_serror_correc_mngrsync_r;
  onu_regbank_stat(16)(1)          <= rx_seen_derror_correc_mngrsync_r;
  onu_regbank_stat(16)(2)          <= rx_seen_crc_errdetect_mngrsync_r;
  onu_regbank_stat(16)(7 downto 3) <= (others => '0');

  onu_regbank_stat(17) <= (others => '0');
  onu_regbank_stat(18) <= (others => '0');
  onu_regbank_stat(19) <= (others => '0');

  -- REG 5 - BYTE0 R/W MGT Status Monitor Ctrl
  --         BYTE1 R   MGT Status Monitor   
  phy_mon_clear        <= onu_regbank_ctrl(20)(0);
  onu_regbank_stat(20) <= onu_regbank_ctrl(20);

  onu_regbank_stat(21)(0)          <= mgt_seen_tx_nready_mngrsync_r;
  onu_regbank_stat(21)(1)          <= mgt_seen_rx_nready_mngrsync_r;
  onu_regbank_stat(21)(2)          <= mgt_seen_txpll_nlock_mngrsync_r;
  onu_regbank_stat(21)(3)          <= mgt_seen_rxpll_nlock_mngrsync_r;
  onu_regbank_stat(21)(4)          <= seen_rx_nlocked_mngrsync_r;
  onu_regbank_stat(21)(7 downto 5) <= (others => '0');

  onu_regbank_stat(22) <= (others => '0');
  onu_regbank_stat(23) <= (others => '0');

  -- REG 6 - BYTE0/1/2/3 Interrupt Config
  interrupt_mask(7 downto 0)       <= onu_regbank_ctrl(24);
  interrupt_mask(15 downto 8)      <= onu_regbank_ctrl(25);
  interrupt_mask(23 downto 16)     <= onu_regbank_ctrl(26);
  interrupt_mask(30 downto 24)     <= onu_regbank_ctrl(27)(6 downto 0);
  onu_regbank_stat(24)             <= onu_regbank_ctrl(24);
  onu_regbank_stat(25)             <= onu_regbank_ctrl(25);
  onu_regbank_stat(26)             <= onu_regbank_ctrl(26);
  onu_regbank_stat(27)(6 downto 0) <= onu_regbank_ctrl(27)(6 downto 0);
  onu_regbank_stat(27)(7)          <= interrupt_mngrsync_r;

  -- REG7-12 - not used yet (loop-back)
  gen_idle_bytes4_12 : for i in 7*4 to 12*4+3 generate
    onu_regbank_stat(i) <= onu_regbank_ctrl(i);
  end generate gen_idle_bytes4_12;

  -- REG13/14 - I2C master
  i2c_en_async <= onu_regbank_ctrl(13*4+3)(7);

  p_i2c_syssync : process(clk_sys_i)
  begin
    if(clk_sys_i'event and clk_sys_i = '1') then
      i2c_en_syssync_meta <= i2c_en_async;
      i2c_en_syssync_r    <= i2c_en_syssync_meta;
      i2c_en_syssync_r2   <= i2c_en_syssync_r;
      i2c_en_syssync_r3   <= i2c_en_syssync_r2;
      phy_i2c_ctrl_en_o   <= '0';
      if(i2c_en_syssync_r3 = '0' and i2c_en_syssync_r2 = '1') then
        phy_i2c_ctrl_o    <= onu_regbank_ctrl(13*4+3)&onu_regbank_ctrl(13*4+2)&onu_regbank_ctrl(13*4+1)&onu_regbank_ctrl(13*4+0);
        phy_i2c_ctrl_en_o <= '1';
      end if;
    end if;
  end process p_i2c_syssync;

  onu_regbank_stat(13*4 + 0) <= onu_regbank_ctrl(13*4 + 0);
  onu_regbank_stat(13*4 + 1) <= onu_regbank_ctrl(13*4 + 1);
  onu_regbank_stat(13*4 + 2) <= onu_regbank_ctrl(13*4 + 2);
  onu_regbank_stat(13*4 + 3) <= onu_regbank_ctrl(13*4 + 3);

  onu_regbank_stat(14*4+0) <= phy_i2c_stat_i(8*(0+1)-1 downto 8*(0));
  onu_regbank_stat(14*4+1) <= phy_i2c_stat_i(8*(1+1)-1 downto 8*(1));
  onu_regbank_stat(14*4+2) <= phy_i2c_stat_i(8*(2+1)-1 downto 8*(2));
  onu_regbank_stat(14*4+3) <= phy_i2c_stat_i(8*(3+1)-1 downto 8*(3));

  -- REG15 - Debugging disable OLT comm.
  onu_regbank_stat(15*4+0)(0)          <= dis_olt_comm;
  onu_regbank_stat(15*4+0)(7 downto 1) <= (others => '0');
  onu_regbank_stat(15*4+1)             <= (others => '0');
  onu_regbank_stat(15*4+2)             <= (others => '0');
  onu_regbank_stat(15*4+3)             <= (others => '0');

  -- REG16 - Not used yet (tied to gnd)
  onu_regbank_stat(16*4+0) <= (others => '0');
  onu_regbank_stat(16*4+1) <= (others => '0');
  onu_regbank_stat(16*4+2) <= (others => '0');
  onu_regbank_stat(16*4+3) <= (others => '0');


  -- REG17 - DRP master
  drp_en_async <= onu_regbank_ctrl(17*4+3)(7);

  p_drp_syssync : process(clk_sys_i)
  begin
    if(clk_sys_i'event and clk_sys_i = '1') then
      drp_en_syssync_meta <= drp_en_async;
      drp_en_syssync_r    <= drp_en_syssync_meta;
      drp_en_syssync_r2   <= drp_en_syssync_r;
      drp_en_syssync_r3   <= drp_en_syssync_r2;
      mgt_drp_wr_strobe_o <= '0';
      if(drp_en_syssync_r3 = '0' and drp_en_syssync_r2 = '1') then
        mgt_drp_wr_o        <= onu_regbank_ctrl(17*4+3)&onu_regbank_ctrl(17*4+2)&onu_regbank_ctrl(17*4+1)&onu_regbank_ctrl(17*4+0);
        mgt_drp_wr_strobe_o <= '1';
      end if;
    end if;
  end process p_drp_syssync;

  onu_regbank_stat(17*4+0) <= mgt_drp_monitor_i(8*(0+1)-1 downto 8*(0));
  onu_regbank_stat(17*4+1) <= mgt_drp_monitor_i(8*(1+1)-1 downto 8*(1));
  onu_regbank_stat(17*4+2) <= mgt_drp_monitor_i(8*(2+1)-1 downto 8*(2));
  onu_regbank_stat(17*4+3) <= mgt_drp_monitor_i(8*(3+1)-1 downto 8*(3));

  -- REG 18 - Rx Equalizer
  rx_eq_ctrl_async <= onu_regbank_ctrl(18*4+3)(7);

  p_rx_eq_ctrl_syssync : process(clk_sys_i)
  begin
    if(clk_sys_i'event and clk_sys_i = '1') then
      rx_eq_ctrl_syssync_meta <= rx_eq_ctrl_async;
      rx_eq_ctrl_syssync_r    <= rx_eq_ctrl_syssync_meta;
      rx_eq_ctrl_syssync_r2   <= rx_eq_ctrl_syssync_r;
      rx_eq_ctrl_syssync_r3   <= rx_eq_ctrl_syssync_r2;
      if(rx_eq_ctrl_syssync_r3 = '0' and rx_eq_ctrl_syssync_r2 = '1') then
        mgt_rx_equalizer_ctrl_o <= onu_regbank_ctrl(18*4+3)&onu_regbank_ctrl(18*4+2)&onu_regbank_ctrl(18*4+1)&onu_regbank_ctrl(18*4+0);
      end if;
    end if;
  end process p_rx_eq_ctrl_syssync;

  onu_regbank_stat(18*4+0) <= mgt_rx_equalizer_stat_i(8*(0+1)-1 downto 8*(0));
  onu_regbank_stat(18*4+1) <= mgt_rx_equalizer_stat_i(8*(1+1)-1 downto 8*(1));
  onu_regbank_stat(18*4+2) <= mgt_rx_equalizer_stat_i(8*(2+1)-1 downto 8*(2));
  onu_regbank_stat(18*4+3) <= mgt_rx_equalizer_stat_i(8*(3+1)-1 downto 8*(3));

  
  -- REG 19 - Tx Phase
  tx_phase_ctrl_async <= onu_regbank_ctrl(19*4+3)(7);

  p_tx_phase_ctrl_syssync : process(clk_sys_i)
  begin
    if(clk_sys_i'event and clk_sys_i = '1') then
      tx_phase_ctrl_syssync_meta <= tx_phase_ctrl_async;
      tx_phase_ctrl_syssync_r    <= tx_phase_ctrl_syssync_meta;
      tx_phase_ctrl_syssync_r2   <= tx_phase_ctrl_syssync_r;
      tx_phase_ctrl_syssync_r3   <= tx_phase_ctrl_syssync_r2;
      if(tx_phase_ctrl_syssync_r3 = '0' and tx_phase_ctrl_syssync_r2 = '1') then
        mgt_tx_phase_ctrl_o <= onu_regbank_ctrl(19*4+3)&onu_regbank_ctrl(19*4+2)&onu_regbank_ctrl(19*4+1)&onu_regbank_ctrl(19*4+0);
      end if;
    end if;
  end process p_tx_phase_ctrl_syssync;

  onu_regbank_stat(19*4+0) <= mgt_tx_phase_stat_i(8*(0+1)-1 downto 8*(0));
  onu_regbank_stat(19*4+1) <= mgt_tx_phase_stat_i(8*(1+1)-1 downto 8*(1));
  onu_regbank_stat(19*4+2) <= mgt_tx_phase_stat_i(8*(2+1)-1 downto 8*(2));
  onu_regbank_stat(19*4+3) <= mgt_tx_phase_stat_i(8*(3+1)-1 downto 8*(3));


  -- REG20-end - not used yet (tied-to-gnd)
  gen_idle_bytes : for i in 20*4 to g_NBR_ONU_CTRL_BYTE-1 generate
    --onu_regbank_stat(i)<=onu_regbank_ctrl(i);
    onu_regbank_stat(i) <= (others => '0');
  end generate gen_idle_bytes;

  -- Auxiliar manager read
  gen_manager_stat : if g_ENABLE_MANAGER = '1' generate
    p_manager_stat : process(onu_regbank_stat) is
    begin
      for i in 0 to ((g_NBR_ONU_CTRL_BYTE/4)-1) loop
        manager_stat_reg_o(i)(7 downto 0)   <= onu_regbank_stat(4*i);
        manager_stat_reg_o(i)(15 downto 8)  <= onu_regbank_stat(4*i+1);
        manager_stat_reg_o(i)(23 downto 16) <= onu_regbank_stat(4*i+2);
        manager_stat_reg_o(i)(31 downto 24) <= onu_regbank_stat(4*i+3);
      end loop;
    end process p_manager_stat;
  end generate;

  --============================================================================
  -- SLOW CONTROL
  --============================================================================
  --============================================================================
  -- OLT ACCESS
  --============================================================================   
  cmp_pon_down_sc_rx : pon_down_sc_rx
    generic map(
      g_CRC_GOOD_TO_LOCK  => c_CRC_FRAMING_GOOD_TO_LOCK,
      g_CRC_BAD_TO_UNLOCK => c_CRC_FRAMING_BAD_TO_UNLOCK
      )
    port map(
      clk_i             => clk_trxusr240_i,
      reset_i           => reset_rxclk_i,
      nibble_rcvd_i     => rx_data_strobe_i,
      data_i            => rx_data_ctrl_i,
      rcvd_ctrl_frame_o => decoded_ctrl_p_async,
      rcvd_ctrl_error_o => decoded_ctrl_error_p_async,
      framing_sync_o    => sc_framing_locked_async,
      data_o            => decoded_ctrl_data_async
      );

  sc_from_olt_async <= decoded_ctrl_error_p_async&decoded_ctrl_data_async;

  cmp_mailbox_rx2manager : cdc_mailbox
    generic map(
      g_mailbox_type => 1
      )
    port map(
      clk_a         => clk_trxusr240_i,
      data_a_in     => sc_from_olt_async,
      data_a_ready  => decoded_ctrl_p_async,
      clk_b         => clk_manager_i,
      data_b_out    => sc_from_olt_mngrsync,
      data_b_accept => decoded_ctrl_p
      );

  decoded_ctrl_error_p <= sc_from_olt_mngrsync(sc_from_olt_mngrsync'left);
  decoded_ctrl_data    <= sc_from_olt_mngrsync(decoded_ctrl_data'range);

  --============================================================================
  -- MANAGER ACCESS
  --============================================================================
  gen_manager_enable : if g_ENABLE_MANAGER = '1' generate
    wr_from_manager_data   <= manager_wr_reg_i;
    wr_from_manager_strobe <= manager_wr_strobe_i;
    wr_from_manager_p      <= fcn_or_reduce(manager_wr_strobe_i);
  end generate;

  gen_manager_disable : if g_ENABLE_MANAGER = '0' generate
    wr_from_manager_data   <= (others => '0');
    wr_from_manager_strobe <= (others => '0');
    wr_from_manager_p      <= '0';
  end generate;

  --============================================================================
  -- Process p_regbank_access
  --! Slow control operation decoding
  --! read:  decoded_ctrl_data,wr_from_manager_strobe,wr_from_manager_p, onu_regbank_stat\n
  --! write: onu_regbank_ctrl,data_send_async,ready_send_p_async\n
  --! r/w: \n
  --============================================================================           
  p_regbank_access : process(clk_manager_i) is
  begin
    if (clk_manager_i'event and clk_manager_i = '1') then
      -- operations from OLT
      ready_send_p_async <= '0';
      if ((decoded_ctrl_p = '1') and (decoded_ctrl_data(ir_ONU_ADDR) = onu_address_i or decoded_ctrl_data(ir_ONU_ADDR) = c_BROADCAST_ADDR) and dis_olt_comm = '0') then
        if(decoded_ctrl_error_p = '0') then
          case decoded_ctrl_data(ir_CTRL_OPE) is
            when c_OPE_WRITE =>
              if(wr_from_manager_p = '0') then
                onu_regbank_ctrl(to_integer(unsigned(decoded_ctrl_data(ir_REGBANK_ADDR)))) <= decoded_ctrl_data(ir_REGBANK_VALUE);
              end if;
            when c_OPE_WRITE_ACK =>
              if(wr_from_manager_p = '0') then
                onu_regbank_ctrl(to_integer(unsigned(decoded_ctrl_data(ir_REGBANK_ADDR)))) <= decoded_ctrl_data(ir_REGBANK_VALUE);
                data_send_async(ir_CTRL_OPE)                                               <= decoded_ctrl_data(ir_CTRL_OPE);
                data_send_async(ir_REGBANK_ADDR)                                           <= decoded_ctrl_data(ir_REGBANK_ADDR);
                data_send_async(ir_REGBANK_VALUE)                                          <= decoded_ctrl_data(ir_REGBANK_VALUE);
                ready_send_p_async                                                         <= '1';
              end if;
            when c_OPE_READ =>
              data_send_async(ir_CTRL_OPE)      <= decoded_ctrl_data(ir_CTRL_OPE);
              data_send_async(ir_REGBANK_ADDR)  <= decoded_ctrl_data(ir_REGBANK_ADDR);
              data_send_async(ir_REGBANK_VALUE) <= onu_regbank_stat(to_integer(unsigned(decoded_ctrl_data(ir_REGBANK_ADDR))));
              ready_send_p_async                <= '1';
            when others =>
              onu_regbank_ctrl   <= onu_regbank_ctrl;
              ready_send_p_async <= '0';
          end case;
        else  -- received an operation but with an error, send back OPE_RCVD_ERROR code
          data_send_async(ir_CTRL_OPE)      <= c_OPE_RCVD_ERROR;
          data_send_async(ir_REGBANK_ADDR)  <= decoded_ctrl_data(ir_REGBANK_ADDR);
          data_send_async(ir_REGBANK_VALUE) <= decoded_ctrl_data(ir_REGBANK_VALUE);
          ready_send_p_async                <= '1';
        end if;
      end if;

      -- operations from manager
      if(wr_from_manager_p = '1') then
        for i in 0 to (g_NBR_ONU_CTRL_BYTE/4-1) loop
          if(wr_from_manager_strobe(i) = '1') then
            for j in 0 to 3 loop
              onu_regbank_ctrl(j+4*i) <= wr_from_manager_data((j+1)*8-1 downto j*8);
            end loop;
          end if;
        end loop;
      end if;

      -- this is used only for test purposes.
      if(wr_from_manager_strobe(15) = '1') then
        dis_olt_comm <= wr_from_manager_data(0);
      end if;
      
    end if;
  end process p_regbank_access;

  cmp_mailbox_mngr2tx : cdc_mailbox
    generic map(
      g_mailbox_type => 1               -- mailbox
      )
    port map(
      clk_a         => clk_manager_i,
      data_a_in     => data_send_async,
      data_a_ready  => ready_send_p_async,
      clk_b         => clk_trxusr240_i,
      data_b_out    => data_send_trxsync,
      data_b_accept => accept_send_p_trxsync
      );

  cmp_pon_up_sc_tx : pon_up_sc_tx
    generic map(
      g_HEADER    => c_SC_UP_HEADER,
      g_IDLE_BYTE => "00000000"
      )
    port map(
      clk_i            => clk_trxusr240_i,
      reset_i          => reset_txclk_i,
      byte_sent_i      => tx_data_ctrl_ready_i,
      data_i           => data_send_trxsync(data_send_trxsync'left downto 8),
      send_data_req_i  => accept_send_p_trxsync,
      send_data_busy_o => open,         -- not being verified yet
      data_o           => tx_data_ctrl_o
      );

  --============================================================================
  -- ONU - Operation Modes Control
  --============================================================================
  --============================================================================
  -- Process p_onu_mode_reg
  --! read:  calibration_on,prbs_on,onu_operational\n
  --! write: calibration_on_r,prbs_on_r,onu_operational_r\n
  --! r/w:   \n
  --============================================================================       
  p_onu_mode_reg : process(clk_trxusr240_i) is
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(reset_rxclk_i = '1') then
        calibration_on_r  <= '0';
        prbs_on_r         <= '0';
        onu_operational_r <= '0';
      else
        calibration_on_r  <= calibration_on;
        prbs_on_r         <= prbs_on;
        onu_operational_r <= onu_operational;
      end if;
    end if;
  end process p_onu_mode_reg;

  calibration_on  <= '1' when onu_mode_trxsync_r = c_CALIBRATION_MODE                                          else '0';
  prbs_on         <= '1' when (onu_mode_trxsync_r = c_PRBS_BURST_MODE or onu_mode_trxsync_r = c_PRBS_CONT_MODE) else '0';
  onu_operational <= '1' when (onu_mode_trxsync_r = c_OPERATIONAL_MODE or onu_mode_trxsync_r = c_OPERATIONAL_ALWAYS_ENABLE_MODE) else '0';

  phy_tx_cont_en_o <= '1' when (onu_mode_trxsync_r = c_CALIBRATION_MODE or onu_mode_trxsync_r = c_PRBS_CONT_MODE or onu_mode_trxsync_r = c_OPERATIONAL_ALWAYS_ENABLE_MODE) else '0';
  phy_tx_off_o <= '1' when (onu_mode_trxsync_r /= c_CALIBRATION_MODE and onu_mode_trxsync_r /= c_PRBS_BURST_MODE and onu_mode_trxsync_r /= c_PRBS_CONT_MODE
                            and onu_mode_trxsync_r /= c_OPERATIONAL_MODE and onu_mode_trxsync_r /= c_OPERATIONAL_ALWAYS_ENABLE_MODE) else '0';

  --============================================================================
  -- Component instantiation
  --! Component onu_tdm_timer
  --! ONU local timer for TDM arbitration
  --============================================================================  
  cmp_onu_tdm_timer : onu_tdm_timer
    port map(
      clk_i               => clk_trxusr240_i,
      reset_i             => reset_rxclk_i,
      rx_heartbeat_i      => rx_heartbeat_header_i,
      tx_enable_limit_i   => tdm_tx_en_limit,                 --! Burst duration   (LSB = PERIOD clk_i)
      local_time_limit_i  => tdm_local_time_limit_trxsync_r,   --! Heartbeat period (LSB = PERIOD clk_i)    
      local_time_offset_i => tdm_local_time_offset_trxsync_r,  --! Local offset when ONU is allowed to transmit
      heartbeat_locked_o  => tdm_heartbeat_locked,
      tx_frame_ena_o      => tdm_tx_frame_ena
      );

  phy_tx_ena_o    <= (tdm_tx_frame_ena);
  phy_tx_en_pos_o <= phy_tx_en_pos_trxsync_r;

  --============================================================================
  -- Component instantiation
  --! Component prbs_data_gen
  --! Used to generate PRBS data upon OLT request
  --============================================================================  
  cmp_prbs_data_gen : prbs_data_gen
    generic map(
      G_DOUT_WIDTH   => g_TX_DATA_USR_WIDTH,
      G_PRBS_TAP     => 0,
      G_REVERSE_BITS => 0)
    port map(
      clk_i => clk_trxusr240_i,
      mode_i  => prbs_on_r,
      ena_i   => tx_data_ctrl_ready_i,
      sel_i   => "00",                  -- "00" PRBS-7, "01" PRBS-23, "1x" fix
      seed_i  => "00110100010101100111100",
      dout_o  => tx_data_usr_ovr_o
      );

  --============================================================================
  -- Downstream FEC and SC-CRC error monitoring
  --============================================================================        
  --============================================================================
  -- Process p_fec_crc_error_cntr
  --! Provides FEC error correction and slow control CRC state monitoring
  --! read:  rx_clear_error_cntr_trxsync_r, rx_clear_error_cntr_trxsync_r2,rx_latch_error_cntr_trxsync_r, rx_latch_error_cntr_trxsync_r2,
  --!        rx_serror_corrected_i, rx_derror_corrected_i\n
  --! write: rx_serror_correc_cntr_latched, rx_derror_correc_cntr_latched, rx_crc_errdetect_cntr_latched\n
  --! r/w: rx_serror_correc_cntr, rx_derror_correc_cntr, rx_crc_errdetect_cntr\n
  --============================================================================        
  p_fec_crc_error_cntr : process(clk_trxusr240_i) is
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(rx_clear_error_cntr_trxsync_r2 = '0' and rx_clear_error_cntr_trxsync_r = '1') then
        rx_serror_correc_cntr         <= (others => '0');
        rx_derror_correc_cntr         <= (others => '0');
        rx_serror_correc_cntr_latched <= (others => '0');
        rx_derror_correc_cntr_latched <= (others => '0');
        rx_crc_errdetect_cntr         <= (others => '0');
        rx_crc_errdetect_cntr_latched <= (others => '0');
        rx_seen_serror_correc         <= '0';
        rx_seen_derror_correc         <= '0';
        rx_seen_crc_errdetect         <= '0';
      else
        if(rx_latch_error_cntr_trxsync_r2 = '0' and rx_latch_error_cntr_trxsync_r = '1') then
          rx_serror_correc_cntr_latched <= std_logic_vector(rx_serror_correc_cntr);
          rx_derror_correc_cntr_latched <= std_logic_vector(rx_derror_correc_cntr);
          rx_crc_errdetect_cntr_latched <= std_logic_vector(rx_crc_errdetect_cntr);
        else
          if(rx_serror_corrected_i = '1') then
            rx_serror_correc_cntr <= rx_serror_correc_cntr + 1;
            rx_seen_serror_correc <= '1';
          end if;

          if(rx_derror_corrected_i = '1') then
            rx_derror_correc_cntr <= rx_derror_correc_cntr + 1;
            rx_seen_derror_correc <= '1';
          end if;

          if(decoded_ctrl_error_p_async = '1' and decoded_ctrl_p_async = '1') then
            rx_crc_errdetect_cntr <= rx_crc_errdetect_cntr + 1;
            rx_seen_crc_errdetect <= '1';
          end if;
          
        end if;
      end if;
    end if;
  end process p_fec_crc_error_cntr;

  --============================================================================
  -- MGT Status Monitoring
  --============================================================================  
  --============================================================================
  -- Process p_mgtstat_monitor
  --! read:  phy_mon_clear, mgt_tx_ready_i, mgt_rx_ready_i, mgt_txpll_lock_i, mgt_rxpll_lock_i\n
  --! write: mgt_seen_tx_nready_mngrsync_meta, mgt_seen_rx_nready_mngrsync_meta, mgt_seen_txpll_nlock_mngrsync_meta, mgt_seen_rxpll_nlock_mngrsync_meta\n
  --! r/w:   \n
  --============================================================================         
  p_mgtstat_monitor : process(clk_manager_i) is
  begin
    if(clk_manager_i'event and clk_manager_i = '1') then
      if(phy_mon_clear = '1') then
        mgt_seen_tx_nready_mngrsync_meta   <= '0';
        mgt_seen_rx_nready_mngrsync_meta   <= '0';
        mgt_seen_txpll_nlock_mngrsync_meta <= '0';
        mgt_seen_rxpll_nlock_mngrsync_meta <= '0';
        seen_rx_nlocked_mngrsync_meta      <= '0';
      else
        if(mgt_tx_ready_i = '0') then
          mgt_seen_tx_nready_mngrsync_meta <= '1';
        end if;
        if(mgt_rx_ready_i = '0') then
          mgt_seen_rx_nready_mngrsync_meta <= '1';
        end if;
        if(mgt_txpll_lock_i = '0') then
          mgt_seen_txpll_nlock_mngrsync_meta <= '1';
        end if;
        if(mgt_rxpll_lock_i = '0') then
          mgt_seen_rxpll_nlock_mngrsync_meta <= '1';
        end if;
        if(rx_locked_i = '0') then
          seen_rx_nlocked_mngrsync_meta <= '1';
        end if;
      end if;
    end if;
  end process p_mgtstat_monitor;
  ------------------------------------------------------------------------------

  --============================================================================
  -- Interrupt
  --============================================================================  
  interrupt_async <= (interrupt_mask(0) and mgt_seen_tx_nready_mngrsync_meta) or
                     (interrupt_mask(1) and mgt_seen_rx_nready_mngrsync_meta) or
                     (interrupt_mask(2) and mgt_seen_txpll_nlock_mngrsync_meta) or
                     (interrupt_mask(3) and mgt_seen_rxpll_nlock_mngrsync_meta) or
                     (interrupt_mask(4) and seen_rx_nlocked_mngrsync_meta) or
                     --(interrupt_mask(5) and seen_phase_ngood_mngrsync_meta) or
                     (interrupt_mask(6) and rx_seen_serror_correc) or
                     (interrupt_mask(7) and rx_seen_derror_correc) or
                     (interrupt_mask(8) and rx_seen_crc_errdetect);
  interrupt_o <= interrupt_mngrsync_r;

  status_sticky(0)           <= mgt_seen_tx_nready_mngrsync_r;
  status_sticky(1)           <= mgt_seen_rx_nready_mngrsync_r;
  status_sticky(2)           <= mgt_seen_txpll_nlock_mngrsync_r;
  status_sticky(3)           <= mgt_seen_rxpll_nlock_mngrsync_r;
  status_sticky(4)           <= seen_rx_nlocked_mngrsync_r;
  status_sticky(5)           <= '0';
  status_sticky(6)           <= rx_seen_serror_correc_mngrsync_r;
  status_sticky(7)           <= rx_seen_derror_correc_mngrsync_r;
  status_sticky(8)           <= rx_seen_crc_errdetect_mngrsync_r;
  status_sticky(30 downto 9) <= (others => '0');
  status_sticky_o            <= status_sticky;
  ------------------------------------------------------------------------------     

  --============================================================================
  -- Synchronizers from/to register bank
  --============================================================================
  --============================================================================
  -- Process p_mngrsync
  --! MNGR Synchronizer
  --! read:  clk_manager_i, - \n
  --! write: -\n
  --! r/w: -\n
  --============================================================================        
  p_mngrsync : process(clk_manager_i) is
  begin
    if (clk_manager_i'event and clk_manager_i = '1') then
      rx_seen_serror_correc_mngrsync_meta <= rx_seen_serror_correc;
      rx_seen_serror_correc_mngrsync_r    <= rx_seen_serror_correc_mngrsync_meta;
      rx_seen_derror_correc_mngrsync_meta <= rx_seen_derror_correc;
      rx_seen_derror_correc_mngrsync_r    <= rx_seen_derror_correc_mngrsync_meta;
      rx_seen_crc_errdetect_mngrsync_meta <= rx_seen_crc_errdetect;
      rx_seen_crc_errdetect_mngrsync_r    <= rx_seen_crc_errdetect_mngrsync_meta;
      sc_framing_locked_mngrsync_meta     <= sc_framing_locked_async;
      sc_framing_locked_mngrsync_r        <= sc_framing_locked_mngrsync_meta;
      mgt_seen_tx_nready_mngrsync_r       <= mgt_seen_tx_nready_mngrsync_meta;
      mgt_seen_rx_nready_mngrsync_r       <= mgt_seen_rx_nready_mngrsync_meta;
      mgt_seen_txpll_nlock_mngrsync_r     <= mgt_seen_txpll_nlock_mngrsync_meta;
      mgt_seen_rxpll_nlock_mngrsync_r     <= mgt_seen_rxpll_nlock_mngrsync_meta;
      seen_rx_nlocked_mngrsync_r          <= seen_rx_nlocked_mngrsync_meta;
      interrupt_mngrsync_meta             <= interrupt_async;
      interrupt_mngrsync_r                <= interrupt_mngrsync_meta;
    end if;
  end process p_mngrsync;

  --============================================================================
  -- Process p_trxsync
  --! TRX synchronizer
  --! read:  - \n
  --! write: -\n
  --! r/w: -\n
  --============================================================================        
  p_trxsync : process(clk_trxusr240_i) is
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      rx_clear_error_cntr_trxsync_meta <= rx_clear_error_cntr_async;
      rx_latch_error_cntr_trxsync_meta <= rx_latch_error_cntr_async;
      rx_clear_error_cntr_trxsync_r    <= rx_clear_error_cntr_trxsync_meta;
      rx_latch_error_cntr_trxsync_r    <= rx_latch_error_cntr_trxsync_meta;
      rx_clear_error_cntr_trxsync_r2   <= rx_clear_error_cntr_trxsync_r;
      rx_latch_error_cntr_trxsync_r2   <= rx_latch_error_cntr_trxsync_r;

      onu_mode_trxsync_meta <= onu_mode_async;
      onu_mode_trxsync_r    <= onu_mode_trxsync_meta;

      tdm_local_time_limit_trxsync_meta  <= tdm_local_time_limit_async;
      tdm_local_time_limit_trxsync_r     <= tdm_local_time_limit_trxsync_meta;
      tdm_local_time_offset_trxsync_meta <= tdm_local_time_offset_async;
      tdm_local_time_offset_trxsync_r    <= tdm_local_time_offset_trxsync_meta;
      phy_tx_en_pos_trxsync_meta         <= phy_tx_en_pos_async;
      phy_tx_en_pos_trxsync_r            <= phy_tx_en_pos_trxsync_meta;
	  
      tx_nb_barrel_trxsync_meta    <= tx_nb_barrel_async;
    end if;
  end process p_trxsync;

  tx_calibration_on_o <= calibration_on_r;
  onu_operational_o   <= onu_operational_r;
  tx_nb_barrel_o      <= tx_nb_barrel_trxsync_meta when calibration_on_r = '0' else "0000";

  tx_ena_o       <= tdm_tx_frame_ena;
  tx_slow_beat_o <= rx_heartbeat_header_i;
  tx_fast_beat_o <= rx_regular_header_i;

end architecture rtl;
--============================================================================
-- architecture end
--============================================================================
