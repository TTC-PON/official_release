--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file onu_tdm_timer.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
--------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
--------------------------------------------------------------------------------
--
-- unit name: ONU TDM timer
--
--! @brief This module generates the local time for ONU data transmission in the tdm scheme
--
--! @author Eduardo Mendes (eduardo.brandao.de.souza.mendes@cern.ch)
--! @date 12/07/2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! none
--!
--! <b>References:</b>\n
--!
--! <b>Modified by:</b>\n
--! Author: EBSM
--------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 12-07-2016  EBSM  Created\n
--------------------------------------------------------------------------------
--! @todo TODO\n
--! TODO details
--------------------------------------------------------------------------------
--==============================================================================
--! Entity declaration for ONU TDM Timer
--==============================================================================
entity onu_tdm_timer is
  port (
    clk_i          : in std_logic;
    reset_i        : in std_logic;
    rx_heartbeat_i : in std_logic;

    tx_enable_limit_i   : in std_logic_vector(7 downto 0);  --! Burst duration  (LSB = PERIOD clk_i)
    local_time_limit_i  : in std_logic_vector(15 downto 0);  --! Heartbeat period (LSB = PERIOD clk_i)      
    local_time_offset_i : in std_logic_vector(15 downto 0);  --! Local offset when ONU is allowed to transmit

    heartbeat_locked_o : out std_logic;
    tx_frame_ena_o     : out std_logic
    );
end onu_tdm_timer;

--==============================================================================
-- architecture declaration
--==============================================================================
architecture rtl of onu_tdm_timer is

  --! Functions

  --! Constants
  constant c_LOCK_POS_THRESHOLD   : integer := 14;
  constant c_UNLOCK_POS_THRESHOLD : integer := 8;

  --! Signal declaration

  --! Lock logic implemented to avoid fake received heartbeats to avoid "local_time_cntr" to reset
  type   t_tdm_lock_fsm_states is (UNLOCKED, LOCKED);
  signal tdm_lock_fsm     : t_tdm_lock_fsm_states;
  signal heartbeat_locked : std_logic;
  signal local_time_cntr  : unsigned(local_time_limit_i'range);

  --! Tx enable logic
  type   t_tx_enable_states is (TX_DISABLED, TX_ENABLED);
  signal tx_enable_fsm_state : t_tx_enable_states;
  signal time_to_send_p    : std_logic;

  signal tx_enable_counter : unsigned(tx_enable_limit_i'range);
  signal tx_enable_timeout : std_logic;

  attribute mark_debug : string;

  attribute MARK_DEBUG of heartbeat_locked  : signal is "TRUE";
  attribute MARK_DEBUG of tdm_lock_fsm      : signal is "TRUE";
  attribute MARK_DEBUG of local_time_cntr   : signal is "TRUE";
  attribute MARK_DEBUG of tx_enable_fsm_state : signal is "TRUE";
  attribute MARK_DEBUG of time_to_send_p    : signal is "TRUE";

--==============================================================================
-- architecture begin
--==============================================================================
begin  -- rtl

  p_tdm_lock_fsm : process(clk_i) is
    variable v_good_pos_cntr : integer range 0 to (c_LOCK_POS_THRESHOLD+1);
  begin
    if (clk_i'event and clk_i = '1') then
      if reset_i = '1' then
        tdm_lock_fsm  <= UNLOCKED;
        v_good_pos_cntr := 0;
      else
        if(tdm_lock_fsm = UNLOCKED) then
          if(v_good_pos_cntr >= c_LOCK_POS_THRESHOLD) then
            tdm_lock_fsm <= LOCKED;
          end if;
        else
          if(v_good_pos_cntr <= c_UNLOCK_POS_THRESHOLD) then
            tdm_lock_fsm <= UNLOCKED;
          end if;
        end if;

        if (local_time_cntr = unsigned(local_time_limit_i)) then
          if (rx_heartbeat_i = '1') then
            if (v_good_pos_cntr < (c_LOCK_POS_THRESHOLD+1)) then
              v_good_pos_cntr := v_good_pos_cntr + 1;
            end if;
          else
            if (v_good_pos_cntr > 0) then
              v_good_pos_cntr := v_good_pos_cntr - 1;
            end if;
          end if;
        elsif (local_time_cntr /= unsigned(local_time_limit_i) and rx_heartbeat_i = '1') then
          if (v_good_pos_cntr > 0) then
            v_good_pos_cntr := v_good_pos_cntr - 1;
          end if;
        end if;

      end if;
    end if;
  end process p_tdm_lock_fsm;

  heartbeat_locked <= '1' when tdm_lock_fsm = LOCKED else '0';

  p_tdm_local_time_cntr : process(clk_i) is
  begin
    if (clk_i'event and clk_i = '1') then
      if reset_i = '1' then
        local_time_cntr <= to_unsigned(0, 16);
      else
        if tdm_lock_fsm = LOCKED then
          if local_time_cntr = unsigned(local_time_limit_i) then
            local_time_cntr <= to_unsigned(0, 16);
          else
            local_time_cntr <= local_time_cntr + 1;
          end if;
        else
          if rx_heartbeat_i = '1' then
            local_time_cntr <= to_unsigned(0, 16);
          elsif local_time_cntr = unsigned(local_time_limit_i) then
            local_time_cntr <= to_unsigned(0, 16);
          else
            local_time_cntr <= local_time_cntr + 1;
          end if;
        end if;
      end if;
    end if;
  end process p_tdm_local_time_cntr;

  p_tx_enable_fsm : process(clk_i) is
  begin
    if (clk_i'event and clk_i = '1') then
      if reset_i = '1' then
        tx_enable_fsm_state <= TX_DISABLED;
      else
        tx_enable_fsm_state <= tx_enable_fsm_state;
        case tx_enable_fsm_state is
          when TX_DISABLED =>
            if time_to_send_p = '1' then
              tx_enable_fsm_state <= TX_ENABLED;
            end if;
          when TX_ENABLED =>
            if tx_enable_timeout = '1' then
              tx_enable_fsm_state <= TX_DISABLED;
            end if;
          when others =>
            tx_enable_fsm_state <= TX_DISABLED;
        end case;
      end if;
    end if;
  end process p_tx_enable_fsm;

  p_tx_enable_fsm_aux : process(clk_i) is
  begin
    if (clk_i'event and clk_i = '1') then
      if reset_i = '1' then
        time_to_send_p  <= '0';
        tx_enable_counter <= to_unsigned(0, tx_enable_counter'length);
        tx_enable_timeout <= '0';
      else
        if (local_time_cntr = unsigned(local_time_offset_i) and heartbeat_locked = '1') then
          time_to_send_p <= '1';
        else
          time_to_send_p <= '0';
        end if;

        if tx_enable_fsm_state = TX_ENABLED then
          tx_enable_counter <= tx_enable_counter + 1;
        else
          tx_enable_counter <= to_unsigned(0, tx_enable_counter'length);
        end if;

        if tx_enable_counter >= unsigned(tx_enable_limit_i) then
          tx_enable_timeout <= '1';
        else
          tx_enable_timeout <= '0';
        end if;
      end if;
    end if;
  end process p_tx_enable_fsm_aux;


  tx_frame_ena_o     <= '1' when tx_enable_fsm_state = TX_ENABLED else '0';
  heartbeat_locked_o <= heartbeat_locked;
  
end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================

