--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file pon_up_sc_tx.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Slow control encoder design for TTC-PON upstream (pon_up_sc_tx)
--
--! @brief Slow control encoder design for TTC-PON upstream
--! - It segment the control frame into bytes to be sent every ONU transmission
--! - Append header in bit 7 to allow frame boundary identification
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 03\05\2017
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 03\05\2017 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for pon_up_sc_tx
--==============================================================================
entity pon_up_sc_tx is
  generic(
    g_HEADER    : std_logic_vector(2 downto 0) := "110";
    g_IDLE_BYTE : std_logic_vector(7 downto 0) := "00000000"
    );
  port (
    clk_i            : in  std_logic;   --! clock input
    reset_i          : in  std_logic;   --! active high sync. reset
    byte_sent_i      : in  std_logic;   --! control scheduling
    data_o           : out std_logic_vector(7 downto 0);  --! encoded output data   
    data_i           : in  std_logic_vector(20 downto 0);  --! user input data
    send_data_req_i  : in  std_logic;   --! user interface control
    send_data_busy_o : out std_logic    --! user interface control      
    );
end pon_up_sc_tx;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of pon_up_sc_tx is

  attribute mark_debug : string;

  --! Constant declaration
  constant c_MOD_BYTE_CNTR : integer := 3;

  --! Signal declaration
  signal data_r : std_logic_vector(data_i'range);

  signal byte_cntr : integer range 0 to c_MOD_BYTE_CNTR;

  signal framing_busy : std_logic;

  attribute mark_debug of data_r       : signal is "true";
  attribute mark_debug of byte_cntr    : signal is "true";
  attribute mark_debug of framing_busy : signal is "true";
begin

  --============================================================================
  -- Process p_byte_cntr
  --! counter for scheduling
  --! read: byte_sent_i\n
  --! write: -\n
  --! r/w: byte_cntr \n
  --============================================================================   
  p_byte_cntr : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        byte_cntr <= 0;
      elsif(byte_cntr > 0 and byte_sent_i = '1') then
        byte_cntr <= byte_cntr - 1;
      elsif(framing_busy = '0' and send_data_req_i = '1') then
        byte_cntr <= c_MOD_BYTE_CNTR;
      end if;
    end if;
  end process p_byte_cntr;

  framing_busy     <= '1' when (byte_cntr > 0) else '0';
  send_data_busy_o <= framing_busy;

  --============================================================================
  -- Process p_data_r
  --! register user data
  --! read: data_i, framing_busy, send_data_req_i\n
  --! write: data_r\n
  --! r/w: - \n
  --============================================================================
  p_data_r : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(framing_busy = '0' and send_data_req_i = '1') then
        data_r <= data_i;
      end if;
    end if;
  end process p_data_r;

  --============================================================================
  -- Process p_output_mux
  --! Multiplexer to send byte to TX
  --! read: data_r, byte_cntr\n
  --! write: data_o\n
  --! r/w: - \n
  --============================================================================
  p_output_mux : process(byte_cntr, data_r)
  begin
    case byte_cntr is
      when 3 =>
        data_o(6 downto 0) <= data_r(7*(2+1)-1 downto 7*2);
        data_o(7)          <= g_HEADER(2);
      when 2 =>
        data_o(6 downto 0) <= data_r(7*(1+1)-1 downto 7*1);
        data_o(7)          <= g_HEADER(1);
      when 1 =>
        data_o(6 downto 0) <= data_r(7*(0+1)-1 downto 7*0);
        data_o(7)          <= g_HEADER(0);
      when others =>
        data_o <= g_IDLE_BYTE;
    end case;
  end process p_output_mux;

end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================
