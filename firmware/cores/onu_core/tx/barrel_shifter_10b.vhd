--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file barrel_shifter_10b.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: 10b width Barrel Shifter
--
--! @brief 10b Barrel shifter
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 05\02\2016 - EBSM - Created\n
--! 14\01\2018 - EBSM - Modified datapath to 10b\n
-------------------------------------------------------------------------------
--! @todo - \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for barrel_shifter_10b
--============================================================================
entity barrel_shifter_10b is
  port(
    clk_i      : in  std_logic;
    nb_shift_i : in  std_logic_vector(3 downto 0);
    data_i     : in  std_logic_vector(9 downto 0);
    data_o     : out std_logic_vector(9 downto 0)
    );
end entity barrel_shifter_10b;


--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of barrel_shifter_10b is

  --! Signal declaration
  signal data_r : std_logic_vector(data_i'range);

--============================================================================
-- architecture begin
--============================================================================  
begin

  --============================================================================
  -- Process p_barrel
  --! read: data_i\n
  --! write: data_o\n
  --! r/w  : data_r\n
  --============================================================================        
  p_barrel : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      data_r <= data_i;
      case nb_shift_i is
        when "0000" => data_o <= data_i(9 downto 0);
        when "0001" => data_o <= data_i(8 downto 0)&data_r(9 downto 9);
        when "0010" => data_o <= data_i(7 downto 0)&data_r(9 downto 8);
        when "0011" => data_o <= data_i(6 downto 0)&data_r(9 downto 7);
        when "0100" => data_o <= data_i(5 downto 0)&data_r(9 downto 6);
        when "0101" => data_o <= data_i(4 downto 0)&data_r(9 downto 5);
        when "0110" => data_o <= data_i(3 downto 0)&data_r(9 downto 4);
        when "0111" => data_o <= data_i(2 downto 0)&data_r(9 downto 3);
        when "1000" => data_o <= data_i(1 downto 0)&data_r(9 downto 2);
        when "1001" => data_o <= data_i(0 downto 0)&data_r(9 downto 1);
        when others  => data_o <= data_i(9 downto 0);
      end case;
    end if;
  end process p_barrel;
  
end rtl;
