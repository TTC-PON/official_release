--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file enc8b10b_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Any-size 8b10b Encoder (enc8b10b_wrapper)
--
--! @brief performs 8b10b encoding using an interfacing similar to Xilinx transceivers 8b10b encoding
--! Instantiates a number N=g_NBR_8B10B_ENC
--! The module 8b10b_enc_modif is a modification from the encoder available at "http://opencores.org"
--! The modification was performed in order to have only rising-edge FFs and allow an easy integration of several encoders
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 05\02\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for enc8b10b_wrapper
--============================================================================
entity enc8b10b_wrapper is
  generic(
    g_NBR_8B10B_ENC : integer := 2      -- >= 2
    );
  port
    (
      clk_i           : in  std_logic;  -- clock input - rising edge triggered
      reset_i         : in  std_logic;  -- sync reset - active high                                       
      k_i             : in  std_logic_vector((g_NBR_8B10B_ENC-1) downto 0);  -- Control (K) input(active high)
      force_disp_i    : in  std_logic_vector((g_NBR_8B10B_ENC-1) downto 0);  -- Force disparity
      disp_val_i      : in  std_logic_vector((g_NBR_8B10B_ENC-1) downto 0);  -- Disparity value (if force disparity)
      tx8b10bbypass_i : in  std_logic_vector((g_NBR_8B10B_ENC-1) downto 0);  -- bypass 8b10b encoding                                           
      data_i          : in  std_logic_vector((g_NBR_8B10B_ENC*8-1) downto 0);  -- user input data
      data_o          : out std_logic_vector((g_NBR_8B10B_ENC*10-1) downto 0)  -- encoded output data 
      );
end entity enc8b10b_wrapper;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of enc8b10b_wrapper is

  --! Signal declaration
  signal disp_in_s    : std_logic_vector((g_NBR_8B10B_ENC-1) downto 0);
  signal disp_fbk_reg : std_logic;
  signal disp_out_s   : std_logic_vector((g_NBR_8B10B_ENC-1) downto 0);

--============================================================================
-- architecture begin
--============================================================================   
begin

  --============================================================================
  -- Generic component declaration
  --! 8b10b encoder declaration
  --============================================================================        
  gen_cmp_8b10b : for i in 1 to g_NBR_8B10B_ENC generate
  begin
    cmp_8b10b_enc : entity work.enc_8b10b_modif
      port map
      (
        RESET        => reset_i,
        SBYTECLK     => clk_i,
        KI           => k_i(i-1),
        DISP_VAL     => disp_in_s(i-1),
        DISP_OUT     => disp_out_s(i-1),
        AI           => data_i(0+(i-1)*8),
        BI           => data_i(1+(i-1)*8),
        CI           => data_i(2+(i-1)*8),
        DI           => data_i(3+(i-1)*8),
        EI           => data_i(4+(i-1)*8),
        FI           => data_i(5+(i-1)*8),
        GI           => data_i(6+(i-1)*8),
        HI           => data_i(7+(i-1)*8),
        BYPASS_8b10b => tx8b10bbypass_i(i-1),
        I_EXT0       => force_disp_i(i-1),
        I_EXT1       => disp_val_i(i-1),
        AO           => data_o(0+(i-1)*10),
        BO           => data_o(1+(i-1)*10),
        CO           => data_o(2+(i-1)*10),
        DO           => data_o(3+(i-1)*10),
        EO           => data_o(4+(i-1)*10),
        IO           => data_o(5+(i-1)*10),
        FO           => data_o(6+(i-1)*10),
        GO           => data_o(7+(i-1)*10),
        HO           => data_o(8+(i-1)*10),
        JO           => data_o(9+(i-1)*10)
        );              

  end generate gen_cmp_8b10b;

  --============================================================================
  -- Process p_feedback_disp
  --! Feedback register to keep disparity value of 8b10b encoding
  --! read: disp_out_s(g_NBR_8B10B_ENC-1) \n
  --! write: disp_fbk_reg \n
  --! r/w: \n
  --============================================================================        
  p_feedback_disp : process (reset_i, clk_i)
  begin
    if clk_i'event and clk_i = '1' then
      if reset_i = '1' then
        disp_fbk_reg <= '0';
      else
        disp_fbk_reg <= disp_out_s(g_NBR_8B10B_ENC-1);
      end if;
    end if;
  end process;

  gen_disp_8b10b : for i in 1 to (g_NBR_8B10B_ENC-1) generate
  begin
    disp_in_s(i) <= disp_out_s(i-1) when (force_disp_i(i) = '0' and tx8b10bbypass_i(i) = '0') else disp_val_i(i);
  end generate gen_disp_8b10b;

  disp_in_s(0) <= disp_fbk_reg when (force_disp_i(0) = '0' and tx8b10bbypass_i(0) = '0') else disp_val_i(0);

end rtl;

