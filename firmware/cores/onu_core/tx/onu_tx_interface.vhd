--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file onu_tx_interface.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: ONU external interfacing (onu_tx_interface)
--
--! @brief ONU interfacing
--! See user interface timing below entity definition
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 17\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 17\06\2016 - EBSM - implemented\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo documentation \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for onu_tx_interface
--============================================================================
entity onu_tx_interface is
  generic(
    g_DATA_USR_WIDTH  : integer := 48;
    g_DATA_CTRL_WIDTH : integer := 16
    );
  port (
    clk_i               : in  std_logic;
    reset_i             : in  std_logic;
    data_accept_i       : in  std_logic;  --! from frame generator         
    interface_usr_ena_i : in  std_logic;  --! enable user interface
    data_usr_strobe_i   : in  std_logic;  --! from user interface
    data_usr_ready_o    : out std_logic;  --! to user interface
    data_ctrl_ready_o   : out std_logic;  --! to control block
    data_usr_i          : in  std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);
    data_usr_ovr_i      : in  std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);  --! when user interface is disabled, send this data
    data_ctrl_i         : in  std_logic_vector(g_DATA_CTRL_WIDTH-1 downto 0);
    data_o              : out std_logic_vector(g_DATA_USR_WIDTH+g_DATA_CTRL_WIDTH-1 downto 0)
    );
end entity onu_tx_interface;

-- ------------------------------- Timing input/output -------------------------------
-- USER INTERFACE (when interface_usr_ena_i='1') -------------------------------------
--
-- *** (1) STROBE BEFORE READY (strobe (REQ) -> ready (ACK)) ***  
--                                       _______________________                                             
-- data_usr_strobe_i _________.____.____/                       \____.____.____.____.
--
-- data_usr_i      X   PREVIOUS DATA    X        NEW DATA                           X                         
--                                                           ___                     
-- data_usr_ready_o __________.____.____.____.____.____.____/   \____.____.____.____.
--
-- data_o(64:16)                   PREVIOUS DATA                X        NEW DATA         
-- 
-- *** (2) READY BEFORE STROBE (ready -> strobe) ***  
--                                                           ___                                              
-- data_usr_strobe_i __________.____.____.____.____.____.___/   \____.____.____.____. 
--
-- data_usr_i      X   PREVIOUS DATA                       X     NEW DATA          X                              
--                                      _______________________                      
-- data_usr_ready_o _________.____.____/                       \____.____.____.____. 
--
-- data_o(64:16)   X               PREVIOUS DATA               X        NEW DATA   X         
--
-- *** (3) READY-STROBE AT THE SAME TIME (strobe - ready) ***  
--                                                          ___                      
-- data_usr_strobe_i__________.____.____.____.____.____.___/   \____.____.____.____. 
--
-- data_usr_i      X   PREVIOUS DATA                       X     NEW DATA          X 
--                                                          ___                      
-- data_usr_ready_o __________.____.____.____.____.____.___/   \____.____.____.____. 
--
-- data_o(64:16)   X               PREVIOUS DATA               X        NEW DATA   X 
--
-- *** (4) READY BUT NO STROBE (send previous captured data) ***  
--                                                                  
-- data_usr_strobe_i__________.____.____.____.____.____.____.____.____.____.____.___
--
-- data_usr_i      X   PREVIOUS DATA                                               X
--                                       ______________________________                     
-- data_usr_ready_o __________.____.____/                              \___.____.___
--                                                                   |
-- data_o(64:16)   X               PREVIOUS DATA (can be used as idle pattern)     X
-- ----------------------------------------------------------------------------------

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of onu_tx_interface is

  attribute mark_debug : string;

  --! Functions

  --! Constants

  --! Signal declaration
  type   t_fsm_interface is (IDLE, WAIT_STROBE, WAIT_NREADY);
  signal frame_interface_fsm_state : t_fsm_interface;

  signal data_accept_r : std_logic;

  signal data_usr_ready_r  : std_logic;
  signal data_ctrl_ready_p : std_logic;

  signal txdata_ctrl_r : std_logic_vector(data_ctrl_i'range);
  signal txdata_usr_r  : std_logic_vector(data_usr_i'range);

  attribute mark_debug of txdata_ctrl_r : signal is "true";

  --! Component declaration

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Process p_usr_interface_fsm
  --! FSM responsible for data interfacing with user data
  --! read:  data_accept_i,data_usr_strobe_i,data_accept_i\n
  --! write: txdata_usr_r, data_usr_ready_r\n
  --! r/w:   frame_interface_fsm_state\n
  --============================================================================          
  p_usr_interface_fsm : process(clk_i)
  begin
    if (clk_i'event and clk_i = '1') then
      if reset_i = '1' then
        frame_interface_fsm_state <= IDLE;
        data_usr_ready_r          <= '0';
      else
        frame_interface_fsm_state <= frame_interface_fsm_state;

        if (interface_usr_ena_i = '0') then  --when usr interface is not enabled, txdata_usr_r receives ovr data
          frame_interface_fsm_state <= IDLE;
          data_usr_ready_r          <= '0';
          if(data_ctrl_ready_p = '1') then
            txdata_usr_r <= data_usr_ovr_i;
          end if;
        else

          case frame_interface_fsm_state is
            when IDLE =>
              data_usr_ready_r <= data_accept_i;
              --                              if(data_accept_i='1' and data_usr_strobe_i='0') then          -- (2) READY BEFORE STROBE
              --                                     frame_interface_fsm_state<=WAIT_STROBE;
              if(data_usr_ready_r = '1' and data_usr_strobe_i = '0') then  -- (2) READY BEFORE STROBE
                frame_interface_fsm_state <= WAIT_STROBE;
              elsif(data_usr_ready_r = '1' and data_usr_strobe_i = '1') then  -- (1) STROBE BEFORE READY / (3) READY-STROBE AT THE SAME TIME
                frame_interface_fsm_state <= WAIT_NREADY;
                data_usr_ready_r          <= '0';
                txdata_usr_r              <= data_usr_i;  -- sample data   
              end if;
            when WAIT_STROBE =>
              if(data_usr_strobe_i = '1') then
                data_usr_ready_r          <= '0';
                frame_interface_fsm_state <= WAIT_NREADY;
                txdata_usr_r              <= data_usr_i;  -- sample data
              elsif(data_accept_i = '0') then  --no data was requested
                frame_interface_fsm_state <= IDLE;
                data_usr_ready_r          <= '0';
                txdata_usr_r              <= data_usr_i;  -- sample current data on the bus (???)
              else
                data_usr_ready_r <= data_accept_i;
              end if;
            when WAIT_NREADY =>
              if(data_accept_i = '0') then
                frame_interface_fsm_state <= IDLE;
              end if;
            when others =>
              frame_interface_fsm_state <= IDLE;
              data_usr_ready_r          <= '0';
          end case;
        end if;
        
      end if;
    end if;
  end process p_usr_interface_fsm;

  data_usr_ready_o                             <= data_usr_ready_r;
  data_o(data_o'left downto g_DATA_CTRL_WIDTH) <= txdata_usr_r;

  --============================================================================
  -- Process p_ctrl_interface
  --! Interface frame_generator to control_block
  --! read:  data_accept_i, data_ctrl_i, data_ctrl_ready_p\n
  --! write: txdata_ctrl_r\n
  --! r/w:   \n
  --============================================================================          
  p_ctrl_interface : process(clk_i)
  begin
    if (clk_i'event and clk_i = '1') then
      data_accept_r <= data_accept_i;

      if(data_ctrl_ready_p = '1') then
        txdata_ctrl_r <= data_ctrl_i;
      end if;
      
    end if;
  end process p_ctrl_interface;

  data_ctrl_ready_p <= '1' when data_accept_i = '1' and data_accept_r = '0' else '0';  --rising edge

  data_ctrl_ready_o                    <= data_ctrl_ready_p;
  data_o(g_DATA_CTRL_WIDTH-1 downto 0) <= txdata_ctrl_r;
  
end architecture rtl;
--============================================================================
-- architecture end
--============================================================================
