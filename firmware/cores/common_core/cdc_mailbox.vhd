--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file cdc_mailbox.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Mailbox Design (cdc_mailbox)
--
--! @brief This module ensure a proper CDC of a bus that doesn't change very often
--! <further description>
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 14\06\2016 - EBSM - Created\n
--! 06\06\2017 - EBSM - Modified topology to remove wait_taps
--!                     Now the mailbox uses a toggling scheme to transfer control signal
-------------------------------------------------------------------------------
--! @todo - \n
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for cdc_mailbox
--============================================================================
entity cdc_mailbox is
  generic(
    g_mailbox_type : integer := 0  -- 0 = regout (simple register logic) / >0 = mailbox
    );
  port(
    clk_a        : in std_logic;
    data_a_in    : in std_logic_vector;
    data_a_ready : in std_logic;

    clk_b         : in  std_logic;
    data_b_out    : out std_logic_vector;
    data_b_accept : out std_logic
    );
end cdc_mailbox;

--============================================================================
--! Timing in/out
--============================================================================
----timing:
----clk_a         :  ____/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\
----data_a_in     :   X X X X X X X X    / DATA STABLE \ X X X X X X X X X X X X X X X X X X X X X X X X 
----data_a_ready  :  ____________________/-------------\__...____________________________________________                       

----clk_b         :  ______/------\______/------\______...__/------\______/------\______/------\______
----data_b_out    :  X X X X X X X X    / X X X X X X X X X/  DATA STABLE                             
----data_b_accept :  _____________________________     ..._/--------------\___________________________
----                                                   ... |     SAMPLE                                                        
--                                      |                   |
--                                       this time is at most: PERIOD_CLK_A+3*PERIOD_CLK_B

--============================================================================
--! Architecture declaration for cdc_mailbox
--============================================================================
architecture rtl of cdc_mailbox is

  attribute ASYNC_REG : string;

  signal data_a2b_togflag : std_logic := '0';
  signal data_a_reg       : std_logic_vector(data_a_in'range) := (others => '0');
  signal data_a_ready_r   : std_logic := '0';

  signal data_b_ready    : std_logic := '0';
  signal data_b_ready_r  : std_logic := '0';
  signal data_b_ready_r2 : std_logic := '0';
  signal data_b_reg      : std_logic_vector(data_b_out'range) := (others => '0');

  attribute ASYNC_REG of data_b_ready    : signal is "TRUE";
  attribute ASYNC_REG of data_b_ready_r  : signal is "TRUE";
  attribute ASYNC_REG of data_b_ready_r2 : signal is "TRUE";

--============================================================================
-- architecture begin
--============================================================================  
begin

  process(clk_a)
  begin
    if(clk_a'event and clk_a = '1') then
      data_a2b_togflag <= data_a2b_togflag xor data_a_ready_r;  --used for mailbox
      data_a_ready_r   <= data_a_ready;

      if(data_a_ready = '1') then
        data_a_reg <= data_a_in;
      end if;

    end if;
  end process;

  gen_mailbox : if g_mailbox_type > 0 generate
    process(clk_b)
    begin
      if(clk_b'event and clk_b = '1') then
        data_b_ready    <= data_a2b_togflag;
        data_b_ready_r  <= data_b_ready;
        data_b_ready_r2 <= data_b_ready_r;

        data_b_accept <= '0';
        if((data_b_ready_r = '0' and data_b_ready_r2 = '1') or (data_b_ready_r = '1' and data_b_ready_r2 = '0')) then  --edge detector                   
          data_b_reg    <= data_a_reg;
          data_b_accept <= '1';
        end if;
        
      end if;
    end process;
  end generate;

  gen_regout : if g_mailbox_type = 0 generate
    data_b_reg    <= data_a_reg;
    data_b_accept <= data_a_ready_r;
  end generate;

  data_b_out <= data_b_reg;

end rtl;
