--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file i2c_rssi_ctrl.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: I2C master with RSSI control (i2c_rssi_ctrl)
--
--! @brief user interface description below
--! i2c_reg_ctrl_i: 
--!   EN                            : i2c_rssi_ctrl_i(0); -- Enable transaction
--!   RD_B2                         : i2c_rssi_ctrl_i(1); -- Read two consecutive addresses   
--!   RD_nWR                        : i2c_rssi_ctrl_i(2); -- Select read or write
--!   I2C_SLV_ADDR                  : i2c_rssi_ctrl_i(9 downto 3);
--!   I2C_REG_ADDR                  : i2c_rssi_ctrl_i(17 downto 10);
--!   I2C_WR_DATA                   : i2c_rssi_ctrl_i(25 downto 18);
--!
--! i2c_reg_stat_o:
--!   i2c_rssi_stat_o(0)            : I2C_DONE
--!   i2c_rssi_stat_o(1)            : I2C_ERROR
--!   i2c_rssi_stat_o(2)            : I2C_DROP_REQ
--!   i2c_rssi_stat_o(10 downto 3)  : I2C_REG_DATA_RD
--!   i2c_rssi_stat_o(18 downto 11) : I2C_REG+1_DATA_RD -- response to RD_B2 
--!
--!
--!
--! obs: i2c_rssi_ctrl_en_i should be high for one clk cycle to initiate transaction
--
--! @author Csaba Soos - csaba.soos@cern.ch
--! @date 05\04\2017
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Csaba Soos
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 05\04\2017 - CS - Created\n
--! 21\04\2017 - EBSM - Modified for burst read transaction
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for i2c_rssi_ctrl
--============================================================================
entity i2c_rssi_ctrl is
  generic (
    g_CLK_SYS_PERIOD : integer range 5 to 20 := 10;   -- clock period in ns
    g_RSSI_TRG_WIDTH : integer               := 600;  -- RSSI trigger pulse width in ns
    g_RSSI_WAIT_TIME : integer               := 500000;  -- RSSI conversion time in ns
    g_RSSI_ENABLE    : boolean               := true  -- ENABLE RSSI LOGIC (false for ONU, true for OLT)
    );
  port (
    -- global input
    clk_sys_i : in std_logic;
    reset_i   : in std_logic;

    -- reg interface
    i2c_rssi_ctrl_i    : in  std_logic_vector(31 downto 0);
    i2c_rssi_ctrl_en_i : in  std_logic;
    i2c_rssi_stat_o    : out std_logic_vector(31 downto 0);

    -- user interface
    i2c_req_o      : out std_logic;
    i2c_rnw_o      : out std_logic;
    i2c_rb2_o      : out std_logic;
    i2c_slv_addr_o : out std_logic_vector(6 downto 0);
    i2c_reg_addr_o : out std_logic_vector(7 downto 0);
    i2c_wr_data_o  : out std_logic_vector(7 downto 0);

    i2c_done_i     : in std_logic;
    i2c_error_i    : in std_logic;
    i2c_drop_req_i : in std_logic;
    i2c_rd_data_i  : in std_logic_vector(15 downto 0);

    -- RSSI trigger
    rssi_trg_o : out std_logic
    );
end i2c_rssi_ctrl;

architecture rtl of i2c_rssi_ctrl is

  attribute async_reg  : string;
  attribute mark_debug : string;

  --! Functions

  --! Constants
  constant c_RSSI_TRG_TIMEOUT  : integer := g_RSSI_TRG_WIDTH/g_CLK_SYS_PERIOD;
  constant c_RSSI_WAIT_TIMEOUT : integer := (g_RSSI_TRG_WIDTH+g_RSSI_WAIT_TIME)/g_CLK_SYS_PERIOD;
  constant c_MAX_TIMEOUT       : integer := c_RSSI_WAIT_TIMEOUT + 10;

  --! Signal declaration
  --============================================================================
  -- Status logic
  --============================================================================
  signal i2c_req      : std_logic;
  signal i2c_done     : std_logic;
  signal i2c_error    : std_logic;
  signal i2c_drop_req : std_logic;
  signal i2c_rd_data  : std_logic_vector(15 downto 0);

  ---- Timer for RSSI trigger 
  signal rssi_timer_counter : integer range 0 to c_MAX_TIMEOUT := 0;
  signal rssi_timer_reset   : std_logic                        := '0';
  signal rssi_trg_timeout   : std_logic                        := '0';
  signal rssi_wait_timeout  : std_logic                        := '0';

  ---- FSM for RSSI trigger and I2C readout
  type t_rssi_fsm_states is (
    IDLE,
    RSSI_TRIGGER,
    RSSI_CONVERSION,
    I2C_REQUEST
    );

  signal rssi_fsm_present : t_rssi_fsm_states := IDLE;

begin

  --============================================================================
  -- Process p_rssi_timer
  --! Generates timer for RSSI pulse generation (required for OLT-SFP)
  --! read:  rssi_timer_reset, c_RSSI_WAIT_TIMEOUT, c_RSSI_TRG_TIMEOUT\n
  --! write: rssi_trg_timeout, rssi_wait_timeout\n
  --! r/w:   rssi_timer_counter\n
  --============================================================================
  p_rssi_timer : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if (reset_i = '1') then
        rssi_timer_counter <= 0;
        rssi_trg_timeout   <= '0';
        rssi_wait_timeout  <= '0';
      else
        if (rssi_timer_reset = '1') then
          rssi_timer_counter <= 0;
          rssi_trg_timeout   <= '0';
          rssi_wait_timeout  <= '0';
        else
          if rssi_timer_counter < c_RSSI_WAIT_TIMEOUT then
            rssi_timer_counter <= rssi_timer_counter + 1;
          end if;
          if rssi_timer_counter = c_RSSI_TRG_TIMEOUT then
            rssi_trg_timeout <= '1';
          end if;
          if rssi_timer_counter = c_RSSI_WAIT_TIMEOUT then
            rssi_wait_timeout <= '1';
          end if;
        end if;
      end if;
    end if;
  end process p_rssi_timer;

  --============================================================================
  -- Process p_rssi_fsm
  --! FSM to interface I2C with core and generate RSSI signals
  --! read:  \n
  --! write: i2c_req_o, i2c_rb2_o, i2c_rnw_o, i2c_slv_addr_o, i2c_reg_addr_o, i2c_wr_data_o, i2c_done, i2c_error, i2c_drop_req\n
  --! r/w:   rssi_fsm_present\n
  --============================================================================
  p_rssi_fsm : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if (reset_i = '1') then
        rssi_fsm_present <= IDLE;
        i2c_req_o        <= '0';
        i2c_rb2_o        <= '0';
        i2c_rnw_o        <= '0';
        i2c_done         <= '0';
        i2c_error        <= '0';
        i2c_drop_req     <= '0';
      else
        case rssi_fsm_present is
          when IDLE =>
            if (i2c_rssi_ctrl_en_i = '1' and i2c_rssi_ctrl_i(0) = '1') then
              if (((i2c_rssi_ctrl_i(9 downto 2)) = x"a3") and (i2c_rssi_ctrl_i(17 downto 10) = x"68") and g_RSSI_ENABLE) then  -- check here
                rssi_fsm_present <= RSSI_TRIGGER;
              else
                rssi_fsm_present <= I2C_REQUEST;
                i2c_req_o        <= '1';
                i2c_rb2_o        <= i2c_rssi_ctrl_i(1);
                i2c_rnw_o        <= i2c_rssi_ctrl_i(2);
                i2c_slv_addr_o   <= i2c_rssi_ctrl_i(9 downto 3);
                i2c_reg_addr_o   <= i2c_rssi_ctrl_i(17 downto 10);
                i2c_wr_data_o    <= i2c_rssi_ctrl_i(25 downto 18);
              end if;
              i2c_done     <= '0';
              i2c_error    <= '0';
              i2c_drop_req <= '0';
            end if;
          when RSSI_TRIGGER =>
            if (rssi_trg_timeout = '1') then
              rssi_fsm_present <= RSSI_CONVERSION;
            end if;
          when RSSI_CONVERSION =>
            if (rssi_wait_timeout = '1') then
              rssi_fsm_present <= I2C_REQUEST;
              i2c_req_o        <= '1';
              i2c_rb2_o        <= i2c_rssi_ctrl_i(1);
              i2c_rnw_o        <= i2c_rssi_ctrl_i(2);
              i2c_slv_addr_o   <= i2c_rssi_ctrl_i(9 downto 3);
              i2c_reg_addr_o   <= i2c_rssi_ctrl_i(17 downto 10);
              i2c_wr_data_o    <= i2c_rssi_ctrl_i(25 downto 18);
              i2c_done         <= '0';
              i2c_error        <= '0';
              i2c_drop_req     <= '0';
            end if;
          when I2C_REQUEST =>
            if (i2c_done_i = '1' or i2c_error_i = '1' or i2c_drop_req_i = '1') then
              i2c_req_o        <= '0';
              i2c_rb2_o        <= '0';
              i2c_rnw_o        <= '0';
              i2c_done         <= i2c_done_i;
              i2c_error        <= i2c_error_i;
              i2c_drop_req     <= i2c_drop_req_i;
              i2c_rd_data      <= i2c_rd_data_i;
              rssi_fsm_present <= IDLE;
            end if;
        end case;
      end if;
    end if;
  end process p_rssi_fsm;

  rssi_timer_reset <= '0' when rssi_fsm_present = RSSI_TRIGGER else
                      '0' when rssi_fsm_present = RSSI_CONVERSION else
                      '1';

  rssi_trg_o <= '1' when rssi_fsm_present = RSSI_TRIGGER else '0';

  i2c_rssi_stat_o(18 downto 0)  <= i2c_rd_data(15 downto 0)&i2c_drop_req&i2c_error&i2c_done;
  i2c_rssi_stat_o(31 downto 19) <= "0000000000000";

end rtl;
