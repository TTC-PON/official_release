--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file bbert_top.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Any size BBERT tester (bbert_top)
--
--! @brief Any size BBERT tester
--! <further description>
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 23\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 07\07\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for bbert_top
--============================================================================
entity bbert_top is
  generic(
    g_CLK_CYCLES_FRAME  : integer   := 1;
    g_BLOCK_ACCUMULATOR : std_logic := '1'
    );
  port (
    clk_i   : in std_logic;
    reset_i : in std_logic;

    bbert_clear_acc_i   : in std_logic;
    bbert_latch_acc_i   : in std_logic;
    superframe_length_i : in std_logic_vector;
    onu_addr_chkr_i     : in std_logic_vector(7 downto 0);

    data_i        : in std_logic_vector;
    onu_addr_i    : in std_logic_vector(7 downto 0);
    data_strobe_i : in std_logic;

    error_sum_o       : out std_logic_vector;
    bit_sum_o         : out std_logic_vector;
    packet_loss_sum_o : out std_logic_vector;
    acc_full_o        : out std_logic;
    onu_locked_o      : out std_logic;
    onu_error_o       : out std_logic
    );
end entity bbert_top;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of bbert_top is

  --! Signal declaration  
  signal reset_r : std_logic; -- reset register to optimize fan-out
	
  signal onu_error_sum      : unsigned(error_sum_o'range);
  signal onu_bit_sum        : unsigned(bit_sum_o'range);
  signal onu_packetloss_sum : unsigned(packet_loss_sum_o'range);

  signal fake_comma             : std_logic;
  signal rx_frame_onu           : std_logic;
  signal onu_locked             : std_logic;
  signal rx_frame_cntr          : integer range 0 to (2**superframe_length_i'length-1);
  signal rx_frame_duration_cntr : integer range 0 to 15;
  signal onu_chkr_ena           : std_logic;

  signal acc_full        : std_logic;
  signal onu_chkr_din    : std_logic_vector(data_i'range);
  signal onu_chkr_refgen : std_logic_vector(data_i'range);
  signal rx_errs_onu     : std_logic_vector(8 downto 0);

  signal window_min_burst : unsigned(superframe_length_i'range);

  --! Component declaration
  component prbs_data_check is
    generic (
      G_DIN_WIDTH         : integer := 32;
      G_REVERSE_BITS      : integer := 0;
      G_MAX_ERRS_OUTWIDTH : integer := 6
      );
    port (
      clk_i  : in  std_logic;
      reset_i  : in  std_logic;
      ena_i    : in  std_logic;
      sel_i    : in  std_logic_vector(1 downto 0);
      -- "00" PRBS-7, "01" PRBS-23
      -- "1x" fix
      din_i    : in  std_logic_vector(G_DIN_WIDTH-1 downto 0);
      lock_o   : out std_logic;
      error_o  : out std_logic;
      nerrs_o  : out std_logic_vector(G_MAX_ERRS_OUTWIDTH-1 downto 0);
      refgen_o : out std_logic_vector(G_DIN_WIDTH-1 downto 0));
  end component prbs_data_check;

--============================================================================
-- architecture begin
--============================================================================
begin

  reset_r <= reset_i when rising_edge(clk_i);

  window_min_burst <= unsigned(superframe_length_i) - to_unsigned(3, superframe_length_i'length);

  --============================================================================
  -- Process p_bbert_ctrl
  --! read:  superframe_length_i,rx_frame_onu,data_i,data_strobe_i,onu_addr_i
  --! write: fake_comma,onu_chkr_din\n
  --! r/w:   rx_frame_duration_cntr, rx_frame_cntr\n
  --============================================================================                   
  p_bbert_ctrl : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_r = '1') then
        rx_frame_cntr          <= 0;
        rx_frame_duration_cntr <= 0;
        fake_comma             <= '0';
        onu_chkr_din           <= (others => '0');
      else
        if((data_strobe_i = '1' and onu_addr_i = onu_addr_chkr_i) and (onu_locked = '0' or (rx_frame_cntr >= to_integer(window_min_burst)))) then
          rx_frame_cntr          <= 0;
          rx_frame_duration_cntr <= g_CLK_CYCLES_FRAME;
          fake_comma             <= '0';
        elsif(rx_frame_cntr >= to_integer(unsigned(superframe_length_i))) then
          rx_frame_cntr          <= 0;
          rx_frame_duration_cntr <= g_CLK_CYCLES_FRAME;
          fake_comma             <= '1';
        else
          rx_frame_cntr <= rx_frame_cntr+1;
          fake_comma    <= '0';
          if(rx_frame_duration_cntr > 0) then
            rx_frame_duration_cntr <= rx_frame_duration_cntr-1;
          end if;
        end if;
        onu_chkr_ena <= rx_frame_onu;

        if(fake_comma = '1') then
          onu_chkr_din <= (others => '1');  -- avoids fake locking to another ONU logic
        elsif rx_frame_onu = '1' then
          onu_chkr_din <= data_i;
        end if;
        
      end if;
    end if;
  end process p_bbert_ctrl;

  rx_frame_onu <= '1' when rx_frame_duration_cntr > 0 else '0';

  --============================================================================
  -- Process p_bbert_acc
  --! Error / Bit / Packet Loss accumulator
  --! read:  rx_errs_onu,onu_chkr_ena,fake_comma,bbert_clear_acc_i
  --! write: acc_full\n
  --! r/w:   onu_error_sum,onu_bit_sum,onu_packetloss_sum\n
  --============================================================================        
  p_bbert_acc : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_r = '1') then
        onu_error_sum      <= (others => '0');
        onu_bit_sum        <= (others => '0');
        onu_packetloss_sum <= (others => '0');
        acc_full           <= '0';
      else
        if (bbert_clear_acc_i = '1') then
          onu_error_sum      <= (others => '0');
          onu_bit_sum        <= (others => '0');
          onu_packetloss_sum <= (others => '0');
          acc_full           <= '0';
        else
          if(g_BLOCK_ACCUMULATOR = '1' and (onu_bit_sum(onu_bit_sum'left-1) = '1')) then
            acc_full <= '1';
          else
            onu_error_sum <= onu_error_sum + unsigned(rx_errs_onu);
            if(onu_chkr_ena = '1') then
              onu_bit_sum <= onu_bit_sum + to_unsigned(data_i'length, onu_bit_sum'length);
            end if;
            if(fake_comma = '1') then
              onu_packetloss_sum <= onu_packetloss_sum + to_unsigned(1, onu_packetloss_sum'length);
            end if;
          end if;
        end if;
      end if;
    end if;
  end process p_bbert_acc;

  --============================================================================
  -- Process p_bbert_latch
  --! Latch error/bit/packetloss accumulators
  --! read:  rx_errs_onu,onu_chkr_ena,fake_comma,bbert_clear_acc_i
  --! write: acc_full\n
  --! r/w:   onu_error_sum,onu_bit_sum,onu_packetloss_sum\n
  --============================================================================           
  p_bbert_latch : process(clk_i) is
  begin
    if(clk_i'event and clk_i = '1') then
      if(bbert_latch_acc_i = '1') then
        error_sum_o       <= std_logic_vector(onu_error_sum);
        bit_sum_o         <= std_logic_vector(onu_bit_sum);
        packet_loss_sum_o <= std_logic_vector(onu_packetloss_sum);
        onu_locked_o      <= onu_locked;
        acc_full_o        <= acc_full;
      end if;
    end if;
  end process p_bbert_latch;

  --============================================================================
  -- Component instantiation
  --! Component prbs_data_check
  --============================================================================
  cmp_prbs_check_onu : prbs_data_check
    generic map (
      G_DIN_WIDTH         => data_i'length,
      G_REVERSE_BITS      => 0,
      G_MAX_ERRS_OUTWIDTH => rx_errs_onu'length
      )
    port map (
      clk_i  => clk_i,
      reset_i  => reset_r,
      ena_i    => onu_chkr_ena,
      sel_i    => "00",                 -- prbs 7
      din_i    => onu_chkr_din,
      lock_o   => onu_locked,
      error_o  => onu_error_o,
      nerrs_o  => rx_errs_onu,
      refgen_o => onu_chkr_refgen);

end architecture rtl;
--============================================================================
-- architecture end
--============================================================================

