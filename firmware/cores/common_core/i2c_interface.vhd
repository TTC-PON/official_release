--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file i2c_interface.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages

--------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON Project
-- --
--------------------------------------------------------------------------------
--
-- unit name: I2C interface (i2c_interface)
--
--! @brief This module provides access to the I2C bus
--! It is used as an example for TTC-PON integrated in the wrapper but it can be replaced by any other IIC master
--
--! @author Csaba Soos (csaba.soos AT cern.ch)
--
--! @date 16-07-2010
--
--! @version 1.0
--
--! @details
--!
--! <b>Dependencies:</b>\n
--! 
--!
--! <b>References:</b>\n
--!
--! <b>Modified by:</b>\n
--! Author: Csaba Soos
--------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 16-07-2010 CS  Created\n
--!  3-11-2014 CS  Modified for general purpose I2C implementation\n
--! 27-03-2017 CS  Simplified transaction by performing single-byte\n
--!                write/read\n
--! 21-04-2017 EBSM Modified for burst read transaction
--------------------------------------------------------------------------------
--! @todo TODO\n
--! TODO details
--------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for i2c_interface
--============================================================================
entity i2c_interface is
  generic (
    g_CLOCK_PERIOD : integer range 5 to 20 := 10;    -- clock period in ns
    g_SCL_PERIOD   : integer               := 10000  -- SCL period in ns
    );
  port (
    -- global input signals --
    clk_i : in std_logic;
    reset_i : in std_logic;
    -------------------------

    -- I2C transaction control/status signal --
    i2c_req_i      : in std_logic;
    i2c_rnw_i      : in std_logic;
    i2c_rb2_i      : in std_logic;
    i2c_slv_addr_i : in std_logic_vector(6 downto 0);
    i2c_reg_addr_i : in std_logic_vector(7 downto 0);
    i2c_wr_data_i  : in std_logic_vector(7 downto 0);

    i2c_done_o     : out std_logic;
    i2c_error_o    : out std_logic;
    i2c_drop_req_o : out std_logic;
    i2c_rd_data_o  : out std_logic_vector(15 downto 0);
    -------------------------------------------

    -- I2C in/out signal ----
    i2c_sdi_i     : in  std_logic;
    i2c_sdo_o     : out std_logic;
    i2c_sda_ena_o : out std_logic;
    i2c_scl_o     : out std_logic;
    i2c_scl_ena_o : out std_logic
    -------------------------
    );

end entity i2c_interface;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of i2c_interface is

  --! Functions

  --! Constants
  constant c_SCL_DIVIDE_VALUE : unsigned := to_unsigned((g_SCL_PERIOD/(4*g_CLOCK_PERIOD))-2, 12);
  constant c_WIDTH_NB_READ    : integer  := 1;

  --! Signal declaration
  type t_scl_state is (
    SCL_HIGH1,
    SCL_HIGH2,
    SCL_LOW1,
    SCL_LOW2);
  signal scl_present  : t_scl_state;
  signal sample_sdi : std_logic;
  signal change_sdo : std_logic;

  type t_i2c_state is (                 -- WRITE|   READ|
    I2C_IDLE,                           --      |       |
    I2C_TXSTART,                        --     1|      1|
    I2C_TXSADDR,                        --     2|      2|
    I2C_RXSAACK,                        --     3|      3|
    I2C_TXRADDR,                        --     4|      4|
    I2C_RXRAACK,                        --     5|      5|
    I2C_TXWDATA,                        --     6|       |
    I2C_RXWDACK,                        --     7|       |
    I2C_TXSTART2,                       --      |      6|
    I2C_TXSADDR2,                       --      |      7|
    I2C_RXSAACK2,                       --      |      8|
    I2C_RXRDATA,                        --      |      9|
    I2C_TXACK,           --      |     10|                       
    I2C_TXNACK,                         --      |     10-11|
    I2C_TXSTOP,                         --     8|     11-12|
    I2C_DONE,                           --  DONE|   DONE|
    I2C_ERROR);                         --   ERR|    ERR|
  signal i2c_present : t_i2c_state;

  signal scl_ena     : std_logic;
  signal scl_tick    : std_logic;
  signal scl_divider : unsigned(11 downto 0);

  signal bit_count     : integer range 0 to 7;
  signal bit_count_ena : std_logic;

  signal i2c_slvaddr : std_logic_vector(7 downto 0);
  signal i2c_regaddr : std_logic_vector(7 downto 0);
  signal i2c_wrdata  : std_logic_vector(7 downto 0);
  signal i2c_rddata  : std_logic_vector(7 downto 0);

  -- EBSM 20.04
  type   t_regbank8b is array (0 to (2**c_WIDTH_NB_READ-1)) of std_logic_vector(7 downto 0);
  signal burst_read_count     : integer range 0 to (2**c_WIDTH_NB_READ-1);
  signal burst_read_count_ena : std_logic;
  signal i2c_rddata_buf       : t_regbank8b;
  signal i2c_nb_bread         : std_logic_vector(c_WIDTH_NB_READ-1 downto 0);


--============================================================================
-- architecture begin
--============================================================================
begin  -- architecture rtl

  --============================================================================
  -- Process p_scl_fsm_aux
  --! Divides system clock to generate the SCL serial clock
  --! read:  \n
  --! write: \n
  --! r/w:   scl_divider\n
  --============================================================================           
  p_scl_fsm_aux : process (clk_i, reset_i) is
  begin  -- process p_scl_fsm_aux
    if reset_i = '1' then               -- asynchronous reset (active high)
      scl_divider <= (others => '0');
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if scl_tick = '1' then
        scl_divider <= c_SCL_DIVIDE_VALUE and x"7ff";
      else
        scl_divider <= scl_divider - 1;
      end if;
    end if;
  end process p_scl_fsm_aux;
  scl_tick <= scl_divider(11);

  --============================================================================
  -- Process p_scl_fsm_states
  --! FSM for generating SCL clock phases
  --! read:  scl_ena, scl_tick\n
  --! write: \n
  --! r/w:   scl_present\n
  --============================================================================           
  p_scl_fsm_states : process (clk_i, reset_i) is
  begin  -- process p_scl_fsm_states
    if reset_i = '1' then               -- asynchronous reset (active high)
      scl_present <= SCL_HIGH1;
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      scl_present <= scl_present;
      case scl_present is
        when SCL_HIGH1 =>
          if (scl_tick and scl_ena) = '1' then
            scl_present <= SCL_HIGH2;
          end if;
        when SCL_HIGH2 =>
          if scl_tick = '1' then
            scl_present <= SCL_LOW1;
          end if;
        when SCL_LOW1 =>
          if scl_tick = '1' then
            scl_present <= SCL_LOW2;
          end if;
        when SCL_LOW2 =>
          if scl_tick = '1' then
            scl_present <= SCL_HIGH1;
          end if;
      end case;
    end if;
  end process p_scl_fsm_states;

  sample_sdi <= scl_tick when scl_present = SCL_HIGH1 else '0';
  change_sdo <= scl_tick when scl_present = SCL_LOW1  else '0';
  i2c_scl_o    <= '1'        when scl_present = SCL_HIGH1 else
                  '1' when scl_present = SCL_HIGH2 else
                  '0';

  --============================================================================
  -- Process p_i2c_fsm_states
  --! FSM for generating I2C transactions
  --! read:  i2c_req_i, i2c_sdi_i, change_sdo, bit_count\n
  --!        sample_sdi\n
  --! write: \n
  --! r/w:   i2c_present\n
  --============================================================================           
  p_i2c_fsm_states : process (clk_i, reset_i) is
  begin  -- process p_i2c_fsm_states
    if reset_i = '1' then               -- asynchronous reset (active high)
      i2c_present <= I2C_IDLE;
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      i2c_present <= i2c_present;
      case i2c_present is
        when I2C_IDLE =>
          if i2c_req_i = '1' then
            i2c_present <= I2C_TXSTART;
          end if;
        when I2C_TXSTART =>
          if change_sdo = '1' then
            i2c_present <= I2C_TXSADDR;
          end if;
        when I2C_TXSADDR =>
          if change_sdo = '1' then
            if bit_count = 0 then
              i2c_present <= I2C_RXSAACK;
            end if;
          end if;
        when I2C_RXSAACK =>
          if sample_sdi = '1' then
            if i2c_sdi_i /= '0' then
              i2c_present <= I2C_ERROR;
            end if;
          elsif change_sdo = '1' then
            i2c_present <= I2C_TXRADDR;
          end if;
        when I2C_TXRADDR =>
          if change_sdo = '1' then
            if bit_count = 0 then
              i2c_present <= I2C_RXRAACK;
            end if;
          end if;
        when I2C_RXRAACK =>
          if sample_sdi = '1' then
            if i2c_sdi_i /= '0' then
              i2c_present <= I2C_ERROR;
            end if;
          elsif change_sdo = '1' then
            if i2c_rnw_i = '1' then
              i2c_present <= I2C_TXSTART2;
            else
              i2c_present <= I2C_TXWDATA;
            end if;
          end if;
        when I2C_TXWDATA =>
          if change_sdo = '1' then
            if bit_count = 0 then
              i2c_present <= I2C_RXWDACK;
            end if;
          end if;
        when I2C_RXWDACK =>
          if sample_sdi = '1' then
            if i2c_sdi_i /= '0' then
              i2c_present <= I2C_ERROR;
            end if;
          elsif change_sdo = '1' then
            i2c_present <= I2C_TXSTOP;
          end if;
        when I2C_TXSTART2 =>
          if change_sdo = '1' then
            i2c_present <= I2C_TXSADDR2;
          end if;
        when I2C_TXSADDR2 =>
          if change_sdo = '1' then
            if bit_count = 0 then
              i2c_present <= I2C_RXSAACK2;
            end if;
          end if;
        when I2C_RXSAACK2 =>
          if sample_sdi = '1' then
            if i2c_sdi_i = '1' then
              i2c_present <= I2C_ERROR;
            end if;
          elsif change_sdo = '1' then
            i2c_present <= I2C_RXRDATA;
          end if;
        when I2C_RXRDATA =>
          if change_sdo = '1' then
            if bit_count = 0 then
              if(burst_read_count = to_integer(unsigned(i2c_nb_bread))) then
                i2c_present <= I2C_TXNACK;
              else
                i2c_present <= I2C_TXACK;
              end if;
            end if;
          end if;
        when I2C_TXACK =>
          if change_sdo = '1' then
            i2c_present <= I2C_RXRDATA;
          end if;
        when I2C_TXNACK =>
          if change_sdo = '1' then
            i2c_present <= I2C_TXSTOP;
          end if;
        when I2C_TXSTOP =>
          if sample_sdi = '1' then
            i2c_present <= I2C_DONE;
          end if;
        when I2C_DONE =>
          --if i2c_req_i = '0' then
          i2c_present <= I2C_IDLE;
          --end if;
        when I2C_ERROR =>
          --if i2c_req_i = '0' then
          i2c_present <= I2C_IDLE;
          --end if;
      end case;
    end if;
  end process p_i2c_fsm_states;

  scl_ena <= '0' when i2c_present = I2C_IDLE else
               '0' when i2c_present = I2C_ERROR                                else
               '0' when i2c_present = I2C_DONE                                 else
               '0' when (i2c_present = I2C_TXSTOP and scl_present = SCL_HIGH1) else
               '1';



  i2c_scl_ena_o <= scl_ena;

  i2c_sda_ena_o <= '0' when i2c_present = I2C_IDLE else
                   '0' when i2c_present = I2C_RXSAACK  else
                   '0' when i2c_present = I2C_RXSAACK2 else
                   '0' when i2c_present = I2C_RXRAACK  else
                   '0' when i2c_present = I2C_RXWDACK  else
                   '0' when i2c_present = I2C_RXRDATA  else
                   '1';

  i2c_error_o    <= '1' when i2c_present = I2C_ERROR else '0';
  i2c_done_o     <= '1' when i2c_present = I2C_DONE  else '0';
  i2c_drop_req_o <= '0';

  --============================================================================
  -- Process p_bit_counter
  --! Counter used for serializing I2C data
  --! read:  change_sdo\n
  --! write: \n
  --! r/w:   bit_count, bit_count_ena\n
  --============================================================================           
  p_bit_counter : process (clk_i, reset_i) is
  begin  -- process p_bit_counter
    if reset_i = '1' then               -- asynchronous reset (active high)
      bit_count     <= 0;
      bit_count_ena <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if i2c_present = I2C_TXSADDR or
        i2c_present = I2C_TXSADDR2 or
        i2c_present = I2C_TXRADDR or
        i2c_present = I2C_TXWDATA or
        i2c_present = I2C_RXRDATA then
        bit_count_ena <= '1';
      else
        bit_count_ena <= '0';
      end if;
      if bit_count_ena = '0' then
        bit_count <= 7;
      elsif change_sdo = '1' and bit_count > 0 then
        bit_count <= bit_count - 1;
      end if;
    end if;
  end process p_bit_counter;

  i2c_wrdata  <= i2c_wr_data_i;
  i2c_regaddr <= i2c_reg_addr_i;
  i2c_slvaddr <= i2c_slv_addr_i&'1' when i2c_present = I2C_TXSADDR2 else i2c_slv_addr_i&'0';

  --============================================================================
  -- Process p_sdo_mux
  --! Generating serial data output 
  --! read:  i2c_present, scl_present\n
  --! write: i2c_sdo_o\n
  --! r/w:   \n
  --============================================================================           
  p_sdo_mux : process (clk_i, reset_i) is
  begin  -- process p_sdo_mux
    if reset_i = '1' then               -- asynchronous reset (active high)
      i2c_sdo_o <= '1';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      case i2c_present is
        when I2C_TXSTART =>
          if scl_present = SCL_HIGH1 then
            i2c_sdo_o <= '1';
          else
            i2c_sdo_o <= '0';
          end if;
        when I2C_TXSTART2 =>
          if scl_present = SCL_HIGH2 or scl_present = SCL_LOW1 then
            i2c_sdo_o <= '0';
          else
            i2c_sdo_o <= '1';
          end if;
        when I2C_TXSTOP|I2C_RXSAACK|I2C_RXSAACK2|I2C_RXWDACK|I2C_TXACK =>
          i2c_sdo_o <= '0';
        when I2C_TXSADDR|I2C_TXSADDR2 =>
          i2c_sdo_o <= i2c_slvaddr(bit_count);
        when I2C_TXRADDR =>
          i2c_sdo_o <= i2c_regaddr(bit_count);
        when I2C_TXWDATA =>
          i2c_sdo_o <= i2c_wrdata(bit_count);
        when others =>
          i2c_sdo_o <= '1';
      end case;
    end if;
  end process p_sdo_mux;

  --============================================================================
  -- Process p_sdi_read
  --! Deserializing data received at SDA input
  --! read:  i2c_present, i2c_sdi_i, bit_count\n
  --! write: i2c_rddata\n
  --! r/w:   \n
  --============================================================================           
  p_sdi_read : process (clk_i, reset_i) is
  begin  -- process p_sdi_read
    if reset_i = '1' then               -- asynchronous reset (active high)
      i2c_rddata <= (others => '1');
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if i2c_present = I2C_RXRDATA and sample_sdi = '1' then
        i2c_rddata(bit_count) <= i2c_sdi_i;
      end if;
    end if;
  end process p_sdi_read;

  --============================================================================
  -- Process p_burst_read_counter
  --! Counter used for burst read operation
  --! read:  change_sdo\n
  --! write: \n
  --! r/w:   burst_read_count, burst_read_count_ena\n
  --============================================================================           
  p_burst_read_counter : process (clk_i, reset_i) is
  begin  -- process p_burst_read_counter
    if reset_i = '1' then               -- asynchronous reset (active high)
      burst_read_count     <= 0;
      burst_read_count_ena <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if i2c_present = I2C_TXACK or
        i2c_present = I2C_RXRDATA then
        burst_read_count_ena <= '1';
      else
        burst_read_count_ena <= '0';
      end if;
      if burst_read_count_ena = '0' then
        burst_read_count <= 0;
      elsif (change_sdo = '1' and bit_count = 0 and burst_read_count < to_integer(unsigned(i2c_nb_bread))) then
        burst_read_count <= burst_read_count + 1;
      end if;
    end if;
  end process p_burst_read_counter;

  --============================================================================
  -- Process p_rddatabuf
  --! Stock read data from burst transactions
  --! read:  i2c_present, change_sdo\n
  --! write: \n
  --! r/w:   i2c_rddata_buf\n
  --============================================================================           
  p_rddatabuf : process (clk_i) is
  begin  -- process p_sdi_read
    if clk_i'event and clk_i = '1' then  -- rising clock edge
      if (bit_count = 0 and change_sdo = '1' and i2c_present = I2C_RXRDATA) then
        i2c_rddata_buf(burst_read_count) <= i2c_rddata;
      end if;
    end if;
  end process p_rddatabuf;

  i2c_rd_data_o <= i2c_rddata_buf(1)&i2c_rddata_buf(0);

  i2c_nb_bread <= "1" when i2c_rb2_i = '1' else "0";

end architecture rtl;
--============================================================================
-- architecture end
--============================================================================

