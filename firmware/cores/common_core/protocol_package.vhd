--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file protocol_package.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Protocol package (protocol_package)
--
--! @brief  Protocol package used both by OLT and ONU cores
--! The parameters of this package shall not be modified by a user
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 23\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 23\06\2016 - EBSM - first .vhd definition\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Package definition for protocol_package.vhd
--============================================================================
package protocol_package is

  --============================================================================
  -- ONU MODES  - !!! used only onu_core !!!
  --============================================================================
  -- The different working modes are set through a specific byte in the ONU_REGBANK, which is done by the OLT
  -- by default, ONU comes in ONU_BOOT state (doesn't transmit)
  -- * IMPLEMENTED MODES:
  -- OPERATIONAL               - ONU is ready for operation from the user
  -- OPERATIONAL_ALWAYS_ENABLE - Similar as above but for single ONU in the system (laser is always enabled) 
  -- PRBS_BURST                - ONU transmits bursts with prbs data
  -- PRBS_CONT                 - ONU transmits continuously with prbs data
  -- CALIBRATION               - ONU transmits continuously with calibration protocol (25ns frames / comma-mirror)
  -- ONU_BOOT                  - ONU is by default in this mode; doesn't transmit anything
  --============================================================================ 
  constant c_OPERATIONAL_MODE               : std_logic_vector(7 downto 0) := "11101001";
  constant c_OPERATIONAL_ALWAYS_ENABLE_MODE : std_logic_vector(7 downto 0) := "11100110";  
  constant c_PRBS_BURST_MODE                : std_logic_vector(7 downto 0) := "00011010";
  constant c_PRBS_CONT_MODE                 : std_logic_vector(7 downto 0) := "00001111";
  constant c_CALIBRATION_MODE               : std_logic_vector(7 downto 0) := "10011001";
  --============================================================================  

  --============================================================================
  -- Slow Control Decoding
  --============================================================================
  constant c_CTRL_DATA_WIDTH : integer := 29;

  subtype ir_CTRL_OPE is integer range 28 downto 25;
  subtype ir_REGBANK_ADDR is integer range 24 downto 16;
  subtype ir_REGBANK_VALUE is integer range 15 downto 8;
  subtype ir_ONU_ADDR is integer range 7 downto 0;

  constant c_OPE_READ       : std_logic_vector(3 downto 0) := "0001";
  constant c_OPE_WRITE_ACK  : std_logic_vector(3 downto 0) := "1110";
  constant c_OPE_WRITE      : std_logic_vector(3 downto 0) := "1111";
  constant c_OPE_RCVD_ERROR : std_logic_vector(3 downto 0) := "1000";

  constant c_BROADCAST_ADDR : std_logic_vector(7 downto 0) := x"FF";

  constant c_SC_UP_HEADER       : std_logic_vector(2 downto 0) := "110";

  constant c_CRC_FRAMING_GOOD_TO_LOCK  : integer := 15;
  constant c_CRC_FRAMING_BAD_TO_UNLOCK : integer := 6;
  --============================================================================  

  --============================================================================
  -- Downstream protocol parameters
  --============================================================================  
  constant c_DOWN_REGULAR_HEADER          : std_logic_vector(7 downto 0) := "01101010";
  constant c_DOWN_HEARTBEAT_HEADER        : std_logic_vector(7 downto 0) := "10011010";
  --============================================================================

  --============================================================================
  -- Downstream receiver lock parameters   - !!! used only onu_core !!!
  --============================================================================
  constant c_DOWN_HEADERMASK              : std_logic_vector(7 downto 0) := "11111111";  
  constant c_DOWN_NBR_HEADER_ACC_MAX      : integer                      := 15;
  constant c_DOWN_NBR_HEADER_ACQUIRE_LOCK : integer                      := 13;
  constant c_DOWN_NBR_HEADER_LOSS_LOCK    : integer                      := 10;
  --============================================================================

end protocol_package;



