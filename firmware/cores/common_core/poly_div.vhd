--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file poly_div.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Polynomial divider (poly_div)
--
--! @brief  Polynomial division module
--! It divides a polynomial dividend_i by a constant divisor (g_DIVISOR_POLY) giving a remainder_o and a quotient_o
--! Please note the notation of the polynomials where the LSB corresponds to the highest power of the polynomial:
--!    ex. "10010001" - Notation: 1 + 0 + 0 + x^3 + 0 + 0 + 0 + x^7
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 23\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 23\06\2016 - EBSM - first .vhd definition\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for poly_div
--==============================================================================
entity poly_div is
  generic (
    g_DIVISOR_POLY   : std_logic_vector := "10010001";  -- Notation: 1 + 0 + 0 + x^3 + 0 + 0 + 0 + x^7
    g_DIVIDEND_WIDTH : integer          := 40
    );
  port (
    clk_i       : in  std_logic;        --! clock input
    reset_i     : in  std_logic;
    enable_i    : in  std_logic;
    dividend_i  : in  std_logic_vector(g_DIVIDEND_WIDTH-1 downto 0);
    quotient_o  : out std_logic_vector(g_DIVIDEND_WIDTH-1 downto 0);
    remainder_o : out std_logic_vector(g_DIVISOR_POLY'length-2 downto 0)
    );
end poly_div;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of poly_div is


  type   t_node_array is array (0 to g_DIVIDEND_WIDTH-1) of std_logic_vector(remainder_o'range);
  signal node_array : t_node_array;
  signal node_ff      : std_logic_vector(remainder_o'range);

begin

  gen_lsfr_parallel : for k in 0 to (g_DIVIDEND_WIDTH-1) generate
    gen_lsfr : for j in remainder_o'range generate
      
      gen_first_state : if k = 0 generate  --first state
        gen_input_node0 : if j = remainder_o'left generate
          node_array(k)(j) <= dividend_i(k) xor (node_ff(0) and g_DIVISOR_POLY(j+1));
        end generate gen_input_node0;

        gen_other_nodes0 : if j < remainder_o'left generate
          node_array(k)(j) <= node_ff(j+1) xor (node_ff(0) and g_DIVISOR_POLY(j+1));
        end generate gen_other_nodes0;

        quotient_o(k) <= node_array(k)(0);
      end generate gen_first_state;

      gen_other_states : if(k > 0) generate
        gen_input_noden : if j = remainder_o'left generate
          node_array(k)(j) <= dividend_i(k) xor (node_array(k-1)(0) and g_DIVISOR_POLY(j+1));
        end generate gen_input_noden;
        gen_other_nodesn : if j < remainder_o'left generate
          node_array(k)(j) <= node_array(k-1)(j+1) xor (node_array(k-1)(0) and g_DIVISOR_POLY(j+1));
        end generate gen_other_nodesn;

        quotient_o(k) <= node_array(k)(0);
      end generate gen_other_states;
      
    end generate gen_lsfr;
  end generate gen_lsfr_parallel;

  remainder_o <= node_array(g_DIVIDEND_WIDTH-1)(remainder_o'range);

  process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        node_ff <= (others => '0');
      elsif(enable_i = '1') then
        node_ff <= node_array(g_DIVIDEND_WIDTH-1)(remainder_o'range);
      end if;
    end if;
  end process;
  
end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================

