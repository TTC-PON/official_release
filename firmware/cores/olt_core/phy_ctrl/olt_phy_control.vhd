--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_phy_control.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.pon_olt_package_static.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT PHY Control top design (olt_phy_control)
--
--! @brief OLT PHY Control top design for TTC-PON
--! <further description>
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 14\06\2016 - EBSM - first .vhd definition\n
--! 05\04\2017 - EBSM - integrated i2c_rssi_ctrl made by CS
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! Implement logic to identify faulty ONU's via Signal detect \n
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_phy_control
--============================================================================
entity olt_phy_control is
  generic(
    g_CLK_SYS_PERIOD : integer range 5 to 20 := 10  -- clock period in ns
    );
  port (
    -- global input signals --
    clk_trxusr240_i : in std_logic;
    clk_sys_i      : in std_logic;
    core_reset_i   : in std_logic;
    -------------------------

    -- global output signals --
    mgt_reset_o   : out std_logic;
    reset_rxclk_o : out std_logic;
    reset_txclk_o : out std_logic;
    ---------------------------

    -- status/control --
    mgt_rx_ready_i : in std_logic;
    mgt_tx_ready_i : in std_logic;

    tdm_heartbeat_i    : in std_logic;
    burst_length_i     : in std_logic_vector(5 downto 0);
    sfp_rx_reset_en_i  : in std_logic;
    sfp_rx_reset_pos_i : in std_logic_vector(5 downto 0);

    clear_rx_sd_stat_i : in  std_logic;
    latch_rx_sd_stat_i : in  std_logic;
    sfp_rx_sd_wmin_o   : out std_logic_vector(9 downto 0);
    sfp_rx_sd_wmax_o   : out std_logic_vector(9 downto 0);
    sfp_rx_sd_wcur_o   : out std_logic_vector(9 downto 0);


    sd_comma_corr_clear_i : in  std_logic;
    sd_comma_corr_latch_i : in  std_logic;
    sd_comma_corr_stat_o  : out std_logic_vector(10 downto 0);

    sd_mask_osrx_i  : in  std_logic;
    sd_mask_delay_i : in  std_logic_vector(3 downto 0);
    rx_comma_osrx_i : in  std_logic;
    rx_en_osrx_o    : out std_logic;

    -- sfp interface
    sfp_rx_sd_i    : in  std_logic;
    sfp_tx_fault_i : in  std_logic;
    sfp_mod_abs_i  : in  std_logic;
    sfp_rx_reset_o : out std_logic;
    sfp_rssi_tri_o : out std_logic;
    sfp_tx_dis_o   : out std_logic;

    --i2c interface
    i2c_rssi_ctrl_i    : in  std_logic_vector(31 downto 0);
    i2c_rssi_ctrl_en_i : in  std_logic;
    i2c_rssi_stat_o    : out std_logic_vector(31 downto 0);

    -- user interface
    i2c_req_o      : out std_logic;
    i2c_rnw_o      : out std_logic;
    i2c_rb2_o      : out std_logic;
    i2c_slv_addr_o : out std_logic_vector(6 downto 0);
    i2c_reg_addr_o : out std_logic_vector(7 downto 0);
    i2c_wr_data_o  : out std_logic_vector(7 downto 0);

    i2c_done_i     : in std_logic;
    i2c_error_i    : in std_logic;
    i2c_drop_req_i : in std_logic;
    i2c_rd_data_i  : in std_logic_vector(15 downto 0)
    -------------------------
    );
end entity olt_phy_control;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of olt_phy_control is
  
  attribute mark_debug : string;
  attribute async_reg  : string;

  --! Functions
  function fcn_or_reduce(arg : std_logic_vector) return std_logic is
    variable v_result : std_logic;
  begin
    v_result := '0';
    for i in arg'range loop
      v_result := v_result or arg(i);
    end loop;
    return v_result;
  end;

  --! Constants

  --! Signal declaration
  --============================================================================
  -- Core logic reset
  --============================================================================
  signal reset_rxclk : std_logic;
  signal reset_txclk : std_logic;

  --============================================================================
  -- SFP_TX_DISABLE logic
  --============================================================================
  signal mgt_tx_ready_syssync_meta, mgt_tx_ready_syssync_r : std_logic;
  attribute async_reg of mgt_tx_ready_syssync_meta, mgt_tx_ready_syssync_r : signal is "true";
  
  --============================================================================
  -- SFP_RX_RESET generation
  --============================================================================           
  signal tdm_heartbeat_r      : std_logic;
  signal sfp_rx_reset_cntr    : unsigned(burst_length_i'range);  --counter aligned to tdm_heartbeat used for reset generation (mod BURST_LENGTH)
  signal sfp_rx_reset_en_p    : std_logic;
  signal sfp_rx_reset_en_pipe : std_logic_vector(c_SFP_RX_RESET_WIDTH_TXCLK_PERIOD-1 downto 0);

  --============================================================================
  -- SFP_RX_SD analysis for Reset positioning
  --============================================================================
  signal sfp_rx_sd_r  : std_logic;
  signal sfp_rx_sd_r2 : std_logic;
  signal sfp_rx_sd_r3 : std_logic;

  attribute async_reg of sfp_rx_sd_r  : signal is "true";
  attribute async_reg of sfp_rx_sd_r2 : signal is "true";
  attribute async_reg of sfp_rx_sd_r3 : signal is "true";

  signal sfp_rx_sd_wmin : unsigned(9 downto 0);
  signal sfp_rx_sd_wmax : unsigned(9 downto 0);
  signal sfp_rx_sd_wcur : unsigned(9 downto 0);

  signal sfp_rx_sd_wcntr : integer range 0 to (2**sfp_rx_sd_wcur'length - 1);

  --============================================================================
  -- SFP_RX_SD analysis for OSRX blind window during absence of SD
  --============================================================================
  signal sfp_rx_sd_rx_r                 : std_logic;
  signal sfp_rx_sd_pipe                 : std_logic_vector(15 downto 0);
  attribute async_reg of sfp_rx_sd_rx_r : signal is "true";
  attribute async_reg of sfp_rx_sd_pipe : signal is "true";

  signal sfp_sd_comma_corr      : std_logic;
  signal sfp_sd_comma_corr_cntr : unsigned(9 downto 0);
  signal sfp_sd_cntr            : unsigned(9 downto 0);
  signal sfp_sd_cntr_end        : std_logic;

  signal rx_en_osrx : std_logic;

  attribute mark_debug of rx_en_osrx           : signal is "true";
  attribute mark_debug of sfp_sd_comma_corr      : signal is "true";
  attribute mark_debug of sfp_sd_comma_corr_cntr : signal is "true";
  attribute mark_debug of sfp_sd_cntr            : signal is "true";
  attribute mark_debug of sfp_sd_cntr_end        : signal is "true";

  --============================================================================
  -- I2C master and SFP_RSSI control   
  --============================================================================
  --! Component declaration
  component i2c_rssi_ctrl is
    generic (
      g_CLK_SYS_PERIOD : integer range 5 to 20 := 10;   -- clock period in ns
      g_RSSI_TRG_WIDTH : integer               := 600;  -- RSSI trigger pulse width in ns
      g_RSSI_WAIT_TIME : integer               := 500000;  -- RSSI conversion time in ns
      g_RSSI_ENABLE    : boolean               := true  -- ENABLE RSSI LOGIC (false for ONU, true for OLT)
      );
    port (
      -- global input
      clk_sys_i : in std_logic;
      reset_i   : in std_logic;

      -- reg interface
      i2c_rssi_ctrl_i    : in  std_logic_vector(31 downto 0);
      i2c_rssi_ctrl_en_i : in  std_logic;
      i2c_rssi_stat_o    : out std_logic_vector(31 downto 0);

      -- user interface
      i2c_req_o      : out std_logic;
      i2c_rnw_o      : out std_logic;
      i2c_rb2_o      : out std_logic;
      i2c_slv_addr_o : out std_logic_vector(6 downto 0);
      i2c_reg_addr_o : out std_logic_vector(7 downto 0);
      i2c_wr_data_o  : out std_logic_vector(7 downto 0);

      i2c_done_i     : in std_logic;
      i2c_error_i    : in std_logic;
      i2c_drop_req_i : in std_logic;
      i2c_rd_data_i  : in std_logic_vector(15 downto 0);

      -- RSSI trigger
      rssi_trg_o : out std_logic
      );
  end component i2c_rssi_ctrl;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Reset logic
  --============================================================================
  mgt_reset_o   <= core_reset_i;
  reset_rxclk   <= not mgt_rx_ready_i;
  reset_txclk   <= not mgt_tx_ready_i;
  reset_rxclk_o <= reset_rxclk;
  reset_txclk_o <= reset_txclk;

  --============================================================================
  -- SFP_RX_RESET generation (clocked by clk_trxusr240_i)
  --============================================================================
  --============================================================================
  -- Process p_sfp_rx_reset_gen
  --! Generates mod(Burst length) counter for sfp_rx_reset pulse generation after each burst
  --! read:  clk_trxusr240_i, reset_txclk, tdm_heartbeat_i, tdm_heartbeat_r\n
  --! write: tdm_heartbeat_r\n
  --! r/w:   sfp_rx_reset_cntr\n
  --============================================================================           
  p_sfp_rx_reset_gen : process(clk_trxusr240_i)
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(reset_txclk = '1') then
        sfp_rx_reset_cntr <= to_unsigned(0, sfp_rx_reset_cntr'length);
      else
        tdm_heartbeat_r <= tdm_heartbeat_i;
        if (tdm_heartbeat_i = '1' and tdm_heartbeat_r = '0') then  --rising edge heartbeat
          sfp_rx_reset_cntr <= to_unsigned(0, sfp_rx_reset_cntr'length);
        elsif (sfp_rx_reset_cntr >= unsigned(burst_length_i)) then
          sfp_rx_reset_cntr <= to_unsigned(0, sfp_rx_reset_cntr'length);
        else
          sfp_rx_reset_cntr <= sfp_rx_reset_cntr + 1;
        end if;
      end if;
    end if;
  end process p_sfp_rx_reset_gen;

  sfp_rx_reset_en_p <= '1' when (sfp_rx_reset_cntr = unsigned(sfp_rx_reset_pos_i) and sfp_rx_reset_en_i = '1') else '0';

  --============================================================================
  -- Process p_sfp_rx_reset_pipe
  --! Creates a (c_SFP_RX_RESET_WIDTH_TXCLK_PERIOD) - width reset pulse
  --! read:  clk_trxusr240_i, reset_txclk, sfp_rx_reset_en_p\n
  --! write: sfp_rx_reset_o\n
  --! r/w:   sfp_rx_reset_en_pipe\n
  --============================================================================                         
  p_sfp_rx_reset_pipe : process(clk_trxusr240_i) is
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if reset_txclk = '1' then
        sfp_rx_reset_en_pipe <= (others => '0');
        sfp_rx_reset_o         <= '0';
      else
        sfp_rx_reset_en_pipe(0)                                    <= sfp_rx_reset_en_p;
        sfp_rx_reset_en_pipe(sfp_rx_reset_en_pipe'left downto 1) <= sfp_rx_reset_en_pipe(sfp_rx_reset_en_pipe'left-1 downto 0);
        sfp_rx_reset_o                                               <= fcn_or_reduce(sfp_rx_reset_en_pipe);
      end if;
    end if;
  end process p_sfp_rx_reset_pipe;

  --============================================================================
  -- SFP_RX_SD analysis (clocked by clk_trxusr240_i) for reset positioning
  --============================================================================
  --============================================================================
  -- Process p_sfp_rx_sd_sync
  --! Synchronizer for sfp_rx_sd_i signal
  --! read:  clk_trxusr240_i, reset_txclk\n
  --! write: sfp_rx_sd_r3\n
  --! r/w:   sfp_rx_sd_r2, sfp_rx_sd_r\n
  --============================================================================           
  p_sfp_rx_sd_sync : process(clk_trxusr240_i)
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      sfp_rx_sd_r  <= sfp_rx_sd_i;
      sfp_rx_sd_r2 <= sfp_rx_sd_r;
      sfp_rx_sd_r3 <= sfp_rx_sd_r2;
    end if;
  end process p_sfp_rx_sd_sync;

  --============================================================================
  -- Process p_sfp_rx_sd_stat
  --! Generates a counter that measures the width (fall -> ris) of the sfp_rx_sd signal, takes the max. min. and cur.
  --! values of this counter
  --! read:  clk_trxusr240_i, reset_txclk\n
  --! write: \n
  --! r/w:   sfp_rx_sd_wmax, sfp_rx_sd_wmin, sfp_rx_sd_wcur, sfp_rx_sd_wcntr\n
  --============================================================================           
  p_sfp_rx_sd_stat : process(clk_trxusr240_i)
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(reset_txclk = '1') then
        sfp_rx_sd_wcntr <= 0;
        sfp_rx_sd_wcur  <= (others => '0');
        sfp_rx_sd_wmax  <= (others => '0');
        sfp_rx_sd_wmin  <= (others => '1');
      else

        if(sfp_rx_sd_r3 = '1' and sfp_rx_sd_r2 = '0') then  --falling edge sfp_rx_sd
          sfp_rx_sd_wcntr <= 0;
        else
		  if(sfp_rx_sd_wcntr = (2**sfp_rx_sd_wcur'length - 1)) then
            sfp_rx_sd_wcntr <= 0;		
		  else
            sfp_rx_sd_wcntr <= sfp_rx_sd_wcntr + 1;
		  end if;
        end if;

        if(clear_rx_sd_stat_i = '1') then
          sfp_rx_sd_wcur <= (others => '0');
          sfp_rx_sd_wmax <= (others => '0');
          sfp_rx_sd_wmin <= (others => '1');
        elsif(sfp_rx_sd_r3 = '0' and sfp_rx_sd_r2 = '1') then  --rising_edge edge sfp_rx_sd

          sfp_rx_sd_wcur <= to_unsigned(sfp_rx_sd_wcntr, sfp_rx_sd_wcur'length);

          if(to_integer(sfp_rx_sd_wmin) > sfp_rx_sd_wcntr) then
            sfp_rx_sd_wmin <= to_unsigned(sfp_rx_sd_wcntr, sfp_rx_sd_wcur'length);
          end if;
          if(to_integer(sfp_rx_sd_wmax) < sfp_rx_sd_wcntr) then
            sfp_rx_sd_wmax <= to_unsigned(sfp_rx_sd_wcntr, sfp_rx_sd_wcur'length);
          end if;
        end if;

      end if;
    end if;
  end process p_sfp_rx_sd_stat;

  --============================================================================
  -- Process p_sfp_rx_sd_latch
  --! Generates a counter that measures the width (fall -> ris) of the sfp_rx_sd signal, takes the max. min. and cur.
  --! values of this counter
  --! read:  clk_trxusr240_i, reset_txclk\n
  --! write: \n
  --! r/w:   \n
  --============================================================================           
  p_sfp_rx_sd_latch : process(clk_trxusr240_i)
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(reset_txclk = '1') then
        sfp_rx_sd_wmin_o <= (others => '0');
        sfp_rx_sd_wmax_o <= (others => '0');
        sfp_rx_sd_wcur_o <= (others => '0');
      else
        if(clear_rx_sd_stat_i = '1') then
          sfp_rx_sd_wmin_o <= (others => '0');
          sfp_rx_sd_wmax_o <= (others => '0');
          sfp_rx_sd_wcur_o <= (others => '0');
        elsif(latch_rx_sd_stat_i = '1') then  -- latch statistics from rx_sd
          sfp_rx_sd_wmin_o <= std_logic_vector(sfp_rx_sd_wmin);
          sfp_rx_sd_wmax_o <= std_logic_vector(sfp_rx_sd_wmax);
          sfp_rx_sd_wcur_o <= std_logic_vector(sfp_rx_sd_wcur);
        end if;
      end if;
    end if;
  end process p_sfp_rx_sd_latch;

  --============================================================================
  -- SFP_RX_SD analysis (clocked by clk_trxusr240_i) for OSRX blind window during absence of SD
  --============================================================================   
  --============================================================================
  -- Process p_sd_comma_corr
  --! -
  --! read:  \n
  --! write: \n
  --! r/w:   \n
  --============================================================================           
  p_sd_comma_corr : process(clk_trxusr240_i)
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(sd_comma_corr_clear_i = '1') then
        sfp_sd_comma_corr_cntr <= to_unsigned(0, sfp_sd_comma_corr_cntr'length);
        sfp_sd_cntr            <= to_unsigned(2**(sfp_sd_cntr'length-2), sfp_sd_cntr'length);
      elsif(sfp_sd_cntr_end = '0') then
        
        if(sfp_sd_comma_corr = '1') then
          sfp_sd_comma_corr_cntr <= sfp_sd_comma_corr_cntr + 1;
        end if;

        if(sfp_rx_sd_pipe(0) = '1' and sfp_rx_sd_pipe(1) = '0') then  --rising edge SD
          sfp_sd_cntr <= sfp_sd_cntr - 1;
        end if;
      end if;
    end if;
  end process p_sd_comma_corr;

  sfp_sd_comma_corr <= rx_comma_osrx_i;
  sfp_sd_cntr_end   <= sfp_sd_cntr(sfp_sd_cntr'left);

  --============================================================================
  -- Process p_sd_comma_corr_latch
  --! -
  --! read:  \n
  --! write: \n
  --! r/w:   \n
  --============================================================================           
  p_sd_comma_corr_latch : process(clk_trxusr240_i)
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(sd_comma_corr_clear_i = '1') then
        sd_comma_corr_stat_o <= (others => '0');
      elsif(sd_comma_corr_latch_i = '1') then
        sd_comma_corr_stat_o(10)         <= sfp_sd_cntr_end;
        sd_comma_corr_stat_o(9 downto 0) <= std_logic_vector(sfp_sd_comma_corr_cntr);
      end if;
    end if;
  end process p_sd_comma_corr_latch;

  --============================================================================
  -- Process p_rx_en_osrx
  --! -
  --! read:  \n
  --! write: \n
  --! r/w:   \n
  --============================================================================           
  p_rx_en_osrx : process(clk_trxusr240_i)
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      sfp_rx_sd_rx_r <= sfp_rx_sd_i;
      sfp_rx_sd_pipe <= sfp_rx_sd_pipe(sfp_rx_sd_pipe'left-1 downto 0)&sfp_rx_sd_rx_r;
      rx_en_osrx   <= sfp_rx_sd_pipe(to_integer(unsigned(sd_mask_delay_i)));
    end if;
  end process p_rx_en_osrx;
  rx_en_osrx_o <= rx_en_osrx when (sd_mask_osrx_i = '1') else '1';


  --============================================================================
  -- SFP_TX_DIS - tied to '0' for OLT
  --============================================================================
  --============================================================================
  -- Process p_sfp_tx_dis
  --! SFP_TX_DIS tied to '0' for continuous transmission
  --! read:  clk_sys_i\n
  --! write: sfp_tx_dis_o\n
  --! r/w:   \n
  --============================================================================     
  mgt_tx_ready_syssync_meta <= mgt_tx_ready_i when rising_edge(clk_sys_i);
  mgt_tx_ready_syssync_r    <= mgt_tx_ready_syssync_meta when rising_edge(clk_sys_i);
  
  p_sfp_tx_dis : process(clk_sys_i) is
    variable v_sfp_tx_disable_cntr : integer range 0 to (c_SFP_TX_DISABLE_RESET_TIME*(1000000/g_CLK_SYS_PERIOD));
  begin
    if (clk_sys_i'event and clk_sys_i = '1') then
        if(core_reset_i='1' or mgt_tx_ready_syssync_r='0') then
            sfp_tx_dis_o <= '1';
            v_sfp_tx_disable_cntr := 0;
        else		
            if(  v_sfp_tx_disable_cntr < (c_SFP_TX_DISABLE_RESET_TIME*(1000000/g_CLK_SYS_PERIOD))) then
			    v_sfp_tx_disable_cntr := v_sfp_tx_disable_cntr+1;
				sfp_tx_dis_o <= '1';
			else
				sfp_tx_dis_o <= '0';		
			end if;
	    end if;
    end if;
  end process p_sfp_tx_dis;

  --============================================================================
  -- I2C master and SFP_RSSI trigger
  --============================================================================
  cmp_i2c_rssi_ctrl : i2c_rssi_ctrl
    generic map(
      g_CLK_SYS_PERIOD => g_CLK_SYS_PERIOD,  -- clock period in ns
      g_RSSI_TRG_WIDTH => c_RSSI_TRG_WIDTH,  -- RSSI trigger pulse width in ns
      g_RSSI_WAIT_TIME => c_RSSI_WAIT_TIME,  -- RSSI conversion time in ns
      g_RSSI_ENABLE    => true
      )
    port map(
      -- global input
      clk_sys_i => clk_sys_i,
      reset_i   => core_reset_i,

      -- reg interface
      i2c_rssi_ctrl_i    => i2c_rssi_ctrl_i,
      i2c_rssi_ctrl_en_i => i2c_rssi_ctrl_en_i,
      i2c_rssi_stat_o    => i2c_rssi_stat_o,

      -- user interface
      i2c_req_o      => i2c_req_o,
      i2c_rnw_o      => i2c_rnw_o,
      i2c_rb2_o      => i2c_rb2_o,
      i2c_slv_addr_o => i2c_slv_addr_o,
      i2c_reg_addr_o => i2c_reg_addr_o,
      i2c_wr_data_o  => i2c_wr_data_o,

      i2c_done_i     => i2c_done_i,
      i2c_error_i    => i2c_error_i,
      i2c_drop_req_i => i2c_drop_req_i,
      i2c_rd_data_i  => i2c_rd_data_i,

      -- RSSI trigger
      rssi_trg_o => sfp_rssi_tri_o
      );

end architecture rtl;
--============================================================================
-- architecture end
--============================================================================

