--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_rx.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT Rx top design(olt_rx)
--
--! @brief OLT Rx top design
--! Integrates BlindOversampler, 8b10b decoder and payload extract in order to reconstruct ONU received frames
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 14\06\2016 - EBSM - first .vhd definition\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo \n
--! \n
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_rx
--============================================================================
entity olt_rx is
  generic(
    g_WORD_WIDTH                : integer := 40;
    g_DATA_USR_WIDTH            : integer := 48;
    g_DATA_CTRL_WIDTH           : integer := 16;
    g_BURST_LENGTH_OLT_RXUSRCLK : integer := 30;    --! BURST_LENGTH_NS/PERIOD_OLT_RX_CLK_NS
    g_WINDOW_BURST_COUNTING     : integer := 32*4;  --! number of bursts to be checked to consider locked
    g_GOOD_BURST_TO_LOCK        : integer := 32*2;  --! number of good bursts among window (g_WINDOW_BURST_COUNTING) to be considered locked
    g_GOOD_BURST_TO_UNLOCK      : integer := 32/2   --! less than this number of bursts among window (g_WINDOW_BURST_COUNTING) to be considered unlocked
  );
  port (
    -- global input signals --
    clk_trxusr240_i : in std_logic;      --! MGT interfacing clock - 120MHz
    reset_i        : in std_logic;
    -------------------------

    --status/control --
    osrx_frame_length_i : in  std_logic_vector(3 downto 0);
    en_osrx_i           : in  std_logic;
    en_lock_i           : in  std_logic;
    fine_position_o     : out std_logic_vector(6 downto 0);
    rx_k28_5_o          : out std_logic;
    rx_k28_1_o          : out std_logic;
    rx_locked_o         : out std_logic; 
    -------------------------

    -- data in/out --
    data_i        : in  std_logic_vector(g_WORD_WIDTH-1 downto 0);
    data_strobe_o : out std_logic;
    data_error_o  : out std_logic;
    data_usr_o    : out std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);
    data_ctrl_o   : out std_logic_vector(g_DATA_CTRL_WIDTH-1 downto 0);
    onu_addr_o    : out std_logic_vector(7 downto 0)
    -------------------------
    );
end entity olt_rx;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture structural of olt_rx is

  attribute mark_debug : string;
  attribute keep       : string;
  --! Functions

  --! Constants

  --! Signal declaration
  signal en_header_from_frame_lock : std_logic;
  signal en_osrx                   : std_logic;

  signal pos_from_osrx   : std_logic_vector(4 downto 0);  -- comma position in word aligner osrx (0-9)  / 0.416 ns
  signal phase_from_osrx : std_logic_vector(1 downto 0);  -- phase chosen by osrx (0-3)                 / 0.104 ns
  signal pos_phase_osrx  : std_logic_vector(6 downto 0);  -- pos_from_osrx & phase_from_osrx      
  signal comma_from_osrx : std_logic;  -- comma detected by oversampler
  signal dout_from_osrx  : std_logic_vector(9 downto 0);

  signal dataout_from_dec : std_logic_vector(7 downto 0);
  signal dataisk_from_dec : std_logic_vector(0 downto 0);
  signal disperr_from_dec : std_logic_vector(0 downto 0);
  signal codeerr_from_dec : std_logic_vector(0 downto 0);

  attribute mark_debug of disperr_from_dec : signal is "true";
  attribute mark_debug of codeerr_from_dec : signal is "true";
  attribute keep of disperr_from_dec       : signal is "true";
  attribute keep of codeerr_from_dec       : signal is "true";
  attribute mark_debug of dataout_from_dec : signal is "true";
  attribute mark_debug of dataisk_from_dec : signal is "true";
  attribute mark_debug of comma_from_osrx  : signal is "true";

  --! Component declaration
  component blind_osrx_wrapper is
    generic (
      ONU_PCOMMA_CHAR   : std_logic_vector := "0101111100";  -- k28.5 (force disparity for upstream commas)
      ONU_MCOMMA_CHAR   : std_logic_vector := "1001111100";  -- K28.1 
      ONU_COMMA_MASK    : std_logic_vector := "1111111111";
      ONU_PREAMBLE_CHAR : std_logic_vector := "0101010101"
      );
    port (
      clk_i        : in  std_logic;
      reset_i        : in  std_logic;
      en_pcomma_i    : in  std_logic;
      en_mcomma_i    : in  std_logic;
      frame_length_i : in  std_logic_vector;
      pos_o          : out std_logic_vector;
      phase_o        : out std_logic_vector;
      comma_o        : out std_logic;
      frame_o        : out std_logic;
      rxdata_i       : in  std_logic_vector;
      dout_o         : out std_logic_vector
      );
  end component blind_osrx_wrapper;

  component dec_8b10b_wrapper is
    generic(
      g_NBR_8B10B_DEC : integer := 2    -- >= 2
      );
    port(
      clk_i   : in  std_logic;        --! falling edge triggered
      reset_i   : in  std_logic;
      datain_i  : in  std_logic_vector(10*g_NBR_8B10B_DEC-1 downto 0);
      dataout_o : out std_logic_vector(8*g_NBR_8B10B_DEC-1 downto 0);
      dataisk_o : out std_logic_vector(g_NBR_8B10B_DEC-1 downto 0);
      disperr_o : out std_logic_vector(g_NBR_8B10B_DEC-1 downto 0);
      codeerr_o : out std_logic_vector(g_NBR_8B10B_DEC-1 downto 0)
      );
  end component dec_8b10b_wrapper;

  component olt_payload_extract is
    port (
      clk_i             : in  std_logic;
      reset_i           : in  std_logic;
      comma_osrx_i      : in  std_logic;
      position_osrx_i   : in  std_logic_vector(6 downto 0);
      position_osrx_o   : out std_logic_vector(6 downto 0);
      rx_k28_1_o        : out std_logic;
      rx_k28_5_o        : out std_logic;
      dataisk_dec_i     : in  std_logic_vector(0 downto 0);
      codeerr_dec_i     : in  std_logic_vector(0 downto 0);
      disperr_dec_i     : in  std_logic_vector(0 downto 0);
      decoded_data_i    : in  std_logic_vector(7 downto 0);
      rx_frame_length_i : in  std_logic_vector(3 downto 0);
      data_error_o      : out std_logic;
      data_strobe_o     : out std_logic;
      data_usr_o        : out std_logic_vector(55 downto 0);
      data_ctrl_o       : out std_logic_vector(7 downto 0);
      onu_addr_o        : out std_logic_vector(7 downto 0)
      );
  end component olt_payload_extract;

  component rx_frame_lock is
    generic(
      g_BURST_LENGTH_OLT_RXUSRCLK : integer := 30;   --! BURST_LENGTH_NS/PERIOD_OLT_RX_CLK_NS
      g_WINDOW_BURST_COUNTING     : integer := 64*4; --! number of bursts to be checked to consider locked
      g_GOOD_BURST_TO_LOCK        : integer := 64*2; --! number of good bursts among window (g_WINDOW_BURST_COUNTING) to be considered locked
      g_GOOD_BURST_TO_UNLOCK       : integer := 32   --! less than this number of bursts among window (g_WINDOW_BURST_COUNTING) to be considered unlocked
    );
    port (
      clk_i             : in  std_logic;  --! clock input
      reset_i           : in  std_logic;  --! active high sync. reset
      enable_i          : in  std_logic;  --! enables this block
      header_i          : in  std_logic;  --! header detected
      en_header_o       : out std_logic;  --! enables header detection
      locked_o          : out std_logic   --! locked output
      );
  end component rx_frame_lock;

--============================================================================
-- architecture begin
--============================================================================
begin

  cmp_rx_frame_lock : rx_frame_lock
    generic map(
      g_BURST_LENGTH_OLT_RXUSRCLK => g_BURST_LENGTH_OLT_RXUSRCLK,   --! BURST_LENGTH_NS/PERIOD_OLT_RX_CLK_NS
      g_WINDOW_BURST_COUNTING     => g_WINDOW_BURST_COUNTING,       --! number of bursts to be checked to consider locked
      g_GOOD_BURST_TO_LOCK        => g_GOOD_BURST_TO_LOCK,          --! number of good bursts among window (g_WINDOW_BURST_COUNTING) to be considered locked
      g_GOOD_BURST_TO_UNLOCK      => g_GOOD_BURST_TO_UNLOCK         --! less than this number of bursts among window (g_WINDOW_BURST_COUNTING) to be considered unlocked
    )
    port map(
      clk_i             => clk_trxusr240_i,
      reset_i           => reset_i,
      enable_i          => en_lock_i,
      header_i          => comma_from_osrx,
      en_header_o       => en_header_from_frame_lock,
      locked_o          => rx_locked_o
      );

  --============================================================================
  -- Component instantiation
  --! Component blind_osrx_wrapper
  --============================================================================   
  cmp_blindoversampler_rx : blind_osrx_wrapper
    generic map(
      ONU_PCOMMA_CHAR   => "0101111100",  -- k28.5 (force disparity for upstream commas)
      ONU_MCOMMA_CHAR   => "1001111100",  -- K28.1 (force disparity for upstream commas)
      ONU_COMMA_MASK    => "1111111111",
      ONU_PREAMBLE_CHAR => "0101010101"
      )
    port map(
      clk_i          => clk_trxusr240_i,
      reset_i        => reset_i,
      en_pcomma_i    => en_osrx,
      en_mcomma_i    => en_osrx,
      frame_length_i => osrx_frame_length_i,
      pos_o          => pos_from_osrx,
      comma_o        => comma_from_osrx,
      frame_o        => open,
      phase_o        => phase_from_osrx,
      rxdata_i       => data_i,
      dout_o         => dout_from_osrx
      );

  en_osrx <= en_header_from_frame_lock and en_osrx_i;

  --============================================================================
  -- Component instantiation
  --! Component 8b10b decoder
  --============================================================================          
  cmp_8b10bdec : dec_8b10b_wrapper
    generic map(
      g_NBR_8B10B_DEC => 1
      )
    port map(
      clk_i   => clk_trxusr240_i,
      reset_i   => reset_i,
      datain_i  => dout_from_osrx,
      dataout_o => dataout_from_dec,
      dataisk_o => dataisk_from_dec,
      disperr_o => disperr_from_dec,
      codeerr_o => codeerr_from_dec
      );

  --============================================================================
  -- Component instantiation
  --! Component OLT Payload Extraction
  --============================================================================   
  cmp_olt_payload_extract : olt_payload_extract
    port map(
      clk_i             => clk_trxusr240_i,
      reset_i           => reset_i,
      comma_osrx_i      => comma_from_osrx,
      position_osrx_i   => pos_phase_osrx ,
      position_osrx_o   => fine_position_o ,
      rx_k28_1_o        => rx_k28_1_o,
      rx_k28_5_o        => rx_k28_5_o,
      dataisk_dec_i     => dataisk_from_dec,
      codeerr_dec_i     => codeerr_from_dec,
      disperr_dec_i     => disperr_from_dec,
      decoded_data_i    => dataout_from_dec,
      rx_frame_length_i => osrx_frame_length_i,
      data_error_o      => data_error_o,
      data_strobe_o     => data_strobe_o,
      data_usr_o        => data_usr_o,
      data_ctrl_o       => data_ctrl_o,
      onu_addr_o        => onu_addr_o
      );

  pos_phase_osrx <= pos_from_osrx & phase_from_osrx;
  
end architecture structural;
--============================================================================
-- architecture end
--============================================================================

