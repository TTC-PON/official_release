--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_core.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.protocol_package.all;
use work.pon_olt_package_static.all;
use work.pon_olt_package_modifiable.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT core design (olt_core)
--
--! @brief OLT core design for TTC-PON
--! <further description>
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 14\06\2016 - EBSM - first .vhd definition\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo More comments \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_core
--============================================================================
entity olt_core is
  port (
    -- global input signals --
    clk_sys_i    : in std_logic;  --! This clock is used for the initialization procedure on MGT and should come from a stable free-running source
    core_reset_i : in std_logic;        --! Sync to clk_sys_i
    -------------------------

    -- manager control/stat -- see manager core interfacing just below for more information about how to interface the core control
    clk_manager_i         : in  std_logic;
    manager_stat_reg_o    : out t_regbank32b(c_OLT_NBR_CTRL_REGS-1 downto 0);
    manager_ctrl_reg_i    : in  std_logic_vector(31 downto 0);
    manager_ctrl_strobe_i : in  std_logic_vector(c_OLT_NBR_CTRL_REGS-1 downto 0);
    --------------------------

    -- Soft-configurable interrupt output
    interrupt_o     : out std_logic;
    status_sticky_o : out std_logic_vector(30 downto 0);
    -------------------------

    -- data user in/out -- see TX usr core interfacing / RX usr core just below for more information about how to interface the core
    tx_data_i        : in std_logic_vector(c_OLT_TX_USR_DATA_WIDTH-1 downto 0);
    tx_data_strobe_i : in std_logic;

    rx_data_o        : out std_logic_vector(c_OLT_RX_USR_DATA_WIDTH-1 downto 0);
    rx_onu_addr_o    : out std_logic_vector(7 downto 0);
    rx_data_strobe_o : out std_logic;
    rx_data_error_o  : out std_logic;
    rx_locked_o      : out std_logic;
    --------------------------

    -- MGT connections --
    -- Clocks
    clk_trxusr240_i : in std_logic;      --! MGT trxusr clock

    -- Status/Ctrl
    mgt_reset_o  : out std_logic;
    mgt_tx_pol_o : out std_logic;
    mgt_rx_pol_o : out std_logic;

    -- Xilinx only
    mgt_drp_wr_o        : out std_logic_vector(31 downto 0);
    mgt_drp_wr_strobe_o : out std_logic;
    mgt_drp_monitor_i   : in  std_logic_vector(31 downto 0);
    mgt_rx_phase_ctrl_o : out std_logic_vector(31 downto 0);
    mgt_rx_phase_stat_i : in  std_logic_vector(31 downto 0);
    mgt_tx_phase_ctrl_o : out std_logic_vector(31 downto 0);
    mgt_tx_phase_stat_i : in  std_logic_vector(31 downto 0);
    
    mgt_tx_ready_i : in std_logic;
    mgt_rx_ready_i : in std_logic;
    mgt_pll_lock_i : in std_logic;

    -- Data
    mgt_tx_data_o : out std_logic_vector(c_OLT_TX_WORD_WIDTH-1 downto 0);
    mgt_rx_data_i : in  std_logic_vector(c_OLT_RX_WORD_WIDTH-1 downto 0);
    -------------------------

    -- transceiver ctrl/stat connections --
    sfp_rx_sd_i    : in  std_logic;
    sfp_tx_fault_i : in  std_logic;
    sfp_mod_abs_i  : in  std_logic;
    sfp_rx_reset_o : out std_logic;
    sfp_rssi_tri_o : out std_logic;
    sfp_tx_dis_o   : out std_logic;
    -------------------------   

    -- i2c interfacing --
    i2c_req_o      : out std_logic;
    i2c_rnw_o      : out std_logic;
    i2c_rb2_o      : out std_logic;
    i2c_slv_addr_o : out std_logic_vector(6 downto 0);
    i2c_reg_addr_o : out std_logic_vector(7 downto 0);
    i2c_wr_data_o  : out std_logic_vector(7 downto 0);

    i2c_done_i     : in std_logic;
    i2c_error_i    : in std_logic;
    i2c_drop_req_i : in std_logic;
    i2c_rd_data_i  : in std_logic_vector(15 downto 0)
    -------------------------
    );
end entity olt_core;

-- ### DATA INTERFACING:
------------------------------------ TX USR CORE INTERFACING --------------------------------------  (sync to clk_trxusr240_i)
-- Tx usr interfacing - each .____. represents a clk_trxusr240_i clock period
-- -------------------------------------------------------------------------------------------------
--
---- (1) - frame interfacing   
--
--                                      ___                          ___                          ___
-- tx_data_strobe_i         _.____.____/   \____.____.____.____.____/   \____.____.____.____.____/   \_
-- tx_data_i                           X            DATA0           X            DATA1           X
--
-- --------------------------------------------------------------------------------------------------   

------------------------------------ RX USR CORE INTERFACING -------------------------------------- (sync to clk_trxusr240_i)
-- Rx usr interfacing - each .____. represents a clk_trxusr240_i clock period
-- ------------------------------- Timing input/output (clocked out)-------------------------------
-- 
---- (1) - frame interfacing    
--
--                                      ___                          ___                         ___                         
-- rx_data_strobe_o         _.____.____/   \____.____  ... ____.____/   \____.____ ... ____.____/   \____
-- rx_data_o                           X        DATA ONU 1          X        DATA ONU 2         X
-- onu_addr_o                          X        ONU1 ADDR           X        ONU2 ADDR          X
--
-- --------------------------------------------------------------------------------------------------   

-- ### CONTROL CORE INTERFACING:
------------------------------------ MANAGER CORE INTERFACING -------------------------------------- (sync to clk_manager_i)
-- Manager core interfacing - each .____. represents a clk_manager_i clock period
-- ------------------------------- Timing input/output --------------------------------------------
--
-- CONTROL WRITE VALUES: 
-- olt_control.vhd has a register bank composed by 64 x 32b registers which are used for core control by the manager
-- in order to write a value to a specific register, user should keep manager_ctrl_strobe(REG_ADDR) to '1' for a single clk_manager_i clock cycle
-- and put the value to write in manager_ctrl_reg_i
--
-- Note: manager_ctrl_strobe is a hot_one encoding of the address user wants to write
--
-- Example - write value CW2 to control register #2:
--
-- CONTROL WRITE REGISTERS:
--                                                                                             
-- manager_ctrl_strobe_i(0)         _.____.____.____.____.____ ...____.____.____.____.____.____.____
--                                                                                                
-- manager_ctrl_strobe_i(1)         _.____.____.____.____.____ ...____.____.____.____.____.____.____
--                                              ___                                                  
-- manager_ctrl_strobe_i(2)         _.____.____/   \____.____  ...____.____.____.____.____.____.____
--                                                                                                
-- manager_ctrl_strobe_i(3)         _.____.____.____.____.____ ...____.____.____.____.____.____.____
--
--         ...
--                                                                                              
-- manager_ctrl_strobe_i(63)        _.____.____.____.____.____ ...____.____.____.____.____.____.____
--
-- manager_ctrl_reg_i(31 downto 0) X          x CW2 x                                               X
--
--
-- STATUS READ VALUES:
-- olt_control.vhd has a register bank composed by 16 x 32b registers which are used to read measurements needed for calibration / other status
-- those values are latched by some specific control commands
--
-- --------------------------------------------------------------------------------------------------   

--============================================================================
-- ! architecture declaration
--============================================================================
architecture structural of olt_core is

  attribute mark_debug : string;
  attribute keep       : string;

  --! Functions

  --! Constants

  --! Signal declaration
  -- global from olt_rx
  signal rx_data_strobe : std_logic;
  signal rx_data_error  : std_logic;
  signal rx_data        : std_logic_vector(c_OLT_RX_USR_DATA_WIDTH-1 downto 0);
  signal rx_onu_addr    : std_logic_vector(7 downto 0);
  signal rx_k28_5       : std_logic;
  signal rx_k28_1       : std_logic;
  signal rx_locked      : std_logic;

  -- global from control
  signal ctrl_heartbeat_header : std_logic;

  -- global from phy
  signal phy_reset_txclk : std_logic;
  signal phy_reset_rxclk : std_logic;

  -- olt_tx <-> olt_control           
  signal ctrl_to_tx_data        : std_logic_vector(c_OLT_TX_CTRL_DATA_WIDTH-1 downto 0);
  signal tx_to_ctrl_data_strobe : std_logic;

  -- rx <-> ctrl
  signal rx_to_ctrl_data          : std_logic_vector(c_OLT_RX_CTRL_DATA_WIDTH-1 downto 0);
  signal rx_to_ctrl_fine_position : std_logic_vector(6 downto 0);
  signal ctrl_to_rx_frame_length  : std_logic_vector(3 downto 0);
  signal ctrl_to_rx_en_lock       : std_logic;

  -- phy <-> rx
  signal phy_to_rx_en_osrx                  : std_logic;

  -- ctrl <-> phy
  signal ctrl_to_phy_burst_length     : std_logic_vector(5 downto 0);
  signal ctrl_to_phy_sfp_rx_reset_en  : std_logic;
  signal ctrl_to_phy_sfp_rx_reset_pos : std_logic_vector(5 downto 0);

  signal ctrl_to_phy_clear_rx_sd_stat : std_logic;
  signal ctrl_to_phy_latch_rx_sd_stat : std_logic;
  signal ctrl_to_phy_sfp_rx_sd_wmin   : std_logic_vector(9 downto 0);
  signal ctrl_to_phy_sfp_rx_sd_wmax   : std_logic_vector(9 downto 0);
  signal ctrl_to_phy_sfp_rx_sd_wcur   : std_logic_vector(9 downto 0);

  signal ctrl_to_phy_sd_mask_osrx  : std_logic;
  signal ctrl_to_phy_sd_mask_delay : std_logic_vector(3 downto 0);

  signal ctrl_to_phy_sd_comma_corr_clear : std_logic;
  signal ctrl_to_phy_sd_comma_corr_latch : std_logic;
  signal phy_to_ctrl_sd_comma_corr_stat  : std_logic_vector(10 downto 0);

  signal ctrl_to_phy_i2c_rssi_ctrl    : std_logic_vector(31 downto 0);
  signal ctrl_to_phy_i2c_rssi_ctrl_en : std_logic;
  signal phy_to_ctrl_i2c_rssi_stat    : std_logic_vector(31 downto 0);

  --! Component declaration
  component olt_tx is
    generic(
      g_WORD_WIDTH       : integer          := 40;
      g_DATA_USR_WIDTH   : integer          := 180;
      g_DATA_CTRL_WIDTH  : integer          := 24;
      g_REGULAR_HEADER   : std_logic_vector := "01101010";
      g_HEARTBEAT_HEADER : std_logic_vector := "10011010"
      );
    port (
      -- global input signals --
      clk_trxusr240_i : in std_logic;
      reset_i        : in std_logic;
      -------------------------

      -- data in/out --      
      data_strobe_i : in std_logic;
      data_usr_i    : in std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);

      data_ctrl_strobe_o : out std_logic;
      data_ctrl_i        : in  std_logic_vector(g_DATA_CTRL_WIDTH-1 downto 0);
      heartbeat_header_i : in  std_logic;

      data_o : out std_logic_vector(g_WORD_WIDTH-1 downto 0)
      -------------------------
      );
  end component olt_tx;

  component olt_rx is
    generic(
      g_WORD_WIDTH      : integer := 40;
      g_DATA_USR_WIDTH  : integer := 48;
      g_DATA_CTRL_WIDTH : integer := 16;
      g_BURST_LENGTH_OLT_RXUSRCLK : integer := 30;    --! BURST_LENGTH_NS/PERIOD_OLT_RX_CLK_NS
      g_WINDOW_BURST_COUNTING     : integer := 32*4;  --! number of bursts to be checked to consider locked
      g_GOOD_BURST_TO_LOCK        : integer := 32*2;  --! number of good bursts among window (g_WINDOW_BURST_COUNTING) to be considered locked
      g_GOOD_BURST_TO_UNLOCK      : integer := 32/2   --! less than this number of bursts among window (g_WINDOW_BURST_COUNTING) to be considered unlocked
      );
    port (
      -- global input signals --
      clk_trxusr240_i : in std_logic;    --! MGT interfacing clock - 120MHz
      reset_i        : in std_logic;
      -------------------------

      --status/control --
      osrx_frame_length_i : in  std_logic_vector(3 downto 0);
      en_osrx_i           : in  std_logic;
      en_lock_i           : in  std_logic;
      fine_position_o     : out std_logic_vector(6 downto 0);
      rx_k28_5_o          : out std_logic;
      rx_k28_1_o          : out std_logic;
      rx_locked_o         : out std_logic; 
      -------------------------

      -- data in/out --
      data_i        : in  std_logic_vector(g_WORD_WIDTH-1 downto 0);
      data_strobe_o : out std_logic;
      data_error_o  : out std_logic;
      data_usr_o    : out std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);
      data_ctrl_o   : out std_logic_vector(g_DATA_CTRL_WIDTH-1 downto 0);
      onu_addr_o    : out std_logic_vector(7 downto 0)
      -------------------------
      );
  end component olt_rx;

  component olt_control is
    generic(
      g_TX_DATA_CTRL_WIDTH : integer := 24;
      g_RX_DATA_USR_WIDTH  : integer := 48;
      g_RX_DATA_CTRL_WIDTH : integer := 16;
      g_NBR_CTRL_REGS      : integer := 16
      );
    port (
      -- global input signals --
      clk_trxusr240_i : in std_logic;
      clk_sys_i      : in std_logic;
      reset_rxclk_i  : in std_logic;
      reset_txclk_i  : in std_logic;
      -------------------------

      -- control from manager --
      clk_manager_i     : in  std_logic;
      olt_stat_reg_o    : out t_regbank32b(g_NBR_CTRL_REGS-1 downto 0);
      olt_ctrl_reg_i    : in  std_logic_vector(31 downto 0);
      olt_ctrl_strobe_i : in  std_logic_vector(g_NBR_CTRL_REGS-1 downto 0);
      --------------------------

      --status/control --  
      -- RX
      rx_locked_i        : in  std_logic;	  
      rx_en_lock_o       : out std_logic;
      rx_frame_length_o  : out std_logic_vector(3 downto 0);
      rx_fine_position_i : in  std_logic_vector(6 downto 0);
      rx_k28_5_i         : in  std_logic;
      rx_k28_1_i         : in  std_logic;

      -- PHY
      phy_burst_length_o     : out std_logic_vector(5 downto 0);
      phy_sfp_rx_reset_en_o  : out std_logic;
      phy_sfp_rx_reset_pos_o : out std_logic_vector(5 downto 0);

      phy_clear_rx_sd_stat_o : out std_logic;
      phy_latch_rx_sd_stat_o : out std_logic;
      phy_sfp_rx_sd_wmin_i   : in  std_logic_vector(9 downto 0);
      phy_sfp_rx_sd_wmax_i   : in  std_logic_vector(9 downto 0);
      phy_sfp_rx_sd_wcur_i   : in  std_logic_vector(9 downto 0);

      phy_sd_comma_corr_clear_o : out std_logic;
      phy_sd_comma_corr_latch_o : out std_logic;
      phy_sd_comma_corr_stat_i  : in  std_logic_vector(10 downto 0);
      phy_sd_mask_osrx_o        : out std_logic;
      phy_sd_mask_delay_o       : out std_logic_vector(3 downto 0);

      phy_i2c_rssi_ctrl_o    : out std_logic_vector(31 downto 0);
      phy_i2c_rssi_ctrl_en_o : out std_logic;
      phy_i2c_rssi_stat_i    : in  std_logic_vector(31 downto 0);

      -- MGT
      mgt_drp_wr_o        : out std_logic_vector(31 downto 0);
      mgt_drp_wr_strobe_o : out std_logic;
      mgt_drp_monitor_i   : in  std_logic_vector(31 downto 0);
      mgt_rx_phase_ctrl_o : out std_logic_vector(31 downto 0);
      mgt_rx_phase_stat_i : in  std_logic_vector(31 downto 0);
      mgt_tx_phase_ctrl_o : out std_logic_vector(31 downto 0);
      mgt_tx_phase_stat_i : in  std_logic_vector(31 downto 0);
    
      mgt_tx_ready_i   : in  std_logic;
      mgt_rx_ready_i   : in  std_logic;
      mgt_pll_lock_i   : in  std_logic;
      mgt_txpolarity_o : out std_logic;
      mgt_rxpolarity_o : out std_logic;

      -- Soft-configurable interrupt output
      interrupt_o     : out std_logic;
      status_sticky_o : out std_logic_vector(30 downto 0);
      -------------------------

      -- data in/out --
      tx_data_strobe_i      : in  std_logic;
      tx_data_ctrl_o        : out std_logic_vector(g_TX_DATA_CTRL_WIDTH-1 downto 0);
      tx_heartbeat_header_o : out std_logic;

      rx_data_strobe_i : in std_logic;
      rx_data_error_i  : in std_logic;
      rx_data_usr_i    : in std_logic_vector(g_RX_DATA_USR_WIDTH-1 downto 0);
      rx_data_ctrl_i   : in std_logic_vector(g_RX_DATA_CTRL_WIDTH-1 downto 0);
      rx_onu_addr_i    : in std_logic_vector(7 downto 0)
      -------------------------
      );
  end component olt_control;

  component olt_phy_control is
    generic(
      g_CLK_SYS_PERIOD : integer range 5 to 20 := 10  -- clock period in ns
      );
    port (
      -- global input signals --
      clk_trxusr240_i : in std_logic;
      clk_sys_i      : in std_logic;
      core_reset_i   : in std_logic;
      -------------------------

      -- global output signals --
      mgt_reset_o   : out std_logic;
      reset_txclk_o : out std_logic;
      reset_rxclk_o : out std_logic;
      ---------------------------

      -- status/control --
      mgt_rx_ready_i : in std_logic;
      mgt_tx_ready_i : in std_logic;

      tdm_heartbeat_i    : in std_logic;
      burst_length_i     : in std_logic_vector(5 downto 0);
      sfp_rx_reset_en_i  : in std_logic;
      sfp_rx_reset_pos_i : in std_logic_vector(5 downto 0);

      clear_rx_sd_stat_i : in  std_logic;
      latch_rx_sd_stat_i : in  std_logic;
      sfp_rx_sd_wmin_o   : out std_logic_vector(9 downto 0);
      sfp_rx_sd_wmax_o   : out std_logic_vector(9 downto 0);
      sfp_rx_sd_wcur_o   : out std_logic_vector(9 downto 0);

      sd_comma_corr_clear_i : in  std_logic;
      sd_comma_corr_latch_i : in  std_logic;
      sd_comma_corr_stat_o  : out std_logic_vector(10 downto 0);

      sd_mask_osrx_i  : in  std_logic;
      sd_mask_delay_i : in  std_logic_vector(3 downto 0);
      rx_comma_osrx_i : in  std_logic;
      rx_en_osrx_o    : out std_logic;

      -- sfp interface
      sfp_rx_sd_i    : in  std_logic;
      sfp_tx_fault_i : in  std_logic;
      sfp_mod_abs_i  : in  std_logic;
      sfp_rx_reset_o : out std_logic;
      sfp_rssi_tri_o : out std_logic;
      sfp_tx_dis_o   : out std_logic;

      --i2c interface
      i2c_rssi_ctrl_i    : in  std_logic_vector(31 downto 0);
      i2c_rssi_ctrl_en_i : in  std_logic;
      i2c_rssi_stat_o    : out std_logic_vector(31 downto 0);

      -- i2c user interface
      i2c_req_o      : out std_logic;
      i2c_rnw_o      : out std_logic;
      i2c_rb2_o      : out std_logic;
      i2c_slv_addr_o : out std_logic_vector(6 downto 0);
      i2c_reg_addr_o : out std_logic_vector(7 downto 0);
      i2c_wr_data_o  : out std_logic_vector(7 downto 0);

      i2c_done_i     : in std_logic;
      i2c_error_i    : in std_logic;
      i2c_drop_req_i : in std_logic;
      i2c_rd_data_i  : in std_logic_vector(15 downto 0)
      -------------------------

      );
  end component olt_phy_control;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- Component instantiation
  --! Component olt_tx
  --============================================================================   
  cmp_olt_tx : olt_tx
    generic map(
      g_WORD_WIDTH       => c_OLT_TX_WORD_WIDTH,
      g_DATA_USR_WIDTH   => c_OLT_TX_USR_DATA_WIDTH,
      g_DATA_CTRL_WIDTH  => c_OLT_TX_CTRL_DATA_WIDTH,
      g_REGULAR_HEADER   => c_DOWN_REGULAR_HEADER,
      g_HEARTBEAT_HEADER => c_DOWN_HEARTBEAT_HEADER
      )
    port map(
      -- global input signals --
      clk_trxusr240_i     => clk_trxusr240_i,
      reset_i            => phy_reset_txclk,
      -------------------------                  
      -- data in/out --   
      data_strobe_i      => tx_data_strobe_i,
      data_usr_i         => tx_data_i,
      data_ctrl_strobe_o => tx_to_ctrl_data_strobe,
      data_ctrl_i        => ctrl_to_tx_data,
      heartbeat_header_i => ctrl_heartbeat_header,
      data_o             => mgt_tx_data_o
      -------------------------
      );

  --============================================================================
  -- Component instantiation
  --! Component olt_rx
  --============================================================================          
  cmp_olt_rx : olt_rx
    generic map(
      g_WORD_WIDTH                => c_OLT_RX_WORD_WIDTH,
      g_DATA_USR_WIDTH            => c_OLT_RX_USR_DATA_WIDTH,
      g_DATA_CTRL_WIDTH           => c_OLT_RX_CTRL_DATA_WIDTH,
      g_BURST_LENGTH_OLT_RXUSRCLK => c_BURST_LENGTH_OLT_RXUSRCLK,
      g_WINDOW_BURST_COUNTING     => c_WINDOW_BURST_COUNTING,    
      g_GOOD_BURST_TO_LOCK        => c_GOOD_BURST_TO_LOCK,       
      g_GOOD_BURST_TO_UNLOCK      => c_GOOD_BURST_TO_UNLOCK     
    )
    port map(
      -- global input signals --
      clk_trxusr240_i => clk_trxusr240_i,
      reset_i        => phy_reset_rxclk,
      -------------------------

      --status/control --
      osrx_frame_length_i => ctrl_to_rx_frame_length,
      en_lock_i           => ctrl_to_rx_en_lock, 	  
      en_osrx_i           => phy_to_rx_en_osrx,
      fine_position_o     => rx_to_ctrl_fine_position,
      rx_k28_5_o          => rx_k28_5,
      rx_k28_1_o          => rx_k28_1,
      rx_locked_o         => rx_locked,
      -------------------------

      -- data in/out --  
      data_i        => mgt_rx_data_i,
      data_strobe_o => rx_data_strobe,
      data_error_o  => rx_data_error,
      data_usr_o    => rx_data,
      data_ctrl_o   => rx_to_ctrl_data,
      onu_addr_o    => rx_onu_addr
      -------------------------
      );

  --============================================================================
  -- Component instantiation
  --! Component olt_control
  --============================================================================   
  cmp_olt_control : olt_control
    generic map(
      g_TX_DATA_CTRL_WIDTH => c_OLT_TX_CTRL_DATA_WIDTH,
      g_RX_DATA_USR_WIDTH  => c_OLT_RX_USR_DATA_WIDTH,
      g_RX_DATA_CTRL_WIDTH => c_OLT_RX_CTRL_DATA_WIDTH,
      g_NBR_CTRL_REGS      => c_OLT_NBR_CTRL_REGS
      )
    port map(
      -- global input signals --
      clk_trxusr240_i => clk_trxusr240_i,
      clk_sys_i      => clk_sys_i,
      reset_txclk_i  => phy_reset_txclk,
      reset_rxclk_i  => phy_reset_rxclk,
      -------------------------

      -- control from manager --
      clk_manager_i     => clk_manager_i,
      olt_stat_reg_o    => manager_stat_reg_o,
      olt_ctrl_reg_i    => manager_ctrl_reg_i,
      olt_ctrl_strobe_i => manager_ctrl_strobe_i,
      --------------------------

      --status/control --         
      -- RX
      rx_locked_i            => rx_locked,	  
      rx_en_lock_o           => ctrl_to_rx_en_lock,
      rx_frame_length_o      => ctrl_to_rx_frame_length,
      rx_fine_position_i     => rx_to_ctrl_fine_position,
      rx_k28_5_i             => rx_k28_5,
      rx_k28_1_i             => rx_k28_1,
      -- PHY
      phy_burst_length_o     => ctrl_to_phy_burst_length,
      phy_sfp_rx_reset_en_o  => ctrl_to_phy_sfp_rx_reset_en,
      phy_sfp_rx_reset_pos_o => ctrl_to_phy_sfp_rx_reset_pos,

      phy_clear_rx_sd_stat_o => ctrl_to_phy_clear_rx_sd_stat,
      phy_latch_rx_sd_stat_o => ctrl_to_phy_latch_rx_sd_stat,
      phy_sfp_rx_sd_wmin_i   => ctrl_to_phy_sfp_rx_sd_wmin,
      phy_sfp_rx_sd_wmax_i   => ctrl_to_phy_sfp_rx_sd_wmax,
      phy_sfp_rx_sd_wcur_i   => ctrl_to_phy_sfp_rx_sd_wcur,

      phy_sd_comma_corr_clear_o => ctrl_to_phy_sd_comma_corr_clear,
      phy_sd_comma_corr_latch_o => ctrl_to_phy_sd_comma_corr_latch,
      phy_sd_comma_corr_stat_i  => phy_to_ctrl_sd_comma_corr_stat,
      phy_sd_mask_osrx_o        => ctrl_to_phy_sd_mask_osrx,
      phy_sd_mask_delay_o       => ctrl_to_phy_sd_mask_delay,

      phy_i2c_rssi_ctrl_o    => ctrl_to_phy_i2c_rssi_ctrl,
      phy_i2c_rssi_ctrl_en_o => ctrl_to_phy_i2c_rssi_ctrl_en,
      phy_i2c_rssi_stat_i    => phy_to_ctrl_i2c_rssi_stat,

      -- MGT
      mgt_tx_ready_i   => mgt_tx_ready_i,
      mgt_rx_ready_i   => mgt_rx_ready_i,
      mgt_pll_lock_i   => mgt_pll_lock_i,
      mgt_txpolarity_o => mgt_tx_pol_o,
      mgt_rxpolarity_o => mgt_rx_pol_o,

      mgt_drp_wr_o        => mgt_drp_wr_o,
      mgt_drp_wr_strobe_o => mgt_drp_wr_strobe_o,
      mgt_drp_monitor_i   => mgt_drp_monitor_i,
      mgt_rx_phase_ctrl_o => mgt_rx_phase_ctrl_o,
      mgt_rx_phase_stat_i => mgt_rx_phase_stat_i,
      mgt_tx_phase_ctrl_o => mgt_tx_phase_ctrl_o,
      mgt_tx_phase_stat_i => mgt_tx_phase_stat_i,
	  
      -- Soft-configurable interrupt output
      interrupt_o     => interrupt_o,
      status_sticky_o => status_sticky_o,
      -------------------------

      -- data in/out --          
      tx_data_strobe_i      => tx_to_ctrl_data_strobe,
      tx_data_ctrl_o        => ctrl_to_tx_data,
      tx_heartbeat_header_o => ctrl_heartbeat_header,

      rx_data_strobe_i => rx_data_strobe,
      rx_data_error_i  => rx_data_error,
      rx_data_usr_i    => rx_data,
      rx_data_ctrl_i   => rx_to_ctrl_data,
      rx_onu_addr_i    => rx_onu_addr
      -------------------------
      );

  --============================================================================
  -- Component instantiation
  --! Component olt_phy_control
  --============================================================================   
  cmp_olt_phy_control : olt_phy_control
    generic map(
      g_CLK_SYS_PERIOD => c_OLT_PERIOD_SYS_CLK
      )
    port map(
      -- global input signals --
      clk_trxusr240_i => clk_trxusr240_i,
      clk_sys_i      => clk_sys_i,
      core_reset_i   => core_reset_i,
      -------------------------

      -- global output signals --
      mgt_reset_o   => mgt_reset_o,
      reset_rxclk_o => phy_reset_rxclk,
      reset_txclk_o => phy_reset_txclk,
      ---------------------------

      -- status/control --
      mgt_rx_ready_i => mgt_rx_ready_i,
      mgt_tx_ready_i => mgt_tx_ready_i,

      tdm_heartbeat_i    => ctrl_heartbeat_header,
      burst_length_i     => ctrl_to_phy_burst_length,
      sfp_rx_reset_en_i  => ctrl_to_phy_sfp_rx_reset_en,
      sfp_rx_reset_pos_i => ctrl_to_phy_sfp_rx_reset_pos,

      clear_rx_sd_stat_i => ctrl_to_phy_clear_rx_sd_stat,
      latch_rx_sd_stat_i => ctrl_to_phy_latch_rx_sd_stat,
      sfp_rx_sd_wmin_o   => ctrl_to_phy_sfp_rx_sd_wmin,
      sfp_rx_sd_wmax_o   => ctrl_to_phy_sfp_rx_sd_wmax,
      sfp_rx_sd_wcur_o   => ctrl_to_phy_sfp_rx_sd_wcur,

      sd_comma_corr_clear_i => ctrl_to_phy_sd_comma_corr_clear,
      sd_comma_corr_latch_i => ctrl_to_phy_sd_comma_corr_latch,
      sd_comma_corr_stat_o  => phy_to_ctrl_sd_comma_corr_stat,
      sd_mask_osrx_i        => ctrl_to_phy_sd_mask_osrx,
      sd_mask_delay_i       => ctrl_to_phy_sd_mask_delay,

      rx_comma_osrx_i => rx_k28_5,
      rx_en_osrx_o    => phy_to_rx_en_osrx,

      sfp_rx_sd_i    => sfp_rx_sd_i,
      sfp_tx_fault_i => sfp_tx_fault_i,
      sfp_mod_abs_i  => sfp_mod_abs_i,
      sfp_rx_reset_o => sfp_rx_reset_o,
      sfp_rssi_tri_o => sfp_rssi_tri_o,
      sfp_tx_dis_o   => sfp_tx_dis_o,

      i2c_rssi_ctrl_i    => ctrl_to_phy_i2c_rssi_ctrl,
      i2c_rssi_ctrl_en_i => ctrl_to_phy_i2c_rssi_ctrl_en,
      i2c_rssi_stat_o    => phy_to_ctrl_i2c_rssi_stat,

      i2c_req_o      => i2c_req_o,
      i2c_rnw_o      => i2c_rnw_o,
      i2c_rb2_o      => i2c_rb2_o,
      i2c_slv_addr_o => i2c_slv_addr_o,
      i2c_reg_addr_o => i2c_reg_addr_o,
      i2c_wr_data_o  => i2c_wr_data_o,

      i2c_done_i     => i2c_done_i,
      i2c_error_i    => i2c_error_i,
      i2c_drop_req_i => i2c_drop_req_i,
      i2c_rd_data_i  => i2c_rd_data_i
      -------------------------
      );

  --============================================================================
  --Output
  --============================================================================            
  rx_data_strobe_o <= rx_data_strobe;
  rx_data_error_o  <= rx_data_error;
  rx_data_o        <= rx_data;
  rx_onu_addr_o    <= rx_onu_addr;
  rx_locked_o      <= rx_locked;

end architecture structural;
--============================================================================
-- architecture end
--============================================================================
