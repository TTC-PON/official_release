--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_control.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
use work.common_package.all;
use work.protocol_package.all;
use work.pon_olt_package_static.all;
use work.pon_olt_package_modifiable.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT Control top design (olt_control)
--
--! @brief OLT Control top design for TTC-PON
--! <further description>
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 14\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 14\06\2016 - EBSM - first .vhd definition\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo Implement \n
--! Implement FIFO stock rx_ctrl_data \n
--! Implement logic to detect missing ONUs
--! 
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_control
--============================================================================
entity olt_control is                   --use work.common_package.all, work.protocol_package.all, work.pon_olt_package_static.all, work.pon_olt_package_modifiable.all;
  generic(
    g_TX_DATA_CTRL_WIDTH : integer := 4;
    g_RX_DATA_USR_WIDTH  : integer := 48;
    g_RX_DATA_CTRL_WIDTH : integer := 16;
    g_NBR_CTRL_REGS      : integer := 16
    );
  port (
    -- global input signals --
    clk_trxusr240_i : in std_logic;
    clk_sys_i      : in std_logic;
    reset_txclk_i  : in std_logic;
    reset_rxclk_i  : in std_logic;
    -------------------------

    -- control from manager --
    clk_manager_i     : in  std_logic;
    olt_stat_reg_o    : out t_regbank32b(g_NBR_CTRL_REGS-1 downto 0);
    olt_ctrl_reg_i    : in  std_logic_vector(31 downto 0);
    olt_ctrl_strobe_i : in  std_logic_vector(g_NBR_CTRL_REGS-1 downto 0);
    --------------------------

    --status/control --   
    -- RX 
    rx_locked_i       : in  std_logic;
    rx_en_lock_o      : out std_logic;
    rx_frame_length_o : out std_logic_vector(3 downto 0);

    rx_fine_position_i : in std_logic_vector(6 downto 0);
    rx_k28_5_i         : in std_logic;
    rx_k28_1_i         : in std_logic;

    -- PHY
    phy_burst_length_o     : out std_logic_vector(5 downto 0);
    phy_sfp_rx_reset_en_o  : out std_logic;
    phy_sfp_rx_reset_pos_o : out std_logic_vector(5 downto 0);

    phy_clear_rx_sd_stat_o : out std_logic;
    phy_latch_rx_sd_stat_o : out std_logic;
    phy_sfp_rx_sd_wmin_i   : in  std_logic_vector(9 downto 0);
    phy_sfp_rx_sd_wmax_i   : in  std_logic_vector(9 downto 0);
    phy_sfp_rx_sd_wcur_i   : in  std_logic_vector(9 downto 0);

    phy_sd_comma_corr_clear_o : out std_logic;
    phy_sd_comma_corr_latch_o : out std_logic;
    phy_sd_comma_corr_stat_i  : in  std_logic_vector(10 downto 0);
    phy_sd_mask_osrx_o        : out std_logic;
    phy_sd_mask_delay_o       : out std_logic_vector(3 downto 0);

    phy_i2c_rssi_ctrl_o    : out std_logic_vector(31 downto 0);
    phy_i2c_rssi_ctrl_en_o : out std_logic;
    phy_i2c_rssi_stat_i    : in  std_logic_vector(31 downto 0);


    -- MGT      
    mgt_drp_wr_o        : out std_logic_vector(31 downto 0);
    mgt_drp_wr_strobe_o : out std_logic;
    mgt_drp_monitor_i   : in  std_logic_vector(31 downto 0);
    mgt_rx_phase_ctrl_o : out std_logic_vector(31 downto 0);
    mgt_rx_phase_stat_i : in  std_logic_vector(31 downto 0);
    mgt_tx_phase_ctrl_o : out std_logic_vector(31 downto 0);
    mgt_tx_phase_stat_i : in  std_logic_vector(31 downto 0);
	
    mgt_tx_ready_i : in std_logic;
    mgt_rx_ready_i : in std_logic;
    mgt_pll_lock_i : in std_logic;

    mgt_txpolarity_o : out std_logic;
    mgt_rxpolarity_o : out std_logic;

    -- Soft-configurable interrupt output
    interrupt_o     : out std_logic;
    status_sticky_o : out std_logic_vector(30 downto 0);
    -------------------------

    -- data in/out --
    tx_data_strobe_i      : in  std_logic;
    tx_data_ctrl_o        : out std_logic_vector(g_TX_DATA_CTRL_WIDTH-1 downto 0);
    tx_heartbeat_header_o : out std_logic;

    rx_data_strobe_i : in std_logic;
    rx_data_error_i  : in std_logic;
    rx_data_usr_i    : in std_logic_vector(g_RX_DATA_USR_WIDTH-1 downto 0);
    rx_data_ctrl_i   : in std_logic_vector(g_RX_DATA_CTRL_WIDTH-1 downto 0);
    rx_onu_addr_i    : in std_logic_vector(7 downto 0)
    -------------------------
    );
end entity olt_control;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of olt_control is

  attribute ASYNC_REG  : string;
  attribute mark_debug : string;
  attribute keep       : string;
  --! Functions

  --! Constants


  --! Signal declaration 
  --============================================================================
  -- TDM arbitration (used to send heartbeat header (k28.1 in 8b10b impl.) to serve as time ref. for ONU's) (clocked by clk_trxusr240_i)
  --============================================================================
  signal heartbeat_freq_trxclk_period : std_logic_vector(15 downto 0);  -- Heartbeat frequency - (LSB=clk_trxusr240_i_PERIOD)
  signal tdm_cntr                     : integer range 0 to (2**16 - 1);
  signal time_coarse                  : integer range 0 to (2**heartbeat_freq_trxclk_period'length-1);
  signal tx_heartbeat_header, tx_heartbeat_header_r, tx_heartbeat_p : std_logic;
  ------------------------------------------------------------------------------

  --============================================================================
  -- CALIBRATION
  --============================================================================     
  signal calib_on                    : std_logic;  -- calibration on
  signal calib_onu_addr              : std_logic_vector(7 downto 0);
  signal onu_position_latch_p        : std_logic;
  signal coarse_pos_meas             : std_logic_vector(heartbeat_freq_trxclk_period'range);
  signal coarse_pos_latched          : std_logic_vector(heartbeat_freq_trxclk_period'range);
  signal fine_pos_meas               : std_logic_vector(6 downto 0);
  signal fine_pos_latched            : std_logic_vector(6 downto 0);
  signal done_pos_meas               : std_logic;
  signal done_pos_meas_latched       : std_logic;
  ------------------------------------------------------------------------------
  
  --============================================================================
  -- SLOW CONTROL
  --============================================================================
  -- TX path   
  signal send_ctrl_data_p    : std_logic;  -- received ctrl data from manager 
  signal send_ctrl_data_wait : std_logic;  -- wait tx_path send ctrl data
  signal tx_ctrl_data        : std_logic_vector(c_CTRL_DATA_WIDTH-1 downto 0);  -- tx control data received from manager
  signal tx_data_ctrl_frame  : std_logic_vector(c_CTRL_DATA_WIDTH-1 downto 0);  -- tx control frame to be sent
  signal sent_ctrl_frame     : std_logic;


  signal rx_ctrl_data                : std_logic_vector(15 downto 0);
  signal rx_sc_onu_addr               : std_logic_vector(7 downto 0);
  signal rx_ctrl_data_frame          : std_logic_vector(c_CTRL_DATA_WIDTH-1 downto 0);
  signal rx_rcvd_ctrl_frame_p        : std_logic;
  signal rx_rcvd_ctrl_error_p        : std_logic;
  signal rx_ctrl_data_buf            : std_logic_vector(c_CTRL_DATA_WIDTH-1 downto 0);  -- to do: implement fifo to stock received ctrl data
  signal rx_ctrl_data_buf_mngrsync   : std_logic_vector(rx_ctrl_data_buf'range);
  signal rx_ctrl_data_error          : std_logic;
  signal rx_ctrl_data_error_mngrsync : std_logic;
  ------------------------------------------------------------------------------

  --============================================================================
  -- BERT
  --============================================================================  
  signal bert_clear_p               : std_logic;
  signal bert_latch_p               : std_logic;
  signal bert_onu1_addr             : std_logic_vector(7 downto 0);
  signal bert_onu2_addr             : std_logic_vector(7 downto 0);
  signal bert_bitonu1_latched       : std_logic_vector(15 downto 0);
  signal bert_bitonu2_latched       : std_logic_vector(15 downto 0);
  signal bert_error_onu1_latched    : std_logic_vector(15 downto 0);
  signal bert_error_onu2_latched    : std_logic_vector(15 downto 0);
  signal bert_ploss_onu1_latched    : std_logic_vector(15 downto 0);
  signal bert_ploss_onu2_latched    : std_logic_vector(15 downto 0);
  signal bert_locked_onu1_latched   : std_logic;
  signal bert_locked_onu2_latched   : std_logic;
  signal bert_acc_full_onu1_latched : std_logic;
  signal bert_acc_full_onu2_latched : std_logic;
  ------------------------------------------------------------------------------

  --============================================================================
  -- 8b10b Error Monitoring
  --============================================================================  
  signal errdet_mon_clear_p                                                        : std_logic;
  signal errdet_mon_latch_p                                                        : std_logic;
  signal errdet_seen_page_sel                                                      : std_logic_vector(4 downto 0);
  signal errdet_seen_page_sel_int                                                  : integer range 0 to 31;
  signal errdet_seen_onu                                                           : std_logic_vector(255 downto 0);  -- each bit corresponds to an ONU_X corresponds to BIT-X
  signal errdet_seen_onu_latched                                                   : std_logic_vector(7 downto 0);
  signal errdet_seen_any                                                           : std_logic;
  signal errdet_seen_any_mngrsync_meta                                             : std_logic;
  signal errdet_seen_any_mngrsync_r                                                : std_logic;
  attribute async_reg of errdet_seen_any_mngrsync_meta, errdet_seen_any_mngrsync_r : signal is "true";

  signal errdet_cntr_onu_addr : std_logic_vector(7 downto 0);
  signal errdet_cntr          : unsigned(7 downto 0);
  signal errdet_cntr_latched  : std_logic_vector(7 downto 0);
  ------------------------------------------------------------------------------

  --============================================================================
  -- MGT - Ctrl/Status Monitoring
  --============================================================================  
  signal mgt_mon_clear_p                                                                 : std_logic;
  signal mgt_seen_rx_nlocked_mngrsync_meta                                               : std_logic;  
  signal mgt_seen_tx_nready_mngrsync_meta                                                : std_logic;
  signal mgt_seen_rx_nready_mngrsync_meta                                                : std_logic;
  signal mgt_seen_pll_nlock_mngrsync_meta                                                : std_logic;
  signal mgt_seen_rx_nlocked_mngrsync_r                                                  : std_logic;   
  signal mgt_seen_tx_nready_mngrsync_r                                                   : std_logic;
  signal mgt_seen_rx_nready_mngrsync_r                                                   : std_logic;
  signal mgt_seen_pll_nlock_mngrsync_r                                                   : std_logic;
  attribute async_reg of mgt_seen_rx_nlocked_mngrsync_meta, mgt_seen_rx_nlocked_mngrsync_r : signal is "true";  
  attribute async_reg of mgt_seen_tx_nready_mngrsync_meta, mgt_seen_tx_nready_mngrsync_r   : signal is "true";
  attribute async_reg of mgt_seen_rx_nready_mngrsync_meta, mgt_seen_rx_nready_mngrsync_r   : signal is "true";
  attribute async_reg of mgt_seen_pll_nlock_mngrsync_meta, mgt_seen_pll_nlock_mngrsync_r   : signal is "true";

  signal mgt_txpolarity_async                                                : std_logic;
  signal mgt_txpolarity_txsync_meta                                          : std_logic;
  signal mgt_txpolarity_txsync_r                                             : std_logic;
  attribute ASYNC_REG of mgt_txpolarity_txsync_meta, mgt_txpolarity_txsync_r : signal is "TRUE";

  signal mgt_rxpolarity_async                                                : std_logic;
  signal mgt_rxpolarity_rxsync_meta                                          : std_logic;
  signal mgt_rxpolarity_rxsync_r                                             : std_logic;
  attribute ASYNC_REG of mgt_rxpolarity_rxsync_meta, mgt_rxpolarity_rxsync_r : signal is "TRUE";
  ------------------------------------------------------------------------------

  --============================================================================
  -- Monitoring interrupt output
  --============================================================================  
  signal interrupt_mask                                                : std_logic_vector(30 downto 0);
  signal interrupt_async                                               : std_logic;
  signal interrupt_mngrsync_meta                                       : std_logic;
  signal interrupt_mngrsync_r                                          : std_logic;
  attribute async_reg of interrupt_mngrsync_meta, interrupt_mngrsync_r : signal is "true";

  signal status_sticky                  : std_logic_vector(30 downto 0);
  attribute mark_debug of status_sticky : signal is "true";
  attribute keep of status_sticky       : signal is "true";
  ------------------------------------------------------------------------------

  --============================================================================
  -- Rx fine phase monitoring
  --============================================================================ 
  signal mgt_rx_phase_ctrl_comb              : std_logic_vector(31 downto 0);    
  signal rx_coarse_phase_measured            : std_logic_vector(heartbeat_freq_trxclk_period'range);
  
  signal rx_fine_phase_measurement_latch             : std_logic;
  signal rx_fine_phase_measurement_latch_rxsync_meta : std_logic; 
  signal rx_fine_phase_measurement_latch_rxsync_r    : std_logic;
  
  signal rx_fine_phase_measurement_enable                : std_logic;
  signal rx_fine_phase_measurement_enable_rxsync_meta    : std_logic;
  signal rx_fine_phase_measurement_enable_rxsync_r       : std_logic;
  
  signal rx_fine_phase_measurement_averaging             : std_logic_vector(5 downto 0);
  signal rx_fine_phase_measurement_averaging_rxsync_meta : std_logic_vector(5 downto 0);
  signal rx_fine_phase_measurement_averaging_rxsync_r    : std_logic_vector(5 downto 0);
  
  signal rx_fine_phase_measurement_onu_addr  : std_logic_vector(7 downto 0);
  signal rx_fine_phase_measurement_onu_addr_rxsync_meta  : std_logic_vector(7 downto 0);
  signal rx_fine_phase_measurement_onu_addr_rxsync_r  : std_logic_vector(7 downto 0);
  
  signal rx_fine_phase_measurement_results_latched : std_logic_vector(63 downto 0);
  signal rx_fine_phase_measurement_idle            : std_logic;

  signal rx_fine_phase_measurement_ps_strobe     : std_logic;
  signal rx_fine_phase_measurement_ps_inc_ndec   : std_logic;
  signal rx_fine_phase_measurement_ps_phase_step : std_logic_vector(3 downto 0);
  signal rx_fine_phase_measurement_ps_done       : std_logic;
  ------------------------------------------------------------------------------

  --============================================================================
  -- Manager Control
  --============================================================================     
  signal control_regbank : t_regbank32b(g_NBR_CTRL_REGS-1 downto 0) := (others => (others => '0'));
  signal control_strobe  : std_logic_vector(g_NBR_CTRL_REGS-1 downto 0) := (others => '0');

  --! Component declaration
  component pon_down_sc_tx is
    port (
      clk_i             : in  std_logic;  --! clock input
      reset_i           : in  std_logic;  --! active high sync. reset
      nibble_sent_i     : in  std_logic;  --! control scheduling
      data_o            : out std_logic_vector(3 downto 0);  --! encoded output data       
      data_i            : in  std_logic_vector(28 downto 0);  --! user input data
      sent_ctrl_frame_o : out std_logic   --! user interface control       
      );
  end component pon_down_sc_tx;

  component pon_up_sc_rx is
    generic(
      g_HEADER : std_logic_vector(2 downto 0) := "110"
      );
    port (
      clk_i             : in  std_logic;  --! clock input
      reset_i           : in  std_logic;  --! active high sync. reset
      byte_rcvd_i       : in  std_logic;  --! control scheduling
      data_error_i      : in  std_logic;  --! detected error 8b10b
      data_i            : in  std_logic_vector(15 downto 0);  --! encoded input data
      onu_addr_i        : in  std_logic_vector(7 downto 0);  --! address of ONU to be checked
      data_o            : out std_logic_vector(28 downto 0);  --! user output data
      rcvd_ctrl_frame_o : out std_logic;  --! user control 
      rcvd_ctrl_error_o : out std_logic  --! error detected in ctrl frame - 8b10b or in SOF field
      );
  end component pon_up_sc_rx;

  component bbert_top is
    generic(
      g_CLK_CYCLES_FRAME  : integer   := 1;
      g_BLOCK_ACCUMULATOR : std_logic := '1'  --'1' / 0  
      );
    port (
      clk_i               : in  std_logic;
      reset_i             : in  std_logic;
      bbert_clear_acc_i   : in  std_logic;
      bbert_latch_acc_i   : in  std_logic;
      superframe_length_i : in  std_logic_vector;
      onu_addr_chkr_i     : in  std_logic_vector(7 downto 0);
      data_i              : in  std_logic_vector;
      data_strobe_i       : in  std_logic;
      onu_addr_i          : in  std_logic_vector(7 downto 0);
      error_sum_o         : out std_logic_vector;
      bit_sum_o           : out std_logic_vector;
      packet_loss_sum_o   : out std_logic_vector;
      acc_full_o          : out std_logic;
      onu_locked_o        : out std_logic;
      onu_error_o         : out std_logic
      );
  end component bbert_top;

  component cdc_mailbox is
    generic(
      g_mailbox_type : integer := 0  -- 0 = regout (simple register logic) / >0 = mailbox
      );
    port(
      clk_a        : in std_logic;
      data_a_in    : in std_logic_vector;
      data_a_ready : in std_logic;

      clk_b         : in  std_logic;
      data_b_out    : out std_logic_vector;
      data_b_accept : out std_logic
      );
  end component cdc_mailbox;

  component rx_fine_phase_measurement is
      generic (
          g_SWEEP_FINE_PHASE_MEASUREMENT : integer := 512 -- 64*4*2 OLT_UI * 4 * 2 = 2*ONU_UI (This value should be bigger than one and smaller than 1000)
      );
      port (
          -- clock/reset
          clk_sys_i      : in std_logic; --! System clock
          clk_trxusr240_i : in std_logic; --! MGT interfacing clock - 120MHz
          reset_i        : in std_logic; --! Reset synchronous to clk_trxusr240_i
          enable_i       : in std_logic; --! Enable logic
  
          -- User interface
          rx_latch_measurement_i  : in std_logic;                    --! Latch measurement		
          number_averaging_i      : in std_logic_vector(5 downto 0); --! Number of averaged measurements to be performed (static)	
          onu_addr_i              : in std_logic_vector(7 downto 0); --! Address of ONU for which phase measurement is currently being performed
  
          -- Measurement status out
          results_meas_o           : out std_logic_vector(63 downto 0); --! Phase measurement Bus
          results_meas_p_o         : out std_logic;                     --! Pulse to indicate a new value is present in results_meas_o bus		
          results_meas_latched_o   : out std_logic_vector(63 downto 0); --! Phase measurement latched
          meas_idle_o              : out std_logic;                     --! Phase measurement is in a idle state  
		    
          -- PON interface
          master_heartbeat_i      : in std_logic;                     --! OLT heartbeat		
          calibration_on_i        : in std_logic;                     --! Calibration is ON			
          rx_heartbeat_header_i   : in std_logic;                     --! Heartbeat header used during calibration
          rx_normal_header_i      : in std_logic;                     --! Normal header used in normal run of the system
          modulo_position_i       : in std_logic_vector(15 downto 0); --! Modulo in which position measurement is based
          coarse_position_i       : in std_logic_vector(15 downto 0); --! Coarse position measured with coarse counter with low resolution (clk_trxusr240_i period)
          fine_position_i         : in std_logic_vector(6 downto 0);  --! Fine position measured by BOS with high resolution (BOS_UI)
          rx_onu_addr_i           : in std_logic_vector(7 downto 0);  --! ONU Received address
  
          -- low-level interface for phase shift
          ps_strobe_o         : out  std_logic;                   --! pulse synchronous to clk_sys_i to activate a shift in the phase (only captured rising edge, so a signal larger than a pulse is also fine)
          ps_inc_ndec_o       : out std_logic;                    --! 1 increments phase by phase_step_i units, 0 decrements phase by phase_step_i units
          ps_phase_step_o     : out std_logic_vector(3 downto 0); --! number of units to shift the phase of the receiver clock (see Xilinx transceiver User Guide to convert units in time)	   
          ps_done_i           : in std_logic                      --! pulse synchronous to clk_sys_i to indicate a phase shift was performed
  
      );
  end component rx_fine_phase_measurement;

--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  -- TDM arbitration (used to send special header (k28.1 in 8b10b impl.) to serve as time ref. for ONU's) (clocked by  clk_trxusr240_i)
  --============================================================================
  --============================================================================
  -- Process p_tdm_arbitration
  --! Generates Heartbeat signal for TDM arbitration
  --! read:  tx_data_strobe_i, heartbeat_freq_trxclk_period\n
  --! write: tx_heartbeat_header\n
  --! r/w:   tdm_cntr\n
  --============================================================================           
  p_tdm_arbitration : process(clk_trxusr240_i)
  begin
    if(clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(reset_txclk_i = '1') then
        tdm_cntr            <= 0;
        tx_heartbeat_header <= '0';
      else
        if(tdm_cntr < to_integer(unsigned(heartbeat_freq_trxclk_period))) then
          if(tx_data_strobe_i = '1') then
            tdm_cntr            <= tdm_cntr+c_RATIO_BCCLK_TRXCLK;
            tx_heartbeat_header <= '0';  -- send regular header 
          end if;
        else
          tdm_cntr            <= 0;
          tx_heartbeat_header <= '1';    -- send heartbeat header
        end if;
      end if;
    end if;
  end process p_tdm_arbitration;

  tx_heartbeat_header_o <= tx_heartbeat_header;
  tx_heartbeat_header_r <= tx_heartbeat_header when rising_edge(clk_trxusr240_i);
  tx_heartbeat_p        <= tx_heartbeat_header and not tx_heartbeat_header_r;	  
  --============================================================================
  -- Process p_time_coarse
  --! Used for coarse time measurement
  --! read:  tx_heartbeat_p, heartbeat_freq_trxclk_period\n
  --! write: \n
  --! r/w:  time_coarse\n
  --============================================================================           
  p_time_coarse : process(clk_trxusr240_i) is
  begin
    if(clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(reset_rxclk_i = '1') then
        time_coarse <= 0;
      else
        if(tx_heartbeat_p = '1') then
          time_coarse      <= 0;  
        else
          time_coarse      <= time_coarse + 1;  
        end if;
      end if;
    end if;
  end process p_time_coarse;

  --============================================================================
  -- SLOW CONTROL
  --============================================================================
  cmp_pon_down_sc_tx : pon_down_sc_tx
    port map(
      clk_i             => clk_trxusr240_i,
      reset_i           => reset_txclk_i,
      nibble_sent_i     => tx_data_strobe_i,
      data_o            => tx_data_ctrl_o,
      data_i            => tx_data_ctrl_frame,
      sent_ctrl_frame_o => sent_ctrl_frame
      ); 

  --============================================================================
  -- Process p_send_ctrl_data
  --! Send control data received from manager to ONU
  --! read:  sent_ctrl_frame\n
  --! write: tx_data_ctrl_frame\n
  --! r/w:   send_ctrl_data_wait\n
  --============================================================================        
  p_send_ctrl_data : process(clk_trxusr240_i)
  begin
    if(clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(reset_txclk_i = '1') then
        tx_data_ctrl_frame  <= (others => '0');
        send_ctrl_data_wait <= '0';
      else
        if(send_ctrl_data_wait = '1' or send_ctrl_data_p = '1') then
          tx_data_ctrl_frame <= tx_ctrl_data;
        else
          tx_data_ctrl_frame <= (others => '0');
        end if;

        if(send_ctrl_data_p = '1') then
          send_ctrl_data_wait <= '1';
        elsif(sent_ctrl_frame = '1') then
          send_ctrl_data_wait <= '0';
        end if;
        
      end if;
    end if;
  end process p_send_ctrl_data;

  cmp_pon_up_sc_rx : pon_up_sc_rx
    generic map(
      g_HEADER => c_SC_UP_HEADER
      )
    port map(
      clk_i             => clk_trxusr240_i,
      reset_i           => reset_rxclk_i,
      byte_rcvd_i       => rx_data_strobe_i,
      data_error_i      => rx_data_error_i,
      data_i            => rx_ctrl_data,
      onu_addr_i        => rx_sc_onu_addr,
      data_o            => rx_ctrl_data_frame,
      rcvd_ctrl_frame_o => rx_rcvd_ctrl_frame_p,
      rcvd_ctrl_error_o => rx_rcvd_ctrl_error_p
      );

  rx_ctrl_data <= rx_data_ctrl_i(7 downto 0)&rx_onu_addr_i;

  --============================================================================
  -- Process p_receive_ctrl_data
  --! Captures control data received from ONU
  --! !!! Note: FIFO will be implemented here
  --! read:  rx_rcvd_ctrl_frame_p, rx_ctrl_data_frame, ir_CTRL_OPE, c_OPE_READ, c_OPE_WRITE_ACK, c_OPE_RCVD_ERROR\n
  --! write: rx_ctrl_data_buf, rx_ctrl_data_error\n
  --! r/w:  \n
  --============================================================================        
  p_receive_ctrl_data : process(clk_trxusr240_i)
  begin
    if(clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(reset_rxclk_i = '1') then
        rx_ctrl_data_buf   <= (others => '0');
        rx_ctrl_data_error <= '0';
      else
        if(send_ctrl_data_wait = '1') then
          rx_ctrl_data_buf   <= (others => '0');
          rx_ctrl_data_error <= '0';
        elsif(rx_rcvd_ctrl_frame_p = '1') then
          rx_ctrl_data_buf   <= rx_ctrl_data_frame;
          rx_ctrl_data_error <= '1';
        end if;
      end if;
    end if;
  end process p_receive_ctrl_data;

  --============================================================================
  -- Process p_receive_ctrl_data_cdc
  --! Sync received data from ONU
  --! !!! Note: FIFO will be implemented here
  --! read:  rx_ctrl_data_buf\n
  --! write: rx_ctrl_data_buf_mngrsync, rx_ctrl_data_error_mngrsync\n
  --! r/w:  \n
  --============================================================================        
  p_receive_ctrl_data_cdc : process(clk_manager_i)
  begin
    if(clk_manager_i'event and clk_manager_i = '1') then
      rx_ctrl_data_buf_mngrsync   <= rx_ctrl_data_buf;
      rx_ctrl_data_error_mngrsync <= rx_ctrl_data_error;
    end if;
  end process p_receive_ctrl_data_cdc;

  --============================================================================
  -- CALIBRATION (clocked by clk_trxusr240_i)
  --============================================================================
  --============================================================================
  -- Process p_roundtrip_meas
  --! Measures ONU round-trip when a heartbeat is received
  --! read:  time_coarse, rx_fine_position_i, calib_on, calib_onu_addr, rx_k28_1_i, rx_k28_5_i\n
  --!        rx_onu_addr_i, onu_position_latch_p
  --! write: coarse_pos_latched,fine_pos_latched\n
  --! r/w:  coarse_pos_meas,fine_pos_meas\n
  --============================================================================        
  p_roundtrip_meas : process(clk_trxusr240_i) is
  begin
    if(clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(reset_rxclk_i = '1') then
        coarse_pos_meas       <= (others => '0');
        fine_pos_meas         <= (others => '0');
        done_pos_meas         <= '0';
        coarse_pos_latched    <= (others => '0');
        fine_pos_latched      <= (others => '0');
        done_pos_meas_latched <= '0';
      else
        if(onu_position_latch_p = '1') then
          coarse_pos_meas       <= (others => '0');
          fine_pos_meas         <= (others => '0');
          done_pos_meas         <= '0';
          coarse_pos_latched    <= coarse_pos_meas;
          fine_pos_latched      <= fine_pos_meas;
          done_pos_meas_latched <= done_pos_meas;
        elsif(rx_onu_addr_i = calib_onu_addr) then
          if((rx_k28_1_i = '1' and calib_on = '1') or (rx_k28_5_i = '1' and calib_on = '0')) then
            coarse_pos_meas <= rx_coarse_phase_measured;
            fine_pos_meas   <= rx_fine_position_i;
            done_pos_meas   <= '1';
          end if;
        end if;
      end if;
    end if;
  end process p_roundtrip_meas;

  rx_coarse_phase_measured <= std_logic_vector(to_unsigned(time_coarse, rx_coarse_phase_measured'length));

  cmp_rx_fine_phase_measurement : rx_fine_phase_measurement
      generic map(
          g_SWEEP_FINE_PHASE_MEASUREMENT => c_SWEEP_FINE_PHASE_MEASUREMENT
      )
      port map(
          -- clock/reset
          clk_sys_i      => clk_sys_i,
          clk_trxusr240_i => clk_trxusr240_i,
          reset_i        => reset_rxclk_i,
          enable_i       => rx_fine_phase_measurement_enable_rxsync_r,
  
          -- User interface
          rx_latch_measurement_i  => rx_fine_phase_measurement_latch_rxsync_r,
          number_averaging_i      => rx_fine_phase_measurement_averaging_rxsync_r,	
          onu_addr_i              => rx_fine_phase_measurement_onu_addr_rxsync_r,
  
          -- Measurement status out
          results_meas_o          => open,
          results_meas_p_o        => open,		
          results_meas_latched_o  => rx_fine_phase_measurement_results_latched,
          meas_idle_o             => rx_fine_phase_measurement_idle,          		
  
          -- PON interface
          master_heartbeat_i      => tx_heartbeat_p,
          calibration_on_i        => calib_on,	
          rx_heartbeat_header_i   => rx_k28_1_i,
          rx_normal_header_i      => rx_k28_5_i,
          modulo_position_i       => heartbeat_freq_trxclk_period,
          coarse_position_i       => rx_coarse_phase_measured,
          fine_position_i         => rx_fine_position_i,
          rx_onu_addr_i           => rx_onu_addr_i,
  
          -- low-level interface for phase shift
          ps_strobe_o             => rx_fine_phase_measurement_ps_strobe,
          ps_inc_ndec_o           => rx_fine_phase_measurement_ps_inc_ndec,
          ps_phase_step_o         => rx_fine_phase_measurement_ps_phase_step,
          ps_done_i               => rx_fine_phase_measurement_ps_done
  
      );

  --============================================================================
  -- BERT (used for reset calibration / link quality monitoring) (clocked by clk_trxusr240_i)
  --============================================================================
  --============================================================================
  -- Component instantiation
  --! Component cmp_bbert_onu_a
  --============================================================================                     
  cmp_bbert_onu_a : bbert_top
    generic map(
      g_CLK_CYCLES_FRAME  => 1,
      g_BLOCK_ACCUMULATOR => '1'        -- 1 / 0
      )
    port map(
      clk_i               => clk_trxusr240_i,
      reset_i             => reset_rxclk_i,
      bbert_clear_acc_i   => bert_clear_p,
      bbert_latch_acc_i   => bert_latch_p,
      superframe_length_i => heartbeat_freq_trxclk_period,
      onu_addr_chkr_i     => bert_onu1_addr,
      data_i              => rx_data_usr_i,
      onu_addr_i          => rx_onu_addr_i,
      data_strobe_i       => rx_data_strobe_i,
      error_sum_o         => bert_error_onu1_latched,
      bit_sum_o           => bert_bitonu1_latched,
      packet_loss_sum_o   => bert_ploss_onu1_latched,
      acc_full_o          => bert_acc_full_onu1_latched,
      onu_locked_o        => bert_locked_onu1_latched,
      onu_error_o         => open
      );

  --============================================================================
  -- Component instantiation
  --! Component cmp_bbert_onu_b
  --============================================================================   
  cmp_bbert_onu_b : bbert_top
    generic map(
      g_CLK_CYCLES_FRAME  => 1,
      g_BLOCK_ACCUMULATOR => '1'
      )
    port map(
      clk_i               => clk_trxusr240_i,
      reset_i             => reset_rxclk_i,
      bbert_clear_acc_i   => bert_clear_p,
      bbert_latch_acc_i   => bert_latch_p,
      superframe_length_i => heartbeat_freq_trxclk_period,
      onu_addr_chkr_i     => bert_onu2_addr,
      data_i              => rx_data_usr_i,
      onu_addr_i          => rx_onu_addr_i,
      data_strobe_i       => rx_data_strobe_i,
      error_sum_o         => bert_error_onu2_latched,
      bit_sum_o           => bert_bitonu2_latched,
      packet_loss_sum_o   => bert_ploss_onu2_latched,
      acc_full_o          => bert_acc_full_onu2_latched,
      onu_locked_o        => bert_locked_onu2_latched,
      onu_error_o         => open
      );

  --============================================================================
  -- 8b10b Error Monitoring
  --============================================================================  
  --============================================================================
  -- Process p_errdet_monitor
  --! read:  rx_data_strobe_i, rx_data_error_i, onu_addr_i, errdet_mon_clear_p, errdet_mon_latch_p, errdet_cntr_onu_addr\n
  --! write: errdet_seen_onu, errdet_seen_any, err_det_cntr\n
  --! r/w:   errdet_seen_onu_latched, err_det_cntr_latched\n
  --============================================================================             
  p_errdet_monitor : process(clk_trxusr240_i) is
  begin
    if(clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(errdet_mon_clear_p = '1') then
        errdet_seen_onu <= (others => '0');
        errdet_seen_any <= '0';
        errdet_cntr     <= (others => '0');
      elsif(rx_data_strobe_i = '1' and rx_data_error_i = '1') then
        errdet_seen_onu(to_integer(unsigned(rx_onu_addr_i))) <= '1';
        errdet_seen_any                                      <= '1';
        if(rx_onu_addr_i = errdet_cntr_onu_addr) then
          errdet_cntr <= errdet_cntr + 1;
        end if;
      end if;
      if(errdet_mon_clear_p = '1') then
        errdet_seen_onu_latched <= (others => '0');
        errdet_cntr_latched     <= (others => '0');
      elsif(errdet_mon_latch_p = '1') then
        errdet_cntr_latched     <= std_logic_vector(errdet_cntr);
        errdet_seen_onu_latched <= errdet_seen_onu((errdet_seen_page_sel_int+1)*8-1 downto errdet_seen_page_sel_int*8);
      end if;
    end if;
  end process p_errdet_monitor;

  errdet_seen_page_sel_int <= to_integer(unsigned(errdet_seen_page_sel));
  ------------------------------------------------------------------------------

  --============================================================================
  -- MGT Status Monitoring
  --============================================================================  
  --============================================================================
  -- Process p_mgtstat_monitor
  --! read:  mgt_mon_clear_p, mgt_tx_ready_i, mgt_rx_ready_i, mgt_pll_lock_i\n
  --! write: mgt_seen_rx_nlocked_mngrsync_meta, mgt_seen_tx_nready_mngrsync_meta, mgt_seen_rx_nready_mngrsync_meta, mgt_seen_pll_nlock_mngrsync_meta\n
  --! r/w:   \n
  --============================================================================         
  p_mgtstat_monitor : process(clk_manager_i) is
  begin
    if(clk_manager_i'event and clk_manager_i = '1') then
      if(mgt_mon_clear_p = '1') then
        mgt_seen_tx_nready_mngrsync_meta  <= '0';
        mgt_seen_rx_nready_mngrsync_meta  <= '0';
        mgt_seen_pll_nlock_mngrsync_meta  <= '0';
		mgt_seen_rx_nlocked_mngrsync_meta <= '0';
      else
        if(mgt_tx_ready_i = '0') then
          mgt_seen_tx_nready_mngrsync_meta <= '1';
        end if;
        if(mgt_rx_ready_i = '0') then
          mgt_seen_rx_nready_mngrsync_meta <= '1';
        end if;
        if(mgt_pll_lock_i = '0') then
          mgt_seen_pll_nlock_mngrsync_meta <= '1';
        end if;
        if(rx_locked_i = '0') then
	      mgt_seen_rx_nlocked_mngrsync_meta <= '1';
        end if;
      end if;
    end if;
  end process p_mgtstat_monitor;
  ------------------------------------------------------------------------------

  --============================================================================
  -- Interrupt
  --============================================================================  
  interrupt_async <= (interrupt_mask(0) and mgt_seen_tx_nready_mngrsync_meta) or
                     (interrupt_mask(1) and mgt_seen_rx_nready_mngrsync_meta) or
                     (interrupt_mask(2) and mgt_seen_pll_nlock_mngrsync_meta) or
                     (interrupt_mask(3) and errdet_seen_any) or					 
                     (interrupt_mask(4) and mgt_seen_rx_nlocked_mngrsync_meta);
  interrupt_o <= interrupt_mngrsync_r;

  status_sticky(0)           <= mgt_seen_tx_nready_mngrsync_r;
  status_sticky(1)           <= mgt_seen_rx_nready_mngrsync_r;
  status_sticky(2)           <= mgt_seen_pll_nlock_mngrsync_r;
  status_sticky(3)           <= errdet_seen_any_mngrsync_r;
  status_sticky(4)           <= mgt_seen_rx_nlocked_mngrsync_r;
  status_sticky(30 downto 5) <= (others => '0');

  status_sticky_o <= status_sticky;
  ------------------------------------------------------------------------------     

  --============================================================================
  -- Synchronizers
  --============================================================================
  --============================================================================
  -- Process p_rx_sync
  --! read:  tx_heartbeat_header, mgt_rxpolarity_async\n
  --! write: mgt_rxpolarity_rxsync_r\n
  --! r/w:   mgt_rxpolarity_rxsync_meta\n
  --============================================================================           
  p_rx_sync : process(clk_trxusr240_i) is
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      mgt_rxpolarity_rxsync_meta <= mgt_rxpolarity_async;
      mgt_rxpolarity_rxsync_r    <= mgt_rxpolarity_rxsync_meta;
	  rx_fine_phase_measurement_enable_rxsync_meta    <= rx_fine_phase_measurement_enable;
	  rx_fine_phase_measurement_enable_rxsync_r       <= rx_fine_phase_measurement_enable_rxsync_meta;
      rx_fine_phase_measurement_latch_rxsync_meta     <= rx_fine_phase_measurement_latch;
      rx_fine_phase_measurement_latch_rxsync_r        <= rx_fine_phase_measurement_latch_rxsync_meta;
      rx_fine_phase_measurement_onu_addr_rxsync_meta  <= rx_fine_phase_measurement_onu_addr;
      rx_fine_phase_measurement_onu_addr_rxsync_r     <= rx_fine_phase_measurement_onu_addr_rxsync_meta;
      rx_fine_phase_measurement_averaging_rxsync_meta <= rx_fine_phase_measurement_averaging;
	  rx_fine_phase_measurement_averaging_rxsync_r    <= rx_fine_phase_measurement_averaging_rxsync_meta;
    end if;
  end process p_rx_sync;

  --============================================================================
  -- Process p_tx_sync
  --! read:  mgt_txpolarity_async\n
  --! write: mgt_txxpolarity_txsync_r\n
  --! r/w:   mgt_txpolarity_txsync_meta\n
  --============================================================================           
  p_tx_sync : process(clk_trxusr240_i) is
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      mgt_txpolarity_txsync_meta <= mgt_txpolarity_async;
      mgt_txpolarity_txsync_r    <= mgt_txpolarity_txsync_meta;
    end if;
  end process p_tx_sync;


  --============================================================================
  -- Process p_mngr_sync
  --! read:  errdet_seen_any, mgt_seen_tx_nready_mngrsync_meta, mgt_seen_rx_nready_mngrsync_meta, mgt_seen_pll_nlock_mngrsync_meta, interrupt_mngrsync_meta, interrupt_async\n
  --! write: mgt_seen_rx_nlocked_mngrsync_r, mgt_seen_tx_nready_mngrsync_r, mgt_seen_rx_nready_mngrsync_r, mgt_seen_pll_nlock_mngrsync_r, interrupt_mngrsync_r, errdet_seen_any_mngrsync_r\n
  --! r/w:   errdet_seen_any_mngrsync_meta\n
  --============================================================================           
  p_mngr_sync : process(clk_manager_i) is
  begin
    if (clk_manager_i'event and clk_manager_i = '1') then
      mgt_seen_tx_nready_mngrsync_r  <= mgt_seen_tx_nready_mngrsync_meta;
      mgt_seen_rx_nready_mngrsync_r  <= mgt_seen_rx_nready_mngrsync_meta;
      mgt_seen_pll_nlock_mngrsync_r  <= mgt_seen_pll_nlock_mngrsync_meta;
      mgt_seen_rx_nlocked_mngrsync_r <= mgt_seen_rx_nlocked_mngrsync_meta;	  
      errdet_seen_any_mngrsync_meta  <= errdet_seen_any;
      errdet_seen_any_mngrsync_r     <= errdet_seen_any_mngrsync_meta;
      interrupt_mngrsync_meta        <= interrupt_async;
      interrupt_mngrsync_r           <= interrupt_mngrsync_meta;
    end if;
  end process p_mngr_sync;

  --============================================================================
  -- CONTROL REG ASSIGNMENT (Read / Write)
  --============================================================================
  -- A mailbox structure is used to pass signals from clk_manager_i domain to clk_trxusr240_i/clk_trxusr240_i domains
  -- This approach for clock domain crossing was chosen since the data passing from one domain to the others are not
  -- supposed to be a stream of data (which would require a FIFO to handle, for instance) but instead, they are single slow transactions
  --
  -- The timing of the mailbox is depicted below:
  --
  ----timing:
  ----clk_a         :  ____/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\
  ----data_a_in     :   X X X X X X X X    / DATA STABLE \ X X X X X X X X X X X X X X X X X X X X X X X X 
  ----data_a_ready  :  ____________________/-------------\__...____________________________________________                    

  ----clk_b         :  ______/------\______/------\______...__/------\______/------\______/------\______
  ----data_b_out    :  X X X X X X X X    / X X X X X X X X X/  DATA STABLE                             
  ----data_b_accept :  _____________________________     ..._/--------------\___________________________
  ----                                                   ... |     SAMPLE                                                        
  --                                      |                   |
  --                                       this time is at most: PERIOD_CLK_A+3*PERIOD_CLK_B
  --
  -- Note: If latching a signal w/ clk_manager_i domain, mailbox is not needed; therefore, the generic g_mailbox_type has to be set to 0
  --        this will simple create a register instead of mailbox
  --
  --
  -- CONTROL REGISTER0: - CAPTURE CLOCK: clk_trxusr240_i
  -- Before this register was used and nowadays is reserved for future applications
  cmp_mailbox0 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(0),
      clk_b         => clk_trxusr240_i,  -- clock domain b = clk_trxusr240_i
      data_b_out    => control_regbank(0),
      data_b_accept => control_strobe(0)
      );
  olt_stat_reg_o(0) <= control_regbank(0);

  -- CONTROL REGISTER1: - CAPTURE CLOCK: clk_trxusr240_i  
  cmp_mailbox1 : cdc_mailbox
    generic map(
      g_mailbox_type => 1)
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(1),
      clk_b         => clk_trxusr240_i,  -- clock domain b = clk_trxusr240_i
      data_b_out    => control_regbank(1),
      data_b_accept => control_strobe(1)
      ); 
  olt_stat_reg_o(1) <= control_regbank(1);

  heartbeat_freq_trxclk_period <= control_regbank(1)(15 downto 0);

  -- CONTROL REGISTER2:  - CAPTURE CLOCK: clk_trxusr240_i
  -- Send Slow Control data to ONU      
  cmp_mailbox2 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(2),
      clk_b         => clk_trxusr240_i,  -- clock domain b = clk_trxusr240_i
      data_b_out    => control_regbank(2),
      data_b_accept => control_strobe(2)
      );
  olt_stat_reg_o(2) <= control_regbank(2);

  tx_ctrl_data     <= control_regbank(2)(c_CTRL_DATA_WIDTH-1 downto 0);
  send_ctrl_data_p <= control_strobe(2);

  rx_sc_onu_addr <= tx_ctrl_data(ir_ONU_ADDR);


  -- CONTROL REGISTER2:  - CAPTURE CLOCK: clk_trxusr240_i
  -- Calibration related functions   
  cmp_mailbox3 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(3),
      clk_b         => clk_trxusr240_i,  -- clock domain b = clk_trxusr240_i
      data_b_out    => control_regbank(3),
      data_b_accept => control_strobe(3)
      );
  olt_stat_reg_o(3) <= control_regbank(3);

  calib_onu_addr       <= control_regbank(3)(7 downto 0);
  calib_on             <= control_regbank(3)(8);
  -- BIT 9 was used in the past and it is nowadays reserved for future applications
  onu_position_latch_p <= control_regbank(3)(10) and control_strobe(3);
  rx_en_lock_o         <= control_regbank(3)(11);  
  rx_frame_length_o    <= "0000" when calib_on = '1' else "0011";

  -- CONTROL REGISTER4:  - CAPTURE CLOCK: clk_trxusr240_i
  -- BERT control
  cmp_mailbox4 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(4),
      clk_b         => clk_trxusr240_i,  -- clock domain b = clk_trxusr240_i
      data_b_out    => control_regbank(4),
      data_b_accept => control_strobe(4)
      );   
  olt_stat_reg_o(4) <= control_regbank(4);

  bert_onu1_addr <= control_regbank(4)(7 downto 0);
  bert_onu2_addr <= control_regbank(4)(15 downto 8);
  bert_clear_p   <= control_regbank(4)(16) and control_strobe(4);
  bert_latch_p   <= control_regbank(4)(17) and control_strobe(4);

  -- CONTROL REGISTER5:  - CAPTURE CLOCK: clk_trxusr240_i  
  -- To PHY: SFP Rx Reset Generation
  cmp_mailbox5 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(5),
      clk_b         => clk_trxusr240_i,  -- clock domain b = clk_trxusr240_i
      data_b_out    => control_regbank(5),
      data_b_accept => control_strobe(5)
      );
  olt_stat_reg_o(5)      <= control_regbank(5);
  phy_burst_length_o     <= control_regbank(5)(5 downto 0);
  phy_sfp_rx_reset_pos_o <= control_regbank(5)(11 downto 6);
  phy_sfp_rx_reset_en_o  <= control_regbank(5)(12);

  -- STATUS REGISTER6:
  olt_stat_reg_o(6)(28 downto 0)  <= rx_ctrl_data_buf_mngrsync;  -- A fifo will be implemented to stock data  
  olt_stat_reg_o(6)(29)           <= rx_ctrl_data_error_mngrsync;
  olt_stat_reg_o(6)(31 downto 30) <= (others => '0');

  -- STATUS REGISTER7:   
  olt_stat_reg_o(7)(6 downto 0)   <= fine_pos_latched;
  olt_stat_reg_o(7)(22 downto 7)  <= coarse_pos_latched;
  olt_stat_reg_o(7)(23)           <= done_pos_meas_latched; 
  olt_stat_reg_o(7)(31 downto 24) <= (others => '0');

  -- STATUS REGISTER8:   
  olt_stat_reg_o(8)(15 downto 0)  <= bert_error_onu1_latched;
  olt_stat_reg_o(8)(31 downto 16) <= bert_error_onu2_latched;

  -- STATUS REGISTER9:   
  olt_stat_reg_o(9)(0)           <= bert_locked_onu1_latched;
  olt_stat_reg_o(9)(1)           <= bert_locked_onu2_latched;
  olt_stat_reg_o(9)(2)           <= bert_acc_full_onu1_latched;
  olt_stat_reg_o(9)(3)           <= bert_acc_full_onu2_latched;
  olt_stat_reg_o(9)(31 downto 4) <= (others => '0');

  -- To MGT: CTRL/STAT - only reg
  cmp_mailbox10 : cdc_mailbox
    generic map(
      g_mailbox_type => 0) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(10),
      clk_b         => clk_manager_i,   -- clock domain b = clk_manager_i
      data_b_out    => control_regbank(10),
      data_b_accept => control_strobe(10)
      );
  olt_stat_reg_o(10)(15 downto 0) <= control_regbank(10)(15 downto 0);
  olt_stat_reg_o(10)(16)          <= mgt_seen_tx_nready_mngrsync_r;
  olt_stat_reg_o(10)(17)          <= mgt_seen_rx_nready_mngrsync_r;
  olt_stat_reg_o(10)(18)          <= mgt_seen_pll_nlock_mngrsync_r;
  olt_stat_reg_o(10)(19)          <= mgt_seen_rx_nlocked_mngrsync_r;

  mgt_txpolarity_async <= control_regbank(10)(0);
  mgt_rxpolarity_async <= control_regbank(10)(1);
  mgt_mon_clear_p      <= control_regbank(10)(2) and control_strobe(10);

  mgt_txpolarity_o <= mgt_txpolarity_txsync_r;
  mgt_rxpolarity_o <= mgt_rxpolarity_rxsync_r;

  -- To PHY: SD stat
  cmp_mailbox11 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(11),
      clk_b         => clk_trxusr240_i,  -- clock domain b = clk_manager_i - synchronized with double FF synchronizer
      data_b_out    => control_regbank(11),
      data_b_accept => control_strobe(11)
      );

  phy_clear_rx_sd_stat_o <= control_regbank(11)(0);
  phy_latch_rx_sd_stat_o <= control_regbank(11)(1);

  phy_sd_comma_corr_clear_o <= control_regbank(11)(2);
  phy_sd_comma_corr_latch_o <= control_regbank(11)(3);

  phy_sd_mask_osrx_o  <= control_regbank(11)(4);
  phy_sd_mask_delay_o <= control_regbank(11)(8 downto 5);

  olt_stat_reg_o(11)(15 downto 0)  <= control_regbank(11)(15 downto 0);
  olt_stat_reg_o(11)(26 downto 16) <= phy_sd_comma_corr_stat_i(10 downto 0);
  olt_stat_reg_o(11)(31 downto 27) <= "00000";

  -- STATUS REGISTER12
  olt_stat_reg_o(12)(9 downto 0)   <= phy_sfp_rx_sd_wmin_i;
  olt_stat_reg_o(12)(19 downto 10) <= phy_sfp_rx_sd_wmax_i;
  olt_stat_reg_o(12)(29 downto 20) <= phy_sfp_rx_sd_wcur_i;

  -- To PHY: I2C master and RSSI
  cmp_mailbox13 : cdc_mailbox
    generic map(
      g_mailbox_type => 1
      ) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(13),
      clk_b         => clk_sys_i,       -- clock domain b = clk_sys_i
      data_b_out    => control_regbank(13),
      data_b_accept => control_strobe(13)
      );

  olt_stat_reg_o(13)     <= control_regbank(13);
  phy_i2c_rssi_ctrl_o    <= control_regbank(13);
  phy_i2c_rssi_ctrl_en_o <= control_strobe(13);

  olt_stat_reg_o(14) <= phy_i2c_rssi_stat_i;

  -- CONTROL REGISTER15:  - CAPTURE CLOCK: clk_trxusr240_i
  -- BERT control
  cmp_mailbox15 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(15),
      clk_b         => clk_trxusr240_i,  -- clock domain b = clk_trxusr240_i
      data_b_out    => control_regbank(15),
      data_b_accept => control_strobe(15)
      );

  errdet_mon_clear_p   <= control_regbank(15)(0) and control_strobe(15);
  errdet_mon_latch_p   <= control_regbank(15)(1) and control_strobe(15);
  errdet_seen_page_sel <= control_regbank(15)(6 downto 2);
  errdet_cntr_onu_addr <= control_regbank(15)(14 downto 7);

  olt_stat_reg_o(15)(14 downto 0)  <= control_regbank(15)(14 downto 0);
  olt_stat_reg_o(15)(15)           <= errdet_seen_any_mngrsync_r;
  olt_stat_reg_o(15)(23 downto 16) <= errdet_seen_onu_latched;
  olt_stat_reg_o(15)(31 downto 24) <= errdet_cntr_latched;

  -- Interrupt Control
  cmp_mailbox16 : cdc_mailbox
    generic map(
      g_mailbox_type => 1
      ) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(16),
      clk_b         => clk_sys_i,       -- clock domain b = clk_sys_i
      data_b_out    => control_regbank(16),
      data_b_accept => control_strobe(16)
      );
  olt_stat_reg_o(16)(30 downto 0) <= control_regbank(16)(30 downto 0);
  olt_stat_reg_o(16)(31)          <= interrupt_mngrsync_r;
  interrupt_mask                  <= control_regbank(16)(30 downto 0);

  -- To MGT: DRP access
  cmp_mailbox17 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(17),
      clk_b         => clk_sys_i,       -- clock domain b = clk_sys_i
      data_b_out    => control_regbank(17),
      data_b_accept => control_strobe(17)
      );

  mgt_drp_wr_o        <= control_regbank(17);
  mgt_drp_wr_strobe_o <= control_strobe(17);
  olt_stat_reg_o(17)  <= mgt_drp_monitor_i;

  -- To MGT: Rx Phase Control
  cmp_mailbox18 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(18),
      clk_b         => clk_sys_i,       -- clock domain b = clk_sys_i
      data_b_out    => control_regbank(18),
      data_b_accept => control_strobe(18)
      );

  -- User interface for low-level Rx PI phase measurement
  mgt_rx_phase_ctrl_comb(0)            <= control_regbank(18)(0)          when (rx_fine_phase_measurement_enable='0' and rx_fine_phase_measurement_idle='1') else rx_fine_phase_measurement_ps_strobe; -- strobe
  rx_fine_phase_measurement_ps_done <= mgt_rx_phase_stat_i(0);
  
  mgt_rx_phase_ctrl_comb(1)            <= control_regbank(18)(1)          when (rx_fine_phase_measurement_enable='0' and rx_fine_phase_measurement_idle='1')  else rx_fine_phase_measurement_ps_inc_ndec;
  mgt_rx_phase_ctrl_comb(5 downto 2)   <= control_regbank(18)(5 downto 2) when (rx_fine_phase_measurement_enable='0' and rx_fine_phase_measurement_idle='1')  else rx_fine_phase_measurement_ps_phase_step;
  mgt_rx_phase_ctrl_comb(31 downto 6)  <= control_regbank(18)(31 downto 6); -- rx phase aligner control

  mgt_rx_phase_ctrl_o  <= mgt_rx_phase_ctrl_comb when rising_edge(clk_sys_i);
  olt_stat_reg_o(18)   <= mgt_rx_phase_stat_i;

  -- To MGT: Rx Fine Phase Measurement
  cmp_mailbox19 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(19),
      clk_b         => clk_sys_i,       -- clock domain b = clk_sys_i
      data_b_out    => control_regbank(19),
      data_b_accept => control_strobe(19)
      );
  rx_fine_phase_measurement_enable    <= control_regbank(19)(0);
  rx_fine_phase_measurement_latch     <= control_regbank(19)(1);
  rx_fine_phase_measurement_onu_addr  <= control_regbank(19)(9 downto 2);
  rx_fine_phase_measurement_averaging <= control_regbank(19)(15 downto 10);
  olt_stat_reg_o(19)                  <= control_regbank(19);
 
  olt_stat_reg_o(20)  <= rx_fine_phase_measurement_results_latched(31 downto 0);
  olt_stat_reg_o(21)  <= rx_fine_phase_measurement_results_latched(63 downto 32);

  -- To MGT: Tx Phase Control
  cmp_mailbox22 : cdc_mailbox
    generic map(
      g_mailbox_type => 1) 
    port map(
      clk_a         => clk_manager_i,   -- clock domain a = clk_manager_i
      data_a_in     => olt_ctrl_reg_i,
      data_a_ready  => olt_ctrl_strobe_i(22),
      clk_b         => clk_sys_i,       -- clock domain b = clk_sys_i
      data_b_out    => control_regbank(22),
      data_b_accept => control_strobe(22)
      );

    mgt_tx_phase_ctrl_o <= control_regbank(22);
    olt_stat_reg_o(22)  <= mgt_tx_phase_stat_i;
	
end architecture rtl;
--============================================================================
-- architecture end
--============================================================================
