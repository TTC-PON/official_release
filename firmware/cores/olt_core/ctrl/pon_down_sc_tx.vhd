--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file pon_down_sc_tx.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages
use work.fec_package.all;
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: Slow control encoder design for TTC-PON downstream (pon_down_sc_tx)
--
--! @brief Slow control encoder design for TTC-PON downstream including CRC-7 protection
--! - It segment the control frame into nibbles to be sent every TTC-PON frame (25ns)
--! - This design can be re-used for any data_i width, respecting the following rule: mod(data_i'length+7,4)=0
--! - CRC-7 is an error detection code with the following properties:
--! - any triple-error detection
--! - burst error detection up to 7 bits
--! - the codeword space vector are the polynomials divisible by g(x) = 1 + x + x^2 + x^4 + x^5 + x^7
--! - the properties above are true for data_i'length up to 56 bits
--! - more information on the CRC polynomial design can be found under the paper "Cyclic Redundancy Code (CRC) Polynomial Selection for Embedded Networks"
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 03\05\2017
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 03\05\2017 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for pon_down_sc_tx
--==============================================================================
entity pon_down_sc_tx is
  port (
    clk_i             : in  std_logic;  --! clock input
    reset_i           : in  std_logic;  --! active high sync. reset
    nibble_sent_i     : in  std_logic;  --! control scheduling
    data_o            : out std_logic_vector(3 downto 0);  --! encoded output data         
    data_i            : in  std_logic_vector(28 downto 0);  --! user input data
    sent_ctrl_frame_o : out std_logic   --! user interface control      
    );
end pon_down_sc_tx;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of pon_down_sc_tx is

  --! Constant declaration
  constant c_MOD_NIBBLE_CNTR : integer := (data_i'length+7)/(data_o'length);

  --! Signal declaration
  signal parity_bits : std_logic_vector(6 downto 0);

  signal nibble_cntr : integer range 0 to c_MOD_NIBBLE_CNTR-1;

  signal nibble_sent_r : std_logic;

  signal data_r : std_logic_vector(data_i'left+7 downto 0);

  signal data_prescale : std_logic_vector(data_i'left+7 downto 0);
  
begin

  --============================================================================
  -- Process p_nibble_cntr
  --! counter for scheduling
  --! read: clk_i, reset_i, nibble_sent_i\n
  --! write: -\n
  --! r/w: nibble_cntr \n
  --============================================================================   
  p_nibble_cntr : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        nibble_cntr <= 0;
      elsif(nibble_sent_i = '1') then
        if(nibble_cntr = (c_MOD_NIBBLE_CNTR-1)) then
          nibble_cntr <= 0;
        else
          nibble_cntr <= nibble_cntr + 1;
        end if;
      end if;
    end if;
  end process p_nibble_cntr;

  --============================================================================
  -- Process p_nibble_sent_delay
  --! delay pulse for scheduling
  --! read: clk_i, nibble_sent_i\n
  --! write: nibble_sent_r\n
  --! r/w: - \n
  --============================================================================   
  p_nibble_sent_delay : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      nibble_sent_r <= nibble_sent_i;
    end if;
  end process p_nibble_sent_delay;

  sent_ctrl_frame_o <= '1' when nibble_sent_i = '1' and nibble_cntr = (c_MOD_NIBBLE_CNTR-1) else '0';

  --============================================================================
  -- Process p_data_r
  --! register data + parity bits
  --! read: clk_i, data_i, parity_bits\n
  --! write: data_r\n
  --! r/w: - \n
  --============================================================================
  p_data_r : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if (nibble_sent_r = '1' and nibble_cntr = 0) then
        data_r(data_i'range)                     <= data_i;
        data_r(data_r'left downto data_r'left-6) <= parity_bits xor c_CRC7_HEADER;
      end if;
    end if;
  end process p_data_r;

  --============================================================================
  -- Process p_output_mux
  --! Multiplexer+FF to send nibble to TX
  --! read: clk_i, reset_i, data_r\n
  --! write: data_o\n
  --! r/w: - \n
  --============================================================================
  p_output_mux : process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then
      if(reset_i = '1') then
        data_o <= (others => '0');
      else
        data_o <= data_r((nibble_cntr+1)*4-1 downto nibble_cntr*4);
      end if;
    end if;
  end process p_output_mux;

  --============================================================================
  -- Component instantiation - crc7 generator
  --! Component cmp_poly_div
  --! Calculates r(x) = rem(data_i/(g(x)))
  --============================================================================
  cmp_poly_div : poly_div
    generic map(
      g_DIVISOR_POLY   => c_CRC7_POLY,
      g_DIVIDEND_WIDTH => (data_i'length+7)
      )
    port map(
      clk_i       => clk_i,
      reset_i     => '1',
      enable_i    => '1',
      dividend_i  => data_prescale,
      quotient_o  => open,
      remainder_o => parity_bits
      );

  data_prescale <= "0000000"&data_i;
  
end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================
