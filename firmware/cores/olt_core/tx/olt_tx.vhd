--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_tx.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT TX top design (olt_tx)
--
--! @brief OLT TX top design for TTC-PON
--! <further description>
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 23\06\2016
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 23\06\2016 - EBSM - Created\n
--! 27\02\2017 - EBSM - Included BCH(120,106) encoder and self-sync. scrambler
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_tx
--============================================================================
entity olt_tx is
  generic(
    g_WORD_WIDTH       : integer          := 40;
    g_DATA_USR_WIDTH   : integer          := 180;
    g_DATA_CTRL_WIDTH  : integer          := 24;
    g_REGULAR_HEADER   : std_logic_vector := "01101010";
    g_HEARTBEAT_HEADER : std_logic_vector := "10011010"
    );
  port (
    -- global input signals --
    clk_trxusr240_i : in std_logic;
    reset_i        : in std_logic;
    -------------------------

    -- data in/out --     
    data_strobe_i : in std_logic;
    data_usr_i    : in std_logic_vector(g_DATA_USR_WIDTH-1 downto 0);

    data_ctrl_strobe_o : out std_logic;
    data_ctrl_i        : in  std_logic_vector(g_DATA_CTRL_WIDTH-1 downto 0);
    heartbeat_header_i : in  std_logic;

    data_o : out std_logic_vector(g_WORD_WIDTH-1 downto 0)
    -------------------------
    );
end entity olt_tx;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of olt_tx is

  attribute mark_debug : string;

  --! Functions

  --! Constants
  constant c_NBR_REDUNDANCY_BITS : integer := 14;

  --! Signal declaration
  signal payload_cntr : integer range 0 to 7;

  signal data_header     : std_logic_vector(g_REGULAR_HEADER'length-1 downto 0);
  signal data_frame      : std_logic_vector((g_REGULAR_HEADER'length+g_DATA_USR_WIDTH+g_DATA_CTRL_WIDTH-1) downto 0);
  signal data_mux        : std_logic_vector(g_WORD_WIDTH-1 downto 0);
  signal data_mux_strobe : std_logic;
  signal start_encoding  : std_logic;
  signal scrambled_data  : std_logic_vector(39 downto 0) := x"0000000000";

  --! Component declaration
  component scrambling is
    generic (
      g_PARAL_FACTOR  : integer := 40;
      g_DATA_SIZE_END : integer := 33;
      g_HEADER_LENGTH : integer := 8
      );
    port (
      clk_i            : in  std_logic;  --! clock input
      en_i             : in  std_logic;  --! enable input
      reset_i          : in  std_logic;  --! active high sync. reset
      data_is_header_i : in  std_logic;  --! data is header -> bypass scrambler
      data_end_i       : in  std_logic;  --! data_end -> bypass scrambler
      data_i           : in  std_logic_vector(g_PARAL_FACTOR-1 downto 0);  --! input data
      data_o           : out std_logic_vector(g_PARAL_FACTOR-1 downto 0)  --! scrambled output data
      );
  end component scrambling;

  component enc_bch120_106 is
    port (
      clk_i       : in  std_logic;      --! clock input
      reset_i     : in  std_logic;      --! active high sync. reset
      start_enc_i : in  std_logic;      --! control scheduling
      data_i      : in  std_logic_vector(39 downto 0);  --! input data
      data_o      : out std_logic_vector(39 downto 0)   --! encoded output data
      );
  end component enc_bch120_106;

--============================================================================
-- architecture begin
--============================================================================
begin

  data_header <= g_REGULAR_HEADER when heartbeat_header_i = '0' else g_HEARTBEAT_HEADER;

  ----------------------------------------------------------------------------
  --! Process p_payload_cntr
  --! Word-serialization control
  --! read:  data_strobe_i\n
  --! write: \n
  --! r/w:   payload_cntr\n
  ----------------------------------------------------------------------------
  p_payload_cntr : process(clk_trxusr240_i) is
  begin
    if (clk_trxusr240_i'event and clk_trxusr240_i = '1') then
      if(reset_i = '1') then
        payload_cntr <= 0;
      else
        if(data_strobe_i = '1') then
          payload_cntr <= 5;
        elsif(payload_cntr = 0) then
          payload_cntr <= 5;
        else
          payload_cntr <= payload_cntr-1;
        end if;
      end if;
    end if;
  end process p_payload_cntr;

  data_mux_strobe    <= '1' when payload_cntr = 0 else '0';
  data_ctrl_strobe_o <= data_mux_strobe;

  ----------------------------------------------------------------------------
  --! Process p_mux_data_order (purely combinatorial)
  --! read:  payload_cntr,data_frame\n
  --! write: data_mux\n
  --! r/w:   \n
  ----------------------------------------------------------------------------   
  p_mux_data_order : process(payload_cntr, data_frame) is
  begin
    case payload_cntr is
      when 0 =>
        data_mux <= data_frame(g_WORD_WIDTH-1 downto 0);
      when 5 =>
        data_mux <= data_frame(2*g_WORD_WIDTH-1 downto g_WORD_WIDTH);
      when 4 =>
        data_mux(g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS-1 downto 0)           <= data_frame(3*g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS-1 downto 2*g_WORD_WIDTH);
        data_mux(data_mux'left downto g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS) <= (others => '0');
      when 3 =>
        data_mux <= data_frame(4*g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS-1 downto 3*g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS);
      when 2 =>
        data_mux <= data_frame(5*g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS-1 downto 4*g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS);
      when others =>
        data_mux(g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS-1 downto 0)           <= data_frame(6*g_WORD_WIDTH-2*c_NBR_REDUNDANCY_BITS-1 downto 5*g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS);
        data_mux(data_mux'left downto g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS) <= (others => '0');
    end case;
  end process p_mux_data_order;

  data_frame <= (data_usr_i&data_ctrl_i&data_header);

  -- Scheduling
  start_encoding <= '1' when (payload_cntr = 4 or payload_cntr = 1) else '0';

  --! Component declaration
  cmp_scrambling : scrambling
    generic map(
      g_PARAL_FACTOR  => g_WORD_WIDTH,
      g_DATA_SIZE_END => (g_WORD_WIDTH-c_NBR_REDUNDANCY_BITS),
      g_HEADER_LENGTH => data_header'length
      )
    port map(
      clk_i            => clk_trxusr240_i,
      en_i             => '1',
      reset_i          => reset_i,
      data_is_header_i => data_mux_strobe,
      data_end_i       => start_encoding,
      data_i           => data_mux,
      data_o           => scrambled_data
      );

  cmp_enc_bch120_106 : enc_bch120_106
    port map(
      clk_i       => clk_trxusr240_i,
      reset_i     => reset_i,
      start_enc_i => start_encoding,
      data_i      => scrambled_data,
      data_o      => data_o
      );

end architecture rtl;
--============================================================================
-- architecture end
--============================================================================
