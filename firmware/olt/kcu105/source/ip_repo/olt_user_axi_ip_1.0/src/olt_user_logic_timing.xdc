set_false_path -to [get_pins -hier -filter {NAME =~ *rxsync_reg*/D}]

set_false_path -to [get_pins -hier -filter {NAME =~ *txsync_reg*/D}]

set_false_path -to [get_pins -hier -filter {NAME =~ *axisync_reg*/D}]

set_false_path -to [get_pins -hier -filter {NAME =~ *axi_rdata_reg*/D}]