--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file drp_ctrl.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: GTH/GTX DRP controller (drp_ctrl)
--
--! @brief Dynamic Reconfiguration Port controller for GTX/GTH-Xilinx transceivers
--! <further description>
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 10\02\2017
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 10\02\2016 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for drp_ctrl
--============================================================================
entity drp_ctrl is
  port (
    -- global input signals --
    clk_i   : in std_logic;
    reset_i : in std_logic;
    --------------------------

    -- Interface to user --
    drp_wr_i        : in  std_logic_vector(31 downto 0);
    drp_wr_strobe_i : in  std_logic;
    drp_monitor_o   : out std_logic_vector(31 downto 0);
    -------------------------------

    -- DRP connection to GTH/GTX --
    mgt_drpwe_o   : out std_logic;
    mgt_drpen_o   : out std_logic;
    mgt_drpaddr_o : out std_logic_vector(8 downto 0);
    mgt_drpdi_o   : out std_logic_vector(15 downto 0);
    mgt_drprdy_i  : in  std_logic;
    mgt_drpdo_i   : in  std_logic_vector(15 downto 0)
    -------------------------------
    );
end entity drp_ctrl;

--============================================================================
-- ! architecture declaration
--============================================================================
architecture rtl of drp_ctrl is

  --! Signal declaration   
  signal drpwe_r   : std_logic;
  signal drpen_r   : std_logic;
  signal drpaddr_r : std_logic_vector(8 downto 0);
  signal drpdi_r   : std_logic_vector(15 downto 0);

  signal drpdo_r  : std_logic_vector(15 downto 0);
  signal drprdy_r : std_logic;

  --! Component declaration


--============================================================================
-- architecture begin
--============================================================================
begin

  --============================================================================
  --! DRP interfacing
  --============================================================================
  -------- DRP control (begin) -----------------------------------------------------
  p_drp_write : process (clk_i) is
  begin  -- process p_drp_write
    if clk_i'event and clk_i = '1' then  -- rising clock edge
      if reset_i = '1' then
        drpwe_r   <= '0';
        drpen_r   <= '0';
        drpaddr_r <= (others => '0');
        drpdi_r   <= (others => '0');
      else
        drpwe_r <= '0';
        drpen_r <= '0';
        if drp_wr_strobe_i = '1' then
          drpdi_r   <= drp_wr_i(15 downto 0);
          drpaddr_r <= drp_wr_i(24 downto 16);
          drpwe_r   <= drp_wr_i(25);
          drpen_r   <= drp_wr_i(26);
        end if;
      end if;
    end if;
  end process p_drp_write;

  p_drp_read : process (clk_i) is
  begin  -- process p_drp_read
    if clk_i'event and clk_i = '1' then  -- rising clock edge
      if reset_i = '1' then
        drpdo_r  <= (others => '0');
        drprdy_r <= '0';
      else
        if drp_wr_strobe_i = '1' and drp_wr_i(26) = '1' then
          drprdy_r <= '0';
        else
          drprdy_r <= drprdy_r or mgt_drprdy_i;
        end if;

        if mgt_drprdy_i = '1' then
          drpdo_r <= mgt_drpdo_i;
        end if;

      end if;
    end if;
  end process p_drp_read;

  mgt_drpaddr_o <= drpaddr_r;
  mgt_drpdi_o   <= drpdi_r;
  mgt_drpen_o   <= drpen_r;
  mgt_drpwe_o   <= drpwe_r;

  drp_monitor_o <= "00000" & drprdy_r & drpwe_r & drpaddr_r & drpdo_r;
  -------- DRP control (end) -----------------------------------------------------    
  
end architecture rtl;
--============================================================================
-- architecture end
--============================================================================
