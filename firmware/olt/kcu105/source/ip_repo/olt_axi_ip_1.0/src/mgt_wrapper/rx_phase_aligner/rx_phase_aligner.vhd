--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--==============================================================================
--! @file rx_phase_aligner.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Specific packages
-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, HPTD
-- --
-------------------------------------------------------------------------------
--
-- unit name: Rx Phase Aligner for usage when elastic buffer is enabled (rx_phase_aligner) with dithering capability
--
--! @brief Rx Phase Aligner for usage when elastic buffer is enabled
--! - Implements rx phase alignment procedure
--! - It is recommended to keep this block in a reset state ('reset_i' = 1) until the transceiver reset procedure is completed
--! - It is also recommended to keep the transmitter user logic in a reset state while the alignment procedure is not finished ('rx_aligned_o' = 0)
--! Different flavours are possible:
--! 1) At each reset, re-align transmitter with fine PI step:
--!    - When is it recommended?
--!        a) applications not requiring a perfect phase determinism (~5-10 ps variation) with resets
--!        b) applications using this block only as a CDC strategy with minimal latency variation
--!    - How to use design?
--!    - Config ports:
--!        Tie   rx_pi_phase_calib_i   to all '0'
--!        Tie   rx_ui_align_calib_i   to '0'
--!
--! 2) At each reset, re-align the transmitter PI to a calibrated value
--!    - When is it recommended?
--!        a) applications requiring a perfect phase determinism (~1 ps variation) with resets
--!        b) applications where the board FPGA is not subject to large temperature variations
--!    - What does it cost?
--!        a) Requires a initial calibration (automatically done by block) during first reset
--!        b) Monitor the rx_fifo_fill_pd_o and perform re-calibration whenever it is all zeros or all ones
--!
--!    - How to use design?
--!    - Config ports:
--!
--!        a) during first reset:
--!           Tie   rx_pi_phase_calib_i   to all X (dont care)
--!           Tie   rx_ui_align_calib_i   to '0'
--!
--!        b) during other resets:
--!           Tie   rx_pi_phase_calib_i   to the value of 'rx_pi_phase_o' after the first reset
--!           Tie   rx_ui_align_calib_i   to '1'
--!
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 03\05\2018
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 03\05\2018 - EBSM - Created\n
--! 13\09\2018 - EBSM - Remove unused ports\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! <another thing to do> \n
--
-------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for rx_phase_aligner
--==============================================================================
entity rx_phase_aligner is
  generic(
    g_DRP_ADDR_RXCDR_CFG3  : std_logic_vector(8 downto 0) := ("000010001");  --! Check the transceiver user guide of your device for this address
    g_RATIO_RXFREQ_SYSFREQ : integer                      := 10  --! This parameter can be calculated as RXUSRCLK2_FREQ / CLKSYS_FREQ, it is recommended to keep to at least 5 for safety reasons
    );                                                                    
  port (
    --==============================================================================
    --! User control/monitor ports
    --==============================================================================    
    -- Clock / reset                                                       
    clk_sys_i : in std_logic;           --! system clock input
    reset_i   : in std_logic;  --! active high sync. reset (recommended to keep reset_i=1 while transceiver reset initialization is being performed)

    -- Top level interface                                                 
    rx_aligned_o : out std_logic;  --! Use it as a reset for the user transmitter logic

    -- Config (for different flavours)
    rx_pi_phase_calib_i   : in std_logic_vector(6 downto 0);  --! previous calibrated rx pi phase (rx_pi_phase_o after first reset calibration)
    rx_ui_align_calib_i   : in std_logic;  --! align with previous calibrated rx pi phase
    rx_fifo_fill_pd_max_i : in std_logic_vector(31 downto 0);  --! phase detector accumulated max output, sets precision of phase detector
                                           --! this is supposedly a static signal, this block shall be reset whenever this signal changes
                                           --! the time for each phase detection after a clear is given by rx_fifo_fill_pd_max_i * PERIOD_clk_rxusr_i
    rx_fine_realign_i     : in std_logic;  --! A rising edge will cause the Rx to perform a fine realignment to the half-response

    -- It is only valid to re-shift clock once aligned (rx_aligned_o = '1') 
    ps_strobe_i     : in  std_logic;  --! pulse synchronous to clk_sys_i to activate a shift in the phase (only captured rising edge, so a signal larger than a pulse is also fine)
    ps_inc_ndec_i   : in  std_logic;  --! 1 increments phase by phase_step_i units, 0 decrements phase by phase_step_i units
    ps_phase_step_i : in  std_logic_vector(3 downto 0);  --! number of units to shift the phase of the receiver clock (see Xilinx transceiver User Guide to convert units in time)       
    ps_done_o       : out std_logic;  --! pulse synchronous to clk_sys_i to indicate a phase shift was performed

    debug_rx_skip_phase_alignment_i : in std_logic;  --! Debug port for test purposes in order to skip phase alignment 

    -- Rx PI phase value
    rx_pi_phase_o : out std_logic_vector(6 downto 0);  --! phase shift accumulated

    -- Rx fifo fill level phase detector                                   
    rx_fifo_fill_pd_o : out std_logic_vector(31 downto 0);  --! phase detector output, when aligned this value should be close to (0x2_0000)


    --==============================================================================
    --! MGT ports
    --==============================================================================
    clk_rxusr_i : in std_logic;         --! rxusr2clk

    -- Rx fifo fill level - see Xilinx transceiver User Guide for more information      
    rx_fifo_fill_level_i : in std_logic;  --! connect to rxbufstatus[0]

    -- Receiver PI ports - see Xilinx transceiver User Guide for more information
    rxcdrhold_o   : out std_logic;      --! CDR hold       
    rxcdrovrden_o : out std_logic;      --! CDR override

    -- DRP interface - see Xilinx transceiver User Guide for more information
    -- obs1: connect clk_sys_i to drpclk        
    drpaddr_o : out std_logic_vector(8 downto 0);  --! For devices with a 10-bit DRP address interface, connect MSB to '0'
    drpen_o   : out std_logic;          --! DRP enable transaction
    drpdi_o   : out std_logic_vector(15 downto 0);  --! DRP data write
    drprdy_i  : in  std_logic;          --! DRP finished transaction
    drpdo_i   : in  std_logic_vector(15 downto 0);  --! DRP data read; not used nowadays, write only interface
    drpwe_o   : out std_logic           --! DRP write enable

    );
end rx_phase_aligner;

--==============================================================================
-- architecture declaration
--==============================================================================

architecture rtl of rx_phase_aligner is

  --! Function declaration

  --! Constant declaration  
  constant c_SPEED_PD_FACTOR : integer range 0 to 19 := 7;
  constant c_PI_COARSE_STEP  : integer range 0 to 15 := 8;
  constant c_PI_FINE_STEP    : integer range 0 to 15 := 1;

  --! Signal declaration
  -- rx_pi_ctrl <-> rx_phase_aligner_fsm/user
  signal rx_aligner_rx_pi_strobe     : std_logic;
  signal rx_aligner_rx_pi_inc_ndec   : std_logic;
  signal rx_aligner_rx_pi_phase_step : std_logic_vector(3 downto 0);
  signal rx_aligner_rx_pi_done       : std_logic;
  signal rx_pi_strobe                : std_logic;
  signal rx_pi_inc_ndec              : std_logic;
  signal rx_pi_phase_step            : std_logic_vector(3 downto 0);
  signal rx_pi_done                  : std_logic;

  signal rx_pi_phase : std_logic_vector(6 downto 0);

  -- rx_fifo_fill_level_acc <-> rx_phase_aligner_fsm  
  signal rx_fifo_fill_pd_clear : std_logic;
  signal rx_fifo_fill_pd_done  : std_logic;
  signal rx_fifo_fill_pd       : std_logic_vector(31 downto 0);
  signal rx_fifo_fill_pd_max   : std_logic_vector(31 downto 0);

  signal reset_fifo_fill_level_acc : std_logic;

  signal rx_aligned : std_logic;

  --! Component declaration
  component rx_phase_aligner_fsm is
    generic(
      g_SPEED_PD_FACTOR : integer range 0 to 19 := 10;  --! coarse alignment procedure takes g_RX_FIFO_FILL_PD_MAX/(2**g_SPEED_PD_FACTOR)

      g_PI_COARSE_STEP : integer range 0 to 15 := 8;  --! coarse PI steps

      g_PI_FINE_STEP : integer range 0 to 15 := 1  --! fine PI steps               
      );
    port (
      -- Clock / reset  
      clk_sys_i : in std_logic;         --! system clock input
      reset_i   : in std_logic;         --! active high sync. reset

      -- Top level interface
      rx_aligned_o : out std_logic;  --! Use it as a reset for the user transmitter logic

      -- Config (for different flavours)
      rx_pi_phase_calib_i             : in std_logic_vector(6 downto 0);  --! previous calibrated rx pi phase
      rx_ui_align_calib_i             : in std_logic;  --! align with previous calibrated rx pi phase
      rx_enable_reset_i               : in std_logic;  --! enable rx reset for perfect phase alignment (only relevant if rx_ui_align_calib_i is '1')
      rx_fifo_fill_pd_max_i           : in std_logic_vector(31 downto 0);  --! phase detector accumulated max output, sets precision of phase detector
                                                       --! this is supposedly a static signal, this block shall be reset whenever this signal changes
                                                       --! the time for each phase detection after a clear is given by rx_fifo_fill_pd_max_i * PERIOD_clk_rxusr_i
      rx_fine_realign_i               : in std_logic;  --! A rising edge will cause the Rx to perform a fine realignment to the half-response
      debug_rx_skip_phase_alignment_i : in std_logic;  --! Tie to '1' to not perform phase alignment

      -- Rx pi controller interface - see user interface rx_pi_ctrl.vhd for more information
      rx_pi_strobe_o     : out std_logic;  --! see user interface rx_pi_ctrl.vhd for more information
      rx_pi_inc_ndec_o   : out std_logic;  --! see user interface rx_pi_ctrl.vhd for more information
      rx_pi_phase_step_o : out std_logic_vector(3 downto 0);  --! see user interface rx_pi_ctrl.vhd for more information
      rx_pi_done_i       : in  std_logic;  --! see user interface rx_pi_ctrl.vhd for more information
      rx_pi_phase_i      : in  std_logic_vector(6 downto 0);  --! see user interface rx_pi_ctrl.vhd for more information

      -- Rx fifo fill level phase detector interface - see user interface fifo_fill_level_acc.vhd for more information
      rx_fifo_fill_pd_clear_o : out std_logic;  --! see user interface fifo_fill_level_acc.vhd for more information
      rx_fifo_fill_pd_done_i  : in  std_logic;  --! see user interface fifo_fill_level_acc.vhd for more information
      rx_fifo_fill_pd_i       : in  std_logic_vector(31 downto 0);  --! see user interface fifo_fill_level_acc.vhd for more information
      rx_fifo_fill_pd_max_o   : out std_logic_vector(31 downto 0);  --! see user interface fifo_fill_level_acc.vhd for more information

      -- Rx MGT reset (only used when rx_enable_reset_i is activated)
      rx_reset_o : out std_logic
      );
  end component rx_phase_aligner_fsm;

  component rx_pi_ctrl is
    generic(
      g_DRP_ADDR_RXCDR_CFG3  : std_logic_vector(8 downto 0) := ("000010001");  --! Check the transceiver user guide of your device for this address
      g_RATIO_RXFREQ_SYSFREQ : integer                      := 10  --! This parameter can be calculated as RXUSRCLK2_FREQ / CLKSYS_FREQ, it is recommended to keep to at least 5 for safety reasons
      );
    port (
      -- User Interface 
      clk_sys_i    : in  std_logic;     --! system clock input
      reset_i      : in  std_logic;     --! active high sync. reset
      strobe_i     : in  std_logic;  --! pulse synchronous to clk_sys_i to activate a shift in the phase (only captured rising edge, so a signal larger than a pulse is also fine)
      inc_ndec_i   : in  std_logic;  --! 1 increments phase by phase_step_i units, 0 decrements phase by phase_step_i units
      phase_step_i : in  std_logic_vector(3 downto 0);  --! number of units to shift the phase of the receiver clock (see Xilinx transceiver User Guide to convert units in time)     
      done_o       : out std_logic;  --! pulse synchronous to clk_sys_i to indicate a phase shift was performed
      phase_o      : out std_logic_vector(6 downto 0);  --! phase shift accumulated

      -- MGT interface                                      
      -- Receiver PI ports - see Xilinx transceiver User Guide for more information
      rxcdrhold_o   : out std_logic;    --! CDR hold       
      rxcdrovrden_o : out std_logic;    --! CDR override

      -- DRP interface - see Xilinx transceiver User Guide for more information
      -- obs1: connect clk_sys_i to drpclk      
      drpaddr_o : out std_logic_vector(8 downto 0);  --! For devices with a 10-bit DRP address interface, connect MSB to '0'
      drpen_o   : out std_logic;        --! DRP enable transaction
      drpdi_o   : out std_logic_vector(15 downto 0);  --! DRP data write
      drprdy_i  : in  std_logic;        --! DRP finished transaction
      drpdo_i   : in  std_logic_vector(15 downto 0);  --! DRP data read; not used nowadays, write only interface
      drpwe_o   : out std_logic         --! DRP write enable
      );
  end component rx_pi_ctrl;

  component rx_fifo_fill_level_acc is
    port (
      -- User Interface 
      clk_sys_i            : in  std_logic;  --! system clock input
      reset_i              : in  std_logic;  --! actived on rising edge sync. reset
      done_o               : out std_logic;  --! latched to '1' to indicate accumulated value was reached, cleared only with clear/reset
      phase_detector_o     : out std_logic_vector(31 downto 0);  --! phase detector accumulated output (increments for each pulse in which rxfifofilllevel is 1)
      phase_detector_max_i : in  std_logic_vector(31 downto 0);  --! phase detector accumulated max output, sets precision of phase detector
                                             --! this is supposedly a static signal, this block shall be reset whenever this signal changes
                                             --! the time for each phase detection after a clear is given by phase_detector_max_i * PERIOD_clk_rxusr_i
      -- MGT interface                                      
      -- Rx fifo fill level - see Xilinx transceiver User Guide for more information    
      clk_rxusr_i          : in  std_logic;  --! rxusr2clk
      rx_fifo_fill_level_i : in  std_logic   --! connect to rxbufstatus[0]
      );
  end component rx_fifo_fill_level_acc;

begin

  cmp_rx_phase_aligner_fsm : rx_phase_aligner_fsm
    generic map(
      g_SPEED_PD_FACTOR => c_SPEED_PD_FACTOR ,
      g_PI_COARSE_STEP  => c_PI_COARSE_STEP ,
      g_PI_FINE_STEP    => c_PI_FINE_STEP
      )
    port map(
      -- Clock / reset  
      clk_sys_i => clk_sys_i,
      reset_i   => reset_i ,

      -- Top level interface
      rx_aligned_o => rx_aligned ,

      -- Config (for different flavours)
      rx_pi_phase_calib_i             => rx_pi_phase_calib_i,
      rx_ui_align_calib_i             => rx_ui_align_calib_i,
      rx_enable_reset_i               => '0',  -- this special mode is not being used to simplify user integration
      rx_fifo_fill_pd_max_i           => rx_fifo_fill_pd_max_i,
      rx_fine_realign_i               => rx_fine_realign_i,
      debug_rx_skip_phase_alignment_i => debug_rx_skip_phase_alignment_i,
      -- Rx pi controller interface - see user interface rx_pi_ctrl.vhd for more information
      rx_pi_strobe_o                  => rx_aligner_rx_pi_strobe,
      rx_pi_inc_ndec_o                => rx_aligner_rx_pi_inc_ndec,
      rx_pi_phase_step_o              => rx_aligner_rx_pi_phase_step,
      rx_pi_done_i                    => rx_aligner_rx_pi_done,
      rx_pi_phase_i                   => rx_pi_phase,

      -- Rx fifo fill level phase detector interface - see user interface fifo_fill_level_acc.vhd for more information
      rx_fifo_fill_pd_clear_o => rx_fifo_fill_pd_clear,
      rx_fifo_fill_pd_done_i  => rx_fifo_fill_pd_done,
      rx_fifo_fill_pd_i       => rx_fifo_fill_pd,
      rx_fifo_fill_pd_max_o   => rx_fifo_fill_pd_max,

      -- Rx MGT reset (only used when rx_enable_reset_i is activated)
      rx_reset_o => open  -- this special mode is not being used to simplify user integration
      );

  rx_aligned_o <= rx_aligned;

  rx_pi_strobe          <= ps_strobe_i     when rx_aligned = '1' else rx_aligner_rx_pi_strobe;
  rx_pi_inc_ndec        <= ps_inc_ndec_i   when rx_aligned = '1' else rx_aligner_rx_pi_inc_ndec;
  rx_pi_phase_step      <= ps_phase_step_i when rx_aligned = '1' else rx_aligner_rx_pi_phase_step;
  ps_done_o             <= rx_pi_done      when rx_aligned = '1' else '0';
  rx_aligner_rx_pi_done <= rx_pi_done      when rx_aligned = '0' else '0';

  cmp_rx_pi_ctrl : rx_pi_ctrl
    generic map(
      g_DRP_ADDR_RXCDR_CFG3  => g_DRP_ADDR_RXCDR_CFG3,
      g_RATIO_RXFREQ_SYSFREQ => g_RATIO_RXFREQ_SYSFREQ
      )
    port map(
      -- User Interface 
      clk_sys_i    => clk_sys_i,
      reset_i      => reset_i ,
      strobe_i     => rx_pi_strobe,
      inc_ndec_i   => rx_pi_inc_ndec,
      phase_step_i => rx_pi_phase_step,
      done_o       => rx_pi_done,
      phase_o      => rx_pi_phase,

      -- MGT interface                                      
      -- Receiver PI ports - see Xilinx transceiver User Guide for more information
      rxcdrhold_o   => rxcdrhold_o,
      rxcdrovrden_o => rxcdrovrden_o,

      -- DRP interface - see Xilinx transceiver User Guide for more information
      -- obs1: connect clk_sys_i to drpclk      
      drpaddr_o => drpaddr_o,
      drpen_o   => drpen_o,
      drpdi_o   => drpdi_o,
      drprdy_i  => drprdy_i,
      drpdo_i   => drpdo_i,
      drpwe_o   => drpwe_o
      );

  rx_pi_phase_o <= rx_pi_phase;

  cmp_fifo_fill_level_acc : rx_fifo_fill_level_acc
    port map(
      -- User Interface 
      clk_sys_i            => clk_sys_i,
      reset_i              => reset_fifo_fill_level_acc,
      done_o               => rx_fifo_fill_pd_done,
      phase_detector_o     => rx_fifo_fill_pd,
      phase_detector_max_i => rx_fifo_fill_pd_max,

      -- MGT interface                                      
      -- Rx fifo fill level - see Xilinx transceiver User Guide for more information    
      clk_rxusr_i          => clk_rxusr_i,
      rx_fifo_fill_level_i => rx_fifo_fill_level_i
      );
  reset_fifo_fill_level_acc <= reset_i or rx_fifo_fill_pd_clear;

  rx_fifo_fill_pd_o <= rx_fifo_fill_pd;
  
end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================
