########### Common Constraints ###########
# Mailbox constraining
set_false_path -from [get_cells -compatibility_mode *cmp_olt_core|cmp_olt_control|*cmp_mailbox*|*data_a2b_togflag*] -to [get_cells -compatibility_mode *cmp_olt_core|cmp_olt_control|*cmp_mailbox*|*data_b_ready*]
set_false_path -from [get_cells -compatibility_mode *cmp_olt_core|cmp_olt_control|*cmp_mailbox*|*data_a_reg[*]] -to [get_cells -compatibility_mode *cmp_olt_core|cmp_olt_control|*cmp_mailbox*|*data_b_reg[*]]
set_false_path -to [get_cells -compatibility_mode *olt_core*|*avalon_readdata*]

# False paths related to synchronizers
set_false_path -to [get_cells -compatibility_mode *bit_synchronizer*inst|i_in_meta*]
set_false_path -to [get_cells -compatibility_mode *reset_synchronizer*inst|rst_in_*]



########### OLT constraints ###########
set_false_path -to [get_cells -compatibility_mode *cmp_olt_control|rx_ctrl_data_error_mngrsync*]
set_false_path -to [get_cells -compatibility_mode *cmp_olt_control|rx_ctrl_data_buf_mngrsync*]
set_false_path -to [get_cells -compatibility_mode *cmp_olt_control|*txsync_meta*]
set_false_path -to [get_cells -compatibility_mode *cmp_olt_control|*rxsync_meta*]
set_false_path -to [get_cells -compatibility_mode *cmp_olt_control|*syssync_meta*]
set_false_path -to [get_cells -compatibility_mode *cmp_olt_control|*mngrsync_meta*]
set_false_path -to [get_cells -compatibility_mode *cmp_olt_phy_control|*syssync_meta*]

# Heartbeat coming from mailbox1 is a pseudo-static signal and therefore its timing can be ignored for the 48-b phase acc.
set_false_path -from [get_cells -compatibility_mode *cmp_olt_control|cmp_mailbox1|data_b_reg[*]] -to [get_cells -compatibility_mode *cmp_olt_control|cmp_rx_fine_phase_measurement|onu_phase_acc[*]]

# MGT synchronizers for rx reset done
set_false_path -from [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|cmp_xcvr_reset_control_a10|*r_reset] -to [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|gt0_txfsmresetdone_r]
set_false_path -from [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|cmp_xcvr_reset_control_a10|*r_reset] -to [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|gt0_txfsmresetdone_r2]
set_false_path -from [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|cmp_xcvr_reset_control_a10|*r_reset] -to [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|gt0_rxfsmresetdone_r]
set_false_path -from [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|cmp_xcvr_reset_control_a10|*r_reset] -to [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|gt0_rxfsmresetdone_r2]

# MGT synchronizer for Tx to Rx CDC
set_false_path -to [get_cells -compatibility_mode *cmp_fifo_sync|flag_rd]
set_false_path -to [get_cells -compatibility_mode *cmp_fifo_sync|phase_good_locked_wr_meta]
set_false_path -from [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|*r_reset] -to [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|reset_fifo_meta]
set_false_path -from [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|*r_reset] -to [get_cells -compatibility_mode *cmp_olt_mgt_wrapper|reset_fifo_r]