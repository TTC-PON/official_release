# Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions 
# and other software and tools, and its AMPP partner logic 
# functions, and any output files from any of the foregoing 
# (including device programming or simulation files), and any 
# associated documentation or information are expressly subject 
# to the terms and conditions of the Altera Program License 
# Subscription Agreement, the Altera Quartus Prime License Agreement,
# the Altera MegaCore Function License Agreement, or other 
# applicable license agreement, including, without limitation, 
# that your use is for the sole purpose of programming logic 
# devices manufactured by Altera and sold by Altera or its 
# authorized distributors.  Please refer to the applicable 
# agreement for further details.

# Quartus Prime: Generate Tcl File for Project
# File: olt_arria10.tcl
# Generated on: Thu Dec 10 13:15:47 2020

# Load Quartus Prime Tcl Project package
package require ::quartus::project

load_package ::quartus::flow

set need_to_close_project 0
set make_assignments 1

# Check that the right project is open
if {[is_project_open]} {
	if {[string compare $quartus(project) "olt_arria10"]} {
		puts "Project olt_arria10 is not open"
		set make_assignments 0
	}
} else {
	# Only open if not already open
	if {[project_exists olt_arria10]} {
		project_open -revision olt_arria10 olt_arria10
	} else {
		project_new -revision olt_arria10 olt_arria10
	}
	set need_to_close_project 1
}

# Make assignments
if {$make_assignments} {
	set_global_assignment -name FAMILY "Arria 10"
	set_global_assignment -name DEVICE 10AX115S2F45I1SG
	set_global_assignment -name EDA_GENERATE_FUNCTIONAL_NETLIST ON -section_id eda_simulation
	set_global_assignment -name ORIGINAL_QUARTUS_VERSION 16.0.0
	set_global_assignment -name PROJECT_CREATION_TIME_DATE "13:36:44  OCTOBER 26, 2016"
	set_global_assignment -name LAST_QUARTUS_VERSION 16.0.0
	set_global_assignment -name TOP_LEVEL_ENTITY system_top		
	set_global_assignment -name PROJECT_OUTPUT_DIRECTORY output_files
	set_global_assignment -name MIN_CORE_JUNCTION_TEMP "-40"
	set_global_assignment -name MAX_CORE_JUNCTION_TEMP 100
	set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
	set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"
	set_global_assignment -name PROJECT_IP_REGENERATION_POLICY NEVER_REGENERATE_IP
	set_global_assignment -name SEARCH_PATH ../ip_repo/**/*
	set_global_assignment -name PROGRAMMABLE_POWER_TECHNOLOGY_SETTING AUTOMATIC
	set_global_assignment -name MLAB_ADD_TIMING_CONSTRAINTS_FOR_MIXED_PORT_FEED_THROUGH_MODE_SETTING_DONT_CARE OFF
	set_global_assignment -name ROUTER_TIMING_OPTIMIZATION_LEVEL MAXIMUM
	set_global_assignment -name SEED 1
	set_global_assignment -name IO_PLACEMENT_OPTIMIZATION ON
	set_global_assignment -name FITTER_EFFORT "STANDARD FIT"
	set_global_assignment -name SPECTRAQ_PHYSICAL_SYNTHESIS OFF
	set_global_assignment -name OPTIMIZE_HOLD_TIMING "ALL PATHS"
	set_global_assignment -name OPTIMIZE_MULTI_CORNER_TIMING ON
	set_global_assignment -name OPTIMIZE_SSN OFF
	set_global_assignment -name OPTIMIZE_TIMING "NORMAL COMPILATION"
	set_global_assignment -name ECO_REGENERATE_REPORT OFF
	set_global_assignment -name OPTIMIZE_IOC_REGISTER_PLACEMENT_FOR_TIMING NORMAL
	set_global_assignment -name FINAL_PLACEMENT_OPTIMIZATION AUTOMATICALLY
	set_global_assignment -name AUTO_DELAY_CHAINS ON
	set_global_assignment -name AUTO_GLOBAL_CLOCK ON
	set_global_assignment -name OPTIMIZE_FOR_METASTABILITY ON
	set_global_assignment -name ENABLE_BUS_HOLD_CIRCUITRY OFF
	set_global_assignment -name QSYS_FILE ip/olt_core/src/mgt_wrapper/olt_mgt_arria10/rst_ctrl_qsys/xcvr_reset_control_a10.qsys
	set_global_assignment -name BDF_FILE system_top.bdf
	set_global_assignment -name QSYS_FILE system.qsys
	set_global_assignment -name SDC_FILE system_timing.sdc
	set_global_assignment -name QSYS_FILE ip/olt_core/src/mgt_wrapper/olt_mgt_arria10/xcvr_qsys/xcvr_a10.qsys
	set_global_assignment -name AUTO_DELAY_CHAINS_FOR_HIGH_FANOUT_INPUT_PINS OFF
	set_global_assignment -name ROUTER_REGISTER_DUPLICATION AUTO
	set_global_assignment -name QSYS_FILE ip/olt_core/src/mgt_wrapper/olt_mgt_arria10/atxpll_qsys/atx_pll_a10_olt.qsys
	set_instance_assignment -name RESERVE_PIN AS_OUTPUT_DRIVING_GROUND -to user_reserve_0
	set_instance_assignment -name RESERVE_PIN AS_OUTPUT_DRIVING_GROUND -to user_reserve_1
	set_instance_assignment -name RESERVE_PIN AS_OUTPUT_DRIVING_GROUND -to user_reserve_2
	set_instance_assignment -name RESERVE_PIN AS_OUTPUT_DRIVING_GROUND -to user_reserve_3
	set_instance_assignment -name RESERVE_PIN AS_OUTPUT_DRIVING_GROUND -to user_reserve_4
	set_instance_assignment -name RESERVE_PIN AS_OUTPUT_DRIVING_GROUND -to user_reserve_5
	set_instance_assignment -name RESERVE_PIN AS_OUTPUT_DRIVING_GROUND -to user_reserve_6
	set_instance_assignment -name RESERVE_PIN AS_OUTPUT_DRIVING_GROUND -to user_reserve_7
	set_location_assignment PIN_L28 -to user_reserve_0
	set_location_assignment PIN_K26 -to user_reserve_1
	set_location_assignment PIN_K25 -to user_reserve_2
	set_location_assignment PIN_L25 -to user_reserve_3
	set_location_assignment PIN_J24 -to user_reserve_4
	set_location_assignment PIN_A19 -to user_reserve_5
	set_location_assignment PIN_C18 -to user_reserve_6
	set_location_assignment PIN_D18 -to user_reserve_7
	set_location_assignment PIN_F34 -to CLK_EMI
	set_instance_assignment -name IO_STANDARD LVDS -to CLK_EMI
	set_location_assignment PIN_F35 -to "CLK_EMI(n)"
	set_instance_assignment -name IO_STANDARD LVDS -to REFCLK1_P
	set_location_assignment PIN_AA3 -to SFP_RX
	set_instance_assignment -name IO_STANDARD "CURRENT MODE LOGIC (CML)" -to SFP_RX
	set_location_assignment PIN_AA4 -to "SFP_RX(n)"
	set_location_assignment PIN_AB1 -to SFP_TX
	set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to SFP_TX
	set_location_assignment PIN_AB2 -to "SFP_TX(n)"
	set_location_assignment PIN_B17 -to SFP_TX_DISABLE
	set_location_assignment PIN_BD27 -to CPU_RESETn
	set_location_assignment PIN_B15 -to SFP_MOD0_PRSNTn
	set_location_assignment PIN_A17 -to SFP_RS1
	set_location_assignment PIN_D16 -to SFP_RX_LOS
	set_location_assignment PIN_D17 -to SFP_TX_FAULT
	set_location_assignment PIN_W8 -to REFCLK1_P
	set_location_assignment PIN_W7 -to "REFCLK1_P(n)"
	set_location_assignment PIN_C15 -to SFP_RS0
	set_location_assignment PIN_G18 -to SFP_MOD1_SCL
	set_location_assignment PIN_F18 -to SFP_MOD2_SDA
	set_location_assignment PIN_H21 -to PLL_RST
	set_location_assignment PIN_G21 -to I2C_RST
	set_location_assignment PIN_AY16 -to FMCA_LA_TX_P14

	# Commit assignments
	export_assignments

    execute_flow -compile
    
	# Close project
	if {$need_to_close_project} {
		project_close
	}
}
