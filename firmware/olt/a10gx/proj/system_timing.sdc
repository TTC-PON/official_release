create_clock -name CLK_SYS -period 10.000 -waveform {0 5.0} [get_ports {CLK_EMI}]
create_clock -name MGT_REFCLK -period 4.166 -waveform {0 2.083} [get_ports {REFCLK1_P}]

create_clock -name {altera_rsv_tck} -period 30.303 [get_ports {altera_reserved_tck}]

derive_pll_clocks -create_base_clocks -use_net_name

derive_clock_uncertainty

set_clock_groups -asynchronous -group {altera_rsv_tck}



