--==============================================================================
-- © Copyright CERN for the benefit of the TTC-PON project. All rights not
--   expressly granted are reserved.
--
--   This file is part of ttc_pon.
--
-- ttc_pon is free VHDL code: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- ttc_pon is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with ttc_pon.  If not, see <https://www.gnu.org/licenses/>.
--=============================================================================
--! @file olt_mgt_wrapper.vhd
--=============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages

-------------------------------------------------------------------------------
-- --
-- CERN, EP-ESE-BE, TTC-PON
-- --
-------------------------------------------------------------------------------
--
-- unit name: OLT High Level MGT for simulation (olt_mgt_wrapper)
--
--! @brief OLT MultiGigabitTransceiver wrapper for simulation 
--
--! @author Eduardo Brandao de Souza Mendes - eduardo.brandao.de.souza.mendes@cern.ch
--! @date 05\09\2018
--! @version 1.0
--! @details
--!
--! <b>Dependencies:</b>\n
--! <Entity Name,...>
--!
--! <b>References:</b>\n
--! <reference one> \n
--! <reference two>
--!
--! <b>Modified by:</b>\n
--! Author: Eduardo Brandao de Souza Mendes
-------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 05\09\2018 - EBSM - Created\n
--! <extended description>
-------------------------------------------------------------------------------
--! @todo - \n
--! synchronize ready signals with clk_sys, better scheme for rx_data_o to ensure sync. \n
--
-------------------------------------------------------------------------------

--============================================================================
--! Entity declaration for olt_mgt_wrapper
--============================================================================
entity olt_mgt_wrapper is
  generic(
    g_UI_PERIOD : time := 0.100 ns  --! UI_PERIOD: this generic should be EXACTLY equal to the period of clk_ref_i/40
    );
  port (
    -- global input signals --  
    clk_sys_i   : in std_logic;         --! system clock
    clk_ref_i   : in std_logic;         --! reference clock
    mgt_reset_i : in std_logic;         --! reference clock
    -------------------------

    -- global output signals --         
    clk_trxusr_o : out std_logic;       --! tx user clock
    -------------------------

    -- status/control --
    -- Xilinx only
    drp_wr_i            : in  std_logic_vector(31 downto 0);  --! not implemented in high_level model
    drp_wr_strobe_i     : in  std_logic;  --! not implemented in high_level model
    drp_monitor_o       : out std_logic_vector(31 downto 0);  --! not implemented in high_level model
    mgt_rx_phase_ctrl_i : in  std_logic_vector(31 downto 0);  --! simple implementation in high_level model
    mgt_rx_phase_stat_o : out std_logic_vector(31 downto 0);  --! simple implementation in high_level model
    mgt_tx_phase_ctrl_i : in  std_logic_vector(31 downto 0);  --! not implemented in high_level model
    mgt_tx_phase_stat_o : out std_logic_vector(31 downto 0);  --! not implemented in high_level model

    txpolarity_i   : in  std_logic;     --! tx polarity (1=invert)
    rxpolarity_i   : in  std_logic;     --! rx polarity (1=invert)
    txfsmrstdone_o : out std_logic;     --! tx ready
    rxfsmrstdone_o : out std_logic;     --! rx ready
    pll_lock_o     : out std_logic;     --! pll lock
    -------------------------   

    -- data in/out --   
    tx_data_i : in  std_logic_vector(39 downto 0);  --! transmitter parallel data
    rx_data_o : out std_logic_vector(39 downto 0);  --! receiver parallel data

    rx_p_i : in  std_logic;             --! receiver high-speed serial data
    rx_n_i : in  std_logic;             --! not used for this high-level model
    tx_p_o : out std_logic;             --! transmitter high-speed serial data
    tx_n_o : out std_logic   --! tied to zero for this high-level model
    -------------------------   
    );
end olt_mgt_wrapper;

architecture high_level of olt_mgt_wrapper is

  signal tx_data_r : std_logic_vector(tx_data_i'range);

  signal ps_strobe_pipe : std_logic_vector(5 downto 0);

begin

  -- transmitter
  p_serializer : process
    variable v_sercount : integer range 0 to tx_data_i'left := 0;
  begin
    v_sercount     := 0;
    txfsmrstdone_o <= '0';
    wait until rising_edge(clk_ref_i);
    wait for 1*g_UI_PERIOD/2;
    while(mgt_reset_i = '0') loop
      tx_p_o <= tx_data_i(v_sercount) xor txpolarity_i;
      --wait for g_UI_PERIOD;
      if(v_sercount = tx_data_i'left) then
        v_sercount     := 0;
        txfsmrstdone_o <= '1';
      else
        v_sercount := v_sercount + 1;
      end if;
      wait for g_UI_PERIOD;
    end loop;
  end process p_serializer;

  tx_n_o       <= '0';
  clk_trxusr_o <= clk_ref_i;

  -- receiver
  p_deserializer : process
    variable v_desercount : integer range 0 to rx_data_o'left := 0;
    variable v_deser_reg  : std_logic_vector(rx_data_o'range);
  begin
    v_desercount   := 0;
    rxfsmrstdone_o <= '0';
    wait for 1*g_UI_PERIOD/2;
    while(mgt_reset_i = '0') loop
      v_deser_reg(v_desercount) := rx_p_i xor rxpolarity_i;
      wait for g_UI_PERIOD;
      if(v_desercount = rx_data_o'left) then
        v_desercount   := 0;
        rx_data_o      <= v_deser_reg;
        rxfsmrstdone_o <= '1';
      elsif(v_desercount = (rx_data_o'left+1)/2 - 1) then
        v_desercount := v_desercount + 1;
      else
        v_desercount := v_desercount + 1;
      end if;
    end loop;
  end process p_deserializer;

  -- common
  pll_lock_o    <= not mgt_reset_i;
  drp_monitor_o <= (others => '0');

  -- simple impl. for phase shift testing (no actual phase shifting is performed)
  ps_strobe_pipe                   <= ps_strobe_pipe(ps_strobe_pipe'left-1 downto 0) & mgt_rx_phase_ctrl_i(0)           when rising_edge(clk_sys_i);
  mgt_rx_phase_stat_o(0)           <= ps_strobe_pipe(ps_strobe_pipe'left-1) and not ps_strobe_pipe(ps_strobe_pipe'left) when rising_edge(clk_sys_i);
  mgt_rx_phase_stat_o(31 downto 1) <= (others => '0');
  mgt_tx_phase_stat_o(31 downto 0) <= (others => '0');

end high_level;
