#!/usr/bin/env python

import csv
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import math

# This script plots Statistical Eye diagrams for Rx Margin Analysis

# Those parameters are not intended to be changed by the user
# The eye scan functionality is provided here as a mere example of DRP interface communication
# If a user wishes to implement his/her own eye scan, they are welcome

# Vertical axis configuration
ystep=2.8       # mV/div
y_step_div=-4
y_init = 127
y_end  = -125
yaxis_min = y_init*ystep
yaxis_max = y_end*ystep
number_y =int((y_end-y_init)/y_step_div) + 1

# Horizontal axis configuration
xstep = 1.0/64.0 #1/64 of the UI
x_step_div = 1
x_init=-32
x_end=31
xaxis_min = x_init*xstep
xaxis_max = x_end*xstep
number_x =int((x_end-x_init)/x_step_div) + 1

def plot_eye_scan(eyescan_file):
    """
        ***************************************************************************
        Plot eye scan for KintexUltrascale-GTH configured as ONU
        ***************************************************************************

        ***************************************************************************
        arg:
            eyescan_file : File to save the eye scan
        ***************************************************************************
    """
    # Matrix to plot
    ber_matrix = []
    for i in range(0,number_y):
        ber_matrix.append([])
        for j in range(0,number_x):
            ber_matrix[-1].append(0)
    	
    # Read data curve
    j=0
    with open(eyescan_file + '.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            for i in range(0, number_x):
                ber_matrix[j][i] = np.log10(float(row['%20s' % ('hor' + str(i))]))
            j=j+1
    
    Z=ber_matrix
    X,Y=np.meshgrid(np.linspace(xaxis_min,xaxis_max,number_x),np.linspace(yaxis_min,yaxis_max,number_y))
    
    #im = plt.pcolormesh(X,Y,Z, cmap='RdYlGn_r', shading='flat')
    im = plt.pcolormesh(X,Y,Z, cmap='jet', shading='flat')
    plt.colorbar(im, orientation='vertical', label='log10(BER)')
    plt.xlabel('X offset - UI')
    plt.ylabel('Y offset - mV')
    plt.axis([xaxis_min, xaxis_max, yaxis_min, yaxis_max])
    plt.title('ONU Rx - Eye Sweep')
    plt.show()