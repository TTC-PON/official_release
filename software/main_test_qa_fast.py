#!/usr/bin/env python

####################################################
###          Python Native Packages              ###
####################################################
import logging
import time
import os
import visa
import shutil
import matplotlib.pyplot as plt

####################################################
###               System Sound Beep              ###
####################################################
import winsound
import time

####################################################
###              PON Exdsg cores                 ###
####################################################
from ttcpon_core.ttcpon_exdsg import (OltExdsg, OnuExdsg)

####################################################
###            PON-ONU Eye diagrams              ###
####################################################
import ttcpon_core.pon_ku_gth_eyescan as pon_ku_gth_eyescan

####################################################
###             PON Monitor Plot                 ###
####################################################

####################################################
###             PON Board Config                 ###
####################################################
import ttcpon_board_config.pon_fmc_i2c_program as program_fmc
import ttcpon_board_config.init_onu_fmc as init_onu_fmc
import ttcpon_board_config.pon_a10gx_i2c_program as program_a10gx

####################################################
###				PON lab. equipment				 ###
####################################################
from lab_equipment.opt_att_MAP200 import OptAtt
from lab_equipment.pwr_met_MAP200 import PwrMet
from lab_equipment.scope_DSA91204A import Scope

####################################################
###				Auxiliar plots   				 ###
####################################################
from ttcpon_qa_plot.plot_eyescan_scope   import plot_eye_scan_uW
from ttcpon_qa_plot.gen_rx_pwr_table     import gen_rx_pwr
from ttcpon_qa_plot.plot_bert_downstream import plot_bert_downstream
from ttcpon_qa_plot.plot_bert_upstream   import plot_bert_upstream
import ttcpon_qa_plot.power_calibration  as power_calibration

####################################################
###					 PON Tests					 ###
####################################################
import tests_ttcpon

def main(olt, onu, optical_attenuator1, power_meter1, power_meter2):

    ####################################################
    ###          Parameters for Application          ###
    ####################################################
    #------------- Integrated system test -------------	
    INIT_OLT                        = 0  # possible values: 1 or 0		
    PERFORM_FULL_CALIBRATION        = 1  # possible values: 1 or 0	
    RESET_SYSTEM_BEFORE_CALIBRATION = 1  # possible values: 1 or 0		
    WAIT_TIME_AFTER_CALIBRATION     = 10 # unit (s)
    PERFORM_OLT_MONITORING          = 1  # possible values: 1 or 0
    SAMPLE_OLT_MONITORING           = 60*3 
    INIT_ONU                        = 0  # possible values: 1 or 0	
    #--------------------------------------------------
	
    #------------ Laboratory system test --------------	
    MEASURE_POWER                   = 1 # possible values: 1 or 0
    PERFORM_EYE_SCOPE_OLT           = 0 # possible values: 1 or 0
    PERFORM_BERT                    = 1 # possible values: 1 or 0
    #--------------------------------------------------
	
    TEST_DIR_ROOT                   = './results/'

    ####################################################
    ###        Start TTC-PON Ex. Application         ###
    ####################################################
    logger_pon.info('*********************************************************')	
    logger_pon.info('******************** Integrated test ********************')	
    logger_pon.info('*********************************************************')

    ####################################################
    ###            Create test directory             ###
    ####################################################
    # Read OLT ID    
    module_vendor_olt           = (olt.get_sfp_vendor()[1]).split(' ')[0]
    module_part_number_olt      = (olt.get_sfp_part_number()[1]).split(' ')[0]
    module_serial_number_olt    = (olt.get_sfp_serial_number()[1]).split(' ')[0]
    PATH_TEST = TEST_DIR_ROOT + module_serial_number_olt
	
    # Read ONU ID    
    module_vendor_onu        = ['']*len(onu) 
    module_part_number_onu   = ['']*len(onu) 
    module_serial_number_onu = ['']*len(onu) 
    for i in range(0,len(onu)):	
        module_vendor_onu[i]        = (onu[i].get_sfp_vendor()[1]).split(' ')[0]
        module_part_number_onu[i]   = (onu[i].get_sfp_part_number()[1]).split(' ')[0]
        module_serial_number_onu[i] = (onu[i].get_sfp_serial_number()[1]).split(' ')[0]
        separator = '_' if i==0 else '-' 
        PATH_TEST = PATH_TEST + separator + module_serial_number_onu[i]

    # Check if test directory already exists

    if not os.path.exists(PATH_TEST):
        os.makedirs(PATH_TEST) # Root test
        logger_pon.info('Created test directory: ' + PATH_TEST)    
    else:
        data_user_valid=0
        while (not data_user_valid):    
            data_user = input('Directory with these serial numbers already exists! (0=cancel test / 1=replace) >> ')
            # User cancel test 
            if(data_user=='0'):
                data_user_valid=1        
                logger_pon.warn('Test cancelled')
                return
            # Replace directory    
            elif(data_user=='1'):
                data_user_valid=1
                logger_pon.warn('Directory will be replaced')            
                # replace directory code
                shutil.rmtree(PATH_TEST)
                os.makedirs(PATH_TEST) # Root test
                logger_pon.info('Created test directory: ' + PATH_TEST)
            # Data provided by user is invalid
            else:
                logger_pon.warn('Invalid value')

    os.makedirs(PATH_TEST + '/integrated_test')
    os.makedirs(PATH_TEST + '/img')

	####################################################
	###			  Initialization Devices			 ###
	####################################################
    logger_pon.info('---------------------------------------------------------')		
    logger_pon.info('---------------- Initializing devices -------------------')
    logger_pon.info('---------------------------------------------------------')		
    #### -> Optical attenuator 1
    optical_attenuator1.set_attenuation(0.0)
    optical_attenuator1.set_wavelength(1577)
    optical_attenuator1.set_initial_config()
    time.sleep(1)
    logger_pon.info(' Initialization done for optical attenuator 1')		
    #### -> Power meter 1
    power_meter1.set_wavelength(1577)
    time.sleep(1)
    logger_pon.info(' Initialization done for power meter 1')			
    #### -> Power meter 2
    power_meter2.set_wavelength(1270)
    time.sleep(1)
    logger_pon.info(' Initialization done for power meter 2')	

    ####################################################
    ###          Initialization of OLT               ###
    ####################################################
    # Program OLT
    if(INIT_OLT):	
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('------------------- PROGRAM OLT-FMC --------------------')	
        program_fmc.program_clkmux(olt, 0xFF)
        program_fmc.program_si570(olt, 1)
        olt.exd_core_reset()
        time.sleep(2)
        program_fmc.program_leds(olt, 0x00)		
        program_fmc.monitor_sfp(olt,1)
        logger_pon.info('                                                         ')
        time.sleep(1)
	
    ### INIT - ONU_1 - Mounted on FMC - KCU105 board (Kintex Ultrascale)
    if(INIT_ONU):		
        logger_pon.info('---------------------------------------------------------')		
        logger_pon.info('------------------- PROGRAM ONU1-FMC --------------------')
        logger_pon.info('---------------------------------------------------------')	
        init_onu_fmc.onu_init(onu[0], 1)
        logger_pon.info('                                                         ')		
        time.sleep(1)

    ### INIT - ONU_2 - Mounted on on-board SFP connector - A10GX Board  (Arria10)
    if(INIT_ONU):	
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('------------------- PROGRAM ONU2-A10GX ------------------')
        onu[1].exd_set_addr(2)	
        program_a10gx.monitor_sfp(onu[1], 1)
        logger_pon.info('                                                         ')		
        time.sleep(1)

	####################################################
    ###                 Calibration                  ###
	####################################################
    if(PERFORM_FULL_CALIBRATION):
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('----------------- Calibration upstream ------------------')	
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('Initializing System Upstream Calibration')
        onu_list = olt.full_calibration_run([1,2])
        olt.save_onu_config_csv(onu_list,PATH_TEST + '/integrated_test/onu_config')
        olt.save_olt_config_csv(PATH_TEST + '/integrated_test/olt_config')
	
        # Wait time after calibration
        logger_pon.info('Waiting ' + str(WAIT_TIME_AFTER_CALIBRATION) + 's before system monitoring...')	
        t0 = time.time()
        deltaT = 0	
        while(deltaT<=WAIT_TIME_AFTER_CALIBRATION):
            deltaT = time.time() - t0
	
	####################################################
    ###        Start OLT monitoring application      ###
	####################################################
    if(PERFORM_OLT_MONITORING):
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('------------------ System Monitoring --------------------')	
        logger_pon.info('---------------------------------------------------------')
        olt.clear_olt_monitor_status()
        for onu_idx in range(0,2):
            olt.clear_onu_monitor_status(onu_idx+1)

        for idx_mon in range(0, SAMPLE_OLT_MONITORING):
            olt_list = olt.full_olt_monitor_run()
            olt.save_olt_monitor_csv(olt_list, PATH_TEST + '/integrated_test/olt_monitor')

            for onu_idx in range(0,2):
                onu_dict = olt.full_onu_monitor_run(onu_idx+1)
                olt.save_onu_monitor_csv(onu_dict, PATH_TEST + '/integrated_test/onu' + str(onu_idx+1) + '_monitor')
            if(idx_mon%50 == 0) : logger_pon.info('Current sample:' + str(idx_mon))

        ####################################################
        ###                 Check data                   ###
        ####################################################
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('-------------------- Checking data ----------------------')	
        logger_pon.info('---------------------------------------------------------')		
        # Read data
        olt_list = olt.read_olt_monitor_csv(PATH_TEST + '/integrated_test/olt_monitor')
        
        onu_dict = []
        for onu_idx in range(0,2):
            onu_dict.append(olt.read_onu_monitor_csv(PATH_TEST + '/integrated_test/onu' + str(onu_idx+1) + '_monitor'))
        
        # Process data for OLT
        olt_sfp_temp          = []   
        olt_sfp_vcc           = []
        olt_sfp_tx_bias       = []
        olt_sfp_tx_pwr        = []
        olt_sfp_rx_pwr        = []
        olt_stk_mgt_tx_nready = []
        olt_stk_mgt_rx_nready = []
        olt_stk_mgt_pll_nlock = []
        olt_stk_missing_burst = []
        olt_stk_rx_nlock      = []
        
        for olt_mon in olt_list:
            olt_sfp_temp.append(olt_mon['sfp_temp'])
            olt_sfp_vcc.append(olt_mon['sfp_vcc'])
            olt_sfp_tx_bias.append(olt_mon['sfp_tx_bias'])
            olt_sfp_tx_pwr.append(olt_mon['sfp_tx_pwr'])
            olt_sfp_rx_pwr.append(olt_mon['sfp_rx_pwr'])
            olt_stk_mgt_tx_nready.append(olt_mon['stk_mgt_tx_nready'])
            olt_stk_mgt_rx_nready.append(olt_mon['stk_mgt_rx_nready'])
            olt_stk_mgt_pll_nlock.append(olt_mon['stk_mgt_pll_nlock'])
            olt_stk_missing_burst.append(olt_mon['stk_missing_burst'])
            olt_stk_rx_nlock.append(olt_mon['stk_rx_nlock'])
        
        # Save plot of OLT digital diagnostic monitor
        plt.figure(figsize=(20,10))
        plt.subplot(2,3,1)
        plt.plot(olt_sfp_temp, 'ro')
        plt.ylabel('Temp(degC)')
	    
        plt.subplot(2,3,2)
        plt.plot(olt_sfp_vcc, 'ro')
        plt.ylabel('Vcc(V)')
        
        plt.subplot(2,3,3)
        plt.plot(olt_sfp_tx_bias, 'ro')
        plt.ylabel('Tx Bias(mA)')
	    
        plt.subplot(2,3,4)
        plt.plot(olt_sfp_tx_pwr, 'ro')
        plt.ylabel('Tx Pwr(dBm)')
        
        plt.subplot(2,3,5)
        plt.plot(olt_sfp_rx_pwr, 'ro')
        plt.ylabel('Rx Pwr(dBm)')
        
        plt.savefig(PATH_TEST + '/img/olt_sfp_monitor.png')
        
        all_ok=1
        for i in range(0,len(olt_list)):
            if(olt_stk_mgt_tx_nready[i]!=0):
                logger_pon.warn('Problem found in olt_stk_mgt_tx_nready')
                all_ok=0
            if(olt_stk_mgt_rx_nready[i]!=0):
                logger_pon.warn('Problem found in olt_stk_mgt_rx_nready')
                all_ok=0
            if(olt_stk_mgt_pll_nlock[i]!=0):
                logger_pon.warn('Problem found in olt_stk_mgt_pll_nlock')
                all_ok=0
            if(olt_stk_missing_burst[i]!=0):
                logger_pon.warn('Problem found in olt_stk_missing_burst')
                all_ok=0
            if(olt_stk_rx_nlock[i]!=0):
                logger_pon.warn('Problem found in olt_stk_rx_nlock')
                all_ok=0
        if(all_ok):
            logger_pon.info('TEST IS OK FOR OLT')
        
        # Process data for ONUs
        for onu_idx in range(0,2):
            onu_sfp_temp             = []   
            onu_sfp_vcc              = []
            onu_sfp_tx_bias          = []
            onu_sfp_tx_pwr           = []
            onu_sfp_rx_pwr           = []
            onu_stk_mgt_tx_nready    = []
            onu_stk_mgt_rx_nready    = []
            onu_stk_mgt_txpll_nlock  = []
            onu_stk_mgt_rxpll_nlock  = []
            onu_stk_rx_nlock         = []     
            onu_stk_phase_ngood      = []
            onu_stk_dn_fec_serr_corr = []
            onu_stk_dn_fec_derr_corr = []
            onu_stk_dn_sc_crc_errdet = []
            onu_dn_fec_serr_corr_cnt = []
            onu_dn_fec_derr_corr_cnt = []
            onu_dn_sc_crc_errdet_cnt = [] 
            onu_stk_up_8b10b_errdet  = [] 
	        
            for onu_mon in onu_dict[onu_idx]:
                onu_sfp_temp.append(onu_mon['sfp_temp'])
                onu_sfp_vcc.append(onu_mon['sfp_vcc'])
                onu_sfp_tx_bias.append(onu_mon['sfp_tx_bias'])
                onu_sfp_tx_pwr.append(onu_mon['sfp_tx_pwr'])
                onu_sfp_rx_pwr.append(onu_mon['sfp_rx_pwr'])
                onu_stk_mgt_tx_nready.append(onu_mon['stk_mgt_tx_nready'])
                onu_stk_mgt_rx_nready.append(onu_mon['stk_mgt_rx_nready'])
                onu_stk_mgt_txpll_nlock.append(onu_mon['stk_mgt_txpll_nlock'])
                onu_stk_mgt_rxpll_nlock.append(onu_mon['stk_mgt_rxpll_nlock'])
                onu_stk_rx_nlock.append(onu_mon['stk_rx_nlock'])     
                onu_stk_phase_ngood.append(onu_mon['stk_phase_ngood'])
                onu_stk_dn_fec_serr_corr.append(onu_mon['stk_dn_fec_serr_corr'])
                onu_stk_dn_fec_derr_corr.append(onu_mon['stk_dn_fec_derr_corr'])
                onu_stk_dn_sc_crc_errdet.append(onu_mon['stk_dn_sc_crc_errdet'])
                onu_dn_fec_serr_corr_cnt.append(onu_mon['dn_fec_serr_corr_cnt'])
                onu_dn_fec_derr_corr_cnt.append(onu_mon['dn_fec_derr_corr_cnt'])
                onu_dn_sc_crc_errdet_cnt.append(onu_mon['dn_sc_crc_errdet_cnt']) 
                onu_stk_up_8b10b_errdet.append(onu_mon['stk_up_8b10b_errdet']) 
        
            # Save plot of SFP digital diagnostic monitor
            plt.figure(figsize=(20,10))
            plt.subplot(2,3,1)
            plt.plot(onu_sfp_temp, 'ro')
            plt.ylabel('Temp(degC)')
	        
            plt.subplot(2,3,2)
            plt.plot(onu_sfp_vcc, 'ro')
            plt.ylabel('Vcc(V)')
	        
            plt.subplot(2,3,3)
            plt.plot(onu_sfp_tx_bias, 'ro')
            plt.ylabel('Tx Bias(mA)')
	        
            plt.subplot(2,3,4)
            plt.plot(onu_sfp_tx_pwr, 'ro')
            plt.ylabel('Tx Pwr(dBm)')
	        
            plt.subplot(2,3,5)
            plt.plot(onu_sfp_rx_pwr, 'ro')
            plt.ylabel('Rx Pwr(dBm)')
	        
            plt.savefig(PATH_TEST + '/img/onu'+str(onu_idx+1)+'_sfp_monitor.png')
	        
            all_ok=1
            for i in range(0,len(onu_dict[onu_idx])):
                if(onu_stk_mgt_tx_nready[i]!=0):
                    logger_pon.warn('Problem found in onu_stk_mgt_tx_nready')
                    all_ok=0
                if(onu_stk_mgt_rx_nready[i]!=0):
                    logger_pon.warn('Problem found in onu_stk_mgt_rx_nready')
                    all_ok=0
                if(onu_stk_mgt_txpll_nlock[i]!=0):
                    logger_pon.warn('Problem found in onu_stk_mgt_txpll_nlock')
                    all_ok=0
                if(onu_stk_mgt_rxpll_nlock[i]!=0):
                    logger_pon.warn('Problem found in onu_stk_mgt_rxpll_nlock')
                    all_ok=0
                if(onu_stk_rx_nlock[i]!=0):
                    logger_pon.warn('Problem found in onu_stk_rx_nlock')
                    all_ok=0
                if(onu_stk_phase_ngood[i]!=0):
                    logger_pon.warn('Problem found in onu_stk_phase_ngood')
                    all_ok=0
                if(onu_stk_dn_fec_serr_corr[i]!=0):
                    logger_pon.warn('Problem found in onu_stk_dn_fec_serr_corr')
                    all_ok=0
                if(onu_stk_dn_fec_derr_corr[i]!=0):
                    logger_pon.warn('Problem found in onu_stk_dn_fec_derr_corr')
                    all_ok=0
                if(onu_stk_dn_sc_crc_errdet[i]!=0):
                    logger_pon.warn('Problem found in onu_stk_dn_sc_crc_errdet')
                    all_ok=0
                if(onu_dn_fec_serr_corr_cnt[i]!=0):
                    logger_pon.warn('Problem found in onu_dn_fec_serr_corr_cnt')
                    all_ok=0
                if(onu_dn_fec_derr_corr_cnt[i]!=0):
                    logger_pon.warn('Problem found in onu_dn_fec_derr_corr_cnt')
                    all_ok=0
                if(onu_dn_sc_crc_errdet_cnt[i]!=0):
                    logger_pon.warn('Problem found in onu_dn_sc_crc_errdet_cnt')
                    all_ok=0
                if(onu_stk_up_8b10b_errdet[i]!=0):
                    logger_pon.warn('Problem found in onu_stk_up_8b10b_errdet')
                    all_ok=0
        
            if(all_ok):
                logger_pon.info('TEST IS OK FOR ONU ' + str(onu_idx+1))

    logger_pon.info('*********************************************************')	
    logger_pon.info('******************** Laboratory test ********************')	
    logger_pon.info('*********************************************************')
    os.makedirs(PATH_TEST + '/laboratory_test')

    if MEASURE_POWER:
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('-------------------- Measure Power ----------------------')	
        logger_pon.info('---------------------------------------------------------')	
        
        # Downstream
        rx_pwr_down = float(power_meter1.get_power())
        power_calibration.save_down_pwr_csv(0.0, rx_pwr_down, PATH_TEST + '/laboratory_test/rx_pwr_down')
        gen_rx_pwr('./results/setup_calibration', PATH_TEST + '/laboratory_test/rx_pwr_down', 1,  PATH_TEST + '/laboratory_test/rx_pwr_down_table')
        logger_pon.info('Measured downstream power')
		
        # Upstream
        rx_pwr_up = []
        for onu_addr in range(1, len(onu)+1):
        	# Disable all ONUs, enable one in continuous mode
        	olt.onu_mode_disable(255)
        	olt.onu_mode_calib(onu_addr)
        	time.sleep(1.0)
        	rx_pwr_up.append(float(power_meter2.get_power()))	
        power_calibration.save_up_pwr_csv(0.0, rx_pwr_up, PATH_TEST + '/laboratory_test/rx_pwr_up')
        gen_rx_pwr('./results/setup_calibration', PATH_TEST + '/laboratory_test/rx_pwr_up', 0,  PATH_TEST + '/laboratory_test/rx_pwr_up_table')
        logger_pon.info('Measured upstream power')

    #if PERFORM_EYE_SCOPE_OLT:
        #logger_pon.info('---------------------------------------------------------')	
        #logger_pon.info('------------------ OLT Tx eye diagram -------------------')	
        #logger_pon.info('---------------------------------------------------------')		
        #data_rate = 9.6e9
        #scope.send('*RST')
        #scope.func_inv(1,1,3)
        #scope.func_lpf(3,0,1, data_rate*0.75)
        #scope.eye_meas(3,0,data_rate, 0, 1, 1, PATH_TEST + '/laboratory_test/olt_tx_eye',1)	
        #plot_eye_scan_uW(1e3*10**(rx_pwr_down/10), PATH_TEST + '/laboratory_test/olt_tx_eye_data',1,1, 9.6e9,PATH_TEST + '/img/olt_tx_eye',1)
        #logger_pon.info(' Done eye diagram for OLT Tx')		

    if PERFORM_BERT:	
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('--------------- Bidirectional BER test ------------------')	
        logger_pon.info('---------------------------------------------------------')			
        # Test config
        num_curves       = 1
        att1_start       = 10.0
        att1_step        = 2.0
        att1_stop        = 22.0
        target_ber       = 5e-11
        confidence_level = 0
        finish_early     = 0		
        limit_up_ndown   = 0
        
        # Load configs from calibration
        olt.load_olt_config_csv(PATH_TEST+'/integrated_test/olt_config')
        onu_config = olt.read_onu_config_csv(PATH_TEST+'/integrated_test/onu_config')
        
        # Execute test
        optical_attenuator1.set_attenuation(0.0)		
        time.sleep(2)
        save_file = PATH_TEST + '/laboratory_test/bidir_bert'			
        tests_ttcpon.bidirectional_bert(olt, onu, onu_config, optical_attenuator1, num_curves, att1_start, att1_step, att1_stop, target_ber, confidence_level, finish_early, limit_up_ndown, save_file)
        optical_attenuator1.set_attenuation(0.0)
        olt.network_init(onu_config)

        # Plot tests
        plot_bert_downstream(PATH_TEST + '/laboratory_test/rx_pwr_down_table','./results/setup_calibration/extinction_ratio_dn', PATH_TEST + '/laboratory_test/bidir_bert_down',1, PATH_TEST + '/img/bidir_bert_down')
        plot_bert_upstream(PATH_TEST + '/laboratory_test/rx_pwr_up_table','./results/setup_calibration/extinction_ratio_up', PATH_TEST + '/laboratory_test/bidir_bert_up',1, PATH_TEST + '/img/bidir_bert_up')


if __name__ == '__main__':
    # Logger TTC-PON CORE definition:
    # create logger with 'ttc_pon' application
    logger_pon = logging.getLogger('ttc_pon')
    logger_pon.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('./logger/ttcpon_log.log')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s: %(message)s',
        datefmt='%d-%m-%y %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger_pon.addHandler(fh)
    logger_pon.addHandler(ch)

    # Logger TTC-PON EXDSG definition:
    # create logger with 'ttc_pon' application
    logger_exd_pon = logging.getLogger('ttc_pon_tests')
    logger_exd_pon.setLevel(logging.DEBUG)
    # add the handlers to the logger
    logger_exd_pon.addHandler(fh)
    logger_exd_pon.addHandler(ch)

    # Open sockets
    olt = OltExdsg('127.0.0.1', 8555)
    onu = [OnuExdsg('127.0.0.1', 8556), OnuExdsg('127.0.0.1', 2540)]

    #rm_visa0 = visa.ResourceManager()
    #scope = Scope(rm_visa0, 'USB0::0x0957::0x9002::MY48240177::0::INSTR')

    rm_visa1 = visa.ResourceManager()
    optical_attenuator1 = OptAtt(rm_visa1, 'TCPIP0::192.168.1.200::inst2::INSTR', 1)
	
    rm_visa2 = visa.ResourceManager()
    power_meter1 = PwrMet(rm_visa2, 'TCPIP0::192.168.1.200::inst3::INSTR', 1)
	
    rm_visa3 = visa.ResourceManager()
    power_meter2 = PwrMet(rm_visa3, 'TCPIP0::192.168.1.200::inst3::INSTR', 2)
	
    main(olt, onu, optical_attenuator1, power_meter1, power_meter2)
    #del scope
    del optical_attenuator1
    del power_meter1
    del power_meter2
	
# Warn the user the script is finished: (frequency, time)
winsound.Beep(494, 300)
winsound.Beep(494, 300)
winsound.Beep(523, 300)
winsound.Beep(587, 300)
winsound.Beep(587, 300)
winsound.Beep(523, 300)
winsound.Beep(494, 300)
winsound.Beep(440, 300)
winsound.Beep(392, 300)
winsound.Beep(392, 300)
winsound.Beep(440, 300)
winsound.Beep(494, 300)
winsound.Beep(494, 450)
winsound.Beep(440, 150)
winsound.Beep(440, 300)
