#!/usr/bin/env python

import os
import socket
import logging
import datetime
import time
import csv
import math
from random import randint

from . driver_comm import DriverComm

# -------------------------------------------------------------
#  ---------------------- Class OnuCore ----------------------
# -------------------------------------------------------------

class OnuCore(DriverComm):
    """
        This class is responsible for controlling the ONU_CORE in the TTC-PON Firmware (obs: ONU can also be controlled via OLT using the TTC-PON Slow Control interface)
        The parameters inside this class are part of the TTC-PON protocol and might be firmware-dependent. It is strongly recommended to NOT change them.

        ***************************************************************************************************************
        ***   It inherits from the class DriverComm (driver-layer) in order to communicate with the board           ***   
        ***   For custom boards, the class DriverComm must be replaced (see help for more information)              ***   
        ***************************************************************************************************************   
    """

    def __init__(self, ip='127.0.0.1', port=8556):
        """
            ***************************************************************************
            Class Constructor
            ***************************************************************************

            ***************************************************************************
            arg:
                ip: string with the IP address of the board being controlled.
                    The board IP in the TTC-PON example design can be changed via the software implemented in the microblaze, i.e. by using the Vivado SDK
                port: integer with the port number
            ***************************************************************************
        """		
        DriverComm.__init__(self, ip, port)

        # Phase aligner parameters
        self.c_MAX_TX_PHASE_ALIGNER_FIFO_FILL_PD = 32

    # ***************************************************************************
    # ***********************  I2C CONTROL METHODS  *****************************
    # ***************************************************************************

    def i2c_write(self, slv_addr, reg_addr, reg_value, wait_done=1):
        """
            Writes via IIC interface to a memory mapped slave

            arg:
                slv_addr : IIC slave address (7-bit)
                reg_addr : register address to be written
                reg_value: register value
            return:
                -1=error formatting, 0=IIC write error or IIC drop request error, 1=success
        """
        slv_addr_format  = slv_addr & 0x7F
        reg_addr_format  = reg_addr & 0xFF
        reg_value_format = reg_value & 0xFF
        if((slv_addr != slv_addr_format) | (reg_addr != reg_addr_format) | (reg_value != reg_value_format)):
            self.logger.warn('Error parameters formatting for IIC write command')		
            return -1
        else:
            message =(0<<31)+(reg_value<<18) + (reg_addr<<10) + (slv_addr<<3) + (0<<1) + (0<<1) + (1)			
            self.write_ctrl_reg(13, message)
            message =(1<<31)+(reg_value<<18) + (reg_addr<<10) + (slv_addr<<3) + (0<<1) + (0<<1) + (1)
            self.write_ctrl_reg(13, message)

        if(wait_done):
            i2c_done = 0
            i2c_error = 0
            i2c_drop_req = 0
            while(i2c_done==0 and i2c_error==0 and i2c_drop_req==0):
                i2c_stat     = self.read_ctrl_reg(14)
                i2c_done     = (i2c_stat & 0x00000001) >> 0
                i2c_error    = (i2c_stat & 0x00000002) >> 1
                i2c_drop_req = (i2c_stat & 0x00000004) >> 2
            
            if(i2c_error):
                self.logger.warn('IIC write error')				
                return 0
            elif(i2c_drop_req):
                self.logger.warn('IIC drop request error')				
                return 0			
            else:
                return 1
        else:
            return 1

    def i2c_read(self, slv_addr, reg_addr, nb_read_access=1):
        """
            Read via IIC interface from a memory mapped slave

            arg:
                slv_addr : IIC slave address (7-bit)
                reg_addr : register address to be written
                nb_read_access: can be 1 or 2 for double bursty read (required by SFF-8472 standard)
            return:
                [-1] if it fails
                [data0, data1] read from interface
        """	
        slv_addr_format  = slv_addr & 0x7F;
        reg_addr_format  = reg_addr & 0xFF;
        nb_read_access   = nb_read_access - 1
        nb_read_access_format = nb_read_access & 0x1

        if((slv_addr != slv_addr_format) | (reg_addr != reg_addr_format) | (nb_read_access_format != nb_read_access)):
            self.logger.warn('Error parameters formatting for IIC read command')		
            return [-1]
        else:
            message = (0<<31)+(0<<18) + (reg_addr<<10) + (slv_addr<<3) + (1<<2) + (nb_read_access<<1) + (1)	
            self.write_ctrl_reg(13, message)		
            message = (1<<31)+(0<<18) + (reg_addr<<10) + (slv_addr<<3) + (1<<2) + (nb_read_access<<1) + (1)	
            self.write_ctrl_reg(13, message)

        i2c_done = 0
        i2c_error = 0
        i2c_drop_req = 0
        while(i2c_done==0 and i2c_error==0 and i2c_drop_req==0):
            i2c_stat  = self.read_ctrl_reg(14)
            i2c_done     = (i2c_stat & 0x00000001) >> 0
            i2c_error    = (i2c_stat & 0x00000002) >> 1
            i2c_drop_req = (i2c_stat & 0x00000004) >> 2
            i2c_data     = (i2c_stat & 0x0007FFF8) >> 3			
        if(i2c_error):
            self.logger.warn('IIC read error')				
            return [-1]
        elif(i2c_drop_req):
            self.logger.warn('IIC drop request error')
            return [-1]			
        else:
            if(nb_read_access > 0):		
                data_return_list = []        		
                for i in range(0,(nb_read_access+1)):
                    data  = (i2c_data >> i*8) & 0xFF
                    data_return_list.append(data)
                return data_return_list
            else:
                return [i2c_data&0xFF]

    # ***************************************************************************
    # ***********************  DRP CONTROL METHODS  *****************************
    # ***************************************************************************

    def drp_write(self, addr, value):
        """
            Writes via DRP interface (Xilinx only)

            arg:
                addr: address register to be read

                value: register value
            return:
                1
        """			
        rdy=0	
        while(rdy==0):
            drpen=1
            drpwe=1
            value = (drpen<<26) + (drpwe<<25) + ((addr&0x000001ff)<<16) + ((value&0x0000ffff)<<0)
            self.write_ctrl_reg(17,value)
            value = (1<<31) + (drpen<<26) + (drpwe<<25) + ((addr&0x000001ff)<<16) + ((value&0x0000ffff)<<0)
            self.write_ctrl_reg(17,value)

            stat = self.read_ctrl_reg(17)

            value =  (stat&0x0000ffff) >> 0;
            addr  =  (stat&0x01ff0000) >> 16;
            rdy   =  (stat&0x04000000) >> 26;

        return rdy

    def drp_read(self, addr):
        """
            Read via DRP interface (Xilinx only)

            arg:
                addr: address register to be read

            return:
                [addr, value, ready]
        """		
        rdy=0	
        while(rdy==0):
            drpen=1
            drpwe=0
            value = (drpen<<26) + (drpwe<<25) + ((addr&0x000001ff)<<16) + 0
            self.write_ctrl_reg(17,value)			
            value = (1<<31) + (drpen<<26) + (drpwe<<25) + ((addr&0x000001ff)<<16) + 0
            self.write_ctrl_reg(17,value)

            stat = self.read_ctrl_reg(17)

            value =  (stat&0x0000ffff) >> 0;
            addr  =  (stat&0x01ff0000) >> 16;	
            rdy   =  (stat&0x04000000) >> 26;
        return [addr, value, rdy]

    # ***************************************************************************
    # *************************  Tx phase control  ******************************
    # ***************************************************************************
    def tx_pi_phase_controller(self, inc_ndec, step_value):
        """
            Write New phase for Tx PI (Xilinx Ultrascale only)
            
            arg:
                inc_ndec   = 0-1
                step_value = 0-15
            return:
                success
        """
        stat = self.read_ctrl_reg(19)
        stat = stat & 0x7FFFFFC0		
        self.write_ctrl_reg(19,(0<<31)+ (step_value<<2)+(inc_ndec<<1)+1 + stat)
        self.write_ctrl_reg(19,(1<<31)+ (step_value<<2)+(inc_ndec<<1)+1 + stat)
        self.write_ctrl_reg(19,(0<<31)+ (step_value<<2)+(inc_ndec<<1)+0 + stat)
        self.write_ctrl_reg(19,(1<<31)+ (step_value<<2)+(inc_ndec<<1)+0 + stat)

    def get_tx_phase_aligner_status(self):
        """
            Get Tx Phase Aligner status (Xilinx Ultrascale only)

            return:
                [phase_calib, fifo_fill_pd]
        """
        stat = self.read_ctrl_reg(19)
        phase_calib = (stat >> 14) & 0x7F
        fifo_fill_pd = (stat >> 21) & 0x3F
        return [phase_calib, fifo_fill_pd/self.c_MAX_TX_PHASE_ALIGNER_FIFO_FILL_PD]

    def set_tx_phase_aligner(self, phase_calib, ui_align=1):
        """
            Set Tx Phase Aligner mode (Xilinx Ultrascale only)

            return:
                1
        """
        stat = self.read_ctrl_reg(19)
        stat = stat & 0x7FFFC03F		
        self.write_ctrl_reg(19, (ui_align<<13) + (phase_calib<<6) + (0<<31) + stat)     		
        self.write_ctrl_reg(19, (ui_align<<13) + (phase_calib<<6) + (1<<31) + stat)         
        return 1

    # ****************************************************************************
    # ***********************  Rx equalizer control  *****************************
    # ****************************************************************************
    def rx_equalizer_force(self, lpmen=1, lpmcgovrden=1, lpmhfovrden=1, lpmlfklovrden=1, lpmosovrden=1, dfevpovrden=0, dfeutovrden=0, osovrden=0, dfeagcovrden=0, dfelfovrden=0):
        """
            Force Rx equalizer values (Xilinx Ultrascale only)

            arg:
                lpmen, lpmcgovrden, lpmhfovrden, lpmlfklovrden, lpmosovrden
                dfevpovrden, dfeutovrden, osovrden, dfeagcovrden, dfelfovrden
            return:
                1
        """
        stat = self.read_ctrl_reg(18)
        msg = (lpmen<<0) + (lpmcgovrden<<1) + (lpmhfovrden<<2) + (lpmlfklovrden<<3) + (lpmosovrden<<4) + (dfevpovrden<<5)+ (dfeutovrden<<6)+ (osovrden<<7)+ (dfeagcovrden<<8)+ (dfelfovrden<<9)  + (0<<31)
        self.write_ctrl_reg(18, msg + (stat&0x7FFE0000))		
        msg = (lpmen<<0) + (lpmcgovrden<<1) + (lpmhfovrden<<2) + (lpmlfklovrden<<3) + (lpmosovrden<<4) + (dfevpovrden<<5)+ (dfeutovrden<<6)+ (osovrden<<7)+ (dfeagcovrden<<8)+ (dfelfovrden<<9)  + (1<<31)
        self.write_ctrl_reg(18, msg+ (stat&0x7FFE0000))

    def get_rx_equalizer(self):
        """
            Get Rx equalizer values (Xilinx Ultrascale only)

            return:
                [lpmen, lpmcgovrden, lpmhfovrden, lpmlfklovrden, lpmosovrden, dfevpovrden, dfeutovrden, osovrden, dfeagcovrden, dfelfovrden, rxmonitorout]
        """
        stat = self.read_ctrl_reg(18)
        lpmen         = (stat>>0) & 0x1
        lpmcgovrden   = (stat>>1) & 0x1 
        lpmhfovrden   = (stat>>2) & 0x1 
        lpmlfklovrden = (stat>>3) & 0x1 
        lpmosovrden   = (stat>>4) & 0x1

        dfevpovrden   = (stat>>5) & 0x1
        dfeutovrden   = (stat>>6) & 0x1
        osovrden      = (stat>>7) & 0x1
        dfeagcovrden  = (stat>>8) & 0x1
        dfelfovrden   = (stat>>9) & 0x1		
		
        rxmonitorout  = (stat>>10) & 0x7F
        return [lpmen, lpmcgovrden, lpmhfovrden, lpmlfklovrden, lpmosovrden, dfevpovrden, dfeutovrden, osovrden, dfeagcovrden, dfelfovrden, rxmonitorout]

    def captureDMON(self, msb, lsb):
        [lpmen, lpmcgovrden, lpmhfovrden, lpmlfklovrden, lpmosovrden,dfevpovrden, dfeutovrden, osovrden, dfeagcovrden, dfelfovrden, rxmonitorout] = self.get_rx_equalizer()
        parameter = (rxmonitorout >> lsb) & (2**(msb-lsb+1)-1)
        return parameter

    def get_lpm_equalizer_monitor(self):
        """
            Read LPM equalizer loop monitor

            return:
                LPM equalizer loop adapt parameters as list [RXLPMHF, RXLPMLF, RXLPMOS, RXLPMAGC]
        """	
        [addr, value, rdy] = self.drp_read(0x003A)
        self.drp_write(0x003A, (value & 0xFEFF) | 0x0100)
        # DFE monitor disable		
        self.drp_write(0x0054, 0x0000)
        # Read/Modify/Write ADAPT_CFG1[12:8]
        [addr, value, rdy] = self.drp_read(0x0092);
        self.drp_write(0x0092, (value & 0xE0FF) | 0x0000);
        # LPM Mode Only: RXLPMHF
        self.drp_write(0x0032, 0x0020);
        RXLPMHF = self.captureDMON(6, 2);
        # LPM Mode Only: RXLPMLF
        self.drp_write(0x0032, 0x0028);
        RXLPMLF = self.captureDMON(6, 2);
        # LPM Mode Only: RXLPMOS
        self.drp_write(0x0032, 0x0030);
        RXLPMOS = self.captureDMON(6, 0);
        # LPM Mode Only: RXLPMAGC
        self.drp_write(0x0032, 0x0038);
        RXLPMAGC = self.captureDMON(4, 0);
        return [RXLPMHF, RXLPMLF, RXLPMOS, RXLPMAGC]

    def set_lpm_equalizer_parameters(self, RXLPMHF, RXLPMLF, RXLPMOS, RXLPMAGC):
        """
            Set LPM equalizer parameters
            In order to use this method, it is necessary to force those values to be overridden with rx_equalizer_force

            arg:
                RXLPMHF, RXLPMLF, RXLPMOS, RXLPMAGC
        """
        # LPM Mode Only: RXLPMHF
        [addr, value, rdy] = self.drp_read(0x0033)
        self.drp_write(0x0033, (value & 0x01FF) | (RXLPMHF<<(2+9)))

        # LPM Mode Only: RXLPMLF
        [addr, value, rdy] = self.drp_read(0x0035)
        self.drp_write(0x0035, (value & 0x01FF) | (RXLPMLF<<(2+9)))

        # LPM Mode Only: RXLPMOS
        [addr, value, rdy] = self.drp_read(0x0037)
        self.drp_write(0x0037, (value & 0x01FF) | (RXLPMOS<<(0+9)))

        # LPM Mode Only: RXLPMAGC
        [addr, value, rdy] = self.drp_read(0x0039)
        self.drp_write(0x0039, (value & 0xE0FF) | (RXLPMAGC<<(0+8)))

        return 1

    def get_dfe_equalizer_monitor(self):
        """
            Read DFE equalizer loop monitor

            return:
                DFE equalizer loop adapt parameters as list [RXDFEVP, RXDFEUT, RXDFEOS, RXDFEAGC, RXDFEKL]
        """	
        [addr, value, rdy] = self.drp_read(0x003A)
        self.drp_write(0x003A, (value & 0xFEFF) | 0x0100)
        # LPM monitor disable		
        self.drp_write(0x0032, 0x0000)
        # Read/Modify/Write ADAPT_CFG1[12:8]
        [addr, value, rdy] = self.drp_read(0x0092);
        self.drp_write(0x0092, (value & 0xE0FF) | 0x0200);
        # DFE Mode Only: RXDFEOS
        self.drp_write(0x0054, 0x0020);
        RXDFEOS=self.captureDMON(6, 0);
        # DFE Mode Only: RXDFEKL
        self.drp_write(0x0054, 0x0021);
        RXDFEKL=self.captureDMON(6, 2);
        # DFE Mode Only: RXDFEVP
        self.drp_write(0x0054, 0x0022);
        RXDFEVP=self.captureDMON(6, 0);
        # DFE Mode Only: RXDFEUT
        self.drp_write(0x0054, 0x0023);
        RXDFEUT=self.captureDMON(6, 0);
        # DFE Mode Only: RXDFEAGC
        self.drp_write(0x0054, 0x0024);
        RXDFEAGC=self.captureDMON(4, 0);
        return [RXDFEVP, RXDFEUT, RXDFEOS, RXDFEAGC, RXDFEKL]

    def set_dfe_equalizer_parameters(self, RXDFEVP, RXDFEUT, RXDFEOS, RXDFEAGC, RXDFEKL):
        """
            Set DFE equalizer parameters
            In order to use this method, it is necessary to force those values to be overridden with rx_equalizer_force

            arg:
                RXDFEVP, RXDFEUT, RXDFEOS, RXDFEAGC, RXDFEKL
        """
        # DFE Mode Only: RXDFEVP
        [addr, value, rdy] = self.drp_read(0x008E)
        self.drp_write(0x008E, (value & 0x01FF) | (RXDFEVP<<(0+9)))

        # DFE Mode Only: RXDFEUT
        [addr, value, rdy] = self.drp_read(0x009E)
        self.drp_write(0x009E, (value & 0x01FF) | (RXDFEUT<<(0+9)))

        # DFE Mode Only: RXDFEOS
        [addr, value, rdy] = self.drp_read(0x0058)
        self.drp_write(0x0058, (value & 0x01FF) | (RXDFEOS<<(0+9)))

        # DFE Mode Only: RXDFEAGC
        [addr, value, rdy] = self.drp_read(0x00A1)
        self.drp_write(0x00A1, (value & 0xFF83) | (RXDFEAGC<<(0+2)))

        # DFE Mode Only: RXDFEKL
        [addr, value, rdy] = self.drp_read(0x0035)
        self.drp_write(0x0035, (value & 0x01FF) | (RXDFEKL<<(2+9)))
        return 1

    # ***************************************************************************
    # *****************************  PHY_MONITOR  *******************************
    # ***************************************************************************

    def phymon_clear(self):
        """
            ***************************************************************************
            Clear PHY monitor sticky bits
            ***************************************************************************

            ***************************************************************************
            return:
                1
            ***************************************************************************
        """				
        stat = self.read_ctrl_reg(5)
        stat = stat | 1
        self.write_ctrl_reg(5, stat)
        stat = stat ^ 1
        self.write_ctrl_reg(5, stat)
        return 1

    def phymon_read_seen(self):
        """
            ***************************************************************************
            Read PHY monitor sticky bits
            ***************************************************************************

            ***************************************************************************
            return:
                [seen_mgt_tx_nready, seen_mgt_rx_nready, seen_mgt_txpll_nlock, seen_mgt_rxpll_nlock, seen_rx_nlocked, seen_phase_ngood]
            ***************************************************************************
        """	
        stat = self.read_ctrl_reg(5)
        seen_mgt_tx_nready      = (stat>>8)&0x1
        seen_mgt_rx_nready      = (stat>>9)&0x1
        seen_mgt_txpll_nlock    = (stat>>10)&0x1
        seen_mgt_rxpll_nlock    = (stat>>11)&0x1
        seen_rx_nlocked         = (stat>>12)&0x1
        seen_phase_ngood        = (stat>>13)&0x1				
        # Return value
        mon_return = [seen_mgt_tx_nready, seen_mgt_rx_nready, seen_mgt_txpll_nlock, seen_mgt_rxpll_nlock, seen_rx_nlocked, seen_phase_ngood]
        return mon_return

    # ***************************************************************************
    # *****************  DN LINK ERROR_MONITOR (FEC/SC-CRC)  ********************
    # ***************************************************************************

    def errmon_clear(self):
        """
            ***************************************************************************
            Clear DN LINK monitor results
            ***************************************************************************

            ***************************************************************************
            return:
                1
            ***************************************************************************
        """			
        stat = self.read_ctrl_reg(3)
        stat = stat | 1
        self.write_ctrl_reg(3, stat)
        stat = stat ^ 1
        self.write_ctrl_reg(3, stat)
        return 1

    def errmon_read(self):
        """
            ***************************************************************************
            Read DN LINK monitor error counters from ONU (8-bit, non-limited)
            ***************************************************************************

            ***************************************************************************
            return:
                [single_error_corrected_fec, double_error_corrected_fec, slow_control_crc_err_detect]
            ***************************************************************************
        """			
        # Latch
        stat = self.read_ctrl_reg(3)
        stat = stat | (1 << 1)
        self.write_ctrl_reg(3, stat)
        stat = stat ^ (1 << 1)
        self.write_ctrl_reg(3, stat)
        # Read status
        stat = self.read_ctrl_reg(3)
        serror_fec    = (stat>>8)&0xff
        derror_fec    = (stat>>16)&0xff
        sc_crc_errdet = (stat>>24)&0xff		
        # Return value
        mon_return = [serror_fec, derror_fec, sc_crc_errdet]
        return mon_return

    def errmon_read_seen_error(self):
        """
            ***************************************************************************
            Read DN LINK monitor sticky bits
            ***************************************************************************

            ***************************************************************************
            return:
                [seen_single_error_corrected, seen_double_error_corrected, seen_crc_err_detect]
            ***************************************************************************
        """			
        # Latch
        stat = self.read_ctrl_reg(4)
        serror_fec    = (stat>>0)&0x1
        derror_fec    = (stat>>1)&0x1
        sc_crc_errdet = (stat>>2)&0x1		
        # Return value
        mon_return = [serror_fec, derror_fec, sc_crc_errdet]
        return mon_return

    # ***************************************************************************
    # **************************  ONU Interrupt  ********************************
    # ***************************************************************************

    def set_interrupt_mask(self, interrupt_mask):
        """
            ***************************************************************************
            Configure ONU interrupt mask
            ***************************************************************************

            ***************************************************************************
            arg:
                Interrupt mask is a list of the first N elements of the interrupt mask vector (N<=31 always)
                interrupt_async <= (interrupt_mask(0) and mgt_seen_tx_nready_mngrsync_meta)    or
                                   (interrupt_mask(1) and mgt_seen_rx_nready_mngrsync_meta)    or
                                   (interrupt_mask(2) and mgt_seen_txpll_nlock_mngrsync_meta)  or
                                   (interrupt_mask(3) and mgt_seen_rxpll_nlock_mngrsync_meta)  or
                                   (interrupt_mask(4) and seen_rx_nlocked_mngrsync_meta)       or
                                   (interrupt_mask(5) and seen_phase_ngood_mngrsync_meta)      or
                                   (interrupt_mask(6) and rx_seen_serror_correc)               or					  
                                   (interrupt_mask(7) and rx_seen_derror_correc)               or	
                                   (interrupt_mask(8) and rx_seen_crc_errdetect);
                 Ex. set an interrupt with rx_seen_derror_correc or rx_seen_crc_errdetect or seen_rx_nlocked:
                 onu.set_interrupt_mask([0 0 0 0 1 0 0 1 1])
            ***************************************************************************
        """
        message = 0
        for i in range(0,len(interrupt_mask)):
            message = message | (interrupt_mask[i]<<i)
        self.write_ctrl_reg(6, message)
        return 1

    def get_interrupt_mask(self):
        """
            ***************************************************************************
            Get current configuration for ONU interrupt mask
            ***************************************************************************

            ***************************************************************************
            return:
                interrupt_mask: list with 31 elements corresponding to the interrupt enabling state
            ***************************************************************************
        """		
        interrupt_mask = [0] * 31
        stat = self.read_ctrl_reg(6)
        for i in range(0,31):
            interrupt_mask[i] = ((stat>>i)&0x1)
        return interrupt_mask

    def get_interrupt(self):
        """
            ***************************************************************************
            Check interrupt bit
            ***************************************************************************

            ***************************************************************************
            return:
                interrupt: 0=interrupt not activated/1=interrupt activated
            ***************************************************************************
        """	
        interrupt = 0
        stat = self.read_ctrl_reg(6)
        interrupt = (stat>>31)&0x1
        return interrupt

    # ***************************************************************************
    # ***********************  SFP MONITOR METHODS  *****************************
    # ***************************************************************************

    def get_sfp_vendor(self):
        """
            ***************************************************************************
            Read SFP vendor name via IIC
            ***************************************************************************

            ***************************************************************************
            return:
                [success, vendor_name]
            ***************************************************************************
        """		
        slv_addr = 0x50	
        reg_bank = []
        for reg_addr in range(20,36,2):
            reg_value = self.i2c_read(slv_addr, reg_addr,2) # double burst-read access
            reg_value1 = reg_value[0]
            if(reg_value1==-1):
                self.logger.warn('Failed to read from SFP')
                return [0]
            reg_value2 = reg_value[1]			
            reg_bank.append(reg_value1)
            reg_bank.append(reg_value2)
        vendor_name = ''.join(chr(i) for i in reg_bank)
        return [1, vendor_name]

    def get_sfp_part_number(self):
        """
            ***************************************************************************
            Read SFP part number via IIC
            ***************************************************************************

            ***************************************************************************
            return:
                [success, vendor_part_number]
            ***************************************************************************
        """		
        slv_addr = 0x50		
        reg_bank = []
        for reg_addr in range(40,56,2):
            reg_value = self.i2c_read(slv_addr, reg_addr,2) # double burst-read access
            reg_value1 = reg_value[0]
            if(reg_value1==-1):
                self.logger.warn('Failed to read from SFP')
                return [0]
            reg_value2 = reg_value[1]			
            reg_bank.append(reg_value1)
            reg_bank.append(reg_value2)
        vendor_part_number = ''.join(chr(i) for i in reg_bank)
        return [1, vendor_part_number]

    def get_sfp_serial_number(self):
        """
            ***************************************************************************
            Read SFP serial number via IIC
            ***************************************************************************

            ***************************************************************************
            return:
                [success, vendor_serial_number]
            ***************************************************************************
        """			
        slv_addr = 0x50		
        reg_bank = []
        for reg_addr in range(68,84,2):
            reg_value = self.i2c_read(slv_addr, reg_addr,2) # double burst-read access
            reg_value1 = reg_value[0]
            if(reg_value1==-1):
                self.logger.warn('Failed to read from SFP')
                return [0]
            reg_value2 = reg_value[1]
            reg_bank.append(reg_value1)
            reg_bank.append(reg_value2)
        vendor_serial_number = ''.join(chr(i) for i in reg_bank)
        return [1, vendor_serial_number]

    def get_sfp_temp(self):
        """
            ***************************************************************************
            Read SFP temperature via IIC
            ***************************************************************************

            ***************************************************************************
            return:
                [success, temp_conv] - unit is degrees celsius
            ***************************************************************************
        """			
        slv_addr  = 0x51
        # Read digital diagnostic
        reg_bank = []
        reg_addr = 96
        reg_value  = self.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            self.logger.warn('Failed to read from ONU - SFP')
            return [0]
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

        temp    = (reg_bank[0]<<8) + reg_bank[1]

        # TEMPERATURE:
        # 1) Internally measured transceiver temperature. Represented as a 16 bit signed twos complement value in
        # increments of 1/256 degrees Celsius
        if(temp&0x8000):
            sign = -1
            temp_conv = (temp ^ 0xFFFF) + 1
        else:
            sign = 1
            temp_conv = temp
        temp_conv = sign*temp_conv*(1.0/256.0) # UNIT: degrees Celsius

        return [1, temp_conv]

    def get_sfp_vcc(self):
        """
            ***************************************************************************
            Read SFP voltage via IIC
            ***************************************************************************

            ***************************************************************************
            return:
                [success, vcc_conv] - unit is Volts
            ***************************************************************************
        """			
        slv_addr  = 0x51
        # Read digital diagnostic
        reg_bank = []
        reg_addr = 98
        reg_value  = self.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            self.logger.warn('Failed to read from ONU - SFP')
            return [0]
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

        vcc     = (reg_bank[0]<<8) + reg_bank[1]

        # SUPPLY VOLTAGE:
        # 2) Internally measured transceiver supply voltage. Represented as a 16 bit unsigned integer with the
        # voltage defined as the full 16 bit value (0-65535) with LSB equal to 100 uVolt
        vcc_conv = (vcc*100e-6) # UNIT: V

        return [1, vcc_conv]

    def get_sfp_tx_bias(self):
        """
            ***************************************************************************
            Read SFP Tx Bias via IIC
            ***************************************************************************

            ***************************************************************************
            return:
                [success, tx_bias_conv] - unit is mA
            ***************************************************************************
        """			
        slv_addr  = 0x51
        # Read digital diagnostic
        reg_bank = []
        reg_addr = 100
        reg_value  = self.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            self.logger.warn('Failed to read from ONU - SFP')
            return [0]
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

        tx_bias = (reg_bank[0]<<8) + reg_bank[1]	

		# TX BIAS CURRENT:
        # 3) Measured TX bias current in uA. Represented as a 16 bit unsigned integer with the current defined as the
        # full 16 bit value (0-65535) with LSB equal to 2 uA
        tx_bias_conv = (tx_bias*2e-6)*1e3 # UNIT: mA

        return [1, tx_bias_conv]
		
    def get_sfp_tx_pwr(self):
        """
            ***************************************************************************
            Read SFP Tx Power via IIC
            ***************************************************************************

            ***************************************************************************
            return:
                [success, tx_pwr_conv] - unit is dBm
            ***************************************************************************
        """		
        slv_addr  = 0x51
        # Read digital diagnostic
        reg_bank = []
        reg_addr = 102
        reg_value  = self.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            self.logger.warn('Failed to read from ONU - SFP')
            return [0]
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

        tx_pwr  = (reg_bank[0]<<8) + reg_bank[1]

		# TX OUTPUT POWER:
        # 4) Measured TX output power in mW. Represented as a 16 bit unsigned integer with the power defined as
        # the full 16 bit value (0-65535) with LSB equal to 0.1 uW
        tx_pwr_conv = 10*math.log10((tx_pwr * 0.1 * 1e-6)/1e-3) # UNIT: dBm

        return [1, tx_pwr_conv]

    def get_sfp_rx_pwr(self):
        """
            ***************************************************************************
            Read SFP Rx Power via IIC
            ***************************************************************************

            ***************************************************************************
            return:
                [success, rx_pwr_conv] - unit is dBm
            ***************************************************************************
        """			
        slv_addr  = 0x51
        # Read digital diagnostic
        reg_bank = []
        reg_addr = 104
        reg_value  = self.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            self.logger.warn('Failed to read from ONU - SFP')
            return [0]
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

        rx_pwr  = (reg_bank[0]<<8) + reg_bank[1]

		# RX RECEIVED OPTICAL POWER:
        # 5) Measured RX received optical power in mW. Value can represent either average received power or OMA
        # depending upon how bit 3 of byte 92 (A0h) is set. Represented as a 16 bit unsigned integer with the
        # power defined as the full 16 bit value (0-65535) with LSB equal to 0.1 uW
        if(rx_pwr>0):
            rx_pwr_conv = 10*math.log10((rx_pwr * 0.1 * 1e-6)/1e-3) # UNIT: dBm
        else:
            rx_pwr_conv = -1*float('inf')

        return [1, rx_pwr_conv]