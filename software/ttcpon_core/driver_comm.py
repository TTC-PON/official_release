#!/usr/bin/env python

import socket
import logging
import time
import csv
from random import randint

# -------------------------------------------------------------
#  -------------- Class DriverComm (driver-layer) ------------
# -------------------------------------------------------------

class DriverComm:
    """
        This class is the driver-layer of the TTC-PON firmware. It is responsible for communicating from Python to the FPGA using the methods write_ctrl_reg and read_ctrl_reg
        The higher level methods implemented in TTC-PON in the OltCore and OnuCore classes make use of those driver-layer methods

        ***************************************************************************************************************
        ***   In case a user is implementing TTC-PON in its CUSTOM BOARD:                                           ***
        ***       option1: this class shall be replaced by the equivalent control interface used for his/her board  *** 
        ***       option2: the user should overwrite the methods read_ctrl_reg and write_ctrl_reg                   *** 
        ***************************************************************************************************************

    """

    def __init__(self, ip='192.168.1.2', port=9001):
        """
            ***************************************************************************
            Class Constructor
            ***************************************************************************

            ***************************************************************************
            arg:
                ip: string with the IP address of the board being controlled.
                    The board IP in the TTC-PON example design can be changed via the software implemented in the microblaze, i.e. by using the Vivado SDK
                port: integer with the port number
            ***************************************************************************
        """	
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((ip, port))
        self.logger = logging.getLogger('ttc_pon')
        self.logger.debug('Opening socket at port:' + str(port) + ", ip:" + ip)
        #slow_down is only used for TTC-PON demo in order to slow down the communication and perform demonstrations of the system, user do not have to care about that
        self.slow_down = 0

    def write_ctrl_reg(self, reg_addr, reg_value):
        """
            ***************************************************************************		
            Write register via microblaze interface for the example design
			
            !!! This method depends on the way the board is controlled !!!
            This method shall be replaced by the equivalent read method in Python in case a user implements PON in a custom board           
            ***************************************************************************

            ***************************************************************************
            arg: reg_addr and reg_value are, respectively, the address register and value to be written to the interface
            return: 1 if success
            ***************************************************************************
			
            ***************************************************************************
            In our example design, two slaves are present: CORE SLAVE + EXDSG SLAVE
            -> To send a message to the CORE slave:
            we send strings in the format: "w 0x0008 0x00000390" to indicate write "0x00000390" to control reg 2 (byte addressing) of the core
            -> To send a message to the EXDSG slave, an offset 0x1000 is given:
            we send strings in the format: "w 0x1008 0x00000390" to indicate write "0x00000390" to control reg 2 (byte addressing) of the exdsg
            
            The method write_ctrl_reg, multiplies the reg_addr by four in order to send to the microblaze
            If the user wants to write to the slave, it should write to => slave_reg_local_addr + 0x1000/4
            It is: if you want to write to register 2 of the exdsg, call this method as: self.write_ctrl_reg(2+1024, value)
            ***************************************************************************
        """		
        message = b"w 0x%0.4X 0x%0.8X\n" % (4 * reg_addr, reg_value)
        if(self.slow_down) : time.sleep(0.1)
        self.s.send(message)
        data = self.s.recv(2048)
        data = int(data, 16)	
        return data


    def read_ctrl_reg(self, reg_addr):
        """
            ***************************************************************************
            Reads register via microblaze interface for the example design

            !!! This method depends on the way the board is controlled !!!
            This method shall be replaced by the equivalent read method in Python in case a user implements PON in a custom board
            ***************************************************************************

            ***************************************************************************
            arg: reg_addr is the address register to be read
            return: register_value as an integer
            ***************************************************************************

            ***************************************************************************
            In our example design, two slaves are present: CORE SLAVE + EXDSG SLAVE
            -> To send a message to the CORE slave:
            we send strings in the format: "r 0x008" to indicate read from control reg 2 (byte addressing) of the core
            -> To send a message to the EXDSG slave, an offset 0x1000 is given:
            we send strings in the format: "r 0x1008" to indicate read from control reg 2 (byte addressing) of the exdsg
            
            The method read_ctrl_reg, multiplies the reg_addr by four in order to send to the microblaze
            If the user wants to read from the slave, it should read from => slave_reg_local_addr + 0x1000/4
            It is: if you want to read to register 2 of the exdsg logic, call this method as: self.write_ctrl_reg(2+1024, value)
            ***************************************************************************
        """		
        message = b"r 0x%0.4X\n" % (4 * reg_addr)	
        self.s.send(message)
        data = self.s.recv(2048)
        data = int(data, 16)
        return(data)