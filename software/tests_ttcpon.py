#!/usr/bin/env python

import csv
import datetime
import math
import time
import logging
from random import randint
import matplotlib.pyplot as plt

import ttcpon_board_config.pon_fmc_i2c_program as program_fmc
logger_exd_pon = logging.getLogger('ttc_pon_tests')

# -------------------------------------------------------------
#  ------------------ Component Logging ---------------------
# -------------------------------------------------------------
def save_components(device, manufacturer, part_number, serial_number, firmware_rev, filename):
	with open(filename + '.csv', 'w') as csvfile:
		fieldnames = ['device', 'manufacturer', 'part_number', 'serial_number', 'firmware_rev']
		for i in range(0, len(fieldnames)):
			# format 30 spaces for each value
			fieldnames[i] = '%30s' % fieldnames[i]

		writer = csv.DictWriter(
			csvfile,
			fieldnames=fieldnames,
			delimiter=',',
			lineterminator='\n')
		writer.writeheader()

		for i in range(0,len(device)):
			dict_write = {
				fieldnames[0] : ('%30s') % device[i],
				fieldnames[1] : ('%30s') % manufacturer[i],				
				fieldnames[2] : ('%30s') % part_number[i],
				fieldnames[3] : ('%30s') % serial_number[i],
				fieldnames[4] : ('%30s') % firmware_rev[i]			
			} 
			writer.writerow(dict_write)

# -------------------------------------------------------------
#  ---------------------- BERT Section -----------------------
# -------------------------------------------------------------


def cl_calc(num_bits, num_errors, target_ber):
	sum = 0
	for k in range(0, num_errors + 1):
		term_aux = 1
		for aux in range(1, (k + 1)):
			term_aux = term_aux * (num_bits - aux)
		sum += (term_aux / math.factorial(k)) * (target_ber**k) * \
			((1 - target_ber)**(num_bits - k))

	cl = 1 - sum

	return cl


def save_bert_config_csv(number_curves, number_onu, att_init, att_step,
						 att_end, target_ber, confidence_level, finish_early, name_file):
	with open(name_file + '_config.csv', 'w') as csvfile:
		fieldnames = [
			'time_start',
			'number_curves',
			'number_onu',
			'att_init',
			'att_step',
			'att_end',
			'target_ber',
			'confidence_level',
			'finish_early']
		for i in range(0, len(fieldnames)):
			# format 20 spaces for each value
			fieldnames[i] = '%20s' % fieldnames[i]

		writer = csv.DictWriter(
			csvfile,
			fieldnames=fieldnames,
			delimiter=',',
			lineterminator='\n')
		writer.writeheader()

		current_data_time = datetime.datetime.now()
		time_start = str(current_data_time.time())

		dict_write = {fieldnames[0]: ('%20s' % time_start)}
		dict_write[fieldnames[1]] = ('%20d' % number_curves)
		dict_write[fieldnames[2]] = ('%20d' % number_onu)
		dict_write[fieldnames[3]] = ('%20.4f' % att_init)
		dict_write[fieldnames[4]] = ('%20.4f' % att_step)
		dict_write[fieldnames[5]] = ('%20.4f' % att_end)
		dict_write[fieldnames[6]] = ('%20.15f' % target_ber)
		dict_write[fieldnames[7]] = ('%20.4f' % confidence_level)
		dict_write[fieldnames[8]] = ('%20d' % finish_early)

		writer.writerow(dict_write)

# -------------------------------------------------------------
#  --------------------- Downstream BERT ---------------------
# -------------------------------------------------------------


def save_down_curve_csv(number_onu, matrix_write, name_file):
	with open(name_file + '.csv', 'w') as csvfile:
		fieldnames = ['time_start', 'time_stop', 'date_start', 'attenuation']
		for i in range(1, number_onu + 1):
			fieldnames += ['total_errors' + str(i), 'total_bits' + str(i), 'ber_value' + str(i), 'total_serror_corr' + str(i), 'total_derror_corr' + str(i)]

		for i in range(0, len(fieldnames)):
			# format 20 spaces for each value
			fieldnames[i] = '%20s' % fieldnames[i]

		writer = csv.DictWriter(
			csvfile,
			fieldnames=fieldnames,
			delimiter=',',
			lineterminator='\n')
		writer.writeheader()

		att_vector = matrix_write[0]
		time_start_vector = matrix_write[1]
		time_stop_vector = matrix_write[2]
		date_start_vector = matrix_write[3]
		error_matrix = matrix_write[4]
		bit_matrix = matrix_write[5]
		ber_matrix = matrix_write[6]
		serror_correc_matrix = matrix_write[7]
		derror_correc_matrix = matrix_write[8]

		for j in range(0, len(att_vector)):
			time_start_vector[j] = '%20s' % time_start_vector[j]
			time_stop_vector[j] = '%20s' % time_stop_vector[j]
			date_start_vector[j] = '%20s' % date_start_vector[j]

			dict_write = {
				fieldnames[0]: time_start_vector[j],
				fieldnames[1]: time_stop_vector[j],
				fieldnames[2]: date_start_vector[j],
				fieldnames[3]: ("%20.4f" % att_vector[j])}
			for i in range(1, number_onu + 1):
				dict_write[fieldnames[4 + (i - 1) * 5]] = "%20d" % error_matrix[j][i - 1]
				dict_write[fieldnames[5 + (i - 1) * 5]] = "%20d" % bit_matrix[j][i - 1]
				dict_write[fieldnames[6 + (i - 1) * 5]] = "%20.15f" % ber_matrix[j][i - 1]
				dict_write[fieldnames[7 + (i - 1) * 5]] = "%20d" % serror_correc_matrix[j][i - 1]
				dict_write[fieldnames[8 + (i - 1) * 5]] = "%20d" % derror_correc_matrix[j][i - 1]				
			writer.writerow(dict_write)


def downstream_bert(olt, onu, optatt, number_curves, att_init, att_step,
					att_end, target_ber, confidence_level, finish_early, name_file):

	# save configurations that were used for BERT
	save_bert_config_csv(
		number_curves,
		len(onu),
		att_init,
		att_step,
		att_end,
		target_ber,
		confidence_level,
		finish_early,
		name_file)

	for i in range(0, number_curves):
		# reset system
		logger_exd_pon.info('Start downstream BERT curve: ' + str(i))
		logger_exd_pon.info('Resetting system ...')
		olt.exd_core_reset()
		for onu_iter in onu:
			onu_iter.exd_core_reset()

		return_bert_curve = downstream_bert_curve(
			onu,
			optatt,
			att_init,
			att_step,
			att_end,
			target_ber,
			confidence_level,
			finish_early)

		# save curve to a file
		name_file_index = name_file + str(i)
		logger_exd_pon.info('Saving curve ' + str(i) + ' to file:' + name_file_index)
		save_down_curve_csv(len(onu), return_bert_curve, name_file_index)


def downstream_bert_curve(onu, optatt, att_init, att_step,
						  att_end, target_ber, confidence_level, finish_early):
	att_vector = []
	time_start_vector = []
	time_stop_vector = []
	date_start_vector = []
	bit_matrix = []
	error_matrix = []
	ber_matrix = []
	serror_correc_matrix = []
	derror_correc_matrix = []
	
	att_value = att_init
	finished_curve = 0

	optatt.set_initial_config()
	optatt.set_wavelength(1577)

	while(not finished_curve):
		optatt.set_attenuation(att_value)
		att_vector.append(att_value)

		current_data_time = datetime.datetime.now()
		time_start_vector.append(str(current_data_time.time()))
		date_start_vector.append(str(current_data_time.date()))

		logger_exd_pon.info('Starting point for attenuation: ' + str(att_value))

		# BER point
		return_bert_point = downstream_bert_point(
			onu, target_ber, confidence_level)

		ber_matrix.append([])
		# Calculate Actual BER
		for i in range(0, len(onu)):
			calculated_ber = (return_bert_point[0][i] / return_bert_point[1][i]) if(return_bert_point[0][i]) else (1 / return_bert_point[1][i])
			ber_matrix[-1].append(calculated_ber)

			# Append values to return
		error_matrix.append(return_bert_point[0])
		bit_matrix.append(return_bert_point[1])
		serror_correc_matrix.append(return_bert_point[2])
		derror_correc_matrix.append(return_bert_point[3])
		
		logger_exd_pon.info('Point attenuation: ' + str(att_value) + ' - finished')

		# Check if test is finished
		if(finish_early):
			finished_curve = 1
			for i in range(0, len(onu)):
				if(ber_matrix[-1][i] > target_ber):
					finished_curve = 0

		if(att_value >= 20 and att_step > 0):
			finished_curve = 1
		elif(att_value <= 0 and att_step < 0):
			finished_curve = 1
		elif(att_value == att_end):
			finished_curve = 1

		current_data_time = datetime.datetime.now()
		time_stop_vector.append(str(current_data_time.time()))
		# Increment attenuation
		att_value += att_step

	return [att_vector, time_start_vector, time_stop_vector,
			date_start_vector, error_matrix, bit_matrix, ber_matrix, serror_correc_matrix, derror_correc_matrix]


def downstream_bert_point(onu, target_ber, confidence_level):
	num_max_bits = 1.0 / target_ber
	finished_point = 0
	error_vector = [0] * len(onu)
	bit_vector = [0] * len(onu)
	serror_correc_vector = [0] * len(onu)
	derror_correc_vector = [0] * len(onu)
	onu_finished = [0] * len(onu)

	# clear bert
	for i in range(0, len(onu)):
		onu[i].exd_bert_clear()
	while (not finished_point):

		# latch bert
		for i in range(0, len(onu)):
			onu[i].exd_bert_latch()

		# read error/bit vector
		for i in range(0, len(onu)):
			[rx40_locked, phase_good, rx_locked, operational, mgt_tx_ready, mgt_rx_ready, mgt_txpll_lock, mgt_rxpll_lock] = onu[i].exd_read_onu_status()
			if(rx_locked):			
				error_vector[i] = onu[i].exd_read_error_sum()
				bit_vector[i] = onu[i].exd_read_bit_sum()
				serror_correc_vector[i] = onu[i].exd_read_serror_correc_sum()
				derror_correc_vector[i] = onu[i].exd_read_derror_correc_sum()
			else:
				error_vector[i]			= (num_max_bits+1)/2
				bit_vector[i]			= (num_max_bits+1)
				serror_correc_vector[i] = (num_max_bits+1)/4
				derror_correc_vector[i] = (num_max_bits+1)/4
				
		# test if point is finished for each ONU
		for i in range(0, len(onu)):
			if (not confidence_level):	# confidence level is zero (it is, not being taken into account)
				onu_finished[i] = 1 if(bit_vector[i] > num_max_bits) else 0		
			else:  # take confidence level into account
				# for more than 100 errors, no CL is taken into account
				if((error_vector[i]) > 100):
					onu_finished[i] = 1 if(bit_vector[i] > num_max_bits) else 0
				# for more than 10 errors, CL is taken for 10 errors
				elif(error_vector[i] > 10):
					cl = cl_calc(bit_vector[i], 10, target_ber)
					onu_finished[i] = 1 if(cl >= confidence_level) else 0
				else:  # for less than 10 errors, calculate actual confidence level
					cl = cl_calc(bit_vector[i], error_vector[i], target_ber)
					onu_finished[i] = 1 if(cl >= confidence_level) else 0

		finished_point = 1
		for i in range(0, len(onu)):
			if(not onu_finished[i]):
				finished_point = 0

	return [error_vector, bit_vector, serror_correc_vector, derror_correc_vector]

# -------------------------------------------------------------
#  --------------------- Upstream BERT ---------------------
# -------------------------------------------------------------


def save_up_curve_csv(number_onu, matrix_write, name_file):
	with open(name_file + '.csv', 'w') as csvfile:
		fieldnames = ['time_start', 'time_stop', 'date_start', 'attenuation']
		for i in range(1, number_onu + 1):
			fieldnames += ['total_errors' + str(i), 'total_bits' + str(i), 'ber_value' + str(i),
						   'total_ploss' + str(i), 'ploss_ratio' + str(i)]

		for i in range(0, len(fieldnames)):
			# format 20 spaces for each value
			fieldnames[i] = '%20s' % fieldnames[i]

		writer = csv.DictWriter(
			csvfile,
			fieldnames=fieldnames,
			delimiter=',',
			lineterminator='\n')
		writer.writeheader()

		att_vector = matrix_write[0]
		time_start_vector = matrix_write[1]
		time_stop_vector = matrix_write[2]
		date_start_vector = matrix_write[3]
		error_matrix = matrix_write[4]
		bit_matrix = matrix_write[5]
		ber_matrix = matrix_write[6]
		ploss_matrix = matrix_write[7]
		ploss_ratio_matrix = matrix_write[8]

		for j in range(0, len(att_vector)):
			time_start_vector[j] = '%20s' % time_start_vector[j]
			time_stop_vector[j] = '%20s' % time_stop_vector[j]
			date_start_vector[j] = '%20s' % date_start_vector[j]

			dict_write = {
				fieldnames[0]: time_start_vector[j],
				fieldnames[1]: time_stop_vector[j],
				fieldnames[2]: date_start_vector[j],
				fieldnames[3]: ("%20.4f" % att_vector[j])}
			for i in range(1, number_onu + 1):
				dict_write[fieldnames[4 + (i - 1) * 5]] = "%20d" % error_matrix[j][i - 1]
				dict_write[fieldnames[5 + (i - 1) * 5]] = "%20d" % bit_matrix[j][i - 1]
				dict_write[fieldnames[6 + (i - 1) * 5]] = "%20.15f" % ber_matrix[j][i - 1]
				dict_write[fieldnames[7 + (i - 1) * 5]] = "%20d" % ploss_matrix[j][i - 1]
				dict_write[fieldnames[8 + (i - 1) * 5]] = "%20.15f" % ploss_ratio_matrix[j][i - 1]
			writer.writerow(dict_write)


def upstream_bert(olt, onu, optatt, number_curves, att_init, att_step,
				  att_end, target_ber, confidence_level, finish_early, name_file):

	# save configurations that were used for BERT
	save_bert_config_csv(
		number_curves,
		len(onu),
		att_init,
		att_step,
		att_end,
		target_ber,
		confidence_level,
		finish_early,
		name_file)

	olt.exd_bert_set_heartbeat(len(onu))
	for i in range(0, number_curves):
		# reset system
		logger_exd_pon.info('Start upstream BERT curve: ' + str(i))
		logger_exd_pon.info('Resetting system ...')
		olt.exd_core_reset()
		for onu_iter in onu:
			onu_iter.exd_core_reset()

		return_bert_curve = upstream_bert_curve(
			olt,
			len(onu),
			optatt,
			att_init,
			att_step,
			att_end,
			target_ber,
			confidence_level,
			finish_early)

		# save curve to a file
		name_file_index = name_file + str(i)
		logger_exd_pon.info('Saving curve ' + str(i) +
							' to file:' + name_file_index)
		save_up_curve_csv(len(onu), return_bert_curve, name_file_index)


def upstream_bert_curve(olt, number_onu, optatt, att_init, att_step,
						att_end, target_ber, confidence_level, finish_early, bits_packet=48):
	att_vector = []
	time_start_vector = []
	time_stop_vector = []
	date_start_vector = []
	bit_matrix = []
	error_matrix = []
	ber_matrix = []
	ploss_matrix = []
	ploss_ratio_matrix = []

	att_value = att_init
	finished_curve = 0

	optatt.set_initial_config()
	optatt.set_wavelength(1270)

	while(not finished_curve):
		optatt.set_attenuation(att_value)
		att_vector.append(att_value)

		current_data_time = datetime.datetime.now()
		time_start_vector.append(str(current_data_time.time()))
		date_start_vector.append(str(current_data_time.date()))

		logger_exd_pon.info(
			'Starting point for attenuation: ' + str(att_value))

		# BER point
		return_bert_point = upstream_bert_point(
			olt, number_onu, target_ber, confidence_level)

		# Calculate Actual BER / Ploss Ratio
		ploss_ratio_matrix.append([])
		ber_matrix.append([])
		for i in range(0, number_onu):
			calculated_ber = (return_bert_point[0][i] / return_bert_point[1][i]) if(return_bert_point[0][i]) else (1 / return_bert_point[1][i])
			ber_matrix[-1].append(calculated_ber)
			calculated_ploss_ratio = (return_bert_point[2][i] / ((return_bert_point[1][i]) / bits_packet)) if(return_bert_point[2][i]) else (1 / ((return_bert_point[1][i]) / bits_packet))
			ploss_ratio_matrix[-1].append(calculated_ploss_ratio)

		# Append values to return
		error_matrix.append(return_bert_point[0])
		bit_matrix.append(return_bert_point[1])
		ploss_matrix.append(return_bert_point[2])

		logger_exd_pon.info('Point attenuation: ' + str(att_value) + ' - finished')

		# Check if test is finished
		if(finish_early):
			finished_curve = 1
			for i in range(0, number_onu):
				if(ber_matrix[-1][i] > target_ber):
					finished_curve = 0

		if(att_value >= 20 and att_step > 0):
			finished_curve = 1
		elif(att_value <= 0 and att_step < 0):
			finished_curve = 1
		elif(att_value == att_end):
			finished_curve = 1

		current_data_time = datetime.datetime.now()
		time_stop_vector.append(str(current_data_time.time()))
		# Increment attenuation
		att_value += att_step

	return [att_vector, time_start_vector, time_stop_vector, date_start_vector,
			error_matrix, bit_matrix, ber_matrix, ploss_matrix, ploss_ratio_matrix]


def upstream_bert_point(olt, number_onu, target_ber, confidence_level):
	num_max_bits = 1.0 / target_ber
	finished_point = 0
	error_vector = [0] * number_onu
	bit_vector = [0] * number_onu
	ploss_vector = [0] * number_onu
	onu_finished = [0] * number_onu

	# clear bert
	olt.exd_bert_clear()
	while (not finished_point):

		# latch bert
		olt.exd_bert_latch()

		# read error/bit vector
		for i in range(0, number_onu):
			olt.exd_bert_select_channel(i)
			error_vector[i] = olt.exd_read_error_sum()
			bit_vector[i] = olt.exd_read_bit_sum()
			ploss_vector[i] = olt.exd_read_ploss_sum()

		# test if point is finished for each ONU
		for i in range(0, number_onu):
			if (not confidence_level):	# confidence level is zero (it is, not being taken into account)
				onu_finished[i] = 1 if(bit_vector[i] > num_max_bits) else 0
			else:  # take confidence level into account
				# for more than 100 errors, no CL is taken into account
				if((error_vector[i]) > 100):
					onu_finished[i] = 1 if(bit_vector[i] > num_max_bits) else 0
				# for more than 10 errors, CL is taken for 10 errors
				elif(error_vector[i] > 10):
					cl = cl_calc(bit_vector[i], 10, target_ber)
					onu_finished[i] = 1 if(cl >= confidence_level) else 0
				else:  # for less than 10 errors, calculate actual confidence level
					cl = cl_calc(bit_vector[i], error_vector[i], target_ber)
					onu_finished[i] = 1 if(cl >= confidence_level) else 0

		finished_point = 1
		for i in range(0, number_onu):
			if(not onu_finished[i]):
				finished_point = 0

	return [error_vector, bit_vector, ploss_vector]

# Upstream bert self-consistency checking
def upstream_bert_check(olt, onu, optatt, attenuation):

	optatt.set_attenuation(attenuation)

	inserted_errors = []
	detected_errors = []
	test_ok = 1

	olt.exd_bert_clear()

	logger_exd_pon.info('Inserting Errors ... ')
	for current_onu in onu:
		inserted_errors.append(randint(0, 60))
		for i in range(0,inserted_errors[-1]):		
			current_onu.exd_tx_inject_error()

	logger_exd_pon.info('Latching Errors ... ')		   
	olt.exd_bert_latch()
	for i in range(0,len(onu)):	 
		logger_exd_pon.info("Inserted " + str(inserted_errors[i]) + " errors for ONU " + str(i))
		olt.exd_bert_select_channel(i)
		detected_errors.append(olt.exd_read_error_sum())
		logger_exd_pon.info("Detected " + str(detected_errors[-1]) + " errors for ONU " + str(i))
		if (detected_errors[-1] != inserted_errors[i]):
			test_ok = 0
			logger_exd_pon.warn('Test failed for ONU ' + str(i))

	if (not test_ok):
		logger_exd_pon.warn('BERT UPSTREAM TEST FAILED')
	else:
		logger_exd_pon.info('BERT UPSTREAM TEST PASSED')

# -------------------------------------------------------------
#  -------- Bidirectional BERT (Upstream/Downstream) -------
# -------------------------------------------------------------
def bidirectional_bert(olt, onu, onu_config, optatt, number_curves, att_init, att_step,
				  att_end, target_ber, confidence_level, finish_early, limit_up_ndown, name_file, enable_reset=0, en_phase_shift=0):

	# save configurations that were used for BERT
	save_bert_config_csv(number_curves,len(onu),att_init,att_step,att_end,target_ber,confidence_level,finish_early,name_file + '_up')
	save_bert_config_csv(number_curves,len(onu),att_init,att_step,att_end,target_ber,confidence_level,finish_early,name_file + '_down')

	olt.exd_bert_set_heartbeat(len(onu))
	for i in range(0, number_curves):
		# reset system
		optatt.set_attenuation(att_init)
		logger_exd_pon.info('Start bidirectional BERT curve: ' + str(i))
		logger_exd_pon.info('Resetting system ...')
		if(enable_reset):		
			olt.exd_core_reset()
			for onu_iter in onu:
				onu_iter.exd_core_reset()

		if(en_phase_shift):
			if(i<128): 		
				onu[1].tx_pi_phase_controller(1,8)
			else: 		
				onu[1].tx_pi_phase_controller(0,8)

		time.sleep(3)
		olt.network_init(onu_config)
		time.sleep(5)

		return_bert_curve = bidirectional_bert_curve(
			olt,
			onu,
			optatt,
			att_init,
			att_step,
			att_end,
			target_ber,
			confidence_level,
			finish_early,
			limit_up_ndown
			)

		# save curve to a file
		name_file_index = name_file + '_up' + str(i)
		logger_exd_pon.info('Saving curve ' + str(i) + ' to file:' + name_file_index)
		save_up_curve_csv(len(onu), return_bert_curve[0], name_file_index)

		name_file_index = name_file + '_down' + str(i)
		logger_exd_pon.info('Saving curve ' + str(i) + ' to file:' + name_file_index)
		save_down_curve_csv(len(onu), return_bert_curve[1], name_file_index)

def bidirectional_bert_curve(olt, onu, optatt, att_init, att_step,
						att_end, target_ber, confidence_level, finish_early, limit_up_ndown, bits_packet=48):

	number_onu = len(onu)

	# common (up/dn)
	att_vector = []
	time_start_vector = []
	time_stop_vector = []
	date_start_vector = []

	# upstream bert
	up_bit_matrix = []
	up_error_matrix = []
	up_ber_matrix = []
	up_ploss_matrix = []
	up_ploss_ratio_matrix = []

	# downstream bert
	dn_bit_matrix = []
	dn_error_matrix = []
	dn_ber_matrix = []
	dn_serror_correc_matrix = []
	dn_derror_correc_matrix = []
	
	att_value = att_init
	finished_curve = 0

	optatt.set_initial_config()
	if (limit_up_ndown) : optatt.set_wavelength(1270)
	else : optatt.set_wavelength(1577)

	# Change this for MLINK DSG
	#olt.exd_bert_select_channel(1)

	while(not finished_curve):
		optatt.set_attenuation(att_value)
		att_vector.append(att_value)

		current_data_time = datetime.datetime.now()
		time_start_vector.append(str(current_data_time.time()))
		date_start_vector.append(str(current_data_time.date()))

		logger_exd_pon.info(
			'Starting point for attenuation: ' + str(att_value))

		if (limit_up_ndown):
			###### BEGIN Upstream limiting ######
			# clear downstream bert
			for i in range(0, len(onu)):
				onu[i].exd_bert_clear()
			
			### begin upstream bert ###
			# BER point
			return_bert_point = upstream_bert_point(
				olt, number_onu, target_ber, confidence_level)
			
			# Calculate Actual BER / Ploss Ratio
			up_ploss_ratio_matrix.append([])
			up_ber_matrix.append([])
			for i in range(0, number_onu):
				calculated_ber = (return_bert_point[0][i] / return_bert_point[1][i]) if(return_bert_point[0][i]) else (1 / return_bert_point[1][i])
				up_ber_matrix[-1].append(calculated_ber)
				calculated_ploss_ratio = (return_bert_point[2][i] / ((return_bert_point[1][i]) / bits_packet)) if(return_bert_point[2][i]) else (1 / ((return_bert_point[1][i]) / bits_packet))
				up_ploss_ratio_matrix[-1].append(calculated_ploss_ratio)
			
			# Append values to return
			up_error_matrix.append(return_bert_point[0])
			up_bit_matrix.append(return_bert_point[1])
			up_ploss_matrix.append(return_bert_point[2])
			
			# Check if test is finished
			if(finish_early):
				finished_curve = 1
				for i in range(0, number_onu):
					if(up_ber_matrix[-1][i] > target_ber):
						finished_curve = 0
			### end upstream bert ###

			### begin downstream bert ###
			# latch bert
			for i in range(0, len(onu)):
				onu[i].exd_bert_latch()

			dn_ber_matrix.append([])
			dn_error_matrix.append([])
			dn_serror_correc_matrix.append([])	
			dn_derror_correc_matrix.append([])				
			dn_bit_matrix.append([])

			# read error/bit vector + calculate BER
			for i in range(0, len(onu)):
				[rx40_locked, phase_good, rx_locked, operational, mgt_tx_ready, mgt_rx_ready, mgt_txpll_lock, mgt_rxpll_lock] = onu[i].exd_read_onu_status()
				if(not rx_locked):
					dn_error_matrix[-1].append(999)
					dn_serror_correc_matrix[-1].append(999)
					dn_derror_correc_matrix[-1].append(999)
					dn_bit_matrix[-1].append(999)				
				else:				
					dn_error_matrix[-1].append(onu[i].exd_read_error_sum())
					dn_serror_correc_matrix[-1].append(onu[i].exd_read_serror_correc_sum())
					dn_derror_correc_matrix[-1].append(onu[i].exd_read_derror_correc_sum())
					dn_bit_matrix[-1].append(onu[i].exd_read_bit_sum())
				calculated_ber = (dn_error_matrix[-1][-1] / dn_bit_matrix[-1][-1]) if(dn_error_matrix[-1][-1]) else (1 / dn_bit_matrix[-1][-1])
				dn_ber_matrix[-1].append(calculated_ber)


			### end downstream bert ###
			###### END Upstream limiting ######
		else:
			###### BEGIN Downstream limiting ######
			# clear upstream bert
			olt.exd_bert_clear()
			
			### begin downstream bert ###
			# BER point
			return_bert_point = downstream_bert_point(onu, target_ber, confidence_level)
			
			dn_ber_matrix.append([])
			# Calculate Actual BER
			for i in range(0, len(onu)):
				calculated_ber = (return_bert_point[0][i] / return_bert_point[1][i]) if(return_bert_point[0][i]) else (1 / return_bert_point[1][i])
				dn_ber_matrix[-1].append(calculated_ber)

			# Append values to return
			dn_error_matrix.append(return_bert_point[0])
			dn_bit_matrix.append(return_bert_point[1])
			dn_serror_correc_matrix.append(return_bert_point[2])
			dn_derror_correc_matrix.append(return_bert_point[3])
				
			# Check if test is finished
			if(finish_early):
				finished_curve = 1
				for i in range(0, len(onu)):
					if(dn_ber_matrix[-1][i] > target_ber):
						finished_curve = 0
			### end downstream bert ###
			
			### begin upstream bert ###
			# latch bert
			olt.exd_bert_latch()

			# Calculate Actual BER / Ploss Ratio
			up_ploss_ratio_matrix.append([])
			up_ber_matrix.append([])
			up_ploss_matrix.append([])
			up_bit_matrix.append([])
			up_error_matrix.append([])
			
			for i in range(0, number_onu):
				# change this for Mlink			
				olt.exd_bert_select_channel(i)
				up_error_matrix[-1].append(olt.exd_read_error_sum())
				up_bit_matrix[-1].append(olt.exd_read_bit_sum())
				up_ploss_matrix[-1].append(olt.exd_read_ploss_sum())
				calculated_ber = (up_error_matrix[-1][-1] / up_bit_matrix[-1][-1]) if(up_error_matrix[-1][-1]) else (1 / up_bit_matrix[-1][-1])
				up_ber_matrix[-1].append(calculated_ber)
				calculated_ploss_ratio = (up_ploss_matrix[-1][-1]/ ((up_bit_matrix[-1][-1]) / bits_packet)) if(up_ploss_matrix[-1][-1]) else (1 / ((up_bit_matrix[-1][-1]) / bits_packet))
				up_ploss_ratio_matrix[-1].append(calculated_ploss_ratio)
			### end downstream bert ###
			###### END Downstream limiting ######

		logger_exd_pon.info('Point attenuation: ' + str(att_value) + ' - finished')

		if(att_value >= 25 and att_step > 0):
			finished_curve = 1
		elif(att_value <= 0 and att_step < 0):
			finished_curve = 1
		elif(att_value == att_end):
			finished_curve = 1

		current_data_time = datetime.datetime.now()
		time_stop_vector.append(str(current_data_time.time()))

		# Increment attenuation
		att_value += att_step

	return [
			[att_vector, time_start_vector, time_stop_vector, date_start_vector,
			up_error_matrix, up_bit_matrix, up_ber_matrix, up_ploss_matrix, up_ploss_ratio_matrix],
			[att_vector, time_start_vector, time_stop_vector, date_start_vector,
			dn_error_matrix, dn_bit_matrix, dn_ber_matrix, dn_serror_correc_matrix, dn_derror_correc_matrix]		 
		   ]

# -------------------------------------------------------------
#  ------------------------- Latency -------------------------
# -------------------------------------------------------------
def save_latency_curve_csv(latency_vector, name_file):
	with open(name_file + '.csv', 'w') as csvfile:
		fieldnames = ['latency']

		for i in range(0, len(fieldnames)):
			# format 20 spaces for each value
			fieldnames[i] = '%20s' % fieldnames[i]

		writer = csv.DictWriter(
			csvfile,
			fieldnames=fieldnames,
			delimiter=',',
			lineterminator='\n')
		writer.writeheader()

		for j in range(0, len(latency_vector)):
			dict_write = {fieldnames[0]: ("%20.16f" % latency_vector[j])}
			writer.writerow(dict_write)


def run_latency_test_avg(olt, onu, reset_olt, reset_onu, scope, nb_resets, time_averaging, aver_ncurrent, name_file):
	latency_vector = latency_test_avg(olt, onu, reset_olt, reset_onu, scope, nb_resets, time_averaging, aver_ncurrent)
	save_latency_curve_csv(latency_vector, name_file)


def latency_test_avg(olt, onu, reset_olt, reset_onu, scope, nb_resets, time_averaging, aver_ncurrent):

	latency_avg_vector = []

	# OLT transmit counter pattern
	olt.exd_tx_cntr()

	for i in range(0, nb_resets):
		if (reset_olt):
			olt.exd_core_reset()
		if (reset_onu):
			for onu_loop in onu:
				onu_loop.exd_core_reset()

		time.sleep(0.5)
		
		scope.trig_run()
		scope.meas_clear()
		scope.meas_edge_to_edge('CHAN4', 'CHAN2', 'RIS', 'RIS')

		time.sleep(time_averaging)

		scope.trig_stop()
		meas_res = scope.meas_get()
		meas_res = meas_res.split(',')

		# Measure current value
		if (aver_ncurrent): avg_lat=float(meas_res[4])
		else: avg_lat=float(meas_res[1])

		latency_avg_vector.append(avg_lat)

		logger_exd_pon.info('Point: ' + str(i) + ' is finished')
	return latency_avg_vector

def latency_monitoring_on_the_fly(olt,onu):
	plt.ion()
	plt.subplot(2,1,1)	
	plt.axis([0,120,-1.0, 1.0])
	plt.ylabel('Variation Skew(ns)')
	plt.title('RT monitoring on-the-fly tool test')
	plt.subplot(2,1,2)	
	plt.axis([0,120,-0.15, 0.15])
	plt.ylabel('Variation Differential Skew(ns)')
	plt.xlabel('Time(s)')	
	olt.calib_config(1,0,2)
	olt.calib_config(0,0,2)

	program_fmc.set_phase_si5344(onu[0],1,23)
	time.sleep(0.5)
	onu[0].exd_core_reset()
	time.sleep(0.2)
	[fine, coarse] = olt.measpos_onu(3,3)
	[fine, coarse] = olt.measpos_onu(3,3)			
	rt0 = coarse * 8.33 + fine*0.104
	t0 = time.time()

	rt_prev			= rt0
	t_prev			= t0
	phase_norm_prev = 0
	rt				= rt0
	t				= t0
	phase_norm		= 0
	n_avg = 1


	while(1):
		phase_sweep_part1 = list(range(23,33))
		phase_sweep_part2 = list(range(33,13,-1))
		phase_sweep_part3 = list(range(13,33))
		#phase_sweep_part4 = list(range(100,110,2))
		#phase_sweep_part5 = list(range(110,90,-2))
		#phase_sweep_part6 = list(range(90,100,2))	
		#phase_sweep_part7	 = [100] * 20
		#phase_sweep_part8 = []		
		#for alf in range(0,100):		
		#	 phase_sweep_part8.append(randint(90,110))
		phase_sweep_part9	= [33] * 20		
		phase_list = phase_sweep_part1
		phase_list.extend(phase_sweep_part2)
		phase_list.extend(phase_sweep_part3)
		#phase_list.extend(phase_sweep_part4)
		#phase_list.extend(phase_sweep_part5)
		#phase_list.extend(phase_sweep_part6)
		#phase_list.extend(phase_sweep_part7)
		#phase_list.extend(phase_sweep_part8)
		#phase_list.extend(phase_sweep_part9)
		
		for phase in phase_list:
			program_fmc.set_phase_si5344(onu[0],1,phase)
			time.sleep(0.5)			
			onu[0].exd_core_reset()
			time.sleep(0.5)
			[fine, coarse] = olt.measpos_onu(2,3)
			[fine, coarse] = [0,0]			
			for j in range(0,n_avg):			
				[fine_single, coarse_single] = olt.measpos_onu(2,3)
				fine   = fine + fine_single
				coarse = coarse + coarse_single

			fine = fine / n_avg
			coarse = coarse / n_avg
			rt_prev = rt
			t_prev = t
			phase_norm_prev = phase_norm
			rt = coarse * 8.33333333 + fine*0.10416667 - rt0
			t = time.time() - t0
			phase_norm = (phase-100)*0.0730994152
			plt.subplot(2,1,1)			
			plt.scatter(t,rt, color='r')
			plt.scatter(t, phase_norm,color= 'b')
			plt.legend(['Meas. RT deviation', 'Phase Skew TX-PLL'])
			if (t>120): plt.axis([t-119,t+1,-1.0, 1.0])
			if (t<=120): plt.axis([0,120,-1.0, 1.0])				
			#plt.pause(0.001)			
			plt.subplot(2,1,2)			
			#plt.scatter(t,rt-rt_prev, color='r')
			#plt.scatter(t,phase_norm-phase_norm_prev,color= 'b')			
			plt.scatter(t,(rt-rt_prev)-(phase_norm-phase_norm_prev), color='k')
			#plt.legend(['Meas. RT deviation differential', 'Phase Skew TX-PLL differential', 'Error'])
			plt.legend(['Error in Skew Deviation'])			
			if (t>120): plt.axis([t-119,t+1,-0.15, 0.15])
			if (t<=120): plt.axis([0,120,-0.15, 0.15])				
			
			plt.pause(0.001)

# -------------------------------------------------------------
#  --------------------- SFP RX RESET -----------------------
# -------------------------------------------------------------
def save_test_sfp_reset_csv(matrix_results, name_file):
	with open(name_file + '.csv', 'w') as csvfile:
		min_vector		  = matrix_results[0]
		max_vector		  = matrix_results[1]
		avg_vector		  = matrix_results[2]
		nsamples_vector	  = matrix_results[3]
		pos_vector		  = matrix_results[4] 
		nb_errors_vector  = matrix_results[5]
		prbs_locked_vector = matrix_results[6] 
		
		fieldnames = ['reset_pos', 'sd_fal_to_ris_min', 'sd_fal_to_ris_max', 'sd_fal_to_ris_avg', 'n_avg_samples', 'nb_errors', 'prbs_locked']

		for i in range(0, len(fieldnames)):
			# format 20 spaces for each value
			fieldnames[i] = '%20s' % fieldnames[i]

		writer = csv.DictWriter(
			csvfile,
			fieldnames=fieldnames,
			delimiter=',',
			lineterminator='\n')
		writer.writeheader()

		for j in range(0, len(pos_vector)):
			dict_write = {fieldnames[0]: ("%20d" % pos_vector[j]), fieldnames[1]: ("%20.16f" % min_vector[j]),
						  fieldnames[2]: ("%20.16f" % max_vector[j]), fieldnames[3]: ("%20.16f" % avg_vector[j]), fieldnames[4]: ("%20d" % nsamples_vector[j]), fieldnames[5]: ("%20d" % nb_errors_vector[j]),
						  fieldnames[6]: ("%20d" % prbs_locked_vector[j])}

			writer.writerow(dict_write)

def test_sfp_reset(olt, onu, scope, nb_resets, time_averaging, name_file):

	# OLT transmit counter pattern
	olt.exd_tx_cntr()

	for i in range(0, nb_resets):
		avg_vector = []
		max_vector = []
		min_vector = []
		nsamples_vector = []
		pos_vector = []

	#	 olt.exd_core_reset()
	#	 for onu_loop in onu:
	#		 onu_loop.exd_core_reset()

		time.sleep(1)
		scope.meas_clear()
		for reset_pos in range(0,30):
			olt.sfp_reset(1,reset_pos)

			scope.meas_edge_to_edge('CHAN2', 'CHAN2', 'FALL', 'RIS')

			scope.trig_run()
			time.sleep(time_averaging)			

			scope.trig_stop()

			meas_res = scope.meas_get()

			scope.meas_clear()

			meas_res = meas_res.split(',')

			# Measure values
			min_vector.append(float(meas_res[2]))
			max_vector.append(float(meas_res[3]))		
			avg_vector.append(float(meas_res[4]))		
			nsamples_vector.append(float(meas_res[6]))
			pos_vector.append(reset_pos)

		save_test_sfp_reset_csv([min_vector, max_vector, avg_vector, nsamples_vector, pos_vector], (name_file+str(i)))
		logger_exd_pon.info('Point: ' + str(i) + ' is finished')
	return 1
	
def test_sfp_reset2(olt, nb_resets, time_averaging, name_file):

	# OLT transmit counter pattern
	olt.exd_tx_cntr()

	for i in range(0, nb_resets):
		avg_vector = []
		max_vector = []
		min_vector = []
		nsamples_vector = []
		pos_vector = []

		nb_errors_vector = []
		prbs_locked_vector = []

	#	 olt.exd_core_reset()
	#	 for onu_loop in onu:
	#		 onu_loop.exd_core_reset()

		#time.sleep(1)
		# scope.meas_clear()
		for reset_pos in range(0,60):
			olt.sfp_reset(1,reset_pos)

			#scope.meas_edge_to_edge('CHAN2', 'CHAN2', 'FALL', 'RIS')

			#scope.trig_run()
			olt.sfp_sd_clear()
			
			# time.sleep(time_averaging)			

			# scope.trig_stop()
			# 
			# meas_res = scope.meas_get()
			# 
			# scope.meas_clear()
			# 
			# meas_res = meas_res.split(',')

			meas_res = olt.get_sfp_sd_stat()

			olt.bert_config(1, 2)
			olt.bert_clear()
			bert_status = [0, 0, 0, 0, 0, 0]

			# while BERT is not finished
			while((not bert_status[0]) or (not bert_status[3])):
				# bert_status returns: (onu1_finished,onu1_locked,onu1_errors,onu2_finished,onu2_locked,onu2_errors)
				bert_status = olt.bert_read()
			
			if(bert_status[1] and bert_status[4]):
				prbs_locked_vector.append(1)
			else:
				prbs_locked_vector.append(0)

			nb_errors_vector.append(bert_status[2]) #+ bert_status[5])

			# Measure values
			min_vector.append(float(meas_res[0]))
			max_vector.append(float(meas_res[1]))		
			avg_vector.append(float(meas_res[2]))		
			nsamples_vector.append(1000)
			pos_vector.append(reset_pos)

		save_test_sfp_reset_csv([min_vector, max_vector, avg_vector, nsamples_vector, pos_vector, nb_errors_vector, prbs_locked_vector], (name_file+str(i)))
		logger_exd_pon.info('Point: ' + str(i) + ' is finished')
	return 1

def test_sfp_reset3(olt, nb_resets, optatt, time_averaging, name_file):

	number_onu = 4

	# OLT transmit counter pattern
	olt.exd_tx_cntr()

	for i in range(0, nb_resets):
		avg_vector = []
		max_vector = []
		min_vector = []
		nsamples_vector = []
		pos_vector = []

		nb_errors_vector = []
		prbs_locked_vector = []

	#	 olt.exd_core_reset()
	#	 for onu_loop in onu:
	#		 onu_loop.exd_core_reset()

		time.sleep(1)
		# scope.meas_clear()
		for reset_pos in range(0,30):
			olt.sfp_reset(1,reset_pos)

			#scope.meas_edge_to_edge('CHAN2', 'CHAN2', 'FALL', 'RIS')

			#scope.trig_run()
			olt.sfp_sd_clear()
			
			#BERT
			olt.exd_bert_set_heartbeat(number_onu)

			# reset system
			logger_exd_pon.info('Start upstream BERT curve: ' + str(reset_pos))
			
			return_bert_curve = upstream_bert_curve(
				olt,
				number_onu,
				optatt,
				10,			# att_init
				0,		   # att_step
				10,			 # att_end
				1e-10,		 # target_ber
				0, # cl
				1)	   # finish_early
			
			# save curve to a file
			name_file_index = name_file + 'bert' + str(reset_pos)

			save_up_curve_csv(number_onu, return_bert_curve, name_file_index)

			meas_res = olt.get_sfp_sd_stat()

			olt.bert_config(1, 2)
			olt.bert_clear()
			bert_status = [0, 0, 0, 0, 0, 0]

			# while BERT is not finished
			while((not bert_status[0]) or (not bert_status[3])):
				# bert_status returns: (onu1_finished,onu1_locked,onu1_errors,onu2_finished,onu2_locked,onu2_errors)
				bert_status = olt.bert_read()
			
			if(bert_status[1] and bert_status[4]):
				prbs_locked_vector.append(1)
			else:
				prbs_locked_vector.append(0)

			nb_errors_vector.append(bert_status[2]) #+ bert_status[5])

			# Measure values
			min_vector.append(float(meas_res[0]))
			max_vector.append(float(meas_res[1]))		
			avg_vector.append(float(meas_res[2]))

			nsamples_vector.append(1000)
			pos_vector.append(reset_pos)

		save_test_sfp_reset_csv([min_vector, max_vector, avg_vector, nsamples_vector, pos_vector, nb_errors_vector, prbs_locked_vector], (name_file+str(i)))
		logger_exd_pon.info('Point: ' + str(i) + ' is finished')
	return 1

def test_cdc_onu_rx2tx(onu):
	for i in range(0,127):
		onu.exd_pll_shift(1,i)
		time.sleep(2)
		print("DELAY:" + str(i) + "	 /	PHASE_GOOD:" + str((onu.read_ctrl_reg(1024+3)&0x00000002)>>1))

def test_cdc_onu2olt(olt, onu):
	for i in range(0,127):
		onu.exd_pll_shift(2,i)
		time.sleep(1)
		olt.write_ctrl_reg(1024+0,2)
		olt.write_ctrl_reg(1024+0,0)		
		time.sleep(0.5)
		print("DELAY:" + str(i) + "	 /	PHASE_GOOD:" + str((olt.read_ctrl_reg(1024+3)&0x00000008)>>3) + "  /  PHASE_GOOD_STICKY:" + str((olt.read_ctrl_reg(1024+3)&0x00080000)>>19)
		+ "	 /	LEAD_NLAG:" + str((olt.read_ctrl_reg(1024+3)&0x00000010)>>4)+ "	 /	LEAD_NLAG_STICKY:" + str((olt.read_ctrl_reg(1024+3)&0x00100000)>>20))
