#!/usr/bin/env python

import socket
import logging

# -------------------------------------------------------------
#  -------------- Class DriverComm (driver-layer) ------------
# -------------------------------------------------------------


class DriverComm:

    def __init__(self, ip='192.168.1.2', port=9001):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((ip, port))
        self.logger = logging.getLogger('ttc_pon')
        self.logger.debug('Opening socket at port:' + str(port) + ", ip:" + ip)

    def send(self, data):
        data=data.encode('utf-8')
        self.s.send(data)

    def recv(self):
        r = ' '
        data = ''
        while r != '\n':
            data += self.s.recv(4 * 1024)
            r = data[-1]
        return data[:-1]

    def query(self, data):
        self.send(data)
        return self.s.recv(4096)

    # msg is a list of integers which will be converted to bytes
    def send_bytes(self, msg):
        msg_bytes = bytes(msg)
        self.s.send(msg_bytes)

    # msg is a list of integers which will be converted to bytes
    # returns list of integers
    def query_bytes(self, msg):
        self.send_bytes(msg)
        data_return =  self.s.recv(4096)
        data_ret_int = [i for i in data_return]		
        return data_ret_int