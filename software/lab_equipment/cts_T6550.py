#!/usr/bin/env python

from . driver_comm import DriverComm

# -------------------------------------------------------------
#  ------------------------- Class CTS ------------------------
# -------------------------------------------------------------


class CTS(DriverComm):

	def __init__(self, ip, port):
		DriverComm.__init__(self, ip, port, 1)

	def set_time(self, DD, MM, YY, hh, mm, ss):
		msg = 't' + DD + MM + YY + hh + mm + ss
		msg = self.query(msg).decode("utf-8")
		return msg[1:3], msg[3:5], msg[5:7], msg[7:9], msg[9:11], msg[11:]
	
	def read_time(self):
		msg = self.query('T').decode("utf-8")
		return msg[1:3], msg[3:5], msg[5:7], msg[7:9], msg[9:11], msg[11:]
	
	def set_digital_channel_chamber(self, channel, state):
		msg = 's' + str(channel) + ' ' + str(state)
		return self.query(msg).decode("utf-8")
	
	def read_chamber_state(self):
		return self.query('S').decode("utf-8")
		
	def set_program_state(self, program):
		msg = 'p' + program
		return self.query(msg).decode("utf-8")

	def read_program_info(self, command):
		msg = 'p' + command
		return self.query(msg).decode("utf-8")
		
	def read_program_state(self):
		return self.query('P').decode("utf-8")
		
	def read_all_error_text(self, command):
		msg = 'H' + str(command)
		return self.query(msg).decode("utf-8")
		
	def read_error_text(self):
		return self.query('F').decode("utf-8")
		
	def set_keyboard_lock(self, state):
		msg = 'l' + str(state)
		return self.query(msg).decode("utf-8")
		
	def read_keyboard_lock(self):
		return self.query('L').decode("utf-8")
	
	def set_additional_digital_channel(self, channel, value):
		msg = 'o0' + str(channel) + ' ' + value
		return self.query(msg).decode("utf-8")
	
	def read_additional_digital_channel(self):
		return self.query('O').decode("utf-8")
	
	def set_analog_channel(self, channel, value):
		msg = 'a' + str(channel) + ' ' + value
		self.query(msg)
		#return self.query(msg).decode("utf-8")
	
	def read_analog_channel(self, channel):
		msg = 'A' + str(channel)
		return self.query(msg).decode("utf-8")
			
	def set_gradient_ramp_up(self, channel, value):
		msg = 'u' + str(channel) + ' ' + value
		return self.query(msg).decode("utf-8")
	
	def set_gradient_ramp_down(self, channel, value):
		msg = 'd' + str(channel) + ' ' + value
		return self.query(msg).decode("utf-8")
		
	def read_adjusted_gradient(self, channel):
		msg = 'U' + str(channel)
		return self.query(msg).decode("utf-8")
	
	def read_adjested_final_ramp(self, channel):
		msg = 'E' + str(channel)
		return self.query(msg).decode("utf-8")
	
	def read_ramp_channel(self, channel):
		msg = 'R' + str(channel)
		return self.query(msg).decode("utf-8")