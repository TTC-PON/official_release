#!/usr/bin/env python

from . visa_comm import VisaComm
import time

# ---------------------------------------------------------------------------
#  ----------------------- Class Lightwave Multimeter -----------------------
# ---------------------------------------------------------------------------

# Commands to this instrument can be found under: https://literature.cdn.keysight.com/litweb/pdf/08164-90B65.pdf?id=118032

class lightwave_multimeter(VisaComm):
	def __init__(self, rm_visa, str_addr):
		VisaComm.__init__(self, rm_visa, str_addr)
		
	# Instrument Interface Commands
	def get_id(self):
		stat = self.query('*IDN?')
		return stat
	
	#Ask for the power unit
	def get_pwr_unit(self):
		pwr_unit=self.query('SENS2:CHAN1:POW:UNIT?')
		pwr_un=float(pwr_unit)
		if (pwr_un!=0):
			pwr_response='Watts'
		else:	
			pwr_response='dBm'
		return pwr_response	
	
	#Set the power unit (value = 0 for dBm, value = 1 for Watts)
	def set_pwr_unit(self,value):
		self.send('SENS2:CHAN1:POW:UNIT'+' '+str(value))

	#Set the wavelength (the value given as input is intended in nanometer) 
	#P.S: Check which are the maximum and minimum values programmable
	def set_wave_length(self,value):
		self.send('SENS2:CHAN1:POW:WAV '+str(value)+'nm')
	
	#Ask for the result of the power measurement
	def get_power(self):
		power=self.query('fetc2:pow?')	
		return power
	
	#Zeroing all powermeter channels
	def zeroing(self):
		self.send('OUTP1:CORR:COLL:ZERO:ALL')
		time.sleep(24)
		