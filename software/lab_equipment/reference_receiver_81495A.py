#!/usr/bin/env python

from . visa_comm import VisaComm
import time
import datetime
import os
import csv

# ---------------------------------------------------------------------------
#  ----------------------- Class Lightwave Multimeter -----------------------
# ---------------------------------------------------------------------------

# Commands to this instrument can be found under: https://literature.cdn.keysight.com/litweb/pdf/08164-90B65.pdf?id=118032

class ReferenceReceiver(VisaComm):
    def __init__(self, rm_visa, str_addr):
        VisaComm.__init__(self, rm_visa, str_addr)
        
    # Instrument Interface Commands
    def get_id(self):
        stat = self.query('*IDN?')
        return stat

    #Ask for the power unit
    def get_pwr_unit(self):
        pwr_unit=self.query('SENS2:CHAN1:POW:UNIT?')
        pwr_un=float(pwr_unit)
        if (pwr_un!=0):
            pwr_response='W'
        else:    
            pwr_response='dBm'
        return pwr_response    
    
    #Set the power unit (value = 0 for dBm, value = 1 for Watts)
    def set_pwr_unit(self,value):
        self.send('SENS2:CHAN1:POW:UNIT'+' '+str(value))

    #Set the wavelength (the value given as input is intended in nanometer) 
    #P.S: Check which are the maximum and minimum values programmable
    def set_wave_length(self,value):
        self.send('SENS2:CHAN1:POW:WAV '+str(value)+'nm')
    
    #Ask for the result of the power measurement
    def get_power(self):
        power= float(self.query('fetc2:pow?'))    
        return power
    
    def save_power(self, name_file, temperature, iteration=0):
        power_unit = self.get_pwr_unit()

        # Create Formatted Vector
        fieldnames = ['%20s' % 'iteration','%20s' % 'temperature','%20s' % 'date','%20s' % 'time', '%20s' % ('power_' + power_unit )]

        power = self.get_power()
        date_time = datetime.datetime.now()
        measurements = { '%20s' % 'iteration'                  : '%20d' % iteration,
						 '%20s' % 'Chamber temperature'        : '%20d' % temperature,
                         '%20s' % 'date'                       : '%20s' % date_time.date(),
                         '%20s' % 'time'                       : '%20s' % date_time.time(),                         
                         '%20s' % ('power_' + power_unit )     : '%20.10f' % power}

        # Save Measurement
        # Check if file exists 
        file_exists = os.path.isfile(name_file + '.csv')  
        with open(name_file + '.csv', 'a') as csvfile:
            writer = csv.DictWriter(
                csvfile,
                fieldnames=fieldnames,
                delimiter=',',
                lineterminator='\n')
            if (not file_exists) : writer.writeheader()
            writer.writerow(measurements)

        return power