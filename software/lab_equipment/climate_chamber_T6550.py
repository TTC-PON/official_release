#!/usr/bin/env python

from . cts_T6550 import CTS
import time

# -------------------------------------------------------------
#  ------------------------- Class CTS ------------------------
# -------------------------------------------------------------


class ClimateChamber(CTS):

	def __init__(self, ip, port):
		CTS.__init__(self, ip, port)

	def format_value(self, value):
		if (isinstance(value, int)):
			value = float(value)
		if (isinstance(value, float)):
			result = str('%05.1f' % value)
			return result
		else:
			return "Not number"
		
	def config (self, up, down):
		DD, MM, YY, hh, mm, ss = self.read_time()
		print("Time: {}.{}.20{} {}:{}:{}".format(DD, MM, YY, hh, mm, ss))
		#hh = str(int(hh)+1)
		DD, MM, YY, hh, mm, ss = self.set_time(DD, MM, YY, hh, mm, ss)
		print("Set Time: {}.{}.20{} {}:{}:{}".format(DD, MM, YY, hh, mm, ss))
		print("Chamber: {}".format(self.read_chamber_state()))
		print("Program: {}".format(self.read_program_state()))
		print("Keyboard: {}".format(self.read_keyboard_lock()))
		print("Digit: {}".format(self.read_additional_digital_channel()))
		print("Analog 0: {}".format(self.read_analog_channel(0)))
		print("Adjusted Gradient 0: {}".format(self.read_adjusted_gradient(0)))
		print("Adjusted Final Ramp 0: {}".format(self.read_adjested_final_ramp(0)))
		print("Ramp 0: {}".format(self.read_ramp_channel(0)))
		print("Set Up 0: {}".format(self.set_gradient_up(up)))
		print("Set Down 0: {}".format(self.set_gradient_down(down)))
		print("Ramp 0: {}".format(self.read_ramp_channel(0)))
		print("Analog 0: {}".format(self.read_analog_channel(0)))
		
	def start(self):
		return self.set_digital_channel_chamber(1, 1)
		
	def stop(self):
		return self.set_digital_channel_chamber(1, 0)
		
	def set_temperature(self, value):
		command = self.format_value(value)
		self.set_analog_channel(0, command)
	
	def get_temperature(self):
		stat =  self.read_analog_channel(0)
		return stat[3:8]
	
	def set_gradient_up(self, value):
		command = self.format_value(value)
		self.set_gradient_ramp_up(0, command)

	def get_gradient_up(self):
		stat =  self.read_adjusted_gradient(0)
		return float(stat[3:8])
	
	def set_gradient_down(self, value):
		command = self.format_value(value)
		self.set_gradient_ramp_down(0, command)
		
	def get_gradient_down(self):
		stat =  self.read_adjusted_gradient(0)
		return float(stat[9:14])