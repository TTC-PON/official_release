#!/usr/bin/env python

import socket
import logging
import os
import csv
import time
import math
import datetime
from . driver_comm import DriverComm
from . sfp_sff8472 import SFP_SFF8472
# -------------------------------------------------------------
#  ------------------- Class IRRAD_CORE (low-layer) ---------------
# -------------------------------------------------------------


class IRRAD_CORE(DriverComm, SFP_SFF8472):

	def __init__(self, ip='127.0.0.1', port=8555, channel=0): # channel: 0=LPC, 1=HPC
		DriverComm.__init__(self, ip, port)
		SFP_SFF8472.__init__(self, 1,1,2,2,1) # OBS: This core will be used to control OLTs from GoFoton which require some scaling in the coeffiecients for Tx Bias and Tx Pwr w.r.t. SFF-8472
		self.channel = 0 

	# channel: 0=LPC, 1=HPC
	def select_channel(self, channel):
		self.channel = channel

	# IBERT control
	# Reset	
	def reset_tx(self):
		self.query('mgt_w' + str(self.channel) + ' LOGIC.TX_RESET_DATAPATH ' + '1\n')
		self.query('mgt_w' + str(self.channel) + ' LOGIC.TX_RESET_DATAPATH ' + '0\n')
		return 1

	def reset_rx(self):	 
		self.query('mgt_w' + str(self.channel) + ' LOGIC.RX_RESET_DATAPATH ' + ' 1\n')
		self.query('mgt_w' + str(self.channel) + ' LOGIC.RX_RESET_DATAPATH ' + ' 0\n')
		return 1

	# Get general status

	# BER-related
	def bert_clear(self):  
		self.query('mgt_w' + str(self.channel) + ' LOGIC.MGT_ERRCNT_RESET_CTRL ' + '1\n')
		self.query('mgt_w' + str(self.channel) + ' LOGIC.MGT_ERRCNT_RESET_CTRL ' + '0\n')
		return 1

	def bert_read_bit_sum(self):  
		return int(self.query('mgt_r' + str(self.channel) + ' RX_RECEIVED_BIT_COUNT ' + '\n'),10)

	def bert_read_error_sum(self):	
		return int(self.query('mgt_r' + str(self.channel) + ' LOGIC.ERRBIT_COUNT ' + '\n'),16)

	# Loopback
	def loopback(self, loopback=0):	 #loopback:0=none, 1=near-end pma; self.channel: 0=LPC, 1=HPC
		if(loopback==0):		
			self.query('mgt_w' + str(self.channel) + ' LOOPBACK ' + '{None}\n')
		elif(loopback==1):		  
			self.query('mgt_w' + str(self.channel) + ' LOOPBACK ' + '{Near-End PMA}\n')
		return 1

	# Pattern-related
	def pattern(self, pattern=0):  #pattern:0=PRBS-7, 1=PRBS-9, 2=PRBS-15,3=PRBS-23,4=PRBS-31,5=Fast Clk,6=Slow Clk; self.channel: 0=LPC, 1=HPC
		if(pattern==0):		   
			self.query('mgt_w' + str(self.channel) + ' TX_PATTERN ' + '{PRBS 7-bit}\n')
			self.query('mgt_w' + str(self.channel) + ' RX_PATTERN ' + '{PRBS 7-bit}\n')
		elif(pattern==1):		 
			self.query('mgt_w' + str(self.channel) + ' TX_PATTERN ' + '{PRBS 9-bit}\n')
			self.query('mgt_w' + str(self.channel) + ' RX_PATTERN ' + '{PRBS 9-bit}\n')
		elif(pattern==2):		 
			self.query('mgt_w' + str(self.channel) + ' TX_PATTERN ' + '{PRBS 15-bit}\n')
			self.query('mgt_w' + str(self.channel) + ' RX_PATTERN ' + '{PRBS 15-bit}\n')
		elif(pattern==3):		 
			self.query('mgt_w' + str(self.channel) + ' TX_PATTERN ' + '{PRBS 23-bit}\n')
			self.query('mgt_w' + str(self.channel) + ' RX_PATTERN ' + '{PRBS 23-bit}\n')
		elif(pattern==4):		 
			self.query('mgt_w' + str(self.channel) + ' TX_PATTERN ' + '{PRBS 31-bit}\n')
			self.query('mgt_w' + str(self.channel) + ' RX_PATTERN ' + '{PRBS 31-bit}\n')
		elif(pattern==5):		 
			self.query('mgt_w' + str(self.channel) + ' TX_PATTERN ' + '{Fast Clk}\n')
			self.query('mgt_w' + str(self.channel) + ' RX_PATTERN ' + '{Fast Clk}\n')
		elif(pattern==6):		 
			self.query('mgt_w' + str(self.channel) + ' TX_PATTERN ' + '{Slow Clk}\n')
			self.query('mgt_w' + str(self.channel) + ' RX_PATTERN ' + '{Slow Clk}\n')
		return 1

	def inject_error(self):	 
		self.query('mgt_w' + str(self.channel) + ' LOGIC.ERR_INJECT_CTRL ' + '0\n')
		self.query('mgt_w' + str(self.channel) + ' LOGIC.ERR_INJECT_CTRL ' + '1\n')
		self.query('mgt_w' + str(self.channel) + ' LOGIC.ERR_INJECT_CTRL ' + '0\n')
		return 1

	# SFP control
	# I2C control
	### I2C CTRL (begin) ###
	def i2c_ctrl(self, i2c_ctrl_msg):
		if(self.channel==0):
			self.query('vio_w i2c_ctrl_lpc ' + format(i2c_ctrl_msg, '08x') + '\n')
			self.query('vio_w i2c_ctrl_en_lpc 1\n')
			self.query('vio_w i2c_ctrl_en_lpc 0\n')
			return 1
		elif(self.channel==1):
			self.query('vio_w i2c_ctrl_hpc ' + format(i2c_ctrl_msg, '08x') + '\n')
			self.query('vio_w i2c_ctrl_en_hpc 1\n')
			self.query('vio_w i2c_ctrl_en_hpc 0\n')
			return 1
		else: return 0

	def i2c_stat(self):	 
		if(self.channel==0):
			return int(self.query('vio_r i2c_stat_lpc \n'),16)
		elif(self.channel==1):
			return int(self.query('vio_r i2c_stat_hpc \n'),16)
		else: return None

	def i2c_write_mem(self, slv_addr, reg_addr, reg_value):
		slv_addr_format	 = slv_addr & 0x7F
		reg_addr_format	 = reg_addr & 0xFF
		reg_value_format = reg_value & 0xFF
		if((slv_addr != slv_addr_format) | (reg_addr != reg_addr_format) | (reg_value != reg_value_format)):
			self.logger.warn('Error parameters formatting for IIC write command')		
			return 0
		else:
			message = (reg_value<<18) + (reg_addr<<10) + (slv_addr<<3) + (0<<1) + (0<<1) + (1)	
			self.i2c_ctrl(message)

		i2c_done = 0
		i2c_error = 0
		i2c_drop_req = 0
		while(i2c_done==0 and i2c_error==0 and i2c_drop_req==0):
			i2c_stat	 = self.i2c_stat()
			i2c_done	 = (i2c_stat & 0x00000001) >> 0
			i2c_error	 = (i2c_stat & 0x00000002) >> 1
			i2c_drop_req = (i2c_stat & 0x00000004) >> 2

		if(i2c_error):
			self.logger.warn('IIC write error')				
			return 0
		elif(i2c_drop_req):
			self.logger.warn('IIC drop request error')				
			return 0			
		else:
			return 1

	# Read IIC (nb_read_access can be 1 or 2 for double bursty read)
	def i2c_read_mem(self, slv_addr, reg_addr, nb_read_access=1): 
		slv_addr_format	 = slv_addr & 0x7F;
		reg_addr_format	 = reg_addr & 0xFF;
		nb_read_access	 = nb_read_access - 1
		nb_read_access_format = nb_read_access & 0x1

		if((slv_addr != slv_addr_format) | (reg_addr != reg_addr_format) | (nb_read_access_format != nb_read_access)):
			self.logger.warn('Error parameters formatting for IIC read command')		
			return [-1]
		else:
			message = (0<<18) + (reg_addr<<10) + (slv_addr<<3) + (1<<2) + (nb_read_access<<1) + (1)	
			self.i2c_ctrl(message)

		i2c_done = 0
		i2c_error = 0
		i2c_drop_req = 0
		while(i2c_done==0 and i2c_error==0 and i2c_drop_req==0):
			i2c_stat	 = self.i2c_stat()
			i2c_done	 = (i2c_stat & 0x00000001) >> 0
			i2c_error	 = (i2c_stat & 0x00000002) >> 1
			i2c_drop_req = (i2c_stat & 0x00000004) >> 2
			i2c_data	 = (i2c_stat & 0x0007FFF8) >> 3			
		if(i2c_error):
			self.logger.warn('IIC read error')				
			return [0]
		elif(i2c_drop_req):
			self.logger.warn('IIC drop request error')
			return [0]
		else:
			if(nb_read_access > 0):		
				data_return_list = [1]				
				for i in range(0,(nb_read_access+1)):
					data  = (i2c_data >> i*8) & 0xFF
					data_return_list.append(data)				
				return data_return_list
			else:
				return [1, i2c_data&0xFF]

	# Method compliant with the SFP-SFF8472 class
	def i2c_writeread(self, slv_addr, nbr_read, data_list):
		return self.i2c_read_mem(slv_addr, data_list[0], nbr_read)

	# FMC_RELATED
	def pon_fmc_select_sfp(self):  
		slv_addr = 0x74
		I2C_FMC_SEL_SFP = 0x20		
		return self.i2c_write_mem(slv_addr, I2C_FMC_SEL_SFP, I2C_FMC_SEL_SFP)

	def pon_fmc_select_leds(self):	
		slv_addr = 0x74
		I2C_FMC_SEL_LED_GPIO = 0x10			
		return self.i2c_write_mem(slv_addr, I2C_FMC_SEL_LED_GPIO, I2C_FMC_SEL_LED_GPIO)

	def pon_fmc_leds(self, on_noff):
		slv_addr = 0x38
		if(on_noff) : reg_value = 0x00
		else		: reg_value = 0xFF
		return self.i2c_write_mem(slv_addr, reg_value, reg_value)
		
	def save_sfp_parameters(self, name_file, iteration, temperature):
		# Create Formatted Vector
		fieldnames=['%20s' % 'iteration',
					'%20s' % 'Chamber temperature',	
					'%20s' % 'date',
					'%20s' % 'time',
					'%20s' % 'temperature_degC',
					'%20s' % 'vcc_V',	
					'%20s' % 'tx_bias_mA',
					'%20s' % 'tx_pwr_dBm',
					'%20s' % 'rx_pwr_dBm']
					
		date_time = datetime.datetime.now()
		temp=self.get_sfp_temp()[1]
		vcc=self.get_sfp_vcc()[1]
		tx_bias=self.get_sfp_tx_bias()[1]
		tx_pwr=self.get_sfp_tx_pwr()[1]
		rx_pwr = self.get_sfp_rx_pwr()[1]
		measurements = { '%20s' % 'iteration'			 : '%20d'   % iteration,
						 '%20s' % 'Chamber Temperature'  : '%20d'    % temperature,	
						  '%20s' % 'date'                : '%20s'    % date_time.date(),
						  '%20s' % 'time'                : '%20s'    % date_time.time(),						 
						  '%20s' % 'temperature_degC'    : '%20d'    % temp,
						  '%20s' % 'vcc_V'               : '%20d'    % vcc,			
						  '%20s' % 'tx_bias_mA'          : '%20d'    % tx_bias,
						 '%20s' % 'tx_pwr_dBm'           : '%20d'     % tx_pwr,
						 '%20s' % 'rx_pwr_dBm'           : '%20d'     % rx_pwr}		
		 # Save Measurement
		 # Check if file exists 
		file_exists = os.path.isfile(name_file + '.csv')  
		with open(name_file + '.csv', 'a') as csvfile:
			writer = csv.DictWriter(
				csvfile,
				fieldnames=fieldnames,
				delimiter=',',
				lineterminator='\n')
			if (not file_exists) : writer.writeheader()
			writer.writerow(measurements)
			
	def save_ber_parameters (self, name_file, temperature, iteration=0, attenuation=None): 
		# Create Formatted Vector
		fieldnames = ['%20s' % 'iteration',
					  '%20s' % 'Chamber temperature',	
					  '%20s' % 'date',
					  '%20s' % 'time',
					  '%20s' % 'errors',
					  '%20s' % 'bits',					  
					  '%20s' % 'ber',
					  '%20s' % 'rx_pwr_dBm']
		if (attenuation != None) : fieldnames.append('%20s' % 'attenuation_dB')
		# BER
		errors = self.bert_read_error_sum()
		bits = self.bert_read_bit_sum()
		if(errors > 0) : ber = (errors/bits)
		else : ber = (1/bits)
		# Get SFP parameters
		rx_pwr = self.get_sfp_rx_pwr()[1]
		date_time = datetime.datetime.now()
		measurements = {'%20s' % 'iteration'           : '%20d'       % iteration,
						'%20s' % 'Chamber Temperature' : '%20d'       % temperature,	
						'%20s' % 'date'                : '%20s'       % date_time.date(),
						'%20s' % 'time'                : '%20s'       % date_time.time(),						 
						'%20s' % 'errors'              : '%20d'       % errors,
						'%20s' % 'bits'                : '%20d'       % bits,			
						'%20s' % 'ber'	                : '%20.18f'   % ber,
						'%20s' % 'rx_pwr_dBm'          : '%20.10f'    % rx_pwr}

		if (attenuation != None) : measurements['%20s' % 'attenuation_dB'] = '%20.10f' % attenuation	  

		# Save Measurement
		# Check if file exists 
		file_exists = os.path.isfile(name_file + '.csv')  
		with open(name_file + '.csv', 'a') as csvfile:
			writer = csv.DictWriter(
				csvfile,
				fieldnames=fieldnames,
				delimiter=',',
				lineterminator='\n')
			if (not file_exists) : writer.writeheader()
			writer.writerow(measurements)
		
		
	# -------------Enabling and disabling ONU_Tx for the stress temperature test----------------------	
	
	def control_tx_disable (self,board=0):
		if(board==0):
			self.query('sfp_tx_dis1_onu_o 0\n')
			self.query('sfp_tx_dis2_onu_o 1\n')
			self.query('sfp_tx_dis3_onu_o 1\n')
			self.query('sfp_tx_dis4_onu_o 1\n')
			self.query('sfp_tx_dis5_onu_o 1\n')
		if(board==1):
			self.query('sfp_tx_dis1_onu_o 1\n')
			self.query('sfp_tx_dis2_onu_o 0\n')
			self.query('sfp_tx_dis3_onu_o 1\n')
			self.query('sfp_tx_dis4_onu_o 1\n')
			self.query('sfp_tx_dis5_onu_o 1\n')
		if(board==2):	
			self.query('sfp_tx_dis1_onu_o 1\n')
			self.query('sfp_tx_dis2_onu_o 1\n')
			self.query('sfp_tx_dis3_onu_o 0\n')
			self.query('sfp_tx_dis4_onu_o 1\n')
			self.query('sfp_tx_dis5_onu_o 1\n')
		if(board==3):	
			self.query('sfp_tx_dis1_onu_o 1\n')
			self.query('sfp_tx_dis2_onu_o 1\n')
			self.query('sfp_tx_dis3_onu_o 1\n')
			self.query('sfp_tx_dis4_onu_o 0\n')
			self.query('sfp_tx_dis5_onu_o 1\n')
		if(board==4):
			self.query('sfp_tx_dis1_onu_o 1\n')
			self.query('sfp_tx_dis2_onu_o 1\n')
			self.query('sfp_tx_dis3_onu_o 1\n')
			self.query('sfp_tx_dis4_onu_o 1\n')
			self.query('sfp_tx_dis5_onu_o 0\n')		
			

	# ------------- SYSMON METHODS ---------------
	def refresh_sysmon(self):		
		self.query('sysmon_refresh \n')

	def get_property_sysmon(self, property_name):			
		return float(self.query('sysmon_r ' + property_name + ' \n'))

	def save_sysmon_state(self, name_file, temperature, iteration=0):
		self.refresh_sysmon()
		property_list = ['TEMPERATURE','MAX_TEMPERATURE','MIN_TEMPERATURE',
			'VCCINT'	 ,'MAX_VCCINT'	   ,'MIN_VCCINT',	  
			'VCCAUX'	 ,'MAX_VCCAUX'	   ,'MIN_VCCAUX',	  
			'VCCBRAM'	 ,'MAX_VCCBRAM'	   ,'MIN_VCCBRAM',	  
			'VUSER0'	 ,'MAX_VUSER0'	   ,'MIN_VUSER0',	  
			'VUSER1'	 ,'MAX_VUSER1'	   ,'MIN_VUSER1',	  
			'VUSER2'	 ,'MAX_VUSER2'	   ,'MIN_VUSER2',	  
			'VUSER3'	 ,'MAX_VUSER3'	   ,'MIN_VUSER3']

		date_time = datetime.datetime.now()

		# Create Formatted Vector
		fieldnames = ['%20s' % 'iteration', '%20s' % 'Chamber temperature', '%20s' % 'date', '%20s' % 'time']
		measurements = { '%20s' % 'iteration' : '%20d' % iteration,
						 '%20s' % 'Chamber temperature' : '%20d' % temperature,
						 '%20s' % 'date'	  : '%20s' % date_time.date(),
						 '%20s' % 'time'	  : '%20s' % date_time.time()}

		for i in range(0,len(property_list)):
			fieldnames.append('%20s' % property_list[i])
			measurements[fieldnames[i+3]] = '%20.10f' % self.get_property_sysmon(property_list[i])

		# Save Measurement
		# Check if file exists	
		file_exists = os.path.isfile(name_file + '.csv')		
		with open(name_file + '.csv', 'a') as csvfile:
			writer = csv.DictWriter(
				csvfile,
				fieldnames=fieldnames,
				delimiter=',',
				lineterminator='\n')

			if (not file_exists) : writer.writeheader()

			writer.writerow(measurements)
