#!/usr/bin/env python

import time

from . visa_comm import VisaComm

# -------------------------------------------------------------
#  ----------------------- Class OptAtt ----------------------
# -------------------------------------------------------------


class JBERT(VisaComm):

    def __init__(self, rm_visa, str_addr):
        VisaComm.__init__(self, rm_visa, str_addr)

    def get_id(self):
        stat = self.query('*IDN?')
        return stat


    # state can be 0=DISABLED, 1=ENABLED
    def output_state(self, state):
        if(state==0):
            msg = ':OUTP1:CENT DISC'
            return self.send(msg)
        elif(state==1):
            msg = ':OUTP1:CENT CONN'
            return self.send(msg)

    # auto alignment process needed for rx
    # return 1 if alignment works, returns 0 if alignment does not work
    # timeout is the maximum time (in seconds - integer) that we are willing to wait for the alignment
    def auto_align(self, timeout):
        msg = ':SENS:EYE:ALIGN:AUTO ONCE'
        self.send(msg)        
        align=0
        time_start = time.time()
        total_time = 0

        while((not align) and total_time<timeout):
            align = self.auto_align_check()
            total_time = time.time() - time_start

        return align			
    # check alignment
    def auto_align_check(self):
        msg = ':SENS:EYE:ALIGN:AUTO?'
        str = self.query(msg)
        if(str=='CS_SUCCESSFUL\n'):
            return 1
        if(str=='CS_FAILED\n'):
            return 0
			
    # data_rate in b/s
    def set_data_rate(self, data_rate):
        msg = ':SOUR9:FREQ ' + str(data_rate)
        return self.send(msg)

    def get_data_rate(self):
        msg = ':FETC:SENSe9:FREQ?'
        return self.query(msg)

    # set PRBS-N pattern
    # prbsnum is the output prbs length (e.g. 7, 31)
    def set_prbs_pattern(self, prbsnum):
        msg = ':PATTern PRBS ' + str(prbsnum)
        return self.send(msg)

    def get_pattern(self):
        msg = ':PATTern?'
        return self.query(msg)

    # BER related
    def exd_bert_clear(self):
        msg = ':SENS1:GATE ON'
        return self.send(msg)

    # It seems that for JBERT each time an read_bit_sum or read_error_sum is done there is no need to latch, so we create a dummy method for this
    def exd_bert_latch(self):
        return 1

    def exd_read_error_sum(self):
        msg = ':FETC:SENS2:ECO?'
        aux = float(self.query(msg))   
        return int(aux)

    def exd_read_bit_sum(self):
        msg = ':FETC:SENS2:BCO?'
        aux = float(self.query(msg))   
        return int(aux)       

    # SYS related
    def read_error_queue(self):
        msg = ':SYSTem:Error:NEXT?'
        return self.query(msg)
