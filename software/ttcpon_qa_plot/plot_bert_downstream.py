#!/usr/bin/env python

import csv
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy
import math

# This script plots downstream BER tests of ONU's in the following format:
# [ ONU 1     ]   [ ONU 2     ] ... [ ONU X/2   ]
# [ ONU X/2+1 ]   [ ONU X/2+2 ] ... [ ONU X     ]
#
# where X=onu_per_figure

# Configuration plot
onu_per_figure = 1
xaxis_min = -30
xaxis_max = -20
yaxis_min = 1e-11
yaxis_max = 1e-2

# cfg_curve_style_est = 'r'

xaxis_scale = 'OMA (dBm)'; # possible values = 'Attenuation (dB)', 'Rx_Pwr (dBm)', 'OMA (dBm)'

# Plot Spec (not implemented here)
spec_oma = [-26.8, -22.0]
spec_ber = [1e-3, 1e-11]

# Returns worst observed sensitivity at BER given by ber_target list
def plot_bert_downstream(rx_pwr_file, extinction_ratio_file, down_bert_file, save_ndisplay, output_file, ber_target_list=[1e-3], read_config_file=1, number_onu=8, number_curves=1):   
    if read_config_file:
        # Read number_onu and number_curves from config_file
        with open(down_bert_file + '_config.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                number_onu = int(row['%20s' % 'number_onu'])
                number_curves = int(row['%20s' % 'number_curves'])

    num_figures = int(numpy.ceil(number_onu / onu_per_figure))

    num_column = int(numpy.ceil((onu_per_figure / 2)))
    #num_column = onu_per_figure

    colors_onus = cm.rainbow(numpy.linspace(0, 1, number_onu))
    cfg_curve_style =  cm.rainbow(numpy.linspace(0, 1, number_onu))
    plt.clf()    
    if ((xaxis_scale == 'OMA (dBm)') or (xaxis_scale == 'Rx_Pwr (dBm)')):
        ref_rx_pwr_matrix = []
        ref_oma_matrix = []
        ref_att_vector = []
        extinction_ratio = 0
        # Read extinction ratio for each ONU
        with open(extinction_ratio_file + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                extinction_ratio = float(row['%20s' % ('extinction_ratio')])		
                for i in range(1,number_onu+1):
                    ref_rx_pwr_matrix.append([])
                    ref_oma_matrix.append([])
    
        # Read received power for each ONU and calculate OMA
        with open(rx_pwr_file + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                ref_att_vector.append(float(row['%20s' % ('attenuation')]))
                for i in range(0,number_onu):
                    ref_rx_pwr_matrix[i].append(float(row['%20s' % ('rx_pwr' + str(i+1))]))
                    ref_oma_matrix[i].append(ref_rx_pwr_matrix[i][-1] + 10 * math.log10(2 * (extinction_ratio - 1) / (extinction_ratio + 1)))

    worst_sensitivity_oma = [-999.0] * len(ber_target_list) #-999 - a very small number which we will surely find any sensitivity bigger than    
    for curve_index in range(0, number_curves):
        # Init
        att_vector = []
        rx_pwr_matrix = []
        oma_matrix = []	
        ber_matrix = []
        
        ber_est_matrix = []
        p_derror_frame_matrix = []
        p_derror_frame_est_matrix = []
        est_err_matrix = []
    
        fec_ber_est_matrix = []
    
        for i in range(0, number_onu):
            ber_matrix.append([])
            rx_pwr_matrix.append([])
            oma_matrix.append([])
            ber_est_matrix.append([])
            p_derror_frame_matrix.append([])
            p_derror_frame_est_matrix.append([])
            est_err_matrix.append([])
            fec_ber_est_matrix.append([])
    
        # Read data curve
        with open(down_bert_file + str(curve_index) + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                att_vector.append(float(row['%20s' % 'attenuation']))
                for i in range(0, number_onu):
                    # Read raw-data				
                    ber_matrix[i].append(float(row['%20s' % ('ber_value' + str(i + 1))]))
                    if (xaxis_scale == 'Rx_Pwr (dBm)'):
                        rx_pwr_matrix[i].append(ref_rx_pwr_matrix[i][ref_att_vector.index(att_vector[-1])])
                    elif (xaxis_scale == 'OMA (dBm)'):
                        oma_matrix[i].append(ref_oma_matrix[i][ref_att_vector.index(att_vector[-1])])
						
                    # FEC-based statistics - corrected single/double errors [review statistics]
                    nb_correc_serror = (int(row['%20s' % ('total_serror_corr' + str(i + 1))]))
                    nb_correc_derror = (int(row['%20s' % ('total_derror_corr' + str(i + 1))]))
                    nb_correc_error = nb_correc_serror + 2 * nb_correc_derror			
                    nb_bits=(int(row['%20s' % ('total_bits' + str(i + 1))]))
                    ber_est = ((nb_correc_error) / (nb_bits*240.0/200.0)) if(nb_correc_error) else (1 / (nb_bits*240.0/200.0))
                    ber_est_matrix[i].append(ber_est)
                    p_derror_est = (2*nb_correc_derror/((nb_bits*240.0/200.0))) if(nb_correc_derror) else (1/((nb_bits*240.0/200.0)))
                    p_derror_frame_matrix[i].append(p_derror_est)				
                    p_derror_frame_est_matrix[i].append(((120.0*119.0/2)*pow(ber_est_matrix[i][-1],2)*pow(1.0 - ber_est_matrix[i][-1],118))*(2/120))
                    est_err_matrix[i].append([p_derror_frame_est_matrix[i][-1]/p_derror_frame_matrix[i][-1]])
                    fec_ber_est = (pow(1.0 - ber_est_matrix[i][-1],120))				
                    fec_ber_est = fec_ber_est + ((120.0)*pow(ber_est_matrix[i][-1],1)*pow(1.0 - ber_est_matrix[i][-1],119))
                    fec_ber_est = fec_ber_est + ((120.0*119.0/2)*pow(ber_est_matrix[i][-1],2)*pow(1.0 - ber_est_matrix[i][-1],118))		
                    fec_ber_est = 1.0 - fec_ber_est
                    fec_ber_est = fec_ber_est * (5.0/120.0) * 2.0
                    fec_ber_est_matrix[i].append(fec_ber_est)
					
        # Plot data
        for i in range(0, number_onu, 1):
            #look for worst observed sensitivity of this ONU in case plot is in OMA
            if(xaxis_scale == 'OMA (dBm)'):
                # look for worst observed sensitivity at each target BER
                for target_ber_idx in range(0, len(ber_target_list)):				
                    not_found=1
                    itr=1					
                    while(not_found and itr<len(ber_matrix[i])):
                        if ((ber_matrix[i][itr] > ber_target_list[target_ber_idx] and ber_matrix[i][itr-1] <= ber_target_list[target_ber_idx])): #assumes croissance - implement more generic code afterwards
                            #linear approximation between points
                            ym=ber_target_list[target_ber_idx]
                            y1=ber_matrix[i][itr]
                            y2=ber_matrix[i][itr-1]
                            x2=oma_matrix[i][itr]							
                            x1=oma_matrix[i][itr-1]
                            m=(y2-y1)/(x2-x1)
                            xm=(ym-y1)/m + x1							
                            #interpolated_oma = oma_matrix[i][itr-1]
                            interpolated_oma = xm                           
                            #print(interpolated_oma)							
                            not_found=0
                        itr=itr+1
                    if(not_found) : interpolated_oma = oma_matrix[i][-1]
                    if(interpolated_oma > worst_sensitivity_oma[target_ber_idx]): worst_sensitivity_oma[target_ber_idx]=interpolated_oma

            cur_fig_index = 0
            plt.figure(cur_fig_index + 1)
    
            # Config BER plot axis
            plt.subplot(1, num_column, (i % onu_per_figure) + 1)
    
            if (xaxis_scale == 'Attenuation (dB)'):
                plt.semilogy(att_vector, ber_matrix[i], color=cfg_curve_style, label="BER system",linestyle=None, marker='*')
                #plt.semilogy(att_vector, ber_est_matrix[i], cfg_curve_style_est, label="BER estimated (No correction single-double errors)")
                #plt.semilogy(att_vector, p_derror_frame_matrix[i], 'b', label="BER estimated (Correction single-errors)")			
                #plt.semilogy(att_vector, p_derror_frame_est_matrix[i], 'y', label="BER estimated (Correction single-errors bin. distr.)")		
            elif (xaxis_scale == 'Rx_Pwr (dBm)'):
                plt.semilogy(rx_pwr_matrix[i], ber_matrix[i], color=cfg_curve_style, label="BER system")
                #plt.semilogy(rx_pwr_matrix[i], ber_est_matrix[i], cfg_curve_style_est, label="BER estimated (No correction single-double errors)")
                #plt.semilogy(rx_pwr_matrix[i], p_derror_frame_matrix[i], 'b', label="BER estimated (Correction single-errors)")			
                #plt.semilogy(rx_pwr_matrix[i], p_derror_frame_est_matrix[i], 'y', label="BER estimated (Correction single-errors bin. distr.)")
            elif (xaxis_scale == 'OMA (dBm)'):
                #plt.semilogy(spec_oma, spec_ber, 'ko', label="SPEC")		
                plt.semilogy(oma_matrix[i], ber_matrix[i], color=cfg_curve_style[i], label="BER system",linestyle=':', marker='o')	
                plt.semilogy(oma_matrix[i], ber_est_matrix[i], color=cfg_curve_style[i], label="BER estimated (No FEC)", linestyle='-', marker='x')
                #plt.semilogy(oma_matrix[i], p_derror_frame_matrix[i], 'b', label="BER estimated (Correction single-errors)")			
                #plt.semilogy(oma_matrix[i], p_derror_frame_est_matrix[i], 'y', label="BER estimated (Correction single-errors bin. distr.)")
    
            plt.title('Downstream (FEC)')
            if ((i % onu_per_figure) == 0):
                plt.ylabel('Bit Error Ratio')
            plt.axis([xaxis_min, xaxis_max, yaxis_min, yaxis_max])
            ax = plt.gca()
    
            #if (curve_index==0): ax.legend()
    		
            ax.grid(True)
            if (xaxis_scale == 'Attenuation (dB)'):
                ax.invert_xaxis()
    
            #plt.subplot(2, num_column, (i % onu_per_figure) + 1 + onu_per_figure)
            #
            #if (xaxis_scale == 'Attenuation (dB)'):
            #    plt.plot(att_vector, est_err_matrix[i], cfg_curve_style_est)
            #
            #elif (xaxis_scale == 'Rx_Pwr (dBm)'):
            #    plt.plot(rx_pwr_matrix[i], est_err_matrix[i], cfg_curve_style_est)
    		#
            #elif (xaxis_scale == 'OMA (dBm)'):
            #    plt.plot(oma_matrix[i], est_err_matrix[i], cfg_curve_style_est)
            #
            #if ((i % onu_per_figure) == 0):
            #    plt.ylabel('Double-Error Estimated/Measured')
            #plt.axis([xaxis_min, xaxis_max, 0, 1.5])
            #ax = plt.gca()
            #ax.grid(True)
            #if (xaxis_scale == 'Attenuation (dB)'):
            #    ax.invert_xaxis()
            plt.xlabel(xaxis_scale)
    
            # BER in x out plot
            cur_fig_index = int(numpy.floor((number_onu-1)/ onu_per_figure)) + 1
            plt.figure(cur_fig_index + 1)
    
            # Config BER plot axis
            str_aux="Measured ONU" + str(i+1)		
            plt.loglog(ber_est_matrix[i], ber_matrix[i], color=colors_onus[i], label=str_aux, basex=10)
    
        plt.loglog(ber_est_matrix[0], fec_ber_est_matrix[0], color='k', linewidth=4.0,linestyle=':', label="Estimated", basex=10)
    
        plt.title('BER: pre-FEC x post-FEC')
        plt.ylabel('BER out')
        plt.xlabel('BER in')		
        plt.axis([1e-12, 1e-2, 1e-12, 1e-2])
        ax = plt.gca()
        #if (curve_index==0): ax.legend()		
        ax.grid(True)

    plt.figure(1)
    plt.legend(['ONU1', 'ONU1 (no FEC)', 'ONU2', 'ONU2 (no FEC)'])

    if(not save_ndisplay) : plt.show() # Display image
    else : plt.savefig(output_file+'_img.png') # Save image

    return worst_sensitivity_oma

def plot_bert_downstream_onu(rx_pwr_file, extinction_ratio_file, down_bert_file, save_ndisplay, output_file, ber_target_list=[1e-3], read_config_file=1, number_onu=8, number_curves=1):   
    if read_config_file:
        # Read number_onu and number_curves from config_file
        with open(down_bert_file + '_config.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                number_onu = int(row['%20s' % 'number_onu'])
                number_curves = int(row['%20s' % 'number_curves'])

    num_figures = int(numpy.ceil(number_onu / onu_per_figure))

    num_column = int(numpy.ceil((onu_per_figure / 2)))
    #num_column = onu_per_figure

    colors_onus = cm.rainbow(numpy.linspace(0, 1, number_onu))
    cfg_curve_style =  cm.rainbow(numpy.linspace(0, 1, number_onu))
    plt.clf()    
    if ((xaxis_scale == 'OMA (dBm)') or (xaxis_scale == 'Rx_Pwr (dBm)')):
        ref_rx_pwr_matrix = []
        ref_oma_matrix = []
        ref_att_vector = []
        extinction_ratio = 0
        # Read extinction ratio for each ONU
        with open(extinction_ratio_file + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                extinction_ratio = float(row['%20s' % ('extinction_ratio')])		
                for i in range(1,number_onu+1):
                    ref_rx_pwr_matrix.append([])
                    ref_oma_matrix.append([])
    
        # Read received power for each ONU and calculate OMA
        with open(rx_pwr_file + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                ref_att_vector.append(float(row['%20s' % ('attenuation')]))
                for i in range(0,number_onu):
                    ref_rx_pwr_matrix[i].append(float(row['%20s' % ('rx_pwr' + str(i+1))]))
                    ref_oma_matrix[i].append(ref_rx_pwr_matrix[i][-1] + 10 * math.log10(2 * (extinction_ratio - 1) / (extinction_ratio + 1)))


    worst_sensitivity_oma = []
    for idx in range(number_onu):
        worst_sensitivity_oma.append([-999.0] * len(ber_target_list)) #-999 - a very small number which we will surely find any sensitivity bigger than    
    for curve_index in range(0, number_curves):
        # Init
        att_vector = []
        rx_pwr_matrix = []
        oma_matrix = []	
        ber_matrix = []
        
        ber_est_matrix = []
        p_derror_frame_matrix = []
        p_derror_frame_est_matrix = []
        est_err_matrix = []
    
        fec_ber_est_matrix = []
    
        for i in range(0, number_onu):
            ber_matrix.append([])
            rx_pwr_matrix.append([])
            oma_matrix.append([])
            ber_est_matrix.append([])
            p_derror_frame_matrix.append([])
            p_derror_frame_est_matrix.append([])
            est_err_matrix.append([])
            fec_ber_est_matrix.append([])
    
        # Read data curve
        with open(down_bert_file + str(curve_index) + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                att_vector.append(float(row['%20s' % 'attenuation']))
                for i in range(0, number_onu):
                    # Read raw-data				
                    ber_matrix[i].append(float(row['%20s' % ('ber_value' + str(i + 1))]))
                    if (xaxis_scale == 'Rx_Pwr (dBm)'):
                        rx_pwr_matrix[i].append(ref_rx_pwr_matrix[i][ref_att_vector.index(att_vector[-1])])
                    elif (xaxis_scale == 'OMA (dBm)'):
                        oma_matrix[i].append(ref_oma_matrix[i][ref_att_vector.index(att_vector[-1])])
						
                    # FEC-based statistics - corrected single/double errors [review statistics]
                    nb_correc_serror = (int(row['%20s' % ('total_serror_corr' + str(i + 1))]))
                    nb_correc_derror = (int(row['%20s' % ('total_derror_corr' + str(i + 1))]))
                    nb_correc_error = nb_correc_serror + 2 * nb_correc_derror			
                    nb_bits=(int(row['%20s' % ('total_bits' + str(i + 1))]))
                    ber_est = ((nb_correc_error) / (nb_bits*240.0/200.0)) if(nb_correc_error) else (1 / (nb_bits*240.0/200.0))
                    ber_est_matrix[i].append(ber_est)
                    p_derror_est = (2*nb_correc_derror/((nb_bits*240.0/200.0))) if(nb_correc_derror) else (1/((nb_bits*240.0/200.0)))
                    p_derror_frame_matrix[i].append(p_derror_est)				
                    p_derror_frame_est_matrix[i].append(((120.0*119.0/2)*pow(ber_est_matrix[i][-1],2)*pow(1.0 - ber_est_matrix[i][-1],118))*(2/120))
                    est_err_matrix[i].append([p_derror_frame_est_matrix[i][-1]/p_derror_frame_matrix[i][-1]])
                    fec_ber_est = (pow(1.0 - ber_est_matrix[i][-1],120))				
                    fec_ber_est = fec_ber_est + ((120.0)*pow(ber_est_matrix[i][-1],1)*pow(1.0 - ber_est_matrix[i][-1],119))
                    fec_ber_est = fec_ber_est + ((120.0*119.0/2)*pow(ber_est_matrix[i][-1],2)*pow(1.0 - ber_est_matrix[i][-1],118))		
                    fec_ber_est = 1.0 - fec_ber_est
                    fec_ber_est = fec_ber_est * (5.0/120.0) * 2.0
                    fec_ber_est_matrix[i].append(fec_ber_est)
					
        # Plot data
        for i in range(0, number_onu, 1):
            #look for worst observed sensitivity of this ONU in case plot is in OMA
            if(xaxis_scale == 'OMA (dBm)'):
                # look for worst observed sensitivity at each target BER
                for target_ber_idx in range(0, len(ber_target_list)):				
                    not_found=1
                    itr=1					
                    while(not_found and itr<len(ber_matrix[i])):
                        if ((ber_matrix[i][itr] > ber_target_list[target_ber_idx] and ber_matrix[i][itr-1] <= ber_target_list[target_ber_idx])): #assumes croissance - implement more generic code afterwards
                            #linear approximation between points
                            ym=ber_target_list[target_ber_idx]
                            y1=ber_matrix[i][itr]
                            y2=ber_matrix[i][itr-1]
                            x2=oma_matrix[i][itr]							
                            x1=oma_matrix[i][itr-1]
                            m=(y2-y1)/(x2-x1)
                            xm=(ym-y1)/m + x1							
                            #interpolated_oma = oma_matrix[i][itr-1]
                            interpolated_oma = xm                           
                            #print(interpolated_oma)							
                            not_found=0
                        itr=itr+1
                    if(not_found) : interpolated_oma = oma_matrix[i][-1]
                    if(interpolated_oma > worst_sensitivity_oma[i][target_ber_idx]): worst_sensitivity_oma[i][target_ber_idx]=interpolated_oma

            cur_fig_index = 0
            plt.figure(cur_fig_index + 1)
    
            # Config BER plot axis
            plt.subplot(1, num_column, (i % onu_per_figure) + 1)
    
            if (xaxis_scale == 'Attenuation (dB)'):
                plt.semilogy(att_vector, ber_matrix[i], color=cfg_curve_style, label="BER system",linestyle=None, marker='*')
                #plt.semilogy(att_vector, ber_est_matrix[i], cfg_curve_style_est, label="BER estimated (No correction single-double errors)")
                #plt.semilogy(att_vector, p_derror_frame_matrix[i], 'b', label="BER estimated (Correction single-errors)")			
                #plt.semilogy(att_vector, p_derror_frame_est_matrix[i], 'y', label="BER estimated (Correction single-errors bin. distr.)")		
            elif (xaxis_scale == 'Rx_Pwr (dBm)'):
                plt.semilogy(rx_pwr_matrix[i], ber_matrix[i], color=cfg_curve_style, label="BER system")
                #plt.semilogy(rx_pwr_matrix[i], ber_est_matrix[i], cfg_curve_style_est, label="BER estimated (No correction single-double errors)")
                #plt.semilogy(rx_pwr_matrix[i], p_derror_frame_matrix[i], 'b', label="BER estimated (Correction single-errors)")			
                #plt.semilogy(rx_pwr_matrix[i], p_derror_frame_est_matrix[i], 'y', label="BER estimated (Correction single-errors bin. distr.)")
            elif (xaxis_scale == 'OMA (dBm)'):
                #plt.semilogy(spec_oma, spec_ber, 'ko', label="SPEC")		
                plt.semilogy(oma_matrix[i], ber_matrix[i], color=cfg_curve_style[i], label="BER system",linestyle=':', marker='o')
                #plt.semilogy(oma_matrix[i], ber_est_matrix[i], color=cfg_curve_style[i], label="BER estimated (No FEC)", linestyle='-', marker='x')
                #plt.semilogy(oma_matrix[i], p_derror_frame_matrix[i], 'b', label="BER estimated (Correction single-errors)")			
                #plt.semilogy(oma_matrix[i], p_derror_frame_est_matrix[i], 'y', label="BER estimated (Correction single-errors bin. distr.)")
    
            plt.title('Downstream (FEC)')
            if ((i % onu_per_figure) == 0):
                plt.ylabel('Bit Error Ratio')
            plt.axis([xaxis_min, xaxis_max, yaxis_min, yaxis_max])
            ax = plt.gca()
    
            #if (curve_index==0): ax.legend()
    		
            ax.grid(True)
            if (xaxis_scale == 'Attenuation (dB)'):
                ax.invert_xaxis()
    
            #plt.subplot(2, num_column, (i % onu_per_figure) + 1 + onu_per_figure)
            #
            #if (xaxis_scale == 'Attenuation (dB)'):
            #    plt.plot(att_vector, est_err_matrix[i], cfg_curve_style_est)
            #
            #elif (xaxis_scale == 'Rx_Pwr (dBm)'):
            #    plt.plot(rx_pwr_matrix[i], est_err_matrix[i], cfg_curve_style_est)
    		#
            #elif (xaxis_scale == 'OMA (dBm)'):
            #    plt.plot(oma_matrix[i], est_err_matrix[i], cfg_curve_style_est)
            #
            #if ((i % onu_per_figure) == 0):
            #    plt.ylabel('Double-Error Estimated/Measured')
            #plt.axis([xaxis_min, xaxis_max, 0, 1.5])
            #ax = plt.gca()
            #ax.grid(True)
            #if (xaxis_scale == 'Attenuation (dB)'):
            #    ax.invert_xaxis()
            plt.xlabel(xaxis_scale)
    
            # BER in x out plot
            cur_fig_index = int(numpy.floor((number_onu-1)/ onu_per_figure)) + 1
            plt.figure(cur_fig_index + 1)
    
            # Config BER plot axis
            str_aux="Measured ONU" + str(i+1)		
            plt.loglog(ber_est_matrix[i], ber_matrix[i], color=colors_onus[i], label=str_aux, basex=10)
    
        plt.loglog(ber_est_matrix[0], fec_ber_est_matrix[0], color='k', linewidth=4.0,linestyle=':', label="Estimated", basex=10)
    
        plt.title('BER: pre-FEC x post-FEC')
        plt.ylabel('BER out')
        plt.xlabel('BER in')		
        plt.axis([1e-12, 1e-2, 1e-12, 1e-2])
        ax = plt.gca()
        #if (curve_index==0): ax.legend()		
        ax.grid(True)

    plt.figure(1)
    plt.legend(['ONU1', 'ONU2', 'ONU3', 'ONU4', 'ONU5', 'ONU6', 'ONU7', 'ONU8'])

    if(not save_ndisplay) : plt.show() # Display image
    else : plt.savefig(output_file+'_img.png') # Save image

    return worst_sensitivity_oma