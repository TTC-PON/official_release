#!/usr/bin/env python

import csv
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import math

# This script plots eye diagrams extracted from scope
def plot_eye_scan_uW(AVG_power,eyescan_file, save_ndisplay, en_mask_test=1,data_rate=9.6e9, eyescan_file_out='', mask_olt_nonu=1):
	total_loss = 14.3 # dB measured
	conversion_gain= 1207.1 # V/W
	conversion_gain = conversion_gain/(10**(total_loss/10))
	AVG_power_scaled = (10**(total_loss/10))*AVG_power

	# Matrix to plot
	ber_matrix = []
	# Read data curve
	with open(eyescan_file + '.csv') as csvfile:
		reader = csv.DictReader(csvfile)
		number_y=0		
		for row in reader:
			number_y+=1		
			ber_matrix.append([])
			keep_reading = 1
			number_x=0			
			while(keep_reading):
				try:	
					#ber_matrix[-1].append(float(row['%20s' % ('hor' + str(number_x))]))
					ber_matrix[-1].append((float(row['%20s' % ('hor' + str(number_x))])*1e3/conversion_gain))
					number_x+=1
				except KeyError:
					keep_reading=0

	# Read coordinates
	with open(eyescan_file + '_axis.csv') as csvfile:
		reader = csv.DictReader(csvfile)
		for row in reader:
			xaxis_min = float(row['%20s' % ('tmin_s')])*1e12 #ps				
			xaxis_max = float(row['%20s' % ('tmax_s')])*1e12 #ps
			yaxis_min = (float(row['%20s' % ('vmin_v')])*1e6/conversion_gain+AVG_power_scaled)#uW
			#yaxis_min = float(row['%20s' % ('vmin_v')])*1e3  #mV				
			yaxis_max = (float(row['%20s' % ('vmax_v')])*1e6/conversion_gain+AVG_power_scaled)#uW
			#yaxis_max = float(row['%20s' % ('vmax_v')])*1e3  #mV	

	plt.clf()
	Z=ber_matrix
	X,Y=np.meshgrid(np.linspace(xaxis_min,xaxis_max,number_x),np.linspace(yaxis_max,yaxis_min,number_y))

	cmap = plt.cm.jet
	cmap.set_under(color='white') 

	im = plt.pcolormesh(X,Y,Z, cmap=cmap, shading='gouraud', vmin=0.1)
	plt.colorbar(im, orientation='vertical', label='Color Grade')
	plt.xlabel('X offset - ps')
	plt.ylabel('Y offset - uW') #change unit
	plt.axis([xaxis_min, xaxis_max, yaxis_min, yaxis_max])
	#plt.title('Eye diagram (file:' + eyescan_file + ')')
	plt.grid()

	### Generate Eye Scan Mask ###
	pass_nfail = 1

	if(en_mask_test):
		# Read amplitude
		with open(eyescan_file[0:-4] + 'params.csv') as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				vamp_v = (float(row['%20s' % ('amplitude_v')])*1e6/conversion_gain) #uW
				#vamp_v = float(row['%20s' % ('amplitude_v')])*1e3 #mV 

		if(mask_olt_nonu): #OLT Tx Optical Compliance
			x3=0.6
			x2=0.4
			y2=0.75 
			y1=0.25 
			y3=0.25 
			y4=0.25 
	        
			# Create a matrix with the specs (0=pass, 1=fail)
			matrix_specs= []
			for i in range(0,number_y):
				matrix_specs.append([])
				for j in range(0,number_x):
					matrix_specs[-1].append(0.0)		
			y_specs = np.linspace(yaxis_max + vamp_v/2, yaxis_min+vamp_v/2, number_y)	
			x_specs = np.linspace(xaxis_min + 0.5*(1e12/data_rate), xaxis_max + 0.5*(1e12/data_rate), number_x)
		    
			for i in range(0, number_y):
				for j in range(0,number_x):		
					if((y_specs[i] > (1+y3) * vamp_v + AVG_power_scaled) or
					   (y_specs[i] < (-1*y4) * vamp_v + AVG_power_scaled) or
					   (y_specs[i] < y2 * vamp_v + AVG_power_scaled and y_specs[i] > y1 * vamp_v + AVG_power_scaled and
						x_specs[j] > x2 * (1e12/data_rate) and x_specs[j] < x3 * (1e12/data_rate))):			
						matrix_specs[i][j] = 1.0
						if(ber_matrix[i][j] > 0 ) : pass_nfail = 0	
		else:
			x1=0.2
			x2=0.4
			x3=0.6
			x4=0.8			
			y2=0.75 
			y1=0.25 
			points_up = [[[x1,0.5],[x2,y2]],[[x2,y2],[x3,y2]],[[x3,y2],[x4,0.5]]]
			points_dn = [[[x1,0.5],[x2,y1]],[[x2,y1],[x3,y1]],[[x3,y1],[x4,0.5]]]
			# Create a matrix with the specs (0=pass, 1=fail)
			matrix_specs= []
			for i in range(0,number_y):
				matrix_specs.append([])
				for j in range(0,number_x):
					matrix_specs[-1].append(0.0)		
			y_specs = np.linspace(yaxis_max + vamp_v/2 - (yaxis_max+yaxis_min)/2.0, yaxis_min+vamp_v/2 - (yaxis_max+yaxis_min)/2.0, number_y)	
			x_specs = np.linspace(xaxis_min + 0.5*(1e12/data_rate), xaxis_max + 0.5*(1e12/data_rate), number_x)
		    
			for i in range(0, number_y):
				for j in range(0,number_x):
					# Polygon
					check_mask_polygon = True
					# Lines up
					for point in points_up:
						if(check_mask_polygon):
							y_target = ((point[1][1]-point[0][1])/(point[1][0]-point[0][0]))*(x_specs[j]/(1e12/data_rate) - point[0][0]) + (point[0][1])
							if((y_specs[i]/vamp_v) > y_target) : check_mask_polygon = False
					for point in points_dn:
						if(check_mask_polygon):
							y_target = ((point[1][1]-point[0][1])/(point[1][0]-point[0][0]))*(x_specs[j]/(1e12/data_rate) - point[0][0]) + (point[0][1])
							if(y_specs[i]/vamp_v < y_target) : check_mask_polygon = False

					if((y_specs[i] > (1+y1) * vamp_v) or
					   (y_specs[i] < (-1*y1) * vamp_v) or check_mask_polygon):		
						matrix_specs[i][j] = 1.0
						if(ber_matrix[i][j] > 0 ) : pass_nfail = 0	
						
		Z = np.array(matrix_specs)
		Z = np.ma.masked_array(Z, Z<1.0)
		X,Y=np.meshgrid(np.linspace(xaxis_min,xaxis_max,number_x),np.linspace(yaxis_max,yaxis_min,number_y))
		cmap2 = plt.cm.jet
		cmap2.set_over(color='black') 
		im2 = plt.pcolormesh(X,Y,Z, cmap=cmap2, shading='gouraud',vmin=0.1, vmax=0.5)

	if(not save_ndisplay) : plt.show() # Display image
	else : plt.savefig(eyescan_file_out+'_img.png') # Save image

	return pass_nfail
	

