#!/usr/bin/env python

import csv
import matplotlib.pyplot as plt
import numpy
import math
import csv

# Files
# Rx power down directory
# rx_pwr_down_measured for last splitter output - file
# rx_pwr_meas_file = './rx_pwr_down'

# Calibration directory
# onu_addr x out_splitter - file
# dev_spltr_out_file = './onu_addr_out_splitter'

# network uniformity calibration - file
# rx_network_uniformity_file = './down_network_uniformity_calibration'

# optical attenuator calibration - file
# attenuator_calib_file = './down_attenuator_calibration'

#Output file
# output - file (attenuation x rx_pwr for each ONU)
# output_file = './setup_down_rx_pwr'

def gen_rx_pwr(calibration_dir, rx_pwr_meas_file, down_nup, output_file, offset_att=0.0, onu_offset=1):

    # Calibration directory
    # device x out_splitter - file
    if(down_nup) : dev_spltr_out_file = calibration_dir + './onu_addr_out_splitter'
    else         : dev_spltr_out_file = calibration_dir + './olt_out_splitter'

    # network uniformity calibration - file
    if(down_nup) : rx_network_uniformity_file = calibration_dir + './down_network_uniformity_calibration'
    else         : rx_network_uniformity_file = calibration_dir + './up_network_uniformity_calibration'

    # optical attenuator calibration - file
    if(down_nup) : attenuator_calib_file = calibration_dir + './down_attenuator_calibration'
    else         : attenuator_calib_file = calibration_dir + './up_attenuator_calibration'

    ################################################################
    ########## Read measured power / reference attenuation #########
    ################################################################
    with open(rx_pwr_meas_file + '.csv') as csvfile:
        reader = csv.DictReader(csvfile)	
        for row in reader:
            ref_att_spltr_out = float(row['%20s' % 'attenuation'])		
            if(down_nup): # Downstream - only a single rx_pwr is measured
                ref_pwr_spltr_out = float(row['%20s' % 'rx_pwr'])
            else:         # Upstream - a rx_pwr is measured per ONU
                ref_pwr_spltr_out=[] 			
                finished_onu=0
                onu_addr=1				
                while(not finished_onu): # Read all ONUs until an exception occurs
                    try:
                        ref_pwr_spltr_out.append(float(row['%20s' % ('rx_pwr' + str(onu_addr))]))
                    except KeyError:
                        number_onu = onu_addr-1
                        finished_onu = 1
                    onu_addr+=1
    ################################################################
    #### Read table associating devie with splitter output ###
    ################################################################
    table_dev_addr     = []
    table_splitter_out = []	
    with open(dev_spltr_out_file + '.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:		
            if(down_nup) : table_dev_addr.append(int(row['%20s' % 'onu_addr']))
            else         : table_dev_addr.append(int(row['%20s' % 'olt_nref']))			
            table_splitter_out.append(int(row['%20s' % 'splitter_out']))

    # only for downstream - in upstream the number of ONUs is read before from the rx_pwr file
    if(down_nup) : 	
        number_onu = len(table_dev_addr) - 1
    table_onu_out_spltr = [0] * (number_onu) 
    for i in range(0,len(table_dev_addr)):
        if(table_dev_addr[i]>0) : table_onu_out_spltr[table_dev_addr[i]-1] = table_splitter_out[i]
        else : ref_spltr_out = table_splitter_out[i] # The splitter is marked with addr 0 in this file to ease of use

    ################################################################
    ################# Read network uniformity file #################
    ################################################################
    network_uniformity_out = []

    with open(rx_network_uniformity_file + '.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            network_uniformity_out.append(float(row['%20s' % 'rx_pwr']))
    
    network_uniformity_ref = network_uniformity_out[ref_spltr_out-1]
    
    for i in range(0,len(network_uniformity_out)):
        network_uniformity_out[i] = network_uniformity_out[i] - network_uniformity_ref

    ################################################################
    ############### Calculate ref power for each ONU ###############
    ################################################################
    ref_pwr_rx = [0] * number_onu	
    if(down_nup) : 	
        for i in range(0, number_onu):
            if(i == (onu_offset-1)) : ref_pwr_rx[i] = ref_pwr_spltr_out + network_uniformity_out[table_onu_out_spltr[i]-1]-offset_att		
            else : ref_pwr_rx[i] = ref_pwr_spltr_out + network_uniformity_out[table_onu_out_spltr[i]-1]
    else:
        for i in range(0, number_onu):
            if(i == (onu_offset-1)) : ref_pwr_rx[i] = ref_pwr_spltr_out[i] + network_uniformity_out[0]-offset_att			
            else : ref_pwr_rx[i] = ref_pwr_spltr_out[i] + network_uniformity_out[0]

    ################################################################
    ############### Read attenuator calibration file ###############
    ################################################################
    attenuation_optatt_calibration = []
    rx_pwr_optatt_calibration = []
    
    with open(attenuator_calib_file + '.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            attenuation_optatt_calibration.append(float(row['%20s' % 'attenuation']))
            rx_pwr_optatt_calibration.append(float(row['%20s' % 'rx_pwr']))
    
    
    ref_optatt_calibration_rwpwr = rx_pwr_optatt_calibration[attenuation_optatt_calibration.index(ref_att_spltr_out)]
    
    # normalize
    for i in range(0,len(attenuation_optatt_calibration)):
        rx_pwr_optatt_calibration[i] = rx_pwr_optatt_calibration[i] - ref_optatt_calibration_rwpwr
    
    rx_pwr_matrix = []
    for i in range(0, number_onu):
        rx_pwr_matrix.append([])
    
    for i in range(0, len(attenuation_optatt_calibration)):
        for j in range(0, number_onu):
            rx_pwr_matrix[j].append(ref_pwr_rx[j] + rx_pwr_optatt_calibration[i])
    
    ################################################################
    ############### Save attenuation x rx_pwr file #################
    ################################################################
    with open(output_file + '.csv', 'w') as csvfile:
        fieldnames = ['attenuation']
        for i in range(1, number_onu + 1):
            fieldnames += ['rx_pwr' + str(i)]
    
        for i in range(0, len(fieldnames)):
            # format 20 spaces for each value
            fieldnames[i] = '%20s' % fieldnames[i]
    
        writer = csv.DictWriter(
            csvfile,
            fieldnames=fieldnames,
            delimiter=',',
            lineterminator='\n')
        writer.writeheader()
    
        for i in range(0, len(attenuation_optatt_calibration)):
            dict_write = {fieldnames[0]: ("%20.4f" % attenuation_optatt_calibration[i])}
            for j in range(1, number_onu + 1):
                dict_write[fieldnames[1 + (j - 1)]] = "%20.4f" % rx_pwr_matrix[j-1][i]
    
            writer.writerow(dict_write)

def gen_tx_pwr_er(calibration_optical_head, calibration_tx_pwr_down, rx_pwr_down_measured, eye_params_file, output_file):
    # Read file calibration_optical_head
    with open(calibration_optical_head + '.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            conversion_gain = float(row['%20s' % 'conversion_gain_v_w']) #unit: V/W
            rx_pwr_scope    = float(row['%20s' % 'rx_pwr_scope']) #unit: dBm
            rx_pwr_pwrmet   = float(row['%20s' % 'rx_pwr_pwrmet']) #unit: dBm

    # Read file calibration_tx_pwr_down in order to estimate total attenuation system
    with open(calibration_tx_pwr_down + '.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            total_loss = float(row['%20s' % 'total_loss']) #unit: dB

    # Read file table_rx_pwr_down and eye_params_file (important to take: v_amplitude)
    with open(rx_pwr_down_measured + '.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            rx_pwr_down = float(row['%20s' % 'rx_pwr']) #unit: dBm	

    # Read file eye_params in order to calculate OMA with the conversion gain of the optical probe
    with open(eye_params_file + '.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            v_amp = float(row['%20s' % 'amplitude_v']) #unit: V

    # Calculate Tx power and extinction ratio
    tx_pwr = rx_pwr_down + (rx_pwr_scope-rx_pwr_pwrmet) + total_loss
          
    p_avg  = 10**((rx_pwr_down + (rx_pwr_scope-rx_pwr_pwrmet))/10.0) #average power - mW	
    p_oma  = (v_amp / conversion_gain) * 1e3          #power in OMA  - mW

    extinction_ratio = (2.0*p_avg + p_oma)/(2.0*p_avg - p_oma)       #unit-less

    # Save into file
    with open(output_file + '.csv', 'w') as csvfile:
       fieldnames = ['extinction_ratio', 'tx_pwr']
       for i in range(0, len(fieldnames)):
           fieldnames[i] = '%20s' % fieldnames[i]
    
       writer = csv.DictWriter(
           csvfile,
           fieldnames=fieldnames,
           delimiter=',',
           lineterminator='\n')
       writer.writeheader()
    
       dict_write = {
           fieldnames[0] : ('%20.4f') % extinction_ratio,
           fieldnames[1] : ('%20.4f') % tx_pwr		
       } 
       writer.writerow(dict_write)

    return [tx_pwr, 10*math.log10(extinction_ratio)]

def gen_tx_pwr_er_onu(calibration_optical_head, calibration_tx_pwr, rx_pwr_measured, eye_params_file_root, output_file):
    fieldnames = []

    # Read file calibration_optical_head
    with open(calibration_optical_head + '.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            conversion_gain = float(row['%20s' % 'conversion_gain_v_w']) #unit: V/W
        #    rx_pwr_scope    = float(row['%20s' % 'rx_pwr_scope']) #unit: dBm
        #    rx_pwr_pwrmet   = float(row['%20s' % 'rx_pwr_pwrmet']) #unit: dBm

    tx_pwr_list = []
    extinction_ratio_list = []
    for onu_addr in range(1,8+1):
        # Read file calibration_tx_pwr in order to estimate total attenuation system
        with open(calibration_tx_pwr + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                total_loss = float(row['%20s' % ('total_loss' + str(onu_addr))]) #unit: dB
        
        # Read file table_rx_pwr and eye_params_file (important to take: v_amplitude)
        with open(rx_pwr_measured + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                rx_pwr = float(row['%20s' % ('rx_pwr' + str(onu_addr))]) #unit: dBm	
        
        # Read file eye_params in order to calculate OMA with the conversion gain of the optical probe
        with open(eye_params_file_root + str(onu_addr) + '_params' + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                v_amp = float(row['%20s' % 'amplitude_v']) #unit: V
        
        # Calculate Tx power and extinction ratio
        tx_pwr = rx_pwr + total_loss
        p_avg  = 10**(rx_pwr/10.0)      
        #p_avg  = 10**((rx_pwr + (rx_pwr_scope-rx_pwr_pwrmet))/10.0) #average power - mW	
        p_oma  = (v_amp / conversion_gain) * 1e3          #power in OMA  - mW
        
        extinction_ratio = (2.0*p_avg + p_oma)/(2.0*p_avg - p_oma)       #unit-less

        # save in return list
        tx_pwr_list.append(tx_pwr)
        extinction_ratio_list.append(extinction_ratio)

        # keep for writing in the file
        fieldnames.append('extinction_ratio' + str(onu_addr))
        fieldnames.append('tx_pwr' + str(onu_addr))
		
    # Save into file
    with open(output_file + '.csv', 'w') as csvfile:
        for i in range(0, len(fieldnames)):
            fieldnames[i] = '%20s' % fieldnames[i]
        
        writer = csv.DictWriter(
            csvfile,
            fieldnames=fieldnames,
            delimiter=',',
            lineterminator='\n')
        writer.writeheader()
        
        dict_write = {}
        for onu_addr in range(1, 8+1):
            dict_write[fieldnames[2*(onu_addr-1)]] = ('%20.4f') % extinction_ratio_list[onu_addr-1]
            dict_write[fieldnames[2*(onu_addr-1)+1]] = ('%20.4f') % tx_pwr_list[onu_addr-1]		
        writer.writerow(dict_write)

    extinction_ratio_list_dB = [10*math.log10(extinction_ratio) for extinction_ratio in extinction_ratio_list]
    return [tx_pwr_list, extinction_ratio_list_dB]








