#!/usr/bin/env python

import time
import csv


def save_down_pwr_csv(att, power, name_file):
    with open(name_file + '.csv', 'w') as csvfile:
        fieldnames = ['attenuation', 'rx_pwr']
        for i in range(0, len(fieldnames)):
            # format 20 spaces for each value
            fieldnames[i] = '%20s' % fieldnames[i]

        writer = csv.DictWriter(
            csvfile,
            fieldnames=fieldnames,
            delimiter=',',
            lineterminator='\n')
        writer.writeheader()

        dict_write = {
			fieldnames[0]: ('%20.4f' % att),
			fieldnames[1]: ('%20.4f' % power)}
        writer.writerow(dict_write)

def save_up_pwr_csv(att, power, name_file):
    with open(name_file + '.csv', 'w') as csvfile:
        fieldnames = []	
        fieldnames.append('attenuation')
        for i in range(0,len(power)):
            fieldnames.append(('rx_pwr'+str(i+1)))

        for i in range(0, len(fieldnames)):
            # format 20 spaces for each value
            fieldnames[i] = '%20s' % fieldnames[i]

        writer = csv.DictWriter(
            csvfile,
            fieldnames=fieldnames,
            delimiter=',',
            lineterminator='\n')
        writer.writeheader()

        dict_write = {fieldnames[0] :  ('%20.4f' % att)}
        for i in range(0,len(power)):
            dict_write[fieldnames[i+1]] =  ('%20.4f' % power[i])

        writer.writerow(dict_write)

# Same attenuation x measured power table during calibration
def save_pwr_calib_csv(att, power, name_file):
    with open(name_file + '.csv', 'w') as csvfile:
        fieldnames = ['attenuation', 'rx_pwr']
        for i in range(0, len(fieldnames)):
            # format 20 spaces for each value
            fieldnames[i] = '%20s' % fieldnames[i]

        writer = csv.DictWriter(
            csvfile,
            fieldnames=fieldnames,
            delimiter=',',
            lineterminator='\n')
        writer.writeheader()

        for i in range(0,len(att)):
            dict_write = {
		    	fieldnames[0]: ('%20.4f' % att[i]),
		    	fieldnames[1]: ('%20.4f' % power[i])}
            writer.writerow(dict_write)
