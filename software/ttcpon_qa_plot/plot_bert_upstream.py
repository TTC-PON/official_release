#!/usr/bin/env python

import csv
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy
import math
import pylab

# Configuration plot
onu_per_figure = 1
xaxis_min = -30
xaxis_max = -20
yaxis_min = 1e-9
yaxis_max = 1e-3
xaxis_scale = 'OMA (dBm)'; # possible values = 'Attenuation (dB)', 'Rx_Pwr (dBm)', 'OMA (dBm)'

# Returns worst observed sensitivity at BER given by ber_target_list in case OMA is chosen
def plot_bert_upstream(rx_pwr_file, extinction_ratio_file, up_bert_file, save_ndisplay, output_file, ber_target_list=[1e-3],read_config_file=1, number_onu=8, number_curves=1):

    if read_config_file:
        # Read number_onu and number_curves from config_file
        with open(up_bert_file + '_config.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                number_onu = int(row['%20s' % 'number_onu'])
                number_curves = int(row['%20s' % 'number_curves'])
    
    num_figures = int(numpy.ceil(number_onu / onu_per_figure))
    cfg_curve_style =  cm.rainbow(numpy.linspace(0, 1, number_onu))
    plt.clf()
    
    if ((xaxis_scale == 'OMA (dBm)') or (xaxis_scale == 'Rx_Pwr (dBm)')):
        ref_rx_pwr_matrix = []
        ref_oma_matrix = []
        ref_att_vector = []
        extinction_ratio = []
        # Read extinction ratio for each ONU
        with open(extinction_ratio_file + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                for i in range(1,number_onu+1):
                    extinction_ratio.append(float(row['%20s' % ('extinction_ratio' + str(i))]))
                    ref_rx_pwr_matrix.append([])
                    ref_oma_matrix.append([])
    
        # Read received power for each ONU and calculate OMA
        with open(rx_pwr_file + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                ref_att_vector.append(float(row['%20s' % ('attenuation')]))
                for i in range(0,number_onu):
                    ref_rx_pwr_matrix[i].append(float(row['%20s' % ('rx_pwr' + str(i+1))]))
                    ref_oma_matrix[i].append(ref_rx_pwr_matrix[i][-1] + 10 * math.log10(2 * (extinction_ratio[i] - 1) / (extinction_ratio[i] + 1)))

    worst_sensitivity_oma = [-999.0] * len(ber_target_list) #-999 - a very small number which we will surely find any sensitivity bigger than
    for curve_index in range(0, number_curves):
        # Init
        att_vector = []
        rx_pwr_matrix = []
        oma_matrix = []
        ber_matrix = []
        ploss_ratio_matrix = []
        for i in range(0, number_onu):
            ber_matrix.append([])
            ploss_ratio_matrix.append([])
            rx_pwr_matrix.append([])
            oma_matrix.append([])

        # Read data curve
        with open(up_bert_file + str(curve_index) + '.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                att_vector.append(float(row['%20s' % 'attenuation']))
                for i in range(0, number_onu):
                    ber_matrix[i].append(float(row['%20s' % ('ber_value' + str(i + 1))]))
                    ploss_ratio_matrix[i].append(float(row['%20s' % ('ploss_ratio' + str(i + 1))]))
                    if (xaxis_scale == 'Rx_Pwr (dBm)'):
                        rx_pwr_matrix[i].append(ref_rx_pwr_matrix[i][ref_att_vector.index(att_vector[-1])])
                    elif (xaxis_scale == 'OMA (dBm)'):
                        oma_matrix[i].append(ref_oma_matrix[i][ref_att_vector.index(att_vector[-1])])
    
        # Plot data
        for i in range(0, number_onu):

            #look for worst observed sensitivity of this ONU in case plot is in OMA
            if(xaxis_scale == 'OMA (dBm)'):
                # look for worst observed sensitivity at each target BER
                for target_ber_idx in range(0, len(ber_target_list)):				
                    not_found=1
                    itr=1					
                    while(not_found and itr<len(ber_matrix[i])):
                        if ((ber_matrix[i][itr] > ber_target_list[target_ber_idx] and ber_matrix[i][itr-1] <= ber_target_list[target_ber_idx])): #assumes croissance - implement more generic code afterwards
                            #linear approximation between points
                            ym=ber_target_list[target_ber_idx]
                            y1=ber_matrix[i][itr]
                            y2=ber_matrix[i][itr-1]
                            x2=oma_matrix[i][itr]							
                            x1=oma_matrix[i][itr-1]
                            m=(y2-y1)/(x2-x1)
                            xm=(ym-y1)/m + x1							
                            #interpolated_oma = oma_matrix[i][itr-1]
                            interpolated_oma = xm                           
                            #print(interpolated_oma)						
                            not_found=0
                        itr=itr+1
                    if(not_found) : interpolated_oma = oma_matrix[i][-1]
                    if(interpolated_oma > worst_sensitivity_oma[target_ber_idx]): worst_sensitivity_oma[target_ber_idx]=interpolated_oma

            #cur_fig_index = int(numpy.floor(i / onu_per_figure))
            cur_fig_index = 0			
            plt.figure(cur_fig_index + 1)

    			
            # Config BER plot axis
            plt.subplot(1, 1, (i % onu_per_figure) + 1)
    
            if (xaxis_scale == 'Attenuation (dB)'):
                plt.semilogy(att_vector, ber_matrix[i], color=cfg_curve_style)
            elif (xaxis_scale == 'Rx_Pwr (dBm)'):
                plt.semilogy(rx_pwr_matrix[i], ber_matrix[i], color=cfg_curve_style)
            elif (xaxis_scale == 'OMA (dBm)'):
                plt.semilogy(oma_matrix[i], ber_matrix[i], color=cfg_curve_style[i],linestyle='-')
            plt.xlabel(xaxis_scale)    
            plt.title('Upstream')
            if ((i % onu_per_figure) == 0):
                plt.ylabel('Bit Error Ratio')
            plt.axis([xaxis_min, xaxis_max, yaxis_min, yaxis_max])
            ax = plt.gca()
            ax.grid(True)
            if (xaxis_scale == 'Attenuation (dB)'):
                ax.invert_xaxis()
    
            ## Config PLR plot axis
            #plt.subplot(1, 2, (i % onu_per_figure) + 1 + onu_per_figure)
            #if (xaxis_scale == 'Attenuation (dB)'):
            #    plt.semilogy(att_vector, ploss_ratio_matrix[i], color=cfg_curve_style)
            #elif (xaxis_scale == 'Rx_Pwr (dBm)'):
            #    plt.semilogy(rx_pwr_matrix[i], ploss_ratio_matrix[i], color=cfg_curve_style)
            #elif (xaxis_scale == 'OMA (dBm)'):
            #    plt.semilogy(oma_matrix[i], ploss_ratio_matrix[i], color=cfg_curve_style[i],linestyle='-')
            #if ((i % onu_per_figure) == 0):
            #    plt.ylabel('Packet Loss Ratio')
            #plt.axis([xaxis_min, xaxis_max, yaxis_min*56, yaxis_max])
            #ax = plt.gca()
            #ax.grid(True)
            #if (xaxis_scale == 'Attenuation (dB)'):
            #    ax.invert_xaxis()
            #plt.xlabel(xaxis_scale)

    plt.figure(1)
    #plt.subplot(1,2,1)
    plt.legend(['ONU1', 'ONU2'])
    #plt.subplot(1,2,2)
    #plt.legend(['ONU1', 'ONU2', 'ONU3', 'ONU4', 'ONU5', 'ONU6', 'ONU7', 'ONU8'])
    

    if(not save_ndisplay) : plt.show() # Display image
    else : plt.savefig(output_file+'_img.png') # Save image

    return worst_sensitivity_oma
	
def plot_bert_upstream_comparison(rx_pwr_file, extinction_ratio_file, up_bert_file, save_ndisplay, output_file, ber_target_list=[1e-3],read_config_file=1, number_onu=8, number_curves=1):
    plt.clf()
    linestyle_cfg = [':', '-']
    for alpha in range(0,2):
        if read_config_file:
            # Read number_onu and number_curves from config_file
            with open(up_bert_file[alpha] + '_config.csv') as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    number_onu = int(row['%20s' % 'number_onu'])
                    number_curves = int(row['%20s' % 'number_curves'])
        
        num_figures = int(numpy.ceil(number_onu / onu_per_figure))
        cfg_curve_style =  cm.rainbow(numpy.linspace(0, 1, number_onu))

        
        if ((xaxis_scale == 'OMA (dBm)') or (xaxis_scale == 'Rx_Pwr (dBm)')):
            ref_rx_pwr_matrix = []
            ref_oma_matrix = []
            ref_att_vector = []
            extinction_ratio = []
            # Read extinction ratio for each ONU
            with open(extinction_ratio_file[alpha] + '.csv') as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    for i in range(1,number_onu+1):
                        extinction_ratio.append(float(row['%20s' % ('extinction_ratio' + str(i))]))
                        ref_rx_pwr_matrix.append([])
                        ref_oma_matrix.append([])
        
            # Read received power for each ONU and calculate OMA
            with open(rx_pwr_file[alpha] + '.csv') as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    ref_att_vector.append(float(row['%20s' % ('attenuation')]))
                    for i in range(0,number_onu):
                        ref_rx_pwr_matrix[i].append(float(row['%20s' % ('rx_pwr' + str(i+1))]))
                        ref_oma_matrix[i].append(ref_rx_pwr_matrix[i][-1] + 10 * math.log10(2 * (extinction_ratio[i] - 1) / (extinction_ratio[i] + 1)))
        
        worst_sensitivity_oma = [-999.0] * len(ber_target_list) #-999 - a very small number which we will surely find any sensitivity bigger than
        for curve_index in range(0, number_curves):
            # Init
            att_vector = []
            rx_pwr_matrix = []
            oma_matrix = []
            ber_matrix = []
            ploss_ratio_matrix = []
            for i in range(0, number_onu):
                ber_matrix.append([])
                ploss_ratio_matrix.append([])
                rx_pwr_matrix.append([])
                oma_matrix.append([])
        
            # Read data curve
            with open(up_bert_file[alpha] + str(curve_index) + '.csv') as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    att_vector.append(float(row['%20s' % 'attenuation']))
                    for i in range(0, number_onu):
                        ber_matrix[i].append(float(row['%20s' % ('ber_value' + str(i + 1))]))
                        ploss_ratio_matrix[i].append(float(row['%20s' % ('ploss_ratio' + str(i + 1))]))
                        if (xaxis_scale == 'Rx_Pwr (dBm)'):
                            rx_pwr_matrix[i].append(ref_rx_pwr_matrix[i][ref_att_vector.index(att_vector[-1])])
                        elif (xaxis_scale == 'OMA (dBm)'):
                            oma_matrix[i].append(ref_oma_matrix[i][ref_att_vector.index(att_vector[-1])])
        
            # Plot data
            for i in range(0, number_onu):
        
                #look for worst observed sensitivity of this ONU in case plot is in OMA
                if(xaxis_scale == 'OMA (dBm)'):
                    # look for worst observed sensitivity at each target BER
                    for target_ber_idx in range(0, len(ber_target_list)):				
                        not_found=1
                        itr=1					
                        while(not_found and itr<len(ber_matrix[i])):
                            if ((ber_matrix[i][itr] > ber_target_list[target_ber_idx] and ber_matrix[i][itr-1] <= ber_target_list[target_ber_idx])): #assumes croissance - implement more generic code afterwards
                                #linear approximation between points
                                ym=ber_target_list[target_ber_idx]
                                y1=ber_matrix[i][itr]
                                y2=ber_matrix[i][itr-1]
                                x2=oma_matrix[i][itr]							
                                x1=oma_matrix[i][itr-1]
                                m=(y2-y1)/(x2-x1)
                                xm=(ym-y1)/m + x1							
                                #interpolated_oma = oma_matrix[i][itr-1]
                                interpolated_oma = xm                           
                                #print(interpolated_oma)						
                                not_found=0
                            itr=itr+1
                        if(not_found) : interpolated_oma = oma_matrix[i][-1]
                        if(interpolated_oma > worst_sensitivity_oma[target_ber_idx]): worst_sensitivity_oma[target_ber_idx]=interpolated_oma
        
                #cur_fig_index = int(numpy.floor(i / onu_per_figure))
                cur_fig_index = 0			
                plt.figure(cur_fig_index + 1)
        
        			
                # Config BER plot axis
                plt.subplot(1, 1, (i % onu_per_figure) + 1)
        
                if (xaxis_scale == 'Attenuation (dB)'):
                    plt.semilogy(att_vector, ber_matrix[i], color=cfg_curve_style)
                elif (xaxis_scale == 'Rx_Pwr (dBm)'):
                    plt.semilogy(rx_pwr_matrix[i], ber_matrix[i], color=cfg_curve_style)
                elif (xaxis_scale == 'OMA (dBm)'):
                    plt.semilogy(oma_matrix[i], ber_matrix[i], color=cfg_curve_style[i],linestyle=linestyle_cfg[alpha])
                plt.xlabel(xaxis_scale)    
                plt.title('Upstream')
                if ((i % onu_per_figure) == 0):
                    plt.ylabel('Bit Error Ratio')
                plt.axis([xaxis_min, xaxis_max, yaxis_min, yaxis_max])
                ax = plt.gca()
                ax.grid(True)
                if (xaxis_scale == 'Attenuation (dB)'):
                    ax.invert_xaxis()
        
                ## Config PLR plot axis
                #plt.subplot(1, 2, (i % onu_per_figure) + 1 + onu_per_figure)
                #if (xaxis_scale == 'Attenuation (dB)'):
                #    plt.semilogy(att_vector, ploss_ratio_matrix[i], color=cfg_curve_style)
                #elif (xaxis_scale == 'Rx_Pwr (dBm)'):
                #    plt.semilogy(rx_pwr_matrix[i], ploss_ratio_matrix[i], color=cfg_curve_style)
                #elif (xaxis_scale == 'OMA (dBm)'):
                #    plt.semilogy(oma_matrix[i], ploss_ratio_matrix[i], color=cfg_curve_style[i],linestyle='-')
                #if ((i % onu_per_figure) == 0):
                #    plt.ylabel('Packet Loss Ratio')
                #plt.axis([xaxis_min, xaxis_max, yaxis_min*56, yaxis_max])
                #ax = plt.gca()
                #ax.grid(True)
                #if (xaxis_scale == 'Attenuation (dB)'):
                #    ax.invert_xaxis()
                #plt.xlabel(xaxis_scale)
        
        plt.figure(1)
        #plt.subplot(1,2,1)
        plt.legend(['ONU1-New GoFoton', 'ONU2-New GoFoton', 'ONU1-Old GoFoton', 'ONU2-Old GoFoton'])
        #plt.subplot(1,2,2)
        #plt.legend(['ONU1', 'ONU2', 'ONU3', 'ONU4', 'ONU5', 'ONU6', 'ONU7', 'ONU8'])
    

    if(not save_ndisplay) : plt.show() # Display image
    else : plt.savefig(output_file+'_img.png') # Save image

    return worst_sensitivity_oma