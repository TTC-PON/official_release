#!/usr/bin/env python

####################################################
### Example design for board config of:          ###
###     one ONU3 in A10GX + TTC-PON FMC          ###
####################################################

####################################################
###          Python Native Packages              ###
####################################################
import logging
import time

####################################################
###              PON Exdsg cores                 ###
####################################################
from ttcpon_core.ttcpon_exdsg import OnuExdsg

####################################################
###            PON-ONU Eye diagrams              ###
####################################################


####################################################
###             PON Board Config                 ###
####################################################
import ttcpon_board_config.pon_a10gx_i2c_program as program_a10gx


def main(onu):

    ####################################################
    ###          Parameters for Application          ###
    ####################################################	
    INIT_ONU                        = 1  # possible values: 1 or 0
    PERFORM_ONU_EYE_SCAN            = 1  # possible values: 1 or 0

    ####################################################
    ###        Start TTC-PON Ex. Application         ###
    ####################################################
    logger_pon.info('Started TTC-PON TEST application')

    ####################################################
    ###          Initialization of OLT/ONUs          ###
    ####################################################

    ### INIT - ONU_3 - Mounted on on-board SFP connector - A10GX Board  (Arria10)
    if(INIT_ONU):	
        logger_pon.info('---------------------------------------------------------')	
        logger_pon.info('------------------- PROGRAM ONU2-A10GX ------------------')
        onu[0].exd_set_addr(2) # replace 2 by the address desired
        program_a10gx.monitor_sfp(onu[0], 1)
        logger_pon.info('                                                         ')
        time.sleep(1)
    if(PERFORM_ONU_EYE_SCAN):	
        logger_pon.info('- Eye scan feature not yet supported for ONU-A10 device')
        logger_pon.info('                                                         ')
        time.sleep(1)

if __name__ == '__main__':
    # Logger TTC-PON CORE definition:
    # create logger with 'ttc_pon' application
    logger_pon = logging.getLogger('ttc_pon')
    logger_pon.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('./logger/ttcpon_log.log')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s: %(message)s',
        datefmt='%d-%m-%y %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger_pon.addHandler(fh)
    logger_pon.addHandler(ch)

    # Logger TTC-PON EXDSG definition:
    # create logger with 'ttc_pon' application
    logger_exd_pon = logging.getLogger('ttc_pon_tests')
    logger_exd_pon.setLevel(logging.DEBUG)
    # add the handlers to the logger
    logger_exd_pon.addHandler(fh)
    logger_exd_pon.addHandler(ch)

    # Open sockets
    # open ONU socket
    onu = [OnuExdsg('127.0.0.1', 2540)]

    main(onu)
