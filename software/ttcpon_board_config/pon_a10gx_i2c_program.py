#!/usr/bin/env python

import csv
import datetime
import math
import time
import logging
from random import randint

logger_pon = logging.getLogger('ttc_pon')

# -------------------------------------------------------------
#  ---------------------I2C SLAVE ADDR ----------------------
# -------------------------------------------------------------
I2C_A10GX_SFPA0_SLAVE_ADDRESS    = 0x50
I2C_A10GX_SFPA2_SLAVE_ADDRESS    = 0x51


# -------------------------------------------------------------
#  ----------------- Program A10GX Section -----------------
# -------------------------------------------------------------

def monitor_sfp(i2c_master, conv_nraw):
    """
        ***************************************************************************
        Monitor SFP
        ***************************************************************************

        ***************************************************************************
        arg:
            i2c_master       : Object with the methods i2c_read, i2c_write as defined in the OltCore and OnuCore classes		
            conv_nraw: (0/1) convert values for internally calibrated module or give raw register values
        return:
            [internally_calibrated, rx_pwr_unit_avg_noma, temp_conv, vcc_conv, tx_bias_conv, tx_pwr_conv, rx_pwr_conv, vendor_name, vendor_part_number, vendor_serial_number]				
        ***************************************************************************
    """
    slv_addr  = I2C_A10GX_SFPA0_SLAVE_ADDRESS

    logger_pon.info('---------------------------------------------------------')
    logger_pon.info('Transceiver diagnostic monitoring')

    # Diagnostic monitoring type
    reg_addr = 92
    reg_value = i2c_master.i2c_read(slv_addr, reg_addr, 1)
    reg_value = reg_value[0]
    if(reg_value==-1):
        logger_pon.warn('Failed to read from SFP')
        return 0

    # Check implementation
    # Digital diagnostic monitoring implemented?
    if(reg_value & 0b01000000):
        logger_pon.info('SFP digital diagnostic monitoring implemented')	
    else:
        logger_pon.warn('SFP digital diagnostic monitoring not implemented, exiting function')	
        return 0

	# Internally or externally calibrated?
    if(reg_value & 0b00100000):
        internally_calibrated = 1
        logger_pon.info('SFP internally calibrated diagnostic interface')    			
    elif(reg_value & 0b00010000):
        internally_calibrated = 0
        logger_pon.warn('Diagnostic monitoring interface is externally calibrated, this function has to be extended to convert values')    	
    else:
        logger_pon.warn('Unrecognized calibration type')    	

    # Rx power is in OMA or Average power?
    if(reg_value & 0b00001000):
        rx_pwr_unit_avg_noma = 1
        rx_pwr_unit_avg_noma_str = '(AVG)'		
    else:
        rx_pwr_unit_avg_noma = 0
        rx_pwr_unit_avg_noma_str = '(OMA)'

    # Read vendor name
    reg_bank = []
    for reg_addr in range(20,36,2):
        reg_value = i2c_master.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            logger_pon.warn('Failed to read from SFP')
            return 0
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)
    vendor_name = ''.join(chr(i) for i in reg_bank)

    # Read vendor part number
    reg_bank = []
    for reg_addr in range(40,56,2):
        reg_value = i2c_master.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            logger_pon.warn('Failed to read from SFP')
            return 0
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)
    vendor_part_number = ''.join(chr(i) for i in reg_bank)

    # Read vendor serial number
    reg_bank = []
    for reg_addr in range(68,84,2):
        reg_value = i2c_master.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            logger_pon.warn('Failed to read from SFP')
            return 0
        reg_value2 = reg_value[1]
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)
    vendor_serial_number = ''.join(chr(i) for i in reg_bank)

    slv_addr  = I2C_A10GX_SFPA2_SLAVE_ADDRESS
    # Read digital diagnostic
    reg_bank = []
    for reg_addr in range(96,106,2):
        reg_value = i2c_master.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            logger_pon.warn('Failed to read from SFP')
            return 0
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

    temp    = (reg_bank[0]<<8) + reg_bank[1]
    vcc     = (reg_bank[2]<<8) + reg_bank[3]
    tx_bias = (reg_bank[4]<<8) + reg_bank[5]	
    tx_pwr  = (reg_bank[6]<<8) + reg_bank[7]
    rx_pwr  = (reg_bank[8]<<8) + reg_bank[9]

    # Convert values if internally calibrated:
    # OBS: The following texts are extracted from SFF-8472 standard for more information, read the standard
    if(internally_calibrated):
        # TEMPERATURE:
        # 1) Internally measured transceiver temperature. Represented as a 16 bit signed twos complement value in
        # increments of 1/256 degrees Celsius
        if(temp&0x8000):
            sign = -1
            temp_conv = (temp ^ 0xFFFF) + 1
        else:
            sign = 1
            temp_conv = temp

        temp_conv = sign*temp_conv*(1.0/256.0) # UNIT: degrees Celsius
        temp_unit = 'degC'

        # SUPPLY VOLTAGE:
        # 2) Internally measured transceiver supply voltage. Represented as a 16 bit unsigned integer with the
        # voltage defined as the full 16 bit value (0-65535) with LSB equal to 100 uVolt
        vcc_conv = (vcc*100e-6) # UNIT: V
        vcc_unit = 'V'

		# TX BIAS CURRENT:
        # 3) Measured TX bias current in uA. Represented as a 16 bit unsigned integer with the current defined as the
        # full 16 bit value (0-65535) with LSB equal to 2 uA
        # OBS: FOR OLT THIS FACTOR IS DIFFERENT - see datasheet (4uA)
        if(vendor_part_number=='LTF7222-BC      ' or vendor_part_number=='SOGX6292-PSGB   '):	
            tx_bias_conv = (tx_bias*4e-6)*1e3 # UNIT: mA
        else:
            tx_bias_conv = (tx_bias*2e-6)*1e3 # UNIT: mA		
        tx_bias_unit = 'mA'

		# TX OUTPUT POWER:
        # 4) Measured TX output power in mW. Represented as a 16 bit unsigned integer with the power defined as
        # the full 16 bit value (0-65535) with LSB equal to 0.1 uW
        if(tx_pwr!=0):
            # OBS: FOR OLT THIS FACTOR IS DIFFERENT - see datasheet (0.2uW)
            if(vendor_part_number=='LTF7222-BC      ' or vendor_part_number=='SOGX6292-PSGB   '):			
                tx_pwr_conv = 10*math.log10((tx_pwr * 0.2 * 1e-6)/1e-3) # UNIT: dBm
            else:
                tx_pwr_conv = 10*math.log10((tx_pwr * 0.1 * 1e-6)/1e-3) # UNIT: dBm			
        else:
            tx_pwr_conv = float('-inf')
        tx_pwr_unit = 'dBm'

		# RX RECEIVED OPTICAL POWER:
        # 5) Measured RX received optical power in mW. Value can represent either average received power or OMA
        # depending upon how bit 3 of byte 92 (A0h) is set. Represented as a 16 bit unsigned integer with the
        # power defined as the full 16 bit value (0-65535) with LSB equal to 0.1 uW
        if(rx_pwr!=0):		
            rx_pwr_conv = 10*math.log10((rx_pwr * 0.1 * 1e-6)/1e-3) # UNIT: dBm
        else:
            rx_pwr_conv = float('-inf')
        rx_pwr_unit = 'dBm'
    else:
        temp_conv    = temp
        vcc_conv     = vcc
        tx_bias_conv = tx_bias
        tx_pwr_conv  = tx_pwr
        rx_pwr_conv  = rx_pwr
        temp_unit    = 'au'
        vcc_unit     = 'au'
        tx_bias_unit = 'au'
        tx_pwr_unit  = 'au'
        rx_pwr_unit  = 'au'

    logger_pon.info('---------------------------------------------------------')
    msg_print = ('Vendor Name          : ' + vendor_name)
    logger_pon.info(msg_print)
    msg_print = ('Vendor Part Number   : ' + vendor_part_number)
    logger_pon.info(msg_print)	
    msg_print = ('Vendor Serial Number : ' + vendor_serial_number)
    logger_pon.info(msg_print)
    logger_pon.info('---------------------------------------------------------')	
    logger_pon.info('Diagnostic reading:')
    msg_print = ('Temperature : %5.1f ' + temp_unit) % temp_conv
    logger_pon.info(msg_print)
    msg_print = ('VCC         : %5.1f ' + vcc_unit) % vcc_conv
    logger_pon.info(msg_print)
    msg_print = ('Tx_bias_cur : %5.1f ' + tx_bias_unit) % tx_bias_conv
    logger_pon.info(msg_print)
    msg_print = ('Tx_Pwr(AVG) : %5.1f ' + tx_pwr_unit) % tx_pwr_conv
    logger_pon.info(msg_print)
    msg_print = ('Rx_Pwr(AVG) : %5.1f ' + rx_pwr_unit + ' ' + rx_pwr_unit_avg_noma_str) % rx_pwr_conv	
    logger_pon.info(msg_print)
    logger_pon.info('---------------------------------------------------------')

    if (conv_nraw): return [internally_calibrated, rx_pwr_unit_avg_noma, temp_conv, vcc_conv, tx_bias_conv, tx_pwr_conv, rx_pwr_conv, vendor_name, vendor_part_number, vendor_serial_number]
    else          : return [internally_calibrated, rx_pwr_unit_avg_noma, temp, vcc, tx_bias, tx_pwr, rx_pwr, vendor_name, vendor_part_number, vendor_serial_number]
