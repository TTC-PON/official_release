#!/usr/bin/env python

import csv
import datetime
import math
import time
import logging
from random import randint

from . regbank_si570 import regbank_si570
from . regbank_si5344_pon_fmc_v3 import regbank_si5344
logger_pon = logging.getLogger('ttc_pon')

# -------------------------------------------------------------
#  ------------------ FMC I2C SLAVE ADDR -------------------
# -------------------------------------------------------------
I2C_FMC_SWITCH_SLAVE_ADDRESS   = 0x74
I2C_FMC_LED_GPIO_SLAVE_ADDRESS = 0x38
I2C_FMC_CLK_GPIO_SLAVE_ADDRESS = 0x38
I2C_FMC_SI570_SLAVE_ADDRESS    = 0x55
I2C_FMC_PLL_SLAVE_ADDRESS      = 0x68
I2C_FMC_SFPA0_SLAVE_ADDRESS    = 0x50
I2C_FMC_SFPA2_SLAVE_ADDRESS    = 0x51
I2C_FMC_EEPROM_SLAVE_ADDRESS   = 0x50

I2C_FMC_SEL_NONE               = 0x00
I2C_FMC_SEL_EEPROM             = 0x01
I2C_FMC_SEL_SI570              = 0x02
I2C_FMC_SEL_CLK_GPIO           = 0x04
I2C_FMC_SEL_PLL                = 0x08
I2C_FMC_SEL_LED_GPIO           = 0x10
I2C_FMC_SEL_SFP                = 0x20

# -------------------------------------------------------------
#  ------------------ Program FMC Section -------------------
# -------------------------------------------------------------

def program_leds(i2c_master, leds):
    """
        ***************************************************************************
        Program on-board LEDS
        ***************************************************************************

        ***************************************************************************
        arg:
            i2c_master       : Object with the methods i2c_read, i2c_write as defined in the OltCore and OnuCore classes				
            leds             : Value to be written to Leds
        return:
            success (0/1)	
        ***************************************************************************
    """
    # Select GPIO_LED on FMC SWITCH
    i2c_sel_leds_ok = i2c_master.i2c_write(I2C_FMC_SWITCH_SLAVE_ADDRESS,I2C_FMC_SEL_LED_GPIO,I2C_FMC_SEL_LED_GPIO)
    if(i2c_sel_leds_ok != 1):
        logger_pon.warn('Failed to select slave LED_GPIO')
        return 0

    # Write value to GPIO_LED slave
    i2c_program_leds_ok = i2c_master.i2c_write(I2C_FMC_LED_GPIO_SLAVE_ADDRESS,leds, leds)
    if(i2c_program_leds_ok != 1):
        logger_pon.warn('Failed to program LED_GPIO')
        return 0

    return 1

def program_clkmux(i2c_master, value):
    """
        ***************************************************************************
        Program on-board clkmux
        ***************************************************************************

        ***************************************************************************
        arg:
            i2c_master       : Object with the methods i2c_read, i2c_write as defined in the OltCore and OnuCore classes				
            value            : Value to be written to clkmux
        return:
            success (0/1)	
        ***************************************************************************
    """
    # Select CLKMUX_GPIO on FMC SWITCH
    i2c_sel_clkmux_ok = i2c_master.i2c_write(I2C_FMC_SWITCH_SLAVE_ADDRESS,I2C_FMC_SEL_CLK_GPIO,I2C_FMC_SEL_CLK_GPIO)
    if(i2c_sel_clkmux_ok != 1):
        logger_pon.warn('Failed to select slave CLKMUX_GPIO')
        return 0

    # Write value to GPIO_LED slave
    i2c_program_clkmux_ok = i2c_master.i2c_write(I2C_FMC_CLK_GPIO_SLAVE_ADDRESS, value, value)
    if(i2c_program_clkmux_ok != 1):
        logger_pon.warn('Failed to program CLKMUX_GPIO')
        return 0

    logger_pon.info('Programming of ClkMux was successful!')

    return 1

def program_si570(i2c_master, nb_max_attempt):
    """
        ***************************************************************************
        Program on-board Si570 oscillator

        regbank for the Si570  is a list of lists [ [reg0_addr, reg0_value],
                                                    [reg1_addr, reg1_value]
                                                               ...           
                                                    [regn_addr, regn_value] ]
        ***************************************************************************

        ***************************************************************************
        arg:
            i2c_master       : Object with the methods i2c_read, i2c_write as defined in the OltCore and OnuCore classes				
            nb_max_attempt: max number of programming attempts
        return:
            success (0/1)	
        ***************************************************************************
    """
    # Select EEPROM on FMC SWITCH
    i2c_sel_eeprom_ok = i2c_master.i2c_write(I2C_FMC_SWITCH_SLAVE_ADDRESS,I2C_FMC_SEL_EEPROM,I2C_FMC_SEL_EEPROM)
    if(i2c_sel_eeprom_ok != 1):
        logger_pon.warn('Failed to select slave EEPROM')
        return 0
       
    slv_addr = I2C_FMC_EEPROM_SLAVE_ADDRESS
    regbank_eeprom  = []
    for i in range(0,7):
        reg_value = i2c_master.i2c_read(slv_addr, i)
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            logger_pon.warn('Failed to read from EEPROM')
            return 0
        else:
            regbank_eeprom.append(reg_value1)
    # Read register bank from EEPROM
    if(regbank_eeprom[0] == 1): # EEPROM is properly programmed with Si570 config
        regbank = [
                [0x89, 0x10], # Freeze the DCO by setting Freeze DCO = 1 (bit 4 of register 137)
                [0x07, regbank_eeprom[1]],
                [0x08, regbank_eeprom[2]],
                [0x09, regbank_eeprom[3]],
                [0x0A, regbank_eeprom[4]],
                [0x0B, regbank_eeprom[5]],
                [0x0C, regbank_eeprom[6]],
                [0x89, 0x00], # Unfreeze the DCO by setting Freeze DCO = 0 (bit 4 of register 137)
                [0x87, 0x40], # Assert the NewFreq bit (bit 6 of register 135)
            ];
    else: # EEPROM i not programmed and therefore we take values from default config
        regbank = regbank_si570
        logger_pon.warn('Configuration for Si570 not programmed in EEPROM')
    # Select SI570 on FMC SWITCH
    i2c_sel_si570_ok = i2c_master.i2c_write(I2C_FMC_SWITCH_SLAVE_ADDRESS,I2C_FMC_SEL_SI570,I2C_FMC_SEL_SI570)
    if(i2c_sel_si570_ok != 1):
        logger_pon.warn('Failed to select slave Si570')
        return 0

    slv_addr = I2C_FMC_SI570_SLAVE_ADDRESS

    t0 = time.time()
    for i in range(0,len(regbank)):
        i2c_transaction_done = 0
        attempt_cntr = 0

        reg_addr  = regbank[i][0]
        reg_value = regbank[i][1]
		
        while(i2c_transaction_done==0 and attempt_cntr<nb_max_attempt):
            i2c_transaction_done = i2c_master.i2c_write(slv_addr, reg_addr, reg_value,0)
            attempt_cntr = attempt_cntr + 1

        if(i2c_transaction_done==0):
            logger_pon.warn('Failed to program Si570')
            return 0
    t1 = time.time()

    logger_pon.info('Programming of Si570 was successful! (elapsed: %3dms)' % (1e3*(t1-t0)))

    return 1

def gen_config_si570(i2c_master, nb_max_attempt=5):
    """
        ***************************************************************************
        Read default values of Si570, generates new configuration and write in EEPROM
        ***************************************************************************

        ***************************************************************************
        arg:
            i2c_master       : Object with the methods i2c_read, i2c_write as defined in the OltCore and OnuCore classes				
        return:
            success (0/1)	
        ***************************************************************************
    """
    t0 = time.time()
    # Select SI570 on FMC SWITCH
    i2c_sel_si570_ok = i2c_master.i2c_write(I2C_FMC_SWITCH_SLAVE_ADDRESS,I2C_FMC_SEL_SI570,I2C_FMC_SEL_SI570)
    if(i2c_sel_si570_ok != 1):
        logger_pon.warn('Failed to select slave Si570')
        return 0

    slv_addr = I2C_FMC_SI570_SLAVE_ADDRESS
    regbank  = []
    for i in range(7,13):
        reg_value = i2c_master.i2c_read(slv_addr, i)
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            logger_pon.warn('Failed to read from Si570')
            return 0
        else:
            regbank.append(reg_value1)

    HS_DIV = (regbank[0]>>5) & 0b111
    N1     = (((regbank[0]>>0) & 0b11111)<<2 )  + ((regbank[1]>>6) & 0b11)
    RFREQ  = (((regbank[1]>>0) & 0b111111)<<32) + (regbank[2]<<24) + (regbank[3]<<16)+ (regbank[4]<<8)+ (regbank[5]<<0)
    
    #Format values
    HS_DIV     = HS_DIV+4
    N1         = N1+1
    RFREQ      = RFREQ/2**28
               
    fxtal      = 100.0e6*HS_DIV*N1/RFREQ
    f1         = 240.0e6
    N1_NEW     = 2
    HS_DIV_NEW = 11
    fdco       = f1*HS_DIV_NEW*N1_NEW
    RFREQ_NEW  = fdco/fxtal    
    
    # Format values inversion
    HS_DIV_NEW = HS_DIV_NEW-4
    N1_NEW     = N1_NEW-1
    RFREQ_NEW  = round(RFREQ_NEW*(2**28))

    regbank_new = [1]
    regbank_new.append( (HS_DIV_NEW<<5) + (N1_NEW>>2))
    regbank_new.append( ((N1_NEW&0b11)<<6) + (RFREQ_NEW>>32)&0xFF)
    regbank_new.append( (RFREQ_NEW>>24)&0xFF )
    regbank_new.append( (RFREQ_NEW>>16)&0xFF )
    regbank_new.append( (RFREQ_NEW>>8)&0xFF )
    regbank_new.append( (RFREQ_NEW>>0)&0xFF )
    
    print(regbank_new)

    # Select EEPROM on FMC SWITCH
    i2c_sel_si570_ok = i2c_master.i2c_write(I2C_FMC_SWITCH_SLAVE_ADDRESS,I2C_FMC_SEL_EEPROM,I2C_FMC_SEL_EEPROM)
    if(i2c_sel_si570_ok != 1):
        logger_pon.warn('Failed to select slave eeprom')
        return 0

    slv_addr = I2C_FMC_EEPROM_SLAVE_ADDRESS
    reg_addr = 0    
    for reg_value in regbank_new:
        i2c_transaction_done = 0
        attempt_cntr = 0

        while(i2c_transaction_done==0 and attempt_cntr<nb_max_attempt):
            i2c_transaction_done = i2c_master.i2c_write(slv_addr, reg_addr, reg_value)
            attempt_cntr = attempt_cntr + 1

        if(i2c_transaction_done==0):
            logger_pon.warn('Failed to program EEPROM')
            return 0

        reg_addr = reg_addr + 1

    logger_pon.info('Programming of EEPROM was successful!')

    return 1

def program_si5344(i2c_master, nb_max_attempt, input_nfreerun=1):
    """
        ***************************************************************************
        Program on-board Si5344 PLL

        regbank for the Si5344 is a list of lists [[page_sel0 / reg0_addr, reg0_value]
                                                   [page_sel1 / reg1_addr, reg1_value]
                                                                   ...                
                                                   [page_seln / regn_addr, regn_value]]
        ***************************************************************************

        ***************************************************************************
        arg:
            i2c_master       : Object with the methods i2c_read, i2c_write as defined in the OltCore and OnuCore classes				
            nb_max_attempt: max number of programming attempts
        return:
            success (0/1)	
        ***************************************************************************
    """
    # Select SI5344 on FMC SWITCH
    i2c_sel_si5344_ok = i2c_master.i2c_write(I2C_FMC_SWITCH_SLAVE_ADDRESS,I2C_FMC_SEL_PLL,I2C_FMC_SEL_PLL)
    if(i2c_sel_si5344_ok != 1):
        logger_pon.warn('Failed to select slave Si5344')
        return 0

    slv_addr = I2C_FMC_PLL_SLAVE_ADDRESS
	
    if(input_nfreerun) : regbank = regbank_si5344
    else               : regbank = regbank_si5344_freerun
    previous_page_sel = 0xFF

    # Program register bank on PLL
    for i in range(0, len(regbank)):
        if(i==3) : time.sleep(1) # Wait after preamble
        page_sel  = (regbank[i][0] & 0xFF00) >> 8
        reg_addr  = regbank[i][0] & 0x00FF
        reg_value = regbank[i][1]

        if(page_sel != previous_page_sel):
            i2c_transaction_done = 0
            attempt_cntr = 0

            while(i2c_transaction_done==0 and attempt_cntr<nb_max_attempt):
                i2c_transaction_done = i2c_master.i2c_write(slv_addr, 0x01, page_sel)
                attempt_cntr = attempt_cntr + 1

            if(i2c_transaction_done==0):
                logger_pon.warn('Failed to set page select on Si5344 - failed to program device')
                return 0

        i2c_transaction_done = 0
        attempt_cntr = 0

        while(i2c_transaction_done==0 and attempt_cntr<nb_max_attempt):
            i2c_transaction_done = i2c_master.i2c_write(slv_addr, reg_addr, reg_value)
            attempt_cntr = attempt_cntr + 1

        if(i2c_transaction_done==0):
            logger_pon.warn('Failed to program Si5344')
            return 0

    logger_pon.info('Programming of Si5344 was successful!')

    return 1

def set_phase_si5344(i2c_master, pll_output, skew):
    """
        ***************************************************************************
        Set skew on MultiSynth divider of Si5344 PLL
        ***************************************************************************

        ***************************************************************************
        arg:
            i2c_master       : Object with the methods i2c_read, i2c_write as defined in the OltCore and OnuCore classes				
            pll_output       : Multisynth divider PLL is connected (0-3)
            skew             : skew value to be written (Nx_Delay)
                               Nx_DELAY/(256 x Fvco)
                               Fvco = 13.68 GHz [ 13 + 17/25 GHz ]
                               In our function we are only using the 8-MSB of the Nx_delay,
                               Each step corresponds to 2**8 steps			
        return:
            success (0/1)	
        ***************************************************************************
    """
    # Select SI5344 on FMC SWITCH
    i2c_sel_si5344_ok = i2c_master.i2c_write(I2C_FMC_SWITCH_SLAVE_ADDRESS,I2C_FMC_SEL_PLL,I2C_FMC_SEL_PLL)
    if(i2c_sel_si5344_ok != 1):
        logger_pon.warn('Failed to select slave Si5344')
        return 0

    # Page select
    slv_addr = I2C_FMC_PLL_SLAVE_ADDRESS
    reg_addr = 0x01
    page_sel = 0x03
    i2c_transaction_done = i2c_master.i2c_write(slv_addr, reg_addr, page_sel)
    if(i2c_transaction_done==0):
        logger_pon.warn('Failed to set page select on Si5344 - failed to program device')
        return 0

    # Write skew register
    reg_addr  = 0x59 + 2*pll_output
    reg_value = skew
    i2c_transaction_done = i2c_master.i2c_write(slv_addr, reg_addr, 0)
    if(i2c_transaction_done==0):
        logger_pon.warn('Failed to set phase of Si5344')
        return 0

    reg_addr  = 0x5A + 2*pll_output
    reg_value = skew
    i2c_transaction_done = i2c_master.i2c_write(slv_addr, reg_addr, reg_value)
    if(i2c_transaction_done==0):
        logger_pon.warn('Failed to set phase of Si5344')
        return 0

    # Page select
    reg_addr = 0x01
    page_sel = 0x00
    i2c_transaction_done = i2c_master.i2c_write(slv_addr, reg_addr, page_sel)
    if(i2c_transaction_done==0):
        logger_pon.warn('Failed to set page select on Si5344 - failed to program device')
        return 0

    # Perform soft reset
    reg_addr  = 0x1C
    reg_value = 0x01
    i2c_transaction_done = i2c_master.i2c_write(slv_addr, reg_addr, reg_value)
    if(i2c_transaction_done==0):
        logger_pon.warn('Failed to perform Soft reset of Si5344')
        return 0

    # logger_pon.info('Phase set done for Si5344 - output:' + str(pll_output) + ' / skew:' + str(skew))

    return 1

def get_status_si5344(i2c_master):
    """
        ***************************************************************************
        Get status of Si5344 PLL
        ***************************************************************************

        ***************************************************************************
        arg:
            i2c_master       : Object with the methods i2c_read, i2c_write as defined in the OltCore and OnuCore classes						
        return:
            [success, LOS, LOL]
        ***************************************************************************
    """
    # Select SI5344 on FMC SWITCH
    i2c_sel_si5344_ok = i2c_master.i2c_write(I2C_FMC_SWITCH_SLAVE_ADDRESS,I2C_FMC_SEL_PLL,I2C_FMC_SEL_PLL)
    if(i2c_sel_si5344_ok != 1):
        logger_pon.warn('Failed to select slave Si5344')
        return  [0,0,0]

    slv_addr = I2C_FMC_PLL_SLAVE_ADDRESS

    # Page select
    reg_addr = 0x01
    page_sel = 0x00
    i2c_transaction_done = i2c_master.i2c_write(slv_addr, reg_addr, page_sel)
    if(i2c_transaction_done==0):
        logger_pon.warn('Failed to set page select on Si5344 - failed to program device')
        return  [0,0,0]

    # Get LOS
    reg_addr = 0x0D
    reg_value = i2c_master.i2c_read(slv_addr, reg_addr,2) # double burst-read access
    reg_value1 = reg_value[0]
    if(reg_value1==-1):
        logger_pon.warn('Failed to read from Si5344')
        return [0,0,0]

    reg_value2 = reg_value[1]

    LOS = (reg_value1&0x02)>>1
    LOL = (reg_value1&0x02)>>1

    return [1, LOS, LOL]

def monitor_sfp(i2c_master, conv_nraw):
    """
        ***************************************************************************
        Monitor SFP
        ***************************************************************************

        ***************************************************************************
        arg:
            i2c_master       : Object with the methods i2c_read, i2c_write as defined in the OltCore and OnuCore classes		
            conv_nraw: (0/1) convert values for internally calibrated module or give raw register values
        return:
            [internally_calibrated, rx_pwr_unit_avg_noma, temp_conv, vcc_conv, tx_bias_conv, tx_pwr_conv, rx_pwr_conv, vendor_name, vendor_part_number, vendor_serial_number]				
        ***************************************************************************
    """
    # Select SFP on FMC SWITCH
    i2c_sel_sfp_ok = i2c_master.i2c_write(I2C_FMC_SWITCH_SLAVE_ADDRESS,I2C_FMC_SEL_SFP,I2C_FMC_SEL_SFP)
    if(i2c_sel_sfp_ok != 1):
        logger_pon.warn('Failed to select slave SFP')
        return 0

    slv_addr  = I2C_FMC_SFPA0_SLAVE_ADDRESS

    logger_pon.info('---------------------------------------------------------')
    logger_pon.info('Transceiver diagnostic monitoring')

    # Diagnostic monitoring type
    reg_addr = 92
    reg_value = i2c_master.i2c_read(slv_addr, reg_addr, 1)
    reg_value = reg_value[0]
    if(reg_value==-1):
        logger_pon.warn('Failed to read from SFP')
        return 0

    # Check implementation
    # Digital diagnostic monitoring implemented?
    if(reg_value & 0b01000000):
        logger_pon.info('SFP digital diagnostic monitoring implemented')	
    else:
        logger_pon.warn('SFP digital diagnostic monitoring not implemented, exiting function')	
        return 0

	# Internally or externally calibrated?
    if(reg_value & 0b00100000):
        internally_calibrated = 1
        logger_pon.info('SFP internally calibrated diagnostic interface')    			
    elif(reg_value & 0b00010000):
        internally_calibrated = 0
        logger_pon.warn('Diagnostic monitoring interface is externally calibrated, this function has to be extended to convert values')    	
    else:
        logger_pon.warn('Unrecognized calibration type')    	

    # Rx power is in OMA or Average power?
    if(reg_value & 0b00001000):
        rx_pwr_unit_avg_noma = 1
        rx_pwr_unit_avg_noma_str = '(AVG)'		
    else:
        rx_pwr_unit_avg_noma = 0
        rx_pwr_unit_avg_noma_str = '(OMA)'

    # Read vendor name
    reg_bank = []
    for reg_addr in range(20,36,2):
        reg_value = i2c_master.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            logger_pon.warn('Failed to read from SFP')
            return 0
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)
    vendor_name = ''.join(chr(i) for i in reg_bank)

    # Read vendor part number
    reg_bank = []
    for reg_addr in range(40,56,2):
        reg_value = i2c_master.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            logger_pon.warn('Failed to read from SFP')
            return 0
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)
    vendor_part_number = ''.join(chr(i) for i in reg_bank)

    # Read vendor serial number
    reg_bank = []
    for reg_addr in range(68,84,2):
        reg_value = i2c_master.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            logger_pon.warn('Failed to read from SFP')
            return 0
        reg_value2 = reg_value[1]
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)
    vendor_serial_number = ''.join(chr(i) for i in reg_bank)

    slv_addr  = I2C_FMC_SFPA2_SLAVE_ADDRESS

    # Read digital diagnostic
    reg_bank = []
    for reg_addr in range(96,106,2):
        reg_value = i2c_master.i2c_read(slv_addr, reg_addr,2) # double burst-read access
        reg_value1 = reg_value[0]
        if(reg_value1==-1):
            logger_pon.warn('Failed to read from SFP')
            return 0
        reg_value2 = reg_value[1]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

    temp    = (reg_bank[0]<<8) + reg_bank[1]
    vcc     = (reg_bank[2]<<8) + reg_bank[3]
    tx_bias = (reg_bank[4]<<8) + reg_bank[5]	
    tx_pwr  = (reg_bank[6]<<8) + reg_bank[7]
    rx_pwr  = (reg_bank[8]<<8) + reg_bank[9]

    # Convert values if internally calibrated:
    # OBS: The following texts are extracted from SFF-8472 standard for more information, read the standard
    if(internally_calibrated):
        # TEMPERATURE:
        # 1) Internally measured transceiver temperature. Represented as a 16 bit signed twos complement value in
        # increments of 1/256 degrees Celsius
        if(temp&0x8000):
            sign = -1
            temp_conv = (temp ^ 0xFFFF) + 1
        else:
            sign = 1
            temp_conv = temp

        temp_conv = sign*temp_conv*(1.0/256.0) # UNIT: degrees Celsius
        temp_unit = 'degC'

        # SUPPLY VOLTAGE:
        # 2) Internally measured transceiver supply voltage. Represented as a 16 bit unsigned integer with the
        # voltage defined as the full 16 bit value (0-65535) with LSB equal to 100 uVolt
        vcc_conv = (vcc*100e-6) # UNIT: V
        vcc_unit = 'V'

		# TX BIAS CURRENT:
        # 3) Measured TX bias current in uA. Represented as a 16 bit unsigned integer with the current defined as the
        # full 16 bit value (0-65535) with LSB equal to 2 uA
        # OBS: FOR OLT THIS FACTOR IS DIFFERENT - see datasheet (4uA)
        if(vendor_part_number=='LTF7222-BC      ' or vendor_part_number=='SOGX6292-PSGB   '):	
            tx_bias_conv = (tx_bias*4e-6)*1e3 # UNIT: mA
        else:
            tx_bias_conv = (tx_bias*2e-6)*1e3 # UNIT: mA		
        tx_bias_unit = 'mA'

		# TX OUTPUT POWER:
        # 4) Measured TX output power in mW. Represented as a 16 bit unsigned integer with the power defined as
        # the full 16 bit value (0-65535) with LSB equal to 0.1 uW
        if(tx_pwr!=0):
            # OBS: FOR OLT THIS FACTOR IS DIFFERENT - see datasheet (0.2uW)
            if(vendor_part_number=='LTF7222-BC      ' or vendor_part_number=='SOGX6292-PSGB   '):			
                tx_pwr_conv = 10*math.log10((tx_pwr * 0.2 * 1e-6)/1e-3) # UNIT: dBm
            else:
                tx_pwr_conv = 10*math.log10((tx_pwr * 0.1 * 1e-6)/1e-3) # UNIT: dBm			
        else:
            tx_pwr_conv = float('-inf')
        tx_pwr_unit = 'dBm'

		# RX RECEIVED OPTICAL POWER:
        # 5) Measured RX received optical power in mW. Value can represent either average received power or OMA
        # depending upon how bit 3 of byte 92 (A0h) is set. Represented as a 16 bit unsigned integer with the
        # power defined as the full 16 bit value (0-65535) with LSB equal to 0.1 uW
        if(rx_pwr!=0):		
            rx_pwr_conv = 10*math.log10((rx_pwr * 0.1 * 1e-6)/1e-3) # UNIT: dBm
        else:
            rx_pwr_conv = float('-inf')
        rx_pwr_unit = 'dBm'
    else:
        temp_conv    = temp
        vcc_conv     = vcc
        tx_bias_conv = tx_bias
        tx_pwr_conv  = tx_pwr
        rx_pwr_conv  = rx_pwr
        temp_unit    = 'au'
        vcc_unit     = 'au'
        tx_bias_unit = 'au'
        tx_pwr_unit  = 'au'
        rx_pwr_unit  = 'au'

    logger_pon.info('---------------------------------------------------------')
    msg_print = ('Vendor Name          : ' + vendor_name)
    logger_pon.info(msg_print)
    msg_print = ('Vendor Part Number   : ' + vendor_part_number)
    logger_pon.info(msg_print)	
    msg_print = ('Vendor Serial Number : ' + vendor_serial_number)
    logger_pon.info(msg_print)
    logger_pon.info('---------------------------------------------------------')	
    logger_pon.info('Diagnostic reading:')
    msg_print = ('Temperature : %5.1f ' + temp_unit) % temp_conv
    logger_pon.info(msg_print)
    msg_print = ('VCC         : %5.1f ' + vcc_unit) % vcc_conv
    logger_pon.info(msg_print)
    msg_print = ('Tx_bias_cur : %5.1f ' + tx_bias_unit) % tx_bias_conv
    logger_pon.info(msg_print)
    msg_print = ('Tx_Pwr      : %5.1f ' + tx_pwr_unit) % tx_pwr_conv
    logger_pon.info(msg_print)
    msg_print = ('Rx_Pwr      : %5.1f ' + rx_pwr_unit + ' ' + rx_pwr_unit_avg_noma_str) % rx_pwr_conv	
    logger_pon.info(msg_print)
    logger_pon.info('---------------------------------------------------------')

    if (conv_nraw): return [internally_calibrated, rx_pwr_unit_avg_noma, temp_conv, vcc_conv, tx_bias_conv, tx_pwr_conv, rx_pwr_conv, vendor_name, vendor_part_number, vendor_serial_number]
    else          : return [internally_calibrated, rx_pwr_unit_avg_noma, temp, vcc, tx_bias, tx_pwr, rx_pwr, vendor_name, vendor_part_number, vendor_serial_number]
