#!/usr/bin/env python

import csv
import datetime
import math
import time
import logging
from random import randint

from . pon_fmc_i2c_program import program_clkmux
from . pon_fmc_i2c_program import program_si570
from . pon_fmc_i2c_program import program_si5344
from . pon_fmc_i2c_program import monitor_sfp
from . pon_fmc_i2c_program import program_leds
from . pon_fmc_i2c_program import set_phase_si5344
from . pon_fmc_i2c_program import get_status_si5344 as get_status_pll

logger_pon = logging.getLogger('ttc_pon')

# Timeout in 100ms units for TX to get ready
c_MAX_TIMEOUT_MGT_READY = 20

# -------------------------------------------------------------
#  ----------------------- Functions ------------------------
# -------------------------------------------------------------

def check_onu_locked(onu):
    """
        ***************************************************************************
        Check if ONU design is locked and phase_good for ONU phase good status
        ***************************************************************************

        ***************************************************************************
        arg:
            onu              : Object of OnuCore class	

        return:
           [locked, phase_good]
        ***************************************************************************
    """
    # Check ONU status before doing phase alignment
    # ONU must be locked in the downstream direction
    status_onu = onu.exd_read_onu_status()
    [rx40_locked, phase_good, rx_locked, operational, mgt_tx_ready, mgt_rx_ready, mgt_txpll_lock, mgt_rxpll_lock] = status_onu
    if(mgt_rxpll_lock==0):
        logger_pon.warn("ONU MGT RX_PLL not locked!")
        return [0,0]
    elif(mgt_rx_ready==0):
        logger_pon.warn("ONU MGT RX not ready!")
        return [0,0]
    elif(rx_locked==0):
        logger_pon.warn("ONU downstream not locked!")
        return [0,0]

    # Check PLL is locked
    pll_locked = 0
    while(pll_locked==0):
        pll_status = get_status_pll(onu)
        if(pll_status[0]==0):
            logger_pon.warn("IIC communication problem during get status of PLL")	
        pll_locked = not (pll_status[2])
        pll_loss_of_signal = pll_status[1]
        if(pll_loss_of_signal):
            logger_pon.warn("ONU PLL lost signal, there might be an issue in the downstream! Check problem!")			
            return [0, 0]

    # If the function arrive here the PLL must be already locked
    # Now we should check that the MGT TX_PLL is locked and the MGT_TX is ready
    timeout_cntr   = 0
    while(not (mgt_txpll_lock and mgt_tx_ready)):
        status_onu = onu.exd_read_onu_status()
        [rx40_locked, phase_good, rx_locked, operational, mgt_tx_ready, mgt_rx_ready, mgt_txpll_lock, mgt_rxpll_lock] = status_onu
        timeout_cntr = timeout_cntr + 1
        time.sleep(0.1)
        if(timeout_cntr > c_MAX_TIMEOUT_MGT_READY):
            logger_pon.warn("TX is not getting ready after timeout! Check problem!")						
            return [0, 0]

    [rx40_locked, phase_good, rx_locked, operational, mgt_tx_ready, mgt_rx_ready, mgt_txpll_lock, mgt_rxpll_lock] = status_onu

    return [1, phase_good]

def onu_init(onu, onu_addr, cascaded_nstandalone=1):
    """
        ***************************************************************************
        ONU local initialization algorithm
        ***************************************************************************

        ***************************************************************************
        arg:
            onu              : Object of OnuCore class
            onu_addr         : Address of ONU for upstream TDM (1-254)
            cascaded_nstandalone : cascaded is the common ONU case
			                       standalone is when a monitoring ONU is used in the same board and OLT clock is the same as ONU clock, all clocks come from oscillator
        return:
           success
        ***************************************************************************
    """
    # Program FMC devices
    if(cascaded_nstandalone) : program_clkmux(onu, 0x37)
    else : program_clkmux(onu, 0xFF)
    program_si570(onu, 1)
    program_si5344(onu, 1, cascaded_nstandalone)

    # Wait some time to PLL output to be stable
    time.sleep(0.6)
    onu.exd_core_reset()
    time.sleep(1.0)

    # Program ONU upstream TDM address
    onu.exd_set_addr(onu_addr)

    # Check ONU status before doing phase alignment - ONU must be locked
    status_onu = onu.exd_read_onu_status()
    [rx40_locked, phase_good, rx_locked, operational, mgt_tx_ready, mgt_rx_ready, mgt_txpll_lock, mgt_rxpll_lock] = status_onu
    timeout_cntr = 0
    while((not (mgt_rx_ready and mgt_rxpll_lock and rx40_locked and rx_locked and mgt_txpll_lock and mgt_tx_ready)) and timeout_cntr< c_MAX_TIMEOUT_MGT_READY):
        status_onu = onu.exd_read_onu_status()
        [rx40_locked, phase_good, rx_locked, operational, mgt_tx_ready, mgt_rx_ready, mgt_txpll_lock, mgt_rxpll_lock] = status_onu
        timeout_cntr = timeout_cntr + 1
        time.sleep(0.1)
        if(timeout_cntr >= c_MAX_TIMEOUT_MGT_READY):
            logger_pon.warn("ONU is not getting ready after timeout, check ONU status!")		
            return 0

    program_leds(onu, 0x00)
    monitor_sfp(onu,1)

    return 1


